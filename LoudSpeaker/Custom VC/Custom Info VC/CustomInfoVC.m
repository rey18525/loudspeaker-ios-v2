//
//  CustomInfoVC.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 25/11/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "CustomInfoVC.h"
#import "SharedMethod.h"
#import "Constant.h"

@interface CustomInfoVC ()

@end

@implementation CustomInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[SharedMethod new] setCornerRadius:self.imgPicture andCornerRadius:10.0f];
//    });
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SharedMethod new] setCornerRadius:self.imgPicture andCornerRadius:5.0f];
    });
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[SharedMethod new] setCornerRadius:self.imgPicture andCornerRadius:10.0f];
//    });
}

-(void)setupUI{
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
    
    [self.lblSubtitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblSubtitle setFont:[UIFont fontWithName:FONT_REGULAR size:12.0f]];
}

@end
