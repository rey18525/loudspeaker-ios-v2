//
//  LoadingVC.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 25/11/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingVC : UIViewController
-(void)startLoad;
-(void)stopLoad;
@end
