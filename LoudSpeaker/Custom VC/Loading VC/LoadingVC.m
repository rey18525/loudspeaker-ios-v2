//
//  LoadingVC.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 25/11/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "LoadingVC.h"
#import "SharedMethod.h"

@interface LoadingVC ()
@property (strong, nonatomic) IBOutlet UIView *HUD;
@property (strong, nonatomic) IBOutlet UIImageView *imgLoad;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation LoadingVC

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupUI];
}

-(void)setupUI{
    [self.view setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    [self.imgLoad setAnimationImages:[NSArray arrayWithObjects:
                                      [UIImage imageNamed:@"gif_1.png"],
                                      [UIImage imageNamed:@"gif_2.png"],
                                      [UIImage imageNamed:@"gif_3.png"],
                                      [UIImage imageNamed:@"gif_4.png"],
                                      [UIImage imageNamed:@"gif_5.png"],
                                      [UIImage imageNamed:@"gif_6.png"],
                                      [UIImage imageNamed:@"gif_7.png"],
                                      [UIImage imageNamed:@"gif_8.png"],
                                      [UIImage imageNamed:@"gif_9.png"],
                                      [UIImage imageNamed:@"gif_10.png"],
                                      [UIImage imageNamed:@"gif_11.png"],
                                      [UIImage imageNamed:@"gif_12.png"],
                                      [UIImage imageNamed:@"gif_13.png"],
                                      [UIImage imageNamed:@"gif_14.png"],
                                      [UIImage imageNamed:@"gif_15.png"],
                                      [UIImage imageNamed:@"gif_16.png"],
                                      [UIImage imageNamed:@"gif_17.png"],
                                      [UIImage imageNamed:@"gif_18.png"],
                                      nil]];
    
    [self.imgLoad setAnimationDuration:1.0f];
    [self.imgLoad setAnimationRepeatCount:0.0f];
    
    [[self.HUD layer] setCornerRadius:10];
    [self.lblTitle setText:NSLocalizedString(@"msg_Loading", nil)];
}

-(void)startLoad{
    [self.imgLoad startAnimating];
}

-(void)stopLoad{
    [self.imgLoad stopAnimating];
    
    for(UIView *temp in self.parentViewController.view.window.subviews){
        if(temp.tag == 900){
            [temp removeFromSuperview];
        }
    }
    
    [self removeFromParentViewController];
}

@end
