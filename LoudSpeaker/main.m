//
//  main.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 25/11/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
