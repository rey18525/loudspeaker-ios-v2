//
//  AppDelegate.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 25/11/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "AppDelegate.h"
#import "Constant.h"
#import "SQLManager.h"
#import "SharedMethod.h"
#import <AFNetworking.h>
#import "AFNetworkActivityLogger.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "SVProgressHUD.h"
#import "WebAPI.h"
#import "VoucherDetailVC.h"
#import "PromotionDetailVC.h"
#import "MenuDetailVC.h"
#import "Voucher.h"
#import "Geotification.h"
#import "Promotion.h"
#import "Menu.h"
#import "IntroVC.h"
#import "ProfileVC.h"
#import <AVFoundation/AVFoundation.h>
#import <PDKeychainBindings.h>
#import <AVFoundation/AVFoundation.h>

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;
@import GoogleMaps;

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate, FIRMessagingDelegate, GIDSignInDelegate, CLLocationManagerDelegate,AVAudioPlayerDelegate>
@end
#endif

@interface AppDelegate ()<FIRMessagingDelegate, GIDSignInDelegate,CLLocationManagerDelegate>

@end

@implementation AppDelegate{
    UIStoryboard *storyboard;
    CLLocationManager *locationManager;
    AVAudioPlayer *player;
    NSString *geofenceMsg;
    NSMutableArray *geotifications;
    BOOL promptAuthorization;
}

- (UIImage *)imageWithColor:(UIColor *)color andBounds:(CGRect)imgBounds {
    UIGraphicsBeginImageContextWithOptions(imgBounds.size, NO, 0);
    [color setFill];
    UIRectFill(imgBounds);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    if([ENABLE_STARTUP_MUSIC isEqualToString:@"1"]){
//        [self performOpeningMusic];
    }
//    NSLog(@"locale - %@",[[NSLocale currentLocale] localeIdentifier]);
    [self setupLoadingIndicator];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DISPLAY_ADVERTISEMENT];
    
    [self setupBottomBar];
    [self setupNavigationBar];
    [self checkDatabase];
    [GMSServices provideAPIKey:FIREBASE_KEY];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    //    [[SharedMethod new] printAllFonts];
    [self checkSecretKey];
    [FIRApp configure];
    
    // Add observer for InstanceID token refresh callback.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    [self registerPushNotification:application];
    
    [[NSUserDefaults standardUserDefaults] setObject:[[FIRInstanceID instanceID] token] forKey:DEVICE_TOKEN];
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN];
    if(deviceToken.length<=0){
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:DEVICE_TOKEN];
    }
    
#ifdef DEBUG
    NSLog(@"DEVICE_TOKEN - %@",[[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN]);
#endif
    
    NSString *ID = [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID];
    if(ID.length<=0){
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:USER_ID];
    }
    
//    [GIDSignIn sharedInstance].clientID = [FIRApp defaultApp].options.clientID;
//    [GIDSignIn sharedInstance].delegate = self;
    
    [self setupFacebookSDK];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [self setupLocationManager];
    [self requestGeofenceData];
    
#ifdef DEBUG
    [[AFNetworkActivityLogger sharedLogger] startLogging];
    [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelDebug];
#endif
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:IS_SECOND_RUN]){
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
    
    //First install//
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_FIRST_INSTALL] == NO){
//        NSLog(@"First install");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_FIRST_INSTALL];
        [[SharedMethod new] addStatistics:@"first_install" itemID:@"i"];
    } else{
        if([[NSUserDefaults standardUserDefaults] boolForKey:HAS_CONFIG] == YES){
            [self sendStatistics];
        }
    }
    
    UIViewController *temp = [storyboard instantiateViewControllerWithIdentifier:@"tabBarController"];
    [self.window setRootViewController:temp];
    self.tabBarController = (UITabBarController*)self.window.rootViewController;
    self.tabBarController.delegate = (id)self;
    
    return YES;
}

-(void)setupFacebookSDK{
    [FBSDKLoginButton class];
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
}

-(void)sendStatistics{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    NSMutableArray *temp = [[NSUserDefaults standardUserDefaults] objectForKey:STATISTICS];
    //    NSLog(@"temp - %@",temp);
    if([temp count]>0){
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        [params setObject:[SharedMethod getTokenWithRoute:@"user/stats/action" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        [params setObject:temp forKey:@"data"];
        
        [WebAPI sendStatistics:params andSuccess:^(id successBlock) {
            NSDictionary *result = successBlock;
            if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:STATISTICS];
            }
        }];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[FIRMessaging messaging] disconnect];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self connectToFcm];
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DISPLAY_ADVERTISEMENT];
}

#pragma mark - Custom Functions
-(void)setupLoadingIndicator{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        imageView.animationImages = [NSArray arrayWithObjects:
                                     [UIImage imageNamed:@"gif_1"],
                                     [UIImage imageNamed:@"gif_2"],
                                     [UIImage imageNamed:@"gif_3"],
                                     [UIImage imageNamed:@"gif_4"],
                                     [UIImage imageNamed:@"gif_5"],
                                     [UIImage imageNamed:@"gif_6"],
                                     [UIImage imageNamed:@"gif_7"],
                                     [UIImage imageNamed:@"gif_8"],
                                     [UIImage imageNamed:@"gif_9"],
                                     [UIImage imageNamed:@"gif_10"],
                                     [UIImage imageNamed:@"gif_11"],
                                     [UIImage imageNamed:@"gif_12"],
                                     [UIImage imageNamed:@"gif_13"],
                                     [UIImage imageNamed:@"gif_14"],
                                     [UIImage imageNamed:@"gif_15"],
                                     nil];
        imageView.animationDuration = 1.0f;
        imageView.animationRepeatCount = 0;
        [imageView startAnimating];
        [SVProgressHUD setLoadingImageView:imageView];
        [SVProgressHUD setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
        [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
        [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeCustom];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    });
}

-(void)performOpeningMusic{
    NSString* resourcePath = [[NSBundle mainBundle] resourcePath];
    resourcePath = [resourcePath stringByAppendingString:@"/startup.mp3"];
    NSError* err;
    
    //Initialize our player pointing to the path to our resource
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:
              [NSURL fileURLWithPath:resourcePath] error:&err];
    
    if( err ){
        NSLog(@"Failed with reason: %@", [err localizedDescription]);
    }
    else{
        //set our delegate and begin playback
        player.delegate = self;
        [player play];
        player.numberOfLoops = 0;
        player.currentTime = 0;
        player.volume = 2.0;
    }
}

-(void)checkSecretKey{
    if(![[NSUserDefaults standardUserDefaults] objectForKey:IS_SECOND_RUN]){
        [[PDKeychainBindings sharedKeychainBindings] removeObjectForKey:SECRET_KEY];
    }
    
    NSString *secretKey = [[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY];
    if(secretKey.length<=0){
        secretKey = [[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY];
        [[PDKeychainBindings sharedKeychainBindings] setObject:secretKey forKey:SECRET_KEY];
    }
    
    #ifdef DEBUG
    NSLog(@"secretKey - %@",secretKey);
    #endif
}

-(void)registerPushNotification:(UIApplication*)application{
    // Register for remote notifications
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter]
             requestAuthorizationWithOptions:authOptions
             completionHandler:^(BOOL granted, NSError * _Nullable error) {
             }
             ];
            
            // For iOS 10 display notification (sent via APNS)
            [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
            // For iOS 10 data message (sent via FCM)
            [[FIRMessaging messaging] setRemoteMessageDelegate:self];
#endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        // [END register_for_notifications]
    }
}

-(void)checkDatabase{
    BOOL result = [[SQLManager new] checkDatabase];
    NSLog(@"Check Database result - %d",result);
    
    [[SQLManager new] checkTermsExist];
}

-(void)setupLocationManager{
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    
    if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined){
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8){
            [locationManager requestAlwaysAuthorization];
        }
    } else if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse){
        
    }
    
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
}

-(void)requestPostDetail:(NSString*)ID{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        
        NSString *route = [NSString stringWithFormat:@"user/post/%@",ID];
        
        NSDictionary *params = @{@"posting_id"       : ID,
                                 @"userId"          : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"platform"         : PLATFORM,
                                 @"push_token"       : [[NSUserDefaults standardUserDefaults] objectForKey: DEVICE_TOKEN],
                                 @"token"            : [SharedMethod getTokenWithRoute:route apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
#ifdef DEBUG
        NSLog(@"Prepare Retrieve Posting Detail Params - %@",params);
#endif
        
        [WebAPI retrievePostingDetail:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            NSMutableArray *temp = [[NSMutableArray alloc] init];
            if([result objectForKey:@"errMsg"]){
                [self.window.rootViewController presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    [temp addObject:[result objectForKey:@"post"]];
                    UINavigationController *navigationController = [[(UITabBarController *)self.window.rootViewController viewControllers] objectAtIndex:0];
                    
                    if([[[result objectForKey:@"post"] objectForKey:@"type"] isEqualToString:@"promo"]){
                        NSMutableArray *promotions = [[NSMutableArray alloc] init];
                        promotions = [[SharedMethod new] processPosting:temp andCurrentArray:promotions];
                        PromotionDetailVC *promotionDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"promotionDetailVC"];
                        promotionDetailVC.selectedPromotion = [promotions objectAtIndex:0];
                        [navigationController pushViewController:promotionDetailVC animated:NO];
                    } else if([[[result objectForKey:@"post"] objectForKey:@"type"] isEqualToString:@"voucher"]){
                        NSMutableArray *vouchers = [[NSMutableArray alloc] init];
                        vouchers = [[SharedMethod new] processVoucher:temp andCurrentArray:vouchers];
                        VoucherDetailVC *voucherDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"voucherDetailVC"];
                        voucherDetailVC.selectedVoucher = [vouchers objectAtIndex:0];
                        [navigationController pushViewController:voucherDetailVC animated:NO];
                    } else if([[[result objectForKey:@"post"] objectForKey:@"type"] isEqualToString:@"menu"]){
                        NSMutableArray *menus = [[NSMutableArray alloc] init];
                        menus = [[SharedMethod new] processMenu:temp andCurrentArray:menus];
                        MenuDetailVC *menuDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"menuDetailVC"];
                        menuDetailVC.selectedMenu = [menus objectAtIndex:0];
                        [navigationController pushViewController:menuDetailVC animated:NO];
                    }
                } else{
                    [self.window.rootViewController presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"Message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
        
    } else{
        [self.window.rootViewController presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)requestCampaignDetail:(NSString*)ID{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        
        NSString *route = [NSString stringWithFormat:@"user/campaign/%@",ID];
        
        NSDictionary *params = @{@"campaign_id"      : ID,
                                 @"userId"           : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"platform"         : PLATFORM,
                                 @"push_token"       : [[NSUserDefaults standardUserDefaults] objectForKey: DEVICE_TOKEN],
                                 @"token"            : [SharedMethod getTokenWithRoute:route apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
        #ifdef DEBUG
        NSLog(@"Prepare Retrieve Campaign Detail Params - %@",params);
        #endif
        
        [WebAPI retrieveCampaignDetail:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            NSMutableArray *temp = [[NSMutableArray alloc] init];
            if([result objectForKey:@"errMsg"]){
                [self.window.rootViewController presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    [temp addObject:[result objectForKey:@"post"]];
                    UINavigationController *navigationController = [[(UITabBarController *)self.window.rootViewController viewControllers] objectAtIndex:0];
                    
                    NSMutableArray *campaignsArray = [[NSMutableArray alloc] init];
                    NSMutableArray *tempCampaign = [[NSMutableArray alloc] init];
                    for(int count=0;count<[[result objectForKey:@"campaigns"] count];count++){
                        [tempCampaign addObject:[[result objectForKey:@"campaigns"] objectAtIndex:count]];
                    }
                    
                    campaignsArray = [[SharedMethod new] processCampaign:tempCampaign andCurrentArray:campaignsArray];
                    
                    VoucherDetailVC *voucherDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"voucherDetailVC"];
                    voucherDetailVC.selectedCampaign = [campaignsArray objectAtIndex:0];
                    [navigationController pushViewController:voucherDetailVC animated:NO];
                } else{
                    [self.window.rootViewController presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"Message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
        
    } else{
        [self.window.rootViewController presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)readMessage:(NSString*)msgID{
    if([[SharedMethod new] checkNetworkConnectivity] && [[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
        NSString *route = [NSString stringWithFormat:@"user/inbox/message/%@",msgID];
        NSDictionary *params = @{@"userId"           : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"platform"         : PLATFORM,
                                 @"msgID"            : msgID,
                                 @"push_token"       : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                 @"token"            : [SharedMethod getTokenWithRoute:route apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
#ifdef DEBUG
        NSLog(@"View Inbox Message - %@",params);
#endif

        [WebAPI viewInboxMessage:params andSuccess:^(id successBlock) {
//            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self.window.rootViewController presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                //Done reading//
//                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DISPLAY_MESSAGE];
//                [[NSUserDefaults standardUserDefaults] setObject:msgID forKey:PUSH_MESSAGE_ID];
//                [self.tabBarController setSelectedIndex:2];
            }
        }];
    } else{
//        [self.window.rootViewController presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

#pragma mark - Geofencing
-(void)requestGeofenceData{
    [locationManager startUpdatingLocation];
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN];
    if(deviceToken.length<=0){
        deviceToken=@"";
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:PLATFORM forKey:@"platform"];
    [params setObject:deviceToken forKey:@"push_token"];
    [params setObject:[SharedMethod getTokenWithRoute:@"user/geofence/nearest" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
    };
    [params setObject:[NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude] forKey:@"longitude"];
    [params setObject:@"2" forKey:@"radius_in_km"];
    NSLog(@"Prepare Geofence Request Params - %@",params);
    [WebAPI retrieveGeofence:params andSuccess:^(id successBlock) {
        NSDictionary *result = successBlock;
        [[NSUserDefaults standardUserDefaults] setObject:result forKey:GEOFENCE_LIST];
        [self setupGeofence];
    }];
}

-(void)sendGeofenceData:(NSString*)geofenceDetailID{
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN];
    if(deviceToken.length<=0){
        deviceToken=@"";
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:PLATFORM forKey:@"platform"];
    [params setObject:deviceToken forKey:@"push_token"];
    [params setObject:[SharedMethod getTokenWithRoute:@"user/geofence/entry" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
    };
    [params setObject:[NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude] forKey:@"longitude"];
    [params setObject:geofenceDetailID forKey:@"geofence_id"];
    NSLog(@"Prepare Geofence Send Params - %@",params);
    [WebAPI sendGeofenceData:params andSuccess:^(id successBlock) {
        NSLog(@"Send Geofence Data Result - %@",successBlock);
        geofenceMsg = [successBlock objectForKey:@"msg"];
    }];
}

-(void)stopMonitoringRegion{
    for (CLRegion *monitored in [locationManager monitoredRegions]){
        NSLog(@"Removing...");
        [locationManager stopMonitoringForRegion:monitored];
    }
}

-(void)setupGeofence{
    NSLog(@"Setting up geofence");
    [locationManager requestAlwaysAuthorization];
    geotifications = [[NSMutableArray alloc]init];
    NSLog(@"GEO -- -- %@",GEOFENCE_LIST);
    //    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"geofence"] count]>0){
    //        [self loadAllGeotifications];
    //    } else{
    if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedAlways){
        [self stopMonitoringRegion];
        NSDictionary *geo = [[NSUserDefaults standardUserDefaults] objectForKey:GEOFENCE_LIST];
        NSLog(@"GEO -- -- %@",geo);
        NSArray *tempGeo = [geo objectForKey:@"locations"];
        NSLog(@"tempGeo - %@",tempGeo);
        
        //        if([tempGeo count]>20){
        NSLog(@"tempGeo > 20");
        for(int count=0;count<[tempGeo count];count++){
            NSLog(@"List %d - %@",count,[tempGeo objectAtIndex:count]);
            NSString *latString = [[tempGeo objectAtIndex:count] objectForKey:@"latitude"];
            NSString *lonString = [[tempGeo objectAtIndex:count] objectForKey:@"longitude"];
            
            CGFloat lat = (CGFloat)[latString floatValue];
            CGFloat lon = (CGFloat)[lonString floatValue];
            
            double rad = [[[tempGeo objectAtIndex:count] objectForKey:@"radius"] doubleValue];
            
            double distance = [[[tempGeo objectAtIndex:count] objectForKey:@"distance"] doubleValue] * 1000;
            
            NSString *identifier = [[tempGeo objectAtIndex:count] objectForKey:@"id"];
            NSString *title = [[tempGeo objectAtIndex:count] objectForKey:@"location"];
            
            [self addGeotification:CLLocationCoordinate2DMake(lat, lon) radius:rad identifier:identifier note:title andEventType:OnEntry];
            
            if(count==[tempGeo count]-1){
                [self addGeotification:CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude) radius:distance identifier:identifier note:@"GEO_REFRESH" andEventType:!OnEntry];
            }
        }
        
        //        } else{
        //            for(int count=0;count<[tempGeo count];count++){
        //                NSLog(@"List %d - %@",count,[tempGeo objectAtIndex:count]);
        //                NSString *latString = [[tempGeo objectAtIndex:count] objectForKey:@"GPS_COORDI_LAT"];
        //                NSString *lonString = [[tempGeo objectAtIndex:count] objectForKey:@"GPS_COORDI_LONG"];
        //
        //                CGFloat lat = (CGFloat)[latString floatValue];
        //                CGFloat lon = (CGFloat)[lonString floatValue];
        //
        //                double rad = [[[tempGeo objectAtIndex:count] objectForKey:@"RADIUS_IN_KM"] doubleValue] * 1000;
        //
        //                double distance = [[[tempGeo objectAtIndex:count] objectForKey:@"DISTANCE"] doubleValue] * 1000;
        //
        //                NSString *identifier = [[tempGeo objectAtIndex:count] objectForKey:@"GEOFENCING_DETAIL_ID"];
        //                NSString *title = [[tempGeo objectAtIndex:count] objectForKey:@"TITLE"];
        //
        //                [self addGeotification:CLLocationCoordinate2DMake(lat, lon) radius:rad identifier:identifier note:title andEventType:OnEntry];
        //
        //                if(count==[tempGeo count]-1){
        //                    [self addGeotification:CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude) radius:distance identifier:identifier note:title andEventType:!OnEntry];
        //                    //                        [self addGeotification:CLLocationCoordinate2DMake(lat, lon) radius:rad identifier:identifier note:title andEventType:!OnEntry];
        //                }
        //            }
        //        }
        
        
        //            for(NSDictionary *temp in tempGeo){
        //                NSString *latString = [temp objectForKey:@"GPS_COORDI_LAT"];
        //                NSString *lonString = [temp objectForKey:@"GPS_COORDI_LONG"];
        //
        //                CGFloat lat = (CGFloat)[latString floatValue];
        //                CGFloat lon = (CGFloat)[lonString floatValue];
        //
        //                double rad = [[temp objectForKey:@"RADIUS_IN_KM"] doubleValue] * 1000;
        //                NSString *identifier = [temp objectForKey:@"GEOFENCING_DETAIL_ID"];
        //                NSString *title = [temp objectForKey:@"TITLE"];
        //
        //                [self addGeotification:CLLocationCoordinate2DMake(lat, lon) radius:rad identifier:identifier note:title andEventType:OnEntry];
        //            }
        
        //Sample//
        //            [self addGeotification:CLLocationCoordinate2DMake(3.105970, 101.592928) radius:5 identifier:[[NSUUID new] UUIDString] note:@"7-11 Kelana Square" andEventType:OnEntry];
    } else{
                    //[locationManager requestAlwaysAuthorization];
                    //[self setupGeofence];
    }
    
    //    }
}

-(void)addGeotification:(CLLocationCoordinate2D)coordinate radius:(CGFloat)radius identifier:(NSString*)identifier note:(NSString*)note andEventType:(EventType)eventType{
    NSLog(@"Add Geotification");
    CGFloat clampedRadius = (radius > locationManager.maximumRegionMonitoringDistance)?locationManager.maximumRegionMonitoringDistance : radius;
    Geotification *geotification = [[Geotification alloc] initWithCoordinate:coordinate radius:clampedRadius identifier:identifier note:note eventType:eventType];
    [self addGeotification:geotification];
    [self startMonitoringGeotification:geotification];
    //    [self saveAllGeotifications];
}

- (void)addGeotification:(Geotification *)geotification{
    NSLog(@"geotification is %@ %f & %f & %d" ,geotification.note,geotification.coordinate.latitude,geotification.coordinate.longitude,(int)geotification.eventType);
    [geotifications addObject:geotification];
    //    [self startMonitoringGeotification:geotification];
}

- (void)saveAllGeotifications{
    NSLog(@"Saving...");
    NSMutableArray *items = [NSMutableArray array];
    for (Geotification *geotification in geotifications) {
        NSLog(@"Inside For Loop");
        id item = [NSKeyedArchiver archivedDataWithRootObject:geotification];
        [items addObject:item];
        NSLog(@"items added");
    }
    [[NSUserDefaults standardUserDefaults] setObject:items forKey:GEOFENCE_LIST];
    NSLog(@"Saved to kSavedItemsKey %@",items);
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)loadAllGeotifications{
    geotifications = [NSMutableArray array];
    
    NSArray *savedItems = [[NSUserDefaults standardUserDefaults] arrayForKey:GEOFENCE_LIST];
    if (savedItems) {
        for (id savedItem in savedItems) {
            Geotification *geotification = [NSKeyedUnarchiver unarchiveObjectWithData:savedItem];
            if ([geotification isKindOfClass:[Geotification class]]) {
                [self addGeotification:geotification];
            }
        }
    }
}

- (void)removeAllGeotifications{
    geotifications = [NSMutableArray array];
    NSArray *savedItems = [[NSUserDefaults standardUserDefaults] arrayForKey:GEOFENCE_LIST];
    NSLog(@"savedItems (Remove) - %@",savedItems);
    if ([savedItems count]>0) {
        for (id savedItem in savedItems) {
            Geotification *geotification = [NSKeyedUnarchiver unarchiveObjectWithData:savedItem];
            [self stopMonitoringGeotification:geotification];
            NSLog(@"Removed Geotification Data...");
        }
    }
}

- (CLCircularRegion *)regionWithGeotification:(Geotification *)geotification{
    NSLog(@"Set region with geotification");
    NSString *coord = [NSString stringWithFormat:@"%@", geotification.identifier] ;
    
    CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:geotification.coordinate radius:geotification.radius identifier:coord];
    NSLog(@"Set region with geotification");

    [region setNotifyOnEntry:geotification.eventType==OnEntry];
    [region setNotifyOnExit:!region.notifyOnEntry];
    
    NSLog(@"region notify on %d",region.notifyOnExit);
    NSLog(@"Done setting region");
    return region;
}

- (void)startMonitoringGeotification:(Geotification *)geotification{
    if (![CLLocationManager isMonitoringAvailableForClass:[CLCircularRegion class]]) {
        [[SharedMethod new] setAndShowAlertView:NSLocalizedString(@"err_Title", nil) message:NSLocalizedString(@"err_Geofence_Support", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)];
        return;
    }
    
    if(promptAuthorization){
        if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways) {
            [[SharedMethod new] setAndShowAlertView:NSLocalizedString(@"err_Title", nil) message:NSLocalizedString(@"err_Geofence_Authorization", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)];
            promptAuthorization = NO;
            return;
        }
    }
    
    CLCircularRegion *region = [self regionWithGeotification:geotification];
    [locationManager startMonitoringForRegion:region];
    [locationManager requestStateForRegion:region];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"Error - %@",error.localizedDescription);
}

-(void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error{
    NSLog(@"Region Error - %@",error.localizedDescription);
}

- (void)stopMonitoringGeotification:(Geotification *)geotification{
    for (CLCircularRegion *circularRegion in locationManager.monitoredRegions) {
        if ([circularRegion isKindOfClass:[CLCircularRegion class]]) {
            NSLog(@"circular identifier %@",circularRegion.identifier);
            NSLog(@"geotifiacation identifier %@",geotification.identifier);
            if ([circularRegion.identifier isEqualToString:geotification.identifier]) {
                [locationManager stopMonitoringForRegion:circularRegion];
            }
        }
    }
}

-(void)handleRegionEvent:(CLRegion*)region{
    if([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
        if(region.notifyOnExit){
            [self requestGeofenceData];
        } else{
            NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN];
            if(deviceToken.length<=0){
                deviceToken=@"";
            }
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            [params setObject:PLATFORM forKey:@"platform"];
            [params setObject:deviceToken forKey:@"push_token"];
            [params setObject:[SharedMethod getTokenWithRoute:@"user/geofence/entry" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
            if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
                [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
            };
            [params setObject:[NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude] forKey:@"latitude"];
            [params setObject:[NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude] forKey:@"longitude"];
            [params setObject:region.identifier forKey:@"geofence_id"];
            NSLog(@"Prepare Geofence Send Params - %@",params);
            [WebAPI sendGeofenceData:params andSuccess:^(id successBlock) {
                NSLog(@"Send Geofence Data Result - %@",successBlock);
                [self sendGeofenceData:region.identifier];
            }];
        }
    } else{
        if(region.notifyOnExit){
            [self sendGeofenceData:region.identifier];
            [self requestGeofenceData];
        } else{
            NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN];
            if(deviceToken.length<=0){
                deviceToken=@"";
            }
            NSString *loginID = [[[[NSUserDefaults standardUserDefaults] objectForKey:USER_PROFILE] objectForKey:@"MemberInfo"] objectForKey:@"IcNo"];
            if(loginID.length<=0){
                loginID=@"";
            }
            NSDictionary *data = @{@"tag" : @"SentGeoFencingContent",
                                   @"LoginId" : loginID,
                                   @"GeoFencingDtlID" : region.identifier,
                                   @"MobileAppPlatform" : @"iOS",
                                   @"MobileAppPushToken" : deviceToken
                                   };
            NSLog(@"Send Geofence Data Params - %@",data);
            [WebAPI retrieveGeofence:data andSuccess:^(id successBlock) {
                NSLog(@"Send Geofence Data Result - %@",successBlock);
                if([successBlock objectForKey:@"msg"]){
                    [self sendGeofenceData:region.identifier];
                }
            }];
        }
    }
}

-(NSString*)noteFromRegionIdentifier:(NSString*)identifier{
    NSArray *savedItems = [[NSUserDefaults standardUserDefaults] arrayForKey:@"geofence"];
    if(savedItems){
        for(id savedItem in savedItems){
            Geotification *geotification = [NSKeyedUnarchiver unarchiveObjectWithData:savedItem];
            if([geotification isKindOfClass:[Geotification class]]){
                if([geotification.identifier isEqualToString:identifier]){
                    return geotification.note;
                }
            }
        }
    }
    return nil;
}

-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(nonnull CLRegion *)region{
    if([region isKindOfClass:[CLCircularRegion class]]){
        [self handleRegionEvent:region];
    }
}

-(void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region{
    if(state == CLRegionStateInside){
        NSLog(@"INSIDE");
        NSLog(@"region identifier - %@",region.identifier);
        if(region.identifier.length>0){
            NSLog(@"PERFORM");
            [self sendGeofenceData:region.identifier];
        } else{
            NSLog(@"NOT PERFORMING");
        }
    } else if(state == CLRegionStateOutside){
        NSLog(@"OutSide");
        //        [self requestGeofenceData];
    }
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region{
    if([region isKindOfClass:[CLCircularRegion class]]){
        [self handleRegionEvent:region];
    }
}

-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region{
    NSLog(@"Now monitoring %@",region.description);
    [locationManager requestStateForRegion:region];
}


#pragma mark - Start Receive Message
// [START ios_10_message_handling]
// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    // Print message ID.
    NSDictionary *userInfo = notification.request.content.userInfo;
    
#ifdef DEBUG
    NSLog(@"iOS10 Message ID: %@", userInfo[@"gcm.message_id"]);
    
    // Pring full message.
    NSLog(@"%@", userInfo);
#endif
    
    NSString *title = [userInfo objectForKey:@"title"];
    NSString *message = [userInfo objectForKey:@"body"];
    NSString *type = [userInfo objectForKey:@"type"];
    NSString *typeID = [userInfo objectForKey:@"type_id"];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Dismiss", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Do nothing//
    }];
    [alertController addAction:cancelAction];
    
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID];
    
    if(([type isEqualToString:@"post"] || [type isEqualToString:@"campaign"]) || ([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN] && userId.length>0 && ([type isEqualToString:@"reward"] || [type isEqualToString:@"transaction"] || [type isEqualToString:@"card"]))){
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Show", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //Perform Actions//
            if([type isEqualToString:@"reward"]){
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DISPLAY_REWARD];
                [self.tabBarController setSelectedIndex:2];
            } else if([type isEqualToString:@"myvoucher"]){ //Deprecated//
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:MY_VOUCHER];
            } else if([type isEqualToString:@"menu"]){
                [self requestPostDetail:typeID];
            } else if([type isEqualToString:@"promo"]){
                [self requestPostDetail:typeID];
            } else if([type isEqualToString:@"campaign"]){
                [self requestCampaignDetail:typeID];
            } else if([type isEqualToString:@"transaction"]){
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DISPLAY_TRANSACTION];
                [self.tabBarController setSelectedIndex:2];
            } else if([type isEqualToString:@"card"]){
                [self.tabBarController setSelectedIndex:2];
            }
        }];
        [alertController addAction:okAction];
    }
    
    [self.window.rootViewController presentViewController:alertController animated:YES completion:^{
        if([type isEqualToString:@"message"] && [[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            if([type isEqualToString:@"message"]){
                NSString *msgID = [userInfo objectForKey:@"message_id"];
                if(msgID.length>0){
                    [self readMessage:msgID];
                }
            }
        }
    }];
    
    completionHandler(UNNotificationPresentationOptionAlert);
}

// Receive data message on iOS 10 devices.
- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    // Print full message
    
#ifdef DEBUG
    NSLog(@"ReceivedRemoteMessage: %@", [remoteMessage appData]);
#endif
    
    NSDictionary *userInfo = [remoteMessage appData];
    
    NSString *title = [userInfo objectForKey:@"title"];
    NSString *message = [userInfo objectForKey:@"body"];
    NSString *type = [userInfo objectForKey:@"type"];
    NSString *typeID = [userInfo objectForKey:@"type_id"];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Dismiss", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Do nothing//
    }];
    [alertController addAction:cancelAction];
    
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID];
    
    if(([type isEqualToString:@"post"] || [type isEqualToString:@"campaign"]) || ([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN] && userId.length>0 && ([type isEqualToString:@"reward"] || [type isEqualToString:@"transaction"] || [type isEqualToString:@"card"]))){
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Show", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //Perform Actions//
            if([type isEqualToString:@"reward"]){
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DISPLAY_REWARD];
                [self.tabBarController setSelectedIndex:2];
            } else if([type isEqualToString:@"myvoucher"]){ //Deprecated//
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:MY_VOUCHER];
            } else if([type isEqualToString:@"menu"]){
                [self requestPostDetail:typeID];
            } else if([type isEqualToString:@"promo"]){
                [self requestPostDetail:typeID];
            } else if([type isEqualToString:@"campaign"]){
                [self requestCampaignDetail:typeID];
            } else if([type isEqualToString:@"transaction"]){
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DISPLAY_TRANSACTION];
                [self.tabBarController setSelectedIndex:2];
            } else if([type isEqualToString:@"card"]){
                [self.tabBarController setSelectedIndex:2];
            }
        }];
        [alertController addAction:okAction];
    }
    
    [self.window.rootViewController presentViewController:alertController animated:YES completion:^{
        if([type isEqualToString:@"message"] && [[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            if([type isEqualToString:@"message"]){
                NSString *msgID = [userInfo objectForKey:@"message_id"];
                if(msgID.length>0){
                    [self readMessage:msgID];
                }
            }
        }
    }];
}
#endif
// [END ios_10_message_handling]

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // Print message ID.
    
#ifdef DEBUG
    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    
    // Pring full message.
    NSLog(@"%@", userInfo);
#endif
    
    NSString *title = [userInfo objectForKey:@"title"];
    NSString *message = [userInfo objectForKey:@"body"];
    NSString *type = [userInfo objectForKey:@"type"];
    NSString *typeID = [userInfo objectForKey:@"type_id"];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Dismiss", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //Do nothing//
    }];
    [alertController addAction:cancelAction];
    
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID];
    
    if(([type isEqualToString:@"post"] || [type isEqualToString:@"campaign"]) || ([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN] && userId.length>0 && ([type isEqualToString:@"reward"] || [type isEqualToString:@"transaction"] || [type isEqualToString:@"card"]))){
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Show", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //Perform Actions//
            if([type isEqualToString:@"reward"]){
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DISPLAY_REWARD];
                [self.tabBarController setSelectedIndex:2];
            } else if([type isEqualToString:@"myvoucher"]){ //Deprecated//
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:MY_VOUCHER];
            } else if([type isEqualToString:@"menu"]){
                [self requestPostDetail:typeID];
            } else if([type isEqualToString:@"promo"]){
                [self requestPostDetail:typeID];
            } else if([type isEqualToString:@"campaign"]){
                [self requestCampaignDetail:typeID];
            } else if([type isEqualToString:@"transaction"]){
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DISPLAY_TRANSACTION];
                [self.tabBarController setSelectedIndex:2];
            } else if([type isEqualToString:@"card"]){
                [self.tabBarController setSelectedIndex:2];
            }
        }];
        [alertController addAction:okAction];
    }
    
    [self.window.rootViewController presentViewController:alertController animated:YES completion:^{
        if([type isEqualToString:@"message"] && [[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            if([type isEqualToString:@"message"]){
                NSString *msgID = [userInfo objectForKey:@"message_id"];
                if(msgID.length>0){
                    [self readMessage:msgID];
                }
            }
        }
    }];
    
    completionHandler(UIBackgroundFetchResultNewData);
}

#pragma mark - Start Refresh Token
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
#ifdef DEBUG
    NSLog(@"InstanceID token: %@", refreshedToken);
#endif
    [[NSUserDefaults standardUserDefaults] setObject:refreshedToken forKey:DEVICE_TOKEN];
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to appliation server.
}

#pragma mark - Start Connect to FCM
- (void)connectToFcm {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
            
            #ifdef DEBUG
            //Development//
            [[FIRMessaging messaging] unsubscribeFromTopic:@"/topics/GlobalIos"];
            [[FIRMessaging messaging] subscribeToTopic:@"/topics/GlobalIosUat"];
            #else
            //Production//
            [[FIRMessaging messaging] unsubscribeFromTopic:@"/topics/GlobalUat"];
            [[FIRMessaging messaging] unsubscribeFromTopic:@"/topics/GlobalIosUat"];
            [[FIRMessaging messaging] subscribeToTopic:@"/topics/GlobalIos"];
            #endif
        }
    }];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    if([[url scheme] hasPrefix:@"fb"]){
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    } else{
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];
    }
}

/**
 *Google Sign-in Method
 */
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error == nil) {
        GIDAuthentication *authentication = user.authentication;
        FIRAuthCredential *credential =
        [FIRGoogleAuthProvider credentialWithIDToken:authentication.idToken
                                         accessToken:authentication.accessToken];
    } else{
        //        NSLog(@"Sign in error - %@",error.localizedDescription);
    }
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
}

#pragma mark - Setup Navigation Bar
-(void)setupNavigationBar{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UINavigationBar appearance] setBackgroundImage:[self imageWithColor:[[SharedMethod new] colorWithHexString:TOP_BAR_BG_COLOR andAlpha:1.0f] andBounds:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20)]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    [[UINavigationBar appearance] setBarTintColor:[[SharedMethod new] colorWithHexString:TOP_BAR_BG_COLOR andAlpha:1.0f]];
    [[UINavigationBar appearance] setBackgroundColor:[[SharedMethod new] colorWithHexString:TOP_BAR_BG_COLOR andAlpha:1.0f]];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           NSForegroundColorAttributeName : [[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f],
                                                           NSFontAttributeName: [UIFont fontWithName:FONT_BOLD size:20.0]}];
}

#pragma mark - Setup Bottom Bar
-(void)setupBottomBar{
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        NSFontAttributeName:[UIFont fontWithName:FONT_BOLD size:13.0f]
                                                        } forState:UIControlStateNormal];
    [[UITabBar appearance] setTintColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
    if(SYSTEM_VERSION_GREATER_THAN(@"10")){
        [[UITabBar appearance] setUnselectedItemTintColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
    } else{
        [[UIView appearanceWhenContainedIn:[UITabBar class], nil] setTintColor:[UIColor whiteColor]];
    }
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] }
                                             forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f] }
                                             forState:UIControlStateNormal];
    
    float height = 50;
    if ([[SharedMethod new] isWithNotch]) {
        height = 85;
        [UITabBar appearance].selectionIndicatorImage = [self imageWithColor:[UIColor whiteColor] andBounds:CGRectMake(0, -1, [UIScreen mainScreen].bounds.size.width/5, height)];
    } else {
        [UITabBar appearance].selectionIndicatorImage = [self imageWithColor:[UIColor whiteColor] andBounds:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width/5, height)];
    }
    
}

@end
