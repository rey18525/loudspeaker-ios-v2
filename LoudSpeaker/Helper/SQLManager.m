//
//  SQLManager.m
//  Oasis
//
//  Created by Wong Ryan on 13/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "SQLManager.h"
#import "FMDatabase.h"
#import "FMResultSet.h"
#import "FMDB.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "Voucher.h"
#import "Promotion.h"
#import "Outlet.h"
#import "Menu.h"

@implementation SQLManager{
    FMDatabase *fmdb;
}

-(BOOL)checkDatabase{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *databasePath = [documentsDirectory stringByAppendingPathComponent:DATABASE_NAME];
    NSString *templatePath = [[NSBundle mainBundle] pathForResource:@"LoudSpeakerDB" ofType:@"db"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if(![fileManager fileExistsAtPath:databasePath]){
        [fileManager copyItemAtPath:templatePath toPath:databasePath error:&error];
        if(error){
            return NO;
        }
    }
    
    FMDatabase *tempDB = [FMDatabase databaseWithPath:databasePath];
    [[NSUserDefaults standardUserDefaults] setObject:databasePath forKey:DATABASE_PATH];
    if(![tempDB open]){
        #ifdef DEBUG
        NSLog(@"Failed to open database!");
        #endif
        return NO;
    }
    return YES;
}

-(void)checkTermsExist{
    fmdb = [FMDatabase databaseWithPath:[[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_PATH]];
    [fmdb open];
    
    BOOL isSuccess;
    if(![fmdb columnExists:@"terms" inTableWithName:@"voucher"]){
        NSLog(@"Terms column not exists");
        isSuccess = [fmdb executeUpdate:@"ALTER TABLE voucher ADD COLUMN terms TEXT"];
        NSAssert(isSuccess, @"alter table failed: %@",[fmdb lastErrorMessage]);
    } else{
        NSLog(@"Terms column exists");
    }
}

//Vouchers SQL//
#pragma mark - Check Voucher Record Exists
-(BOOL)isVoucherExists:(NSString*)ID{
    BOOL exist = NO;
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM voucher"];
    FMResultSet *result = [fmdb executeQuery:query];
    while ([result next]) {
        NSString *voucherID = [result stringForColumn:@"ID"];
        voucherID = [NSString stringWithFormat:@"%d",[voucherID intValue]];
        if([voucherID isEqualToString:ID]){
            exist = YES;
            break;
        }
    }
    return exist;

}

#pragma mark - Insert/Update Voucher
-(void)insertAndUpdateVouchers:(NSMutableArray*)array{
    fmdb = [FMDatabase databaseWithPath:[[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_PATH]];
    [fmdb open];
    for(Voucher *voucher in array){
        if([self isVoucherExists:[NSString stringWithFormat:@"%d", [voucher.ID intValue]]]){
            //Found same ID//
            //Perform Update//
            NSString *query = [NSString stringWithFormat:@"UPDATE voucher SET campaign_id = ?, category_id = ?, content = ?, terms = ?, created_at = ?, featured = ?, featured_note = ?, image = ?, images = ?, priority = ?, published_at = ?, subtitle = ?, title = ?, type = ?, updated_at = ?, url = ? WHERE ID = ?"];
            
            [fmdb executeUpdate:query, voucher.campaign_id, voucher.category_id, voucher.content, voucher.terms,voucher.created_at, voucher.featured, voucher.featured_note, voucher.image, voucher.images, voucher.priority, voucher.publish_at, voucher.subtitle, voucher.title, voucher.type, voucher.updated_at, voucher.url, voucher.ID];
            
        } else{
            //Not same ID found//
            //Perform Insert//;
            NSString *query = [NSString stringWithFormat:@"INSERT INTO voucher(campaign_id, category_id, content, terms, created_at, featured, featured_note, ID, image, images, priority, published_at, subtitle, title,  type, updated_at, url) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" ];
            
            [fmdb executeUpdate:query, voucher.campaign_id, voucher.category_id, voucher.content, voucher.terms, voucher.created_at, voucher.featured, voucher.featured_note, voucher.ID, voucher.image, voucher.images, voucher.priority, voucher.publish_at, voucher.subtitle, voucher.title, voucher.type, voucher.updated_at, voucher.url];
        }
    }
    
    [fmdb close];

}

#pragma mark - Delete Voucher
-(void)deleteVoucher:(NSString*)ID{
    
}

-(void)deleteVouchers:(NSMutableArray*)array{
    
}

#pragma mark - Retrieve Vouchers
-(NSMutableArray*)retrieveVouchers{
    NSMutableArray *vouchers = [[NSMutableArray alloc]init];
    fmdb = [FMDatabase databaseWithPath:[[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_PATH]];
    [fmdb open];
    NSString *query = @"SELECT * FROM voucher";
    FMResultSet *result = [fmdb executeQuery:query];
    while([result next]){
        Voucher *voucher = [[Voucher alloc]init];
        voucher.campaign_id = [NSString stringWithFormat:@"%@",[result stringForColumn:@"campaign_id"]];
        voucher.category_id = [NSString stringWithFormat:@"%@",[result stringForColumn:@"category_id"]];
        voucher.content = [NSString stringWithFormat:@"%@",[result stringForColumn:@"content"]];
        voucher.terms = [NSString stringWithFormat:@"%@",[result stringForColumn:@"terms"]];
        voucher.featured = [NSString stringWithFormat:@"%@",[result stringForColumn:@"featured"]];
        voucher.featured_note = [NSString stringWithFormat:@"%@",[result stringForColumn:@"featured_note"]];
        voucher.ID = [NSString stringWithFormat:@"%@",[result stringForColumn:@"ID"]];
        voucher.image = [NSString stringWithFormat:@"%@",[result stringForColumn:@"image"]];
        voucher.images = [NSString stringWithFormat:@"%@",[result stringForColumn:@"images"]];
        voucher.priority = [NSString stringWithFormat:@"%@",[result stringForColumn:@"priority"]];
        voucher.publish_at = [NSString stringWithFormat:@"%@",[result stringForColumn:@"published_at"]];
        voucher.subtitle = [NSString stringWithFormat:@"%@",[result stringForColumn:@"subtitle"]];
        voucher.title = [NSString stringWithFormat:@"%@",[result stringForColumn:@"title"]];
        voucher.type = [NSString stringWithFormat:@"%@",[result stringForColumn:@"type"]];
        voucher.updated_at = [NSString stringWithFormat:@"%@",[result stringForColumn:@"updated_at"]];
        voucher.url = [NSString stringWithFormat:@"%@",[result stringForColumn:@"url"]];
        [vouchers addObject:voucher];
    }
    [fmdb close];
    
    return vouchers;
}

#pragma mark - Search Vouchers
-(NSMutableArray*)searchVouchers:(NSString*)searched{
    NSMutableArray *vouchers = [[NSMutableArray alloc]init];
    fmdb = [FMDatabase databaseWithPath:[[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_PATH]];
    [fmdb open];
    NSString *query;
    FMResultSet *result;
    
    query = @"SELECT * FROM promotions WHERE Title LIKE ?";
    result = [fmdb executeQuery:query, [NSString stringWithFormat:@"%%%@%%",searched]];

    while([result next]){
        
    }
    [fmdb close];
    
    return vouchers;
}
//End of Vouchers SQL//


#pragma mark - Check Promotion Record Exists
-(BOOL)isPromotionExists:(NSString*)ID{
    BOOL exist = NO;
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM promotion"];
    FMResultSet *result = [fmdb executeQuery:query];
    while ([result next]) {
        NSString *promotionID = [result stringForColumn:@"ID"];
        promotionID = [NSString stringWithFormat:@"%d",[promotionID intValue]];
        if([promotionID isEqualToString:ID]){
            exist = YES;
            break;
        }
    }
    return exist;
}

#pragma mark - Insert/Update Promotion
-(void)insertAndUpdatePromotions:(NSMutableArray*)array{
    fmdb = [FMDatabase databaseWithPath:[[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_PATH]];
    [fmdb open];
    for(Promotion *promotion in array){
        if([self isPromotionExists:[NSString stringWithFormat:@"%d", [promotion.ID intValue]]]){
            //Found same ID//
            //Perform Update//
            NSString *query = [NSString stringWithFormat:@"UPDATE promotion SET campaign_id = ?, category_id = ?, content = ?, created_at = ?, featured = ?, featured_note = ?, image = ?, images = ?, priority = ?, published_at = ?, subtitle = ?, title = ?, type = ?, updated_at = ?, url = ? WHERE ID = ?"];
            
            [fmdb executeUpdate:query, promotion.campaign_id, promotion.category_id, promotion.content, promotion.created_at, promotion.featured, promotion.featured_note, promotion.image, promotion.images, promotion.priority, promotion.publish_at, promotion.subtitle, promotion.title, promotion.type, promotion.updated_at, promotion.url, promotion.ID];
            
        } else{
            //Not same ID found//
            //Perform Insert//;
            NSString *query = [NSString stringWithFormat:@"INSERT INTO promotion(campaign_id, category_id, content, created_at, featured, featured_note, ID, image, images, priority, published_at, subtitle, title, type, updated_at, url) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" ];
            
            [fmdb executeUpdate:query, promotion.campaign_id, promotion.category_id, promotion.content, promotion.created_at, promotion.featured, promotion.featured_note, promotion.ID, promotion.image, promotion.images, promotion.priority, promotion.publish_at, promotion.subtitle, promotion.title, promotion.type, promotion.updated_at, promotion.url];
        }
    }

    [fmdb close];
}

#pragma mark - Delete Promotions
-(void)deletePromotion:(NSString*)ID{
    
}

-(void)deletePromotions:(NSMutableArray*)array{
    
}

#pragma mark - Retrieve Promotions
-(NSMutableArray*)retrievePromotions:(BOOL)isPromo{
    NSMutableArray *promotions = [[NSMutableArray alloc]init];
    fmdb = [FMDatabase databaseWithPath:[[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_PATH]];
    [fmdb open];
    NSString *query = @"SELECT * FROM promotion";
    FMResultSet *result = [fmdb executeQuery:query];
    while([result next]){
        if(isPromo && [[NSString stringWithFormat:@"%@",[result stringForColumn:@"type"]] isEqualToString:@"promo"]){
            Promotion *promotion = [[Promotion alloc]init];
            promotion.campaign_id = [NSString stringWithFormat:@"%@",[result stringForColumn:@"campaign_id"]];
            promotion.category_id = [NSString stringWithFormat:@"%@",[result stringForColumn:@"category_id"]];
            promotion.content = [NSString stringWithFormat:@"%@",[result stringForColumn:@"content"]];
            promotion.featured = [NSString stringWithFormat:@"%@",[result stringForColumn:@"featured"]];
            promotion.featured_note = [NSString stringWithFormat:@"%@",[result stringForColumn:@"featured_note"]];
            promotion.ID = [NSString stringWithFormat:@"%@",[result stringForColumn:@"ID"]];
            promotion.image = [NSString stringWithFormat:@"%@",[result stringForColumn:@"image"]];
            promotion.images = [NSString stringWithFormat:@"%@",[result stringForColumn:@"images"]];
            promotion.priority = [NSString stringWithFormat:@"%@",[result stringForColumn:@"priority"]];
            promotion.publish_at = [NSString stringWithFormat:@"%@",[result stringForColumn:@"published_at"]];
            promotion.subtitle = [NSString stringWithFormat:@"%@",[result stringForColumn:@"subtitle"]];
            promotion.title = [NSString stringWithFormat:@"%@",[result stringForColumn:@"title"]];
            promotion.type = [NSString stringWithFormat:@"%@",[result stringForColumn:@"type"]];
            promotion.updated_at = [NSString stringWithFormat:@"%@",[result stringForColumn:@"updated_at"]];
            promotion.url = [NSString stringWithFormat:@"%@",[result stringForColumn:@"url"]];
            [promotions addObject:promotion];
        } else if(!isPromo && [[NSString stringWithFormat:@"%@",[result stringForColumn:@"type"]] isEqualToString:@"event"]){
            Promotion *promotion = [[Promotion alloc]init];
            promotion.campaign_id = [NSString stringWithFormat:@"%@",[result stringForColumn:@"campaign_id"]];
            promotion.category_id = [NSString stringWithFormat:@"%@",[result stringForColumn:@"category_id"]];
            promotion.content = [NSString stringWithFormat:@"%@",[result stringForColumn:@"content"]];
            promotion.featured = [NSString stringWithFormat:@"%@",[result stringForColumn:@"featured"]];
            promotion.featured_note = [NSString stringWithFormat:@"%@",[result stringForColumn:@"featured_note"]];
            promotion.ID = [NSString stringWithFormat:@"%@",[result stringForColumn:@"ID"]];
            promotion.image = [NSString stringWithFormat:@"%@",[result stringForColumn:@"image"]];
            promotion.images = [NSString stringWithFormat:@"%@",[result stringForColumn:@"images"]];
            promotion.priority = [NSString stringWithFormat:@"%@",[result stringForColumn:@"priority"]];
            promotion.publish_at = [NSString stringWithFormat:@"%@",[result stringForColumn:@"published_at"]];
            promotion.subtitle = [NSString stringWithFormat:@"%@",[result stringForColumn:@"subtitle"]];
            promotion.title = [NSString stringWithFormat:@"%@",[result stringForColumn:@"title"]];
            promotion.type = [NSString stringWithFormat:@"%@",[result stringForColumn:@"type"]];
            promotion.updated_at = [NSString stringWithFormat:@"%@",[result stringForColumn:@"updated_at"]];
            promotion.url = [NSString stringWithFormat:@"%@",[result stringForColumn:@"url"]];
            [promotions addObject:promotion];
        }
    }
    [fmdb close];
    
    return promotions;
}

#pragma mark - Search Promotions
-(NSMutableArray*)searchPromotions:(NSString*)searched{
    NSMutableArray *promotions = [[NSMutableArray alloc]init];
    

    return promotions;
}


//Menu SQL//
#pragma mark - Check Menu Record Exists
-(BOOL)isMenuExists:(NSString*)ID{
    BOOL exist = NO;
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM menu"];
    FMResultSet *result = [fmdb executeQuery:query];
    while ([result next]) {
        NSString *menuID = [result stringForColumn:@"ID"];
        menuID = [NSString stringWithFormat:@"%d",[menuID intValue]];
        if([menuID isEqualToString:ID]){
            exist = YES;
            break;
        }
    }
    return exist;
    
}

#pragma mark - Insert/Update Voucher
-(void)insertAndUpdateMenu:(NSMutableArray*)array{
    fmdb = [FMDatabase databaseWithPath:[[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_PATH]];
    [fmdb open];
    for(Menu *menu in array){
        if([self isMenuExists:[NSString stringWithFormat:@"%d", [menu.ID intValue]]]){
            //Found same ID//
            //Perform Update//
            NSString *query = [NSString stringWithFormat:@"UPDATE menu SET campaign_id = ?, category_id = ?, content = ?, created_at = ?, featured = ?, featured_note = ?, image = ?, images = ?, priority = ?, published_at = ?, subtitle = ?, title = ?, type = ?, updated_at = ?, url = ? WHERE ID = ?"];
            
            [fmdb executeUpdate:query, menu.campaign_id, menu.category_id, menu.content, menu.created_at, menu.featured, menu.featured_note, menu.image, menu.images, menu.priority, menu.publish_at, menu.subtitle, menu.title, menu.type, menu.updated_at, menu.url, menu.ID];
        } else{
            //Not same ID found//
            //Perform Insert//;
            NSString *query = [NSString stringWithFormat:@"INSERT INTO menu(campaign_id, category_id, content, created_at, featured, featured_note, ID, image, images, priority, published_at, subtitle, title, type, updated_at, url) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" ];
            
            [fmdb executeUpdate:query, menu.campaign_id, menu.category_id, menu.content, menu.created_at, menu.featured, menu.featured_note, menu.ID, menu.image, menu.images, menu.priority, menu.publish_at, menu.subtitle, menu.title, menu.type, menu.updated_at, menu.url];
        }
    }
    
    [fmdb close];
    
}

#pragma mark - Delete Menu
-(void)deleteMenu:(NSString*)ID{
    
}

-(void)deleteMenus:(NSMutableArray*)array{
    
}

#pragma mark - Retrieve Menu
-(NSMutableArray*)retrieveMenu:(NSString *)ID{
    NSMutableArray *menus = [[NSMutableArray alloc]init];
    fmdb = [FMDatabase databaseWithPath:[[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_PATH]];
    [fmdb open];
    NSString *query = @"SELECT * FROM menu";
    FMResultSet *result = [fmdb executeQuery:query];
    while([result next]){
        Menu *menu = [[Menu alloc]init];
        menu.campaign_id = [NSString stringWithFormat:@"%@",[result stringForColumn:@"campaign_id"]];
        menu.category_id = [NSString stringWithFormat:@"%@",[result stringForColumn:@"category_id"]];
        menu.content = [NSString stringWithFormat:@"%@",[result stringForColumn:@"content"]];
        menu.featured = [NSString stringWithFormat:@"%@",[result stringForColumn:@"featured"]];
        menu.featured_note = [NSString stringWithFormat:@"%@",[result stringForColumn:@"featured_note"]];
        menu.ID = [NSString stringWithFormat:@"%@",[result stringForColumn:@"ID"]];
        menu.image = [NSString stringWithFormat:@"%@",[result stringForColumn:@"image"]];
        menu.images = [NSString stringWithFormat:@"%@",[result stringForColumn:@"images"]];
        menu.priority = [NSString stringWithFormat:@"%@",[result stringForColumn:@"priority"]];
        menu.publish_at = [NSString stringWithFormat:@"%@",[result stringForColumn:@"published_at"]];
        menu.subtitle = [NSString stringWithFormat:@"%@",[result stringForColumn:@"subtitle"]];
        menu.title = [NSString stringWithFormat:@"%@",[result stringForColumn:@"title"]];
        menu.type = [NSString stringWithFormat:@"%@",[result stringForColumn:@"type"]];
        menu.updated_at = [NSString stringWithFormat:@"%@",[result stringForColumn:@"updated_at"]];
        menu.url = [NSString stringWithFormat:@"%@",[result stringForColumn:@"url"]];
        
        if([menu.category_id isEqualToString:[NSString stringWithFormat:@"%d",[ID intValue]]]){
            [menus addObject:menu];
        }
    }
    [fmdb close];
    
    return menus;
}

#pragma mark - Search Menu
-(NSMutableArray*)searchMenu:(NSString*)searched{
    NSMutableArray *vouchers = [[NSMutableArray alloc]init];
    fmdb = [FMDatabase databaseWithPath:[[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_PATH]];
    [fmdb open];
    NSString *query;
    FMResultSet *result;
    
    query = @"SELECT * FROM promotions WHERE Title LIKE ?";
    result = [fmdb executeQuery:query, [NSString stringWithFormat:@"%%%@%%",searched]];
    
    while([result next]){
        
    }
    [fmdb close];
    
    return vouchers;
}
//End of Menu SQL//


#pragma mark - Check Outlet Record Exists
-(BOOL)isOutletExists:(NSString*)ID{
    BOOL exist = NO;
    NSString *query = [NSString stringWithFormat:@"SELECT * FROM outlets"];
    FMResultSet *result = [fmdb executeQuery:query];
    while ([result next]) {
        NSString *outletID = [result stringForColumn:@"ID"];
        outletID = [NSString stringWithFormat:@"%d",[outletID intValue]];
        if([outletID isEqualToString:ID]){
            exist = YES;
            break;
        }
    }
    return exist;
}

#pragma mark - Check & Remove Records Not Exist In Latest Response
-(void)removeNotLatest:(NSMutableArray*)array{
    fmdb = [FMDatabase databaseWithPath:[[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_PATH]];
    [fmdb open];
    
    for(NSString *ID in array){
        NSString *query = [NSString stringWithFormat:@"DELETE FROM outlets WHERE ID = ?"];
        [fmdb executeUpdate:query, ID];
    }
    
    [fmdb close];
}

#pragma mark - Insert/Update Outlet
-(void)insertAndUpdateOutlets:(NSMutableArray*)array{
    fmdb = [FMDatabase databaseWithPath:[[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_PATH]];
    [fmdb open];
        for(Outlet *outlet in array){
            if([self isOutletExists:[NSString stringWithFormat:@"%d", [outlet.ID intValue]]]){
                //Found same ID//
                //Perform Update//
                NSString *query = [NSString stringWithFormat:@"UPDATE outlets SET name = ?, address = ?, address1 = ?, address2 = ?, address3 = ?, city = ?, country = ?, email = ?, fax = ?, hours = ?, latitude = ?, longitude = ?, phone = ?, postcode = ?, priority = ?, status = ?, state = ?, types = ?, imageString = ? WHERE ID = ?"];
                
                [fmdb executeUpdate:query, outlet.name, outlet.address, outlet.address1, outlet.address2, outlet.address3, outlet.city, outlet.country, outlet.email, outlet.fax, [[SharedMethod new] convertArrayToString:outlet.hours], outlet.latitude, outlet.longitude, outlet.phone, outlet.postcode, outlet.priority, outlet.status, outlet.state, outlet.type, outlet.imageString, outlet.ID];
            } else{
                //Not same ID found//
                //Perform Insert//;
                NSString *query = [NSString stringWithFormat:@"INSERT INTO outlets(ID, name, address, address1, address2, address3, city, country, email, fax, hours, latitude, longitude, phone, postcode, priority, status, state, types, imageString) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" ];
                
                [fmdb executeUpdate:query, outlet.ID, outlet.name, outlet.address, outlet.address1, outlet.address2, outlet.address3, outlet.city, outlet.country, outlet.email, outlet.fax, [[SharedMethod new] convertArrayToString:outlet.hours], outlet.latitude, outlet.longitude, outlet.phone, outlet.postcode, outlet.priority, outlet.status, outlet.state, outlet.type, outlet.imageString];
            }
        }
    [fmdb close];
}

#pragma mark - Delete Outlet
-(void)deleteOutlet:(NSMutableArray*)array{
    
}

#pragma mark - Retrieve Outlets
-(NSMutableArray*)retrieveOutlets{
    NSMutableArray *outlets = [[NSMutableArray alloc]init];
    fmdb = [FMDatabase databaseWithPath:[[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_PATH]];
    
    [fmdb open];
    NSString *query = @"SELECT * FROM outlets";
    FMResultSet *result = [fmdb executeQuery:query];
    while([result next]){
        Outlet *outlet = [[Outlet alloc]init];
        outlet.ID = [NSString stringWithFormat:@"%@",[result stringForColumn:@"ID"]];
        outlet.name =[NSString stringWithFormat:@"%@",[result stringForColumn:@"name"]];
        outlet.address = [NSString stringWithFormat:@"%@",[result stringForColumn:@"address"]];
        outlet.address1 = [NSString stringWithFormat:@"%@",[result stringForColumn:@"address1"]];
        outlet.address2 = [NSString stringWithFormat:@"%@",[result stringForColumn:@"address2"]];
        outlet.address3 = [NSString stringWithFormat:@"%@",[result stringForColumn:@"address3"]];
        outlet.city = [NSString stringWithFormat:@"%@",[result stringForColumn:@"city"]];
        outlet.country = [NSString stringWithFormat:@"%@",[result stringForColumn:@"country"]];
        outlet.email = [NSString stringWithFormat:@"%@",[result stringForColumn:@"email"]];
        outlet.fax = [NSString stringWithFormat:@"%@",[result stringForColumn:@"fax"]];
        outlet.hours = [[SharedMethod new] convertJSONToArray:[result objectForKeyedSubscript:@"hours"]];
        outlet.latitude = [NSString stringWithFormat:@"%@",[result stringForColumn:@"latitude"]];
        outlet.longitude = [NSString stringWithFormat:@"%@",[result stringForColumn:@"longitude"]];
        outlet.phone = [NSString stringWithFormat:@"%@",[result stringForColumn:@"phone"]];
        outlet.postcode = [NSString stringWithFormat:@"%@",[result stringForColumn:@"postcode"]];
        outlet.priority = [NSString stringWithFormat:@"%@",[result stringForColumn:@"priority"]];
        outlet.status = [NSString stringWithFormat:@"%@",[result stringForColumn:@"status"]];
        outlet.state = [NSString stringWithFormat:@"%@",[result stringForColumn:@"state"]];
        outlet.type = [NSString stringWithFormat:@"%@",[result stringForColumn:@"types"]];
        outlet.imageString = [NSString stringWithFormat:@"%@",[result stringForColumn:@"imageString"]];
        [outlets addObject:outlet];
    }
    [fmdb close];
    
    return outlets;
}

@end
