//
//  WebAPI.m
//  Oasis
//
//  Created by Wong Ryan on 13/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "WebAPI.h"
#import "Constant.h"
#import "SharedMethod.h"
#import "XMLReader.h"
#import "NSString+SBJSON.h"
#import <AFNetworking.h>

@implementation WebAPI

#pragma mark - Retrieve App Settings
+(void)retrieveAppSettingsSuccess:(SuccessBlocks)successBlock{
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingAppSetting:[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL] andParams:(NSMutableDictionary*)nil andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSError *error,*parseError;;
            NSData *responseData = success;
            if(responseData.length<1){
                //                NSLog(@"Request Time Out");
            } else{
                NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLData:responseData
                                                                        error:&parseError];
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:xmlDictionary
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:&error];
                
                if(jsonData.length>0){
                    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    //                    NSString *cleanedJson = [jsonString stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                    //                    cleanedJson = [cleanedJson stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                    id finalJSON = [jsonString JSONValue];
                    successBlock(finalJSON);
                }
            }
        }
    }];
}

#pragma mark - Retrieve App Settings With Params
+(void)retrieveAppSettingsWithParams:(NSDictionary *)inputParams andSuccess:(SuccessBlocks)successBlock{
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingAppSetting:[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL] andParams:(NSMutableDictionary*)inputParams andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSError *error,*parseError;;
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLData:responseData
                                                                        error:&parseError];
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:xmlDictionary
                                                                   options:NSJSONWritingPrettyPrinted
                                                                     error:&error];
                
                if(jsonData.length>0){
                    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    //                    NSString *cleanedJson = [jsonString stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                    //                    cleanedJson = [cleanedJson stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                    id finalJSON = [jsonString JSONValue];
                    successBlock(finalJSON);
                }
            }
        }
    }];
}
#pragma mark - Geofence Setup
+(void)retrieveGeofence:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],REQUEST_GEO_URL]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *headerParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *newParams = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    
    if([inputParams objectForKey:@"type"]){
        [newParams setObject:[inputParams objectForKey:@"type"] forKey:@"type"];
        //        [newParams removeObjectForKey:@"type"];
    }
    
    if([inputParams objectForKey:@"token"]){
        [headerParams setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
        [newParams removeObjectForKey:@"token"];
    }
    
    if([inputParams objectForKey:@"userId"]){
        [headerParams setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
        [newParams removeObjectForKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"headerParams - %@",headerParams);
    NSLog(@"newParams - %@",newParams);
    
    NSLog(@"Header Token - %@",[inputParams objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:newParams andHeader:headerParams andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Retrieve Home Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

+(void)retrieveGeofence:(NSDictionary*)inputParams andHeader:(NSDictionary*)params andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],REQUEST_GEO_URL]];
    NSLog(@"TARGER URL ---%@",targetURL);
    [[SharedMethod new] setupAFNetworkingGET:targetURL.absoluteString params:(NSMutableDictionary*)inputParams andHeader:(NSMutableDictionary*)params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
            NSLog(@"Request Geo RequestReply - %@",requestReply);
            NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
            cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
            NSLog(@"Request Geo Result - %@",[cleanedJSON JSONValue]);
            id finalJSON = [cleanedJSON JSONValue];
            successBlock(finalJSON);
        }
    }];
}

+(void)sendGeofenceData:(NSDictionary*)inputParams andHeader:params andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],SEND_GEO_URL]];
    NSLog(@"TARGET URL ---%@",targetURL);
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:(NSMutableDictionary*)inputParams andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
            NSLog(@"Request Geo RequestReply - %@",requestReply);
            NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
            cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
            NSLog(@"Request Geo Result - %@",[cleanedJSON JSONValue]);
            id finalJSON = [cleanedJSON JSONValue];
            successBlock(finalJSON);
        }
    }];
}

+(void)sendGeofenceData:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],SEND_GEO_URL]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *headerParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *newParams = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    
    if([inputParams objectForKey:@"type"]){
        [newParams setObject:[inputParams objectForKey:@"type"] forKey:@"type"];
        //        [newParams removeObjectForKey:@"type"];
    }
    
    if([inputParams objectForKey:@"token"]){
        [headerParams setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
        [newParams removeObjectForKey:@"token"];
    }
    
    if([inputParams objectForKey:@"userId"]){
        [headerParams setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
        [newParams removeObjectForKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"headerParams - %@",headerParams);
    NSLog(@"newParams - %@",newParams);
    
    NSLog(@"Header Token - %@",[inputParams objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:newParams andHeader:headerParams andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Retrieve Geo Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

#pragma mark - Users
//Sign Up
+(void)signUpNewUser:(NSDictionary *)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_REGISTER]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
#ifdef DEBUG
                NSLog(@"Sign Up Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Login
+(void)loginUser:(NSDictionary *)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_LOGIN]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Login Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Card Sign Up
+(void)cardSignUp:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_CARD_VERIFY]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
#ifdef DEBUG
                NSLog(@"Card Sign Up Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Forgot Password
+(void)forgotPassword:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_FORGOT_PASSWORD]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
#ifdef DEBUG
                NSLog(@"Login Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Sign up Facebook New User
+(void)signUpFacebookNewUser:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_REGISTER_FB]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    [newInputParam setObject:[newInputParam objectForKey:@"socialToken"] forKey:@"token"];
    [newInputParam removeObjectForKey:@"socialToken"];
    
#ifdef DEBUG
    NSLog(@"newInputParam - %@",newInputParam);
#endif
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                
#ifdef DEBUG
                NSLog(@"RequestReply - %@",requestReply);
#endif
                
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Sign Up FB Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Facebook Login
+(void)loginFacebook:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_LOGIN_FB]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    [newInputParam setObject:[newInputParam objectForKey:@"socialToken"] forKey:@"token"];
    [newInputParam removeObjectForKey:@"socialToken"];
    
#ifdef DEBUG
    NSLog(@"newInputParam - %@",newInputParam);
#endif
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                
#ifdef DEBUG
                NSLog(@"RequestReply - %@",requestReply);
#endif
                
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Login FB Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Sign up Google New User
+(void)signUpGoogleNewUser:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_REGISTER_GOOGLE]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    [newInputParam setObject:[newInputParam objectForKey:@"socialToken"] forKey:@"token"];
    [newInputParam removeObjectForKey:@"socialToken"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Sign Up Google Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

+(void)loginGoogle:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_LOGIN_GOOGLE]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    [newInputParam setObject:[newInputParam objectForKey:@"socialToken"] forKey:@"token"];
    [newInputParam removeObjectForKey:@"socialToken"];
    
#ifdef DEBUG
    NSLog(@"newInputParam - %@",newInputParam);
#endif
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
#ifdef DEBUG
                NSLog(@"Login Google Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}


//Retrieve Profile
+(void)retrieveProfile:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_PROFILE]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    [newInputParam removeObjectForKey:@"userId"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Retrieve Profile Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Get All
+(void)retrieveAll:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_GET_ALL]];
    
#ifdef DEBUG
    NSLog(@"retrieve all targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    [newInputParam removeObjectForKey:@"userId"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Retrieve All Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Upload Profile Details
+(void)updateProfileDetail:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_PROFILE]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    [newInputParam removeObjectForKey:@"userId"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPATCH:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Update Profile Detail Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Retrieve Inbox
+(void)retrieveInbox:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_INBOX]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    [newInputParam removeObjectForKey:@"userId"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                //                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                NSString *cleanedJSON = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
                
#ifdef DEBUG
                NSLog(@"Retrieve Inbox Result - %@",cleanedJSON);
#endif
                
                id finalJSON = cleanedJSON;//[requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//View Inbox Message
+(void)viewInboxMessage:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_INBOX_MESSAGE,[inputParams objectForKey:@"msgID"]]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    [newInputParam removeObjectForKey:@"userId"];
    [newInputParam removeObjectForKey:@"msgID"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                //                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSUTF8StringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
                NSString *cleanedJSON = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
                
#ifdef DEBUG
                NSLog(@"View Inbox Message Result - %@",cleanedJSON);
#endif
                
                id finalJSON = cleanedJSON;
                successBlock(finalJSON);
            }
        }
    }];
}

//Delete Inbox Message
+(void)deleteInboxMessage:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_INBOX_MESSAGE,[inputParams objectForKey:@"msgID"]]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"userId"];
    [newInputParam removeObjectForKey:@"token"];
    [newInputParam removeObjectForKey:@"msgID"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    
#ifdef DEBUG
    NSLog(@"Header Token - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingDELETE:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Delete Inbox Message Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}


//Logout User
+(void)logoutUser:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_LOGOUT]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"userId"];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Retrieve Logout Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Upload Profile Picture Only
+(void)uploadProfilePicture:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_PROFILE_PICTURE]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"userId"];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    
#ifdef DEBUG
    NSLog(@"Header Token - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPUT:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Upload Profile Picture Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

+(void)deleteProfilePicture:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_PROFILE_PICTURE]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"userId"];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    
#ifdef DEBUG
    NSLog(@"Header Token - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingDELETE:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Delete Profile Picture Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

+(void)retrieveProfilePicture:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_PROFILE_PICTURE]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"userId"];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    
#ifdef DEBUG
    NSLog(@"Header Token - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Retrieve Profile Picture Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Retrieve Outlets
+(void)retrieveOutlets:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_OUTLET]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"userId"];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    
#ifdef DEBUG
    NSLog(@"Header Token - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@" "];cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Retrieve Outlet Result - %@",[requestReply JSONValue]);
#endif
                
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"  " withString:@" "];
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Retrieve Home Detail
+(void)retrieveHomeDetail:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_HOME]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *headerParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *newParams = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    
    if([inputParams objectForKey:@"type"]){
        [newParams setObject:[inputParams objectForKey:@"type"] forKey:@"type"];
        //        [newParams removeObjectForKey:@"type"];
    }
    
    if([inputParams objectForKey:@"token"]){
        [headerParams setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
        [newParams removeObjectForKey:@"token"];
    }
    
    if([inputParams objectForKey:@"userId"]){
        [headerParams setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
        [newParams removeObjectForKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"headerParams - %@",headerParams);
    NSLog(@"newParams - %@",newParams);
    
    NSLog(@"Header Token - %@",[inputParams objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:newParams andHeader:headerParams andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Retrieve Home Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Retrieve Posting
+(void)retrievePosting:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL;
    
    if([inputParams objectForKey:@"q"]){
        targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_POSTING_MENU]];
    } else{
        targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_POSTING]];
    }
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *headerParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *newParams = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    
    if([inputParams objectForKey:@"type"]){
        [newParams setObject:[inputParams objectForKey:@"type"] forKey:@"type"];
        //        [newParams removeObjectForKey:@"type"];
    }
    
    if([inputParams objectForKey:@"order_by"]){
        if([[inputParams objectForKey:@"type"] isEqualToString:POST_TYPE_1]){
            targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_POSTING_PROMO,[inputParams objectForKey:@"order_by"]]];
            [newParams removeObjectForKey:@"order_by"];
        } else if([[inputParams objectForKey:@"type"] isEqualToString:POST_TYPE_3]){
            targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_POSTING_MENU,[inputParams objectForKey:@"order_by"]]];
            [newParams removeObjectForKey:@"order_by"];
        }
    }
    
    if([inputParams objectForKey:@"token"]){
        [headerParams setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
        [newParams removeObjectForKey:@"token"];
    }
    
    if([inputParams objectForKey:@"userId"]){
        [headerParams setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
        [newParams removeObjectForKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"headerParams - %@",headerParams);
    NSLog(@"newParams - %@",newParams);
    NSLog(@"final targetURL - %@",targetURL);
    NSLog(@"Header Token - %@",[inputParams objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:newParams andHeader:headerParams andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Retrieve Posting Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Retrieve Posting Detail
+(void)retrievePostingDetail:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_POSTING_DETAIL,[inputParams objectForKey:@"posting_id"]]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newParam removeObjectForKey:@"posting_id"];
    
#ifdef DEBUG
    NSLog(@"Header Token - %@",[newParam objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:nil andHeader:newParam andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Retrieve Posting Detail Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Retrieve Events
+(void)retrieveEvent:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL;// = [NSURL URLWithString:USER_POSTING_EVENT];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *headerParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *newParams = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    
    if([inputParams objectForKey:@"type"]){
        [newParams setObject:[inputParams objectForKey:@"type"] forKey:@"type"];
        //        [newParams removeObjectForKey:@"type"];
    }
    
    if([inputParams objectForKey:@"order_by"]){
        targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_POSTING_EVENT,[inputParams objectForKey:@"order_by"]]];
        [newParams removeObjectForKey:@"order_by"];
    }
    
    if([inputParams objectForKey:@"token"]){
        [headerParams setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
        [newParams removeObjectForKey:@"token"];
    }
    
    if([inputParams objectForKey:@"userId"]){
        [headerParams setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
        [newParams removeObjectForKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"headerParams - %@",headerParams);
    NSLog(@"newParams - %@",newParams);
    
    NSLog(@"Header Token - %@",[inputParams objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:newParams andHeader:headerParams andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Retrieve Event Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}


//Retrieve Campaign
+(void)retrieveCampaign:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_CAMPAIGNS]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *headerParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *newParams = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    
    if([inputParams objectForKey:@"type"]){
        [newParams setObject:[inputParams objectForKey:@"type"] forKey:@"type"];
        //        [newParams removeObjectForKey:@"type"];
    }
    
    if([inputParams objectForKey:@"token"]){
        [headerParams setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
        [newParams removeObjectForKey:@"token"];
    }
    
    if([inputParams objectForKey:@"userId"]){
        [headerParams setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
        [newParams removeObjectForKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"headerParams - %@",headerParams);
    NSLog(@"newParams - %@",newParams);
    
    NSLog(@"Header Token - %@",[inputParams objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:newParams andHeader:headerParams andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Retrieve Posting Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Retrieve Campaign Detail
+(void)retrieveCampaignDetail:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_CAMPAIGNS,[inputParams objectForKey:@"campaign_id"]]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newParam removeObjectForKey:@"campaign_id"];
    
#ifdef DEBUG
    NSLog(@"Header Token - %@",[newParam objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:nil andHeader:newParam andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Retrieve Campaign Detail Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

+(void)redeemVoucher:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_VOUCHER_REDEEM,[inputParams objectForKey:@"campaign_id"]]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"userId"];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    
#ifdef DEBUG
    NSLog(@"Header Token - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Redeem Voucher Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Send Enquiry
+(void)sendEnquiry:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_ENQUIRY]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    if([newInputParam objectForKey:@"userId"]){
        [newInputParam removeObjectForKey:@"userId"];
    }
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Send Enquiry Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}


//Retrieve Registration Price
+(void)retrievePrice:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_PRICE]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
#ifdef DEBUG
                NSLog(@"Retrieve Register Price Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Flash Spend
+(void)retrieveFlashSpend:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_FLASH_SPEND]];
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    NSLog(@"ASDFX_spend_userId - %@",[inputParams objectForKey:@"userId"]);
    NSLog(@"ASDFX_spend_token - %@",[params objectForKey:@"token"]);
    NSLog(@"ASDFX_spend_newInputParam - %@",newInputParam);
    NSLog(@"ASDFX_spend_params - %@",params);
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                
            } else{
                
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Flash Grab
+(void)retrieveFlashGrab:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_FLASH_GRAB]];
    /*
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    */
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    
    NSLog(@"ASDFX_spend_userId - %@",[inputParams objectForKey:@"userId"]);
    NSLog(@"ASDFX_spend_token - %@",[params objectForKey:@"token"]);
    NSLog(@"ASDFX_spend_newInputParam - %@",newInputParam);
    NSLog(@"ASDFX_spend_params - %@",params);

    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                
            } else{
                
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Booking E-wallet
+(void)bookingEwallet:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_BOOKING_SPEND]];
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                
            } else{
                
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Retrieve Latest Order Detail
+(void)retrieveLatestOrderDetail:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_RETRIEVE_ORDER]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
                NSLog(@"Retrieve Latest Order Detail Result - %@",[requestReply JSONValue]);
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Generate Order Id
+(void)generateOrderId:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_GENERATE_ORDER]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    NSLog(@"ASDFX_generate_userId - %@",[inputParams objectForKey:@"userId"]);
    NSLog(@"ASDFX_generate_token - %@",[params objectForKey:@"token"]);
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
#ifdef DEBUG
                NSLog(@"Generate Order Id Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Update Order Id
+(void)updateOrderStatus:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_UPDATE_ORDER]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
#ifdef DEBUG
                NSLog(@"Update Order Id Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

//Virtual Card Registration
+(void)signUpVirtualCardUser:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_REGISTER_VIRTUAL]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
#ifdef DEBUG
                NSLog(@"Register Virtual Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}


//Renew Membership
+(void)renewMembership:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_RENEW_MEMBERSHIP]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
#ifdef DEBUG
                NSLog(@"Register Virtual Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}



#pragma mark - Send Statistics
+(void)sendStatistics:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],ACTION_STATISTICS]];
    
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *contentParams = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    if([contentParams objectForKey:@"userId"]){
        [contentParams removeObjectForKey:@"userId"];
    }
    [contentParams removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [dateFormatter setDateFormat:@"yyyyMMddHHMMss"];
    
    NSString *refNo = [dateFormatter stringFromDate:[NSDate date]];
    
    //    NSLog(@"inputParams - %@",inputParams);
    
    NSMutableDictionary *finalParams = [[NSMutableDictionary alloc] init];
    [finalParams setObject:refNo forKey:@"refno"];
    [finalParams setObject:[inputParams objectForKey:@"data"] forKey:@"data"];
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:finalParams andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
                NSLog(@"Request Time Out");
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
                //                NSString *cleanedJSON = [requestReply stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
                //                cleanedJSON = [cleanedJSON stringByReplacingOccurrencesOfString:@"\\r" withString:@""];
                
#ifdef DEBUG
                NSLog(@"Send Statistics Result - %@",[requestReply JSONValue]);
#endif
                
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

#pragma mark - Booking
+(void)retrieveBookingList:(NSDictionary *)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_BOOKING_LIST]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
#ifdef DEBUG
                NSLog(@"Retrieve Booking List Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

+(void)retrieveBookingDetail:(NSDictionary *)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_BOOKING_DETAIL,[inputParams objectForKey:@"booking_id"]]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    [newInputParam removeObjectForKey:@"booking_id"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingGET:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
#ifdef DEBUG
                NSLog(@"Retrieve Booking Detail Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

+(void)retrieveTimeSlots:(NSDictionary *)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_BOOKING_TIMESLOT]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
#ifdef DEBUG
                NSLog(@"Retrieve Booking Time Slots Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

+(void)retrieveAvailableSessions:(NSDictionary *)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_BOOKING_AVAILABLE_SESSION]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
#ifdef DEBUG
                NSLog(@"Retrieve Available Sessions Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

+(void)createBooking:(NSDictionary *)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_BOOKING_CREATE]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
#ifdef DEBUG
                NSLog(@"Create Booking Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

+(void)retrieveBookingPrice:(NSDictionary *)inputParams andSuccess:(SuccessBlocks)successBlock{
    NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:WEBSERVICE_URL],USER_BOOKING_PRICE]];
#ifdef DEBUG
    NSLog(@"targetURL %@",targetURL);
#endif
    
    NSMutableDictionary *newInputParam = [[NSMutableDictionary alloc] initWithDictionary:inputParams];
    [newInputParam removeObjectForKey:@"token"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[inputParams objectForKey:@"token"] forKey:@"token"];
    
    if([inputParams objectForKey:@"userId"]){
        [params setObject:[inputParams objectForKey:@"userId"] forKey:@"userId"];
    }
    
#ifdef DEBUG
    NSLog(@"myDeviceToken - %@",[params objectForKey:@"token"]);
#endif
    
    SharedMethod *shareMethod = [[SharedMethod alloc]init];
    [shareMethod setupAFNetworkingPOST:targetURL.absoluteString params:(NSMutableDictionary*)newInputParam andHeader:params andSuccess:^(id success) {
        if([success isKindOfClass:[NSDictionary class]]){
            successBlock(success);
        } else{
            NSData *responseData = success;
            if(responseData.length<1){
#ifdef DEBUG
                NSLog(@"Request Time Out");
#endif
            } else{
                NSData *responseData = success;
                NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes] length:[responseData length] encoding:NSASCIIStringEncoding];
#ifdef DEBUG
                NSLog(@"Retrieve Booking Price Result - %@",[requestReply JSONValue]);
#endif
                id finalJSON = [requestReply JSONValue];
                successBlock(finalJSON);
            }
        }
    }];
}

#pragma mark - Stop AFNetworking Request
+(void)stopAFNetworkingRequest{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.operationQueue cancelAllOperations];
}

#pragma mark - Stop NSURL Request
+(void)stopNSURLRequest{
    NSOperationQueue *queue = [NSOperationQueue new];
    //    NSInvocationOperation *operation = [[NSInvocationOperation alloc] init];
    [queue cancelAllOperations];
}

@end
