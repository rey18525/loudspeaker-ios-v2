//
//  SharedMethod.h
//  Oasis
//
//  Created by Wong Ryan on 11/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;
typedef void(^SuccessBlock)(id success);
typedef NS_ENUM(NSUInteger, ScreenType) {
    iPhones35inch, // 4, 4s
    iPhones40inch, // 5, 5s, 5c, SE
    iPhones47inch, // 6, 6s, 7, 8
    iPhones55inch, // 6+, 6s+, 7+, 8+
    iPhone58inch, // 11 Pro, X, Xs
    iPhone61inch, // 11, Xr
    iPhone65inch, // 11 Pro Max, Xs Max
    unknown
};
@interface SharedMethod : NSObject

#pragma  mark - Print All Fonts
-(void)printAllFonts;

#pragma mark - Check Connectivity
-(BOOL)checkNetworkConnectivity;

#pragma mark - Set Color
-(UIColor*)colorWithHexString:(NSString*)hex andAlpha:(float)alpha;

#pragma mark - Set Padding to TextField
-(void)setPaddingToTextField:(UITextField*)textField;

#pragma mark - Set Button Rounded Corner and Fill Color
-(void)setButtonStyle:(UIButton*)view cornerRadius:(float)radius andBackgroundColor:(NSString*)colorCode;

#pragma mark - Set Custom Button Style
-(void)setCustomButtonStyle:(UIButton*)button image:(NSString*)image borders:(BOOL)bordersResult borderWidth:(float)width cornerRadius:(float)radius borderColor:(NSString*)borderColorCode textColor:(NSString*)textColor andBackgroundColor:(NSString*)colorCode;

#pragma mark - Set Rounded Corner
-(void)setCornerRadius:(UIView*)view andCornerRadius:(float)radius;

#pragma mark - Set Bottom Line
-(void)setBottomBorder:(UIView*)view andBackgroundColor:(NSString*)colorCode;

#pragma mark - Set Border
-(void)setBorder:(UIView*)view borderColor:(NSString*)borderColorCode andBackgroundColor:(NSString*)colorCode;

#pragma mark - Setup Textfield
-(void)setupTextfield:(UITextField*)textfield fontType:(NSString*)fontType fontSize:(float)fontSize textColor:(NSString*)textColor textAlpha:(float)textAlpha addPadding:(BOOL)padding andAddBorder:(BOOL)border;

#pragma mark - Convert Date Time Format
-(NSString*)convertDateTimeFormat:(NSString*)date andInputFormat:(NSString*)inputFormat andOutputFormat:(NSString*)outputFormat;
-(NSString*)convertToDefaultDateFormat:(NSString*)text;
-(NSString*)convertToSendDateFormat:(NSString*)text;
-(NSString*)convertToTransactionDateFormat:(NSString*)text;
-(NSString*)convertToTransactionDetailDateFormat:(NSString*)text;
-(NSString*)convertToTransactionDateFormat_V2:(NSString*)text;
-(NSString*)convertToTransactionTimeFormat_V2:(NSString*)text;
-(NSString*)convertToNotificationDateFormat_V2:(NSString*)text;

#pragma mark - Process Flash
-(NSMutableArray*)processFlash:(NSArray*)flash andCurrentArray:(NSMutableArray*)flashArray;

#pragma mark - Process Category
-(NSMutableArray*)processCategory:(NSArray*)categories andCurrentArray:(NSMutableArray*)categoryArray;

#pragma mark - Process Posting
-(NSMutableArray*)processPosting:(NSArray*)posts andCurrentArray:(NSMutableArray*)promotionArray;

#pragma mark - Process Outlet
-(NSMutableArray*)processOutlet:(NSArray*)outlets;

#pragma mark - Process Reward
-(NSMutableArray*)processReward:(NSArray*)rewards andCurrentArray:(NSMutableArray*)rewardsArray;

#pragma mark - Process Campaign
-(NSMutableArray*)processCampaign:(NSArray*)campaigns andCurrentArray:(NSMutableArray*)campaignsArray;

#pragma mark - Process Voucher
-(NSMutableArray*)processVoucher:(NSArray*)vouchers andCurrentArray:(NSMutableArray*)vouchersArray;

#pragma mark - Process Menu
-(NSMutableArray*)processMenu:(NSArray*)menus andCurrentArray:(NSMutableArray*)menuArray;

#pragma mark - Process Transaction
-(NSMutableArray*)processTransactions:(NSArray*)transactions;

#pragma mark - Check Phones Validation
-(BOOL)validatePhone:(NSString *)checkString;

#pragma mark - Check Email Validation
-(BOOL)validateEmail:(NSString *)checkString;

#pragma mark - Convert to Black and Grey image
- (UIImage *)getBlackAndWhiteVersionOfImage:(UIImage *)anImage;

#pragma mark - Rorate Image
-(UIImage*)rorateImagePortrait:(UIImage*)image;

#pragma mark - UIAlertView
-(UIAlertController*)setAndShowAlertController:(NSString*)title message:(NSString*)message cancelButton:(NSString*)buttonName;

#pragma mark - Prepare Statistics
-(void)addStatistics:(NSString*)action_cd itemID:(NSString*)itemID;

#pragma mark - Save to file
-(void)writeToFile:(NSString*)fileName andDictioanry:(NSDictionary*)dict;

#pragma mark - Read from file
-(NSDictionary*)readFromFile:(NSString*)fileName;

#pragma mark - Check Device Token
-(NSString*)checkDeviceToken;

#pragma mark - Transition Animation for Navigation Controller
-(CATransition*)setupTransitionAnimation:(NSString*)animation;

#pragma mark - Set Gradient
-(void)setGradient:(UIView*)imgView;
-(void)setGradient2:(UIView*)imgView;

#pragma mark - Loading Indicators
-(void)setLoadingIndicatorOnImage:(UIView *)view;

-(void)setupLoadingIndicator:(UIView*)view;
-(void)setImageViewWithImages;
-(void)setLoadingStatusWithText;
-(void)setLoadingHUDView;
-(void)removeLoadingIndicator:(UIView *)view;

#pragma mark - Date Formatter
-(NSString*)convertToNotificationDateFormat:(NSString*)text;

#pragma mark - Save Profile Image
-(void)saveNewProfileImage:(UIImage*)image andIsSocial:(BOOL)isSocial;

#pragma mark - Remove Profile Image
-(void)removeProfileImage:(BOOL)isSocial;

#pragma mark - Load Profile Image
-(UIImage *)loadProfileImage:(BOOL)isSocial;

//Convert UIImage to NSString (JSON)
#pragma mark - Convert UIImage to NSString (JSON)
-(NSData*)convertImageDataToJSON:(NSData*)imgData;

#pragma mark - Generate QR Code
-(CIImage*)generateQRCode:(NSString*)string;

#pragma mark - SHA256
-(NSString*)sha256:(NSString *)clear;

//Convert NSString to JSON//
#pragma mark - Convert Array to JSON
- (NSString*)convertArrayToString:(NSArray*)array;

//Convert JSON to Array//
#pragma mark - Convert JSON to Array
- (NSMutableArray*)convertJSONToArray:(NSString*)json;

#pragma mark - Dictionary Convert Nil to ""
- (NSDictionary *) dictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary;
-(NSDictionary *) nestedDictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary;

#pragma mark - Convert To Encrypted Data
-(NSMutableDictionary*)convertToEncryptedDictionary:(NSDictionary*)data;

#pragma mark - Generate Token
+(NSString*)getTokenWithRoute:(NSString*)route apiKey:(NSString*)apiKey;
+(NSString *)timeStampExpiryHex;
+(NSString*)sha1HashFor:(NSString*)input;
+(NSString*)sha256HashFor:(NSString*)input;

#pragma mark - URL Request
-(NSMutableDictionary *)setupUrlRequest:(NSURL *)url timeOut:(CGFloat)timeout andEncryptParams:(NSMutableDictionary*)params;

#pragma mark - UIAlertController Alert Action
-(UIAlertController*)setAndShowAlert:(NSString *)title message:(NSString *)message cancelButton:(NSString *)buttonName;
-(void)setAndShowAlertView:(NSString *)title message:(NSString *)message cancelButton:(NSString *)buttonName;

#pragma mark - Setup AFNetworking
-(void)setupAFNetworkingAppSetting:(NSString *)url andParams:(NSMutableDictionary*)params andSuccess:(SuccessBlock)success;
-(void)setupAFNetworkingAppSetting:(NSString *)url  andSuccess:(SuccessBlock)success;
-(void)setupAFNetworkingJSON:(NSString *)url andParams:(NSMutableDictionary *)params andSuccess:(SuccessBlock)success;
-(void)setupAFNetworkingPOST:(NSString *)url params:(NSMutableDictionary *)params andHeader:(NSMutableDictionary *)header andSuccess:(SuccessBlock)success;
-(void)setupAFNetworkingGET:(NSString *)url params:(NSMutableDictionary *)params andHeader:(NSMutableDictionary *)header andSuccess:(SuccessBlock)success;
-(void)setupAFNetworkingDELETE:(NSString *)url params:(NSMutableDictionary *)params andHeader:(NSMutableDictionary *)header andSuccess:(SuccessBlock)success;
-(void)setupAFNetworkingPUT:(NSString *)url params:(NSMutableDictionary *)params andHeader:(NSMutableDictionary *)header andSuccess:(SuccessBlock)success;
-(void)setupAFNetworkingPATCH:(NSString *)url params:(NSMutableDictionary *)params andHeader:(NSMutableDictionary *)header andSuccess:(SuccessBlock)success;

#pragma mark - Device sizes
-(ScreenType)currentDeviceType;
-(BOOL)isSmallDevice;
-(BOOL)isWideDevice;
-(BOOL)isWithNotch;
@end
