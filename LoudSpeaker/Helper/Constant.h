// DEV endpoint

#define API_APP_SERVER              @"http://api.loudspeaker.apppay.my/"
#define API_APP_SERVER_SHARE        @"http://loudspeaker.apppay.my/"
#define API_COMPANY_WEBSITE         @"http://web2.loudspeaker.my/"
#define API_INITIAL_KEY             @"Z8srBwWXr0D4UxWnLrJdQ4Phd/5ZUumZ/AGsG4W7eRc="
#define IPAY_MERCHANT_KEY           @"PinZkN3MSC"
#define IPAY_MERCHANT_CODE          @"M12911_S0001"
#define IPAY_CALLBACK_URL           @"https://payment.apppay.tech/loudspeaker/ipay88/backend.php"

/*
// PROD endpoint
#define API_APP_SERVER              @"http://api.loudspeakerktv.my/"
#define API_APP_SERVER_SHARE        @"http://loudspeakerktv.my/"
#define API_COMPANY_WEBSITE         @"http://www.loudspeakerktv.my"
#define API_INITIAL_KEY             @"H0dko5m67wFMPFfUuHG5NaGoDoPLilZLGgqo+mwtOHE="
#define IPAY_MERCHANT_KEY           @"ipJCV47vIC"
#define IPAY_MERCHANT_CODE          @"M19977"
#define IPAY_CALLBACK_URL           @"https://payment.apppay.biz/tkbakery/ipay88/backend.php"
*/
//Keys
#define SECRET_KEY                                          @"mySecretKey"
//#define INITIAL_KEY                                         @"8SymDUkOCPk8AArM8+IA2OQfyZIZK4AwWHntbH0o0y8="
#define INITIAL_KEY                                         @"myInitialSecretKey"//@"Z8srBwWXr0D4UxWnLrJdQ4Phd/5ZUumZ/AGsG4W7eRc="
#define ENCRYPT_KEY                                         @"0123456789abcdef"
#define DEVICE_TOKEN                                        @"DeviceToken"

#define FIREBASE_KEY                                        @"AIzaSyDFlguyNvhONBB-3UyTWmdYV4do9vJ8eWc"//@"AIzaSyAW3ffcl588SYzrTR4ID_m9WM9UJVJ5fE4"

#define TRANSITION_TOP                                      @"toTop"
#define TRANSITION_BOTTOM                                   @"toBottom"
#define TRANSITION_LEFT                                     @"toLeft"
#define TRANSITION_RIGHT                                    @"toRight"

// LOCAL_CONFIG (This is used as a hack to manually override Firebase Remote Config, affects HomeVC and PaymentGatewayVC)
#define IS_DEBUG_FIREBASE_OVERRIDE                          @"0"
// 1 = Get PROD endpoint, PROD MOL
// 0 = Get DEV endpoint, DEV MOL


//CONFIG //1 = ON || 0 = OFF//
#define DISPLAY_INTRODUCTION                                @"1"
#define ENABLE_STARTUP_MUSIC                                @"1"
#define ENABLE_ANIMATION_NOTIFICATION                       @"1"

//File Name
#define OUTLET_FILE                                         @"outlet"

#define TOPUP_LIST                                          @"myTopUpList"

//iPay88 Account
#define MERCHANT_CODE                                       @"iPayMerchantCode"
#define MERCHANT_KEY                                        @"iPayMerchantKey"

//URL
#define WEBSERVICE_URL                                      @"myWebService"
//"http:/api.loudspeaker.apppay.my/"
#define SHARE_BASE                                          @"myShareBase"
//@"http:/oasis.apppay.my/"
#define PAYMENT_CALLBACK                                    @"ipayCallbackUrl"

#define USER_URL                                            @"user/"
#define USER_REWARD_URL                                     @"reward/"
#define USER_INBOX_URL                                      @"inbox/"
#define USER_CARD_VERIFY                                    USER_URL "card/verify"
#define USER_REGISTER                                       USER_URL "register"
#define USER_REGISTER_VIRTUAL                               USER_URL "register/virtualcard"
#define USER_REGISTER_FB                                    USER_REGISTER "/facebook"
#define USER_REGISTER_GOOGLE                                USER_REGISTER "/google"
#define USER_HOME                                           USER_URL "home"
#define USER_FORGOT_PASSWORD                                USER_URL "password/forgot"
#define USER_LOGIN                                          USER_URL "login"
#define USER_LOGIN_FB                                       USER_URL "login/facebook"
#define USER_LOGIN_GOOGLE                                   USER_URL "login/google"
#define USER_PROFILE                                        USER_URL "profile"
#define USER_INBOX                                          USER_URL "inbox"
#define USER_INBOX_MESSAGE                                  USER_URL "inbox/message/"
#define USER_GET_ALL                                        USER_URL "all"
#define REQUEST_GEO_URL                                     USER_URL "geofence/nearest"
#define SEND_GEO_URL                                        USER_URL "geofence/entry"
#define USER_LOGOUT                                         USER_URL "logout"
#define USER_ENQUIRY                                        USER_URL "enquiry"
#define USER_OUTLET                                         USER_URL "outlets"

#define USER_PRICE                                          USER_URL "pricing/virtualcard"
#define USER_RETRIEVE_ORDER                                 USER_URL "order/latest"
#define USER_GENERATE_ORDER                                 USER_URL "order/create"
#define USER_UPDATE_ORDER                                   USER_URL "order/update"

#define USER_FLASH_GRAB                                     USER_URL "flashdeal/grab"
#define USER_FLASH_SPEND                                    USER_URL "transaction/spend/flashdeal"
#define USER_BOOKING_SPEND                                  USER_URL "user/transaction/spend/booking"

#define USER_RENEW_MEMBERSHIP                               USER_URL "card/renew"
#define USER_BOOKING_LIST                                   USER_URL "bookings"
#define USER_BOOKING_TIMESLOT                               USER_URL "booking/timeslots"
#define USER_BOOKING_DETAIL                                 USER_URL "booking/view"
#define USER_BOOKING_AVAILABLE_SESSION                      USER_URL "booking/sessions"
#define USER_BOOKING_CREATE                                 USER_URL "booking/create"
#define USER_BOOKING_PRICE                                  USER_URL "booking/pricing"

#define ACTION_STATISTICS                                   USER_URL "stats/action"

#define OUTLET_SHARE                                        @"o/"

#define USER_VOUCHER_REDEEM                                 USER_URL USER_REWARD_URL "claim/"

#define USER_POSTING                                        USER_URL "posts"
#define USER_POSTING_PROMO                                  USER_URL "posts/promo"
#define USER_POSTING_MENU                                   USER_URL "posts/menu"
#define USER_POSTING_EVENT                                  USER_URL "posts/event"
#define USER_POSTING_DETAIL                                 USER_URL "post"
#define USER_PROFILE_PICTURE                                USER_PROFILE "/image"

#define USER_CAMPAIGNS                                      USER_URL "campaigns"

#define OASIS_URL                                           @"http://oasis.com.my"
#define OASIS_USER                                          @"/u"
#define OASIS_USE_VOUCHER                                   @"/r"

#define LOUDSPEAKER_URL                                           @"http://loudspeakerktv.my"
#define LOUDSPEAKER_USER                                          @"/u"
#define LOUDSPEAKER_USE_VOUCHER                                   @"/r"

//Response Code
#define NOT_FOUND                                           @"U04"
#define UNAUTHORIZED                                        @"401"

//Standards
#define IS_FIRST_INSTALL                                    @"isFirstInstall"
#define HAS_CONFIG                                          @"isConfigRetrieved"
#define PROCESS_FIRST_INSTALL                               @"isFirstInstallProcessed"
#define IS_SECOND_RUN                                       @"isSecondRun"
#define IS_LOGGED_IN                                        @"isUserLoggedIn"
#define IS_GUEST                                            @"isGuest"
#define IS_MEMBER                                           @"isMember"
#define UPGRADE_REGISTER_RATE                               @"guestUpgrade"
#define HOME_DETAIL                                         @"homeDetails"
#define USER_DETAIL                                         @"myUserDetail"
#define USER_ID                                             @"myUserID"
#define USER_POINT                                          @"myUserPoint"
#define USER_POINT_DETAIL                                   @"myUserPointDetail"
#define INBOX_COUNT                                         @"myInboxCount"
#define FB_TOKEN                                            @"myFBToken"
#define GOOGLE_TOKEN                                        @"myGoogleToken"
#define GEOFENCE_LIST                                       @"geofenceList"
#define IS_SOCIAL                                           @"loginFromSocial"
#define SOCIAL_PORTRAIT                                     @"portraitFromSocial"

#define DISPLAY_REWARD                                      @"displayReward"
#define REWARD_ID                                           @"rewardID"
#define FROM_QR_VOUCHER_DETAIL                              @"fromVoucherDetailQR"
#define DISPLAY_TRANSACTION                                 @"displayTransaction"
#define MY_VOUCHER                                          @"myVoucher"
#define DISPLAY_ADVERTISEMENT                               @"displayAdvertisement"
#define MOL_CREATE_ID                                       @"molCreateId"
#define POP_BACK                                            @"popToRoot"

#define PLATFORM                                            @"iOS"

#define GENDER                                              @"myGenderList"
#define RACE                                                @"myRaceList"
#define STATES                                              @"myStateList"
#define COUNTRIES                                           @"myCountriesList"
#define TOPICS                                              @"myEnquiryTopicList"
#define MEMBER_TYPES                                        @"memberTypes"
#define ALLOW_GUEST_BOOKING                                 @"allowGuestBooking"
#define IS_CARD_EXPIRED                                     @"isCardExpired"
#define COMPLETE_BOOKING                                    @"completeBooking"
#define USER_BALANCE                                        @"balance"

#define VOUCHER_LIMIT                                       @"6"
#define PROMOTION_LIMIT                                     @"6"

#define PROMOTION_LOAD                                      @"10"
#define VOUCHER_LOAD                                        @"5"
#define MENU_LOAD                                           @"7"

#define POST_TYPE_1                                         @"promo"
#define POST_TYPE_2                                         @"voucher"
#define POST_TYPE_3                                         @"menu"

#define MENU_PARENTS                                        @"menuParentsData"

#define STATISTICS                                          @"loudspeakerStatistics"

#define TERMS_CONDITION                                     @"myTermsConditions"
#define PRIVACY                                             @"myPrivacy"
#define PDPA                                                @"myPDPA"
#define FAQ                                                 @"myFAQ"
#define BOOKING_TERMS                                       @"myBookingTerms"

#define TRANSPARENT                                         @"clear"

//Database
#define DATABASE_NAME                                       @"LoudSpeakerDB.db"
#define DATABASE_PATH                                       @"databasePath"

//Fonts
#define FONT_REGULAR                                        @"CenturyGothic"
#define FONT_BOLD                                           @"CenturyGothic-Bold"

//Colors
#define THEME_COLOR                                         @"3396DA"
#define DEEP_SKY_BLUE_COLOR                                 @"00ADEE"
#define TOP_BAR_BG_COLOR                                    @"F1F4F9"
#define BOTTOM_BORDER_COLOR                                 @"D0D2D3"
#define BUTTONS_BG_COLOR                                    @"50c0e8"
#define FULL_BORDER_COLOR                                   @"3396DA"
#define LABEL_COLOR                                         @"474747"
#define BUTTON_COLOR                                        @"4C4D4F"
#define LOADING_BG_COLOR                                    @"FFFFFF"
#define NON_EDITABLE_COLOR                                  @"E7E7E7"

#define FACEBOOK_BUTTON_COLOR                               @"3A559F"
#define GOOGLE_BUTTON_COLOR                                 @"EFEFEF"

#define BLACK_COLOR                                         @"000000"
#define WHITE_COLOR                                         @"FFFFFF"
#define GREEN_COLOR                                         @"C3fE5E"
#define MEDIUM_GREEN_COLOR                                  @"2ECC71"
#define RED_COLOR                                           @"FF0000"
#define GREY_COLOR                                          @"AFAFAF"
#define LIGHT_GREY_COLOR                                    @"F7F7F7"
#define BLUE_COLOR                                          @"00ADEE"
#define HYPERLINK_COLOR                                     @"0000EE"
#define DARK_GREY_1_COLOR                                   @"58595B"
#define DARK_GREY_2_COLOR                                   @"808285"

//Resources
#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

//Profile Image
#define IMG_PROFILE_PATH                                    @"myProfilePath"
#define IMG_NAME                                            @"myProfileName"
#define IMG_SOCIAL_PROFILE_PATH                             @"mySocialProfilePath"
#define IMG_SOCIAL_NAME                                     @"mySocialProfileName"

//Email
#define EMAIL_SUBJECT                                       @"Enquiry from Loud Speaker"
#define EMAIL_RECEPIENT                                     @"info@loudspeaker.com"

//Company Info
#define COMPANY_TITLE                                       @"Loud Speaker"
#define COMPANY_DESCRIPTION                                 @"About Us? It is not really about us, isn’t it?\n\nYes! It’s all about you and you matter!\n\nLoud Speaker’s ideology was conceived by a group of karaoke fanatics! Like you, we have travelled and search for the “best” karaoke joints and developed ideas on how a karaoke should be like ideally.\n\nWith these as our impetus, Loud Speaker Karaoke is born on the 08 August 2009 with our first outlet at Taman Sutera Utama, Johor Bahru.\n\nLoud Speaker is a radiant and jubilant karaoke joint with super affordable rates, value-added services and friendly crew to welcome your arrival.\n\nThe name 大嘴叭 denotes warmth, bringing down formalities, and release stress, with all the purpose to make you, our most precious customer, sing happily!\n\nWe, at Loud Speaker, are passionate in what we do and we are gathering all the momentum we could gather. We grow with your criticism and praises!\n\nImportantly, you, our precious customer can make us, Loud Speaker possible and grow. So, it’s not about us. It’s all about you!"
#define COMPANY_WEBSITE                                     @"myCompanyWebsite"//@"http://web2.loudspeaker.my/"
#define COMPANY_CONTACT                                     @"603-78863546"
#define COMPANY_ADDRESS                                     @"A202, Block A, Kelana Square, Jalan SS7/26, Kelana Jaya, 47301 Selangor ,Malaysia"
#define COMPANY_LATITUDE                                    @"3.106015"
#define COMPANY_LONGITUDE                                   @"101.591859"
#define COMPANY_TWITTER                                     @"twitter:///user?screen_name="
#define COMPANY_FACEBOOK                                    @"fb://profile/157899964220139"
#define COMPANY_FACEBOOK_WEB                                @"https://www.facebook.com/loudspeakerktv/"
#define COMPANY_YOUTUBE                                     @"https://www.youtube.com/user/loudspeakerKTV"
#define COMPANY_INSTAGRAM                                   @"instagram://user?username=loudspeakerktv"
#define COMPANY_INSTAGRAM_WEB                               @"http://www.instagram.com/loudspeakerktv/"
#define COMPANY_ABOUT_SHARE                                 @"http://www.apppay.my"

//Accepted Characters
#define IC_CHARACTERS                                     @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"


