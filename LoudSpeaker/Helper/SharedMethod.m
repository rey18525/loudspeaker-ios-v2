//
//  SharedMethod.m
//  Oasis
//
//  Created by Wong Ryan on 11/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "SharedMethod.h"
#import "Constant.h"
#import "AFNetworking.h"
#import "NSData+Base64.h"
#import "FBEncryptorAES.h"
#import "JSON.h"
#import "SBJsonWriter.h"
#import "Flash.h"
#import "Category.h"
#import "Post.h"
#import "Price.h"
#import "Promotion.h"
#import "Transaction.h"
#import "Outlet.h"
#import "Voucher.h"
#import "Campaign.h"
#import "Reward.h"
#import "Menu.h"
#include <CommonCrypto/CommonDigest.h>

@implementation SharedMethod{
    UIImageView *imgIndicator;
    UILabel *lblText;
    UIView *hudView;
}

//General Methods//
#pragma  mark - Print All Fonts
-(void)printAllFonts{
    for (NSString* family in [UIFont familyNames]){
        NSLog(@"%@", family);
        for (NSString* name in [UIFont fontNamesForFamilyName: family]){
            NSLog(@"  %@", name);
        }
    }
}

#pragma mark - Check Connectivity
-(BOOL)checkNetworkConnectivity{
    /*1 = Has connectivity
     0 = No connectivity*/
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

#pragma mark - Set Color
-(UIColor*)colorWithHexString:(NSString*)hex andAlpha:(float)alpha{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:alpha];
}

#pragma mark - Set Padding to TextField
-(void)setPaddingToTextField:(UITextField*)textField{
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    textField.leftView = paddingView;
    textField.leftViewMode = UITextFieldViewModeAlways;
}

#pragma mark - Set Button Rounded Corner and Fill Color
-(void)setButtonStyle:(UIButton*)button cornerRadius:(float)radius andBackgroundColor:(NSString*)colorCode{
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[button layer] setCornerRadius:radius];
    [button setBackgroundColor:[[SharedMethod new] colorWithHexString:BUTTON_COLOR andAlpha:1.0f]];
}

#pragma mark - Set Custom Button Style
-(void)setCustomButtonStyle:(UIButton*)button image:(NSString*)image borders:(BOOL)bordersResult borderWidth:(float)width cornerRadius:(float)radius borderColor:(NSString*)borderColorCode textColor:(NSString*)textColor andBackgroundColor:(NSString*)colorCode{
    
    [button setTitleColor:[[SharedMethod new] colorWithHexString:textColor andAlpha:1.0f] forState:UIControlStateNormal];
    [[button layer] setCornerRadius:radius];
    
    if(bordersResult){
        [[button layer] setBorderWidth:width];
        [[button layer] setBorderColor:([[SharedMethod new] colorWithHexString:borderColorCode andAlpha:1.0f]).CGColor] ;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImageView *imgView = [[UIImageView alloc]init];
        [imgView setFrame:CGRectMake(0, 3, button.frame.size.height-8, button.frame.size.height-8)];
        [imgView setContentMode:UIViewContentModeScaleAspectFit];
        
        if([image isEqualToString:@"Facebook"]){
            [imgView setImage:[UIImage imageNamed:@"ic_fb"]];
            [button setTitle:NSLocalizedString(@"btn_Facebook", nil) forState:UIControlStateNormal];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        } else if([image isEqualToString:@"Google"]){
            [imgView setImage:[UIImage imageNamed:@"ic_google"]];
            [button setTitle:NSLocalizedString(@"btn_Google", nil) forState:UIControlStateNormal];
            [button setTitleColor:[[SharedMethod new] colorWithHexString:LABEL_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
        }
        
        [button addSubview:imgView];
        
        if(![colorCode isEqualToString:TRANSPARENT]){
            [button setBackgroundColor:[[SharedMethod new] colorWithHexString:colorCode andAlpha:1.0f]];
        } else{
            [button setBackgroundColor:[UIColor clearColor]];
        }
    });
}

#pragma mark - Set Rounded Corner
-(void)setCornerRadius:(UIView*)view andCornerRadius:(float)radius{
    dispatch_async(dispatch_get_main_queue(), ^{
        [view setClipsToBounds:YES];
        [[view layer] setCornerRadius:radius];
        [[view layer] setMasksToBounds:YES];
    });
}

#pragma mark - Set Border
-(void)setBorder:(UIView*)view borderColor:(NSString*)borderColorCode andBackgroundColor:(NSString*)colorCode{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[view layer] setBorderColor:[[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] CGColor]];
        [[view layer] setBorderWidth:1.0f];
    });
}

#pragma mark - Set Bottom Line
-(void)setBottomBorder:(UIView *)view andBackgroundColor:(NSString *)colorCode{
    dispatch_async(dispatch_get_main_queue(), ^{
        //        CGFloat width,originY;
        //        width = [UIScreen mainScreen].bounds.size.width;//*9;//* 0.76;
        //        originY = view.frame.size.height;
        //        CALayer *bottomBorder = [CALayer layer];
        //        [bottomBorder setFrame: CGRectMake(0, originY, width, 4)];
        //        [bottomBorder setBackgroundColor:[[self colorWithHexString:colorCode andAlpha:1.0f] CGColor]];
        //        [[view layer] addSublayer:bottomBorder];
        UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, view.frame.size.height - 1.0f, view.frame.size.width, 1)];
        [bottomBorder setBackgroundColor:[self colorWithHexString:colorCode andAlpha:1.0f]];
        [view addSubview:bottomBorder];
    });
}

#pragma mark - Setup Textfield
-(void)setupTextfield:(UITextField*)textfield fontType:(NSString*)fontType fontSize:(float)fontSize textColor:(NSString*)textColor textAlpha:(float)textAlpha addPadding:(BOOL)padding andAddBorder:(BOOL)border{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [textfield setFont:[UIFont fontWithName:fontType size:fontSize]];
        [textfield setTextColor:[[SharedMethod new] colorWithHexString:textColor andAlpha:textAlpha]];
        
        if(padding){
            [self setPaddingToTextField:textfield];
        }
        
        if(border){
            [self setBorder:textfield borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
        }
    });
}

#pragma mark - Convert Date Time Format
-(NSString*)convertDateTimeFormat:(NSString*)date andInputFormat:(NSString*)inputFormat andOutputFormat:(NSString*)outputFormat{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [formatter setDateFormat:inputFormat];
    NSDate *temp = [formatter dateFromString:date];
    
    [formatter setDateFormat:outputFormat];
    NSString *convert = [formatter stringFromDate:temp];
    return convert;
}

-(NSString*)convertToDefaultDateFormat:(NSString*)text{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *temp = [formatter dateFromString:text];
    
    [formatter setDateFormat:@"dd.M.yyyy"];
    NSString *convert = [formatter stringFromDate:temp];
    return convert;
}

-(NSString*)convertToSendDateFormat:(NSString*)text{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [formatter setDateFormat:@"dd.M.yyyy"];
    NSDate *temp = [formatter dateFromString:text];
    
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *convert = [formatter stringFromDate:temp];
    return convert;
}

-(NSString*)convertToTransactionDateFormat:(NSString*)text{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *temp = [formatter dateFromString:text];
    
    [formatter setDateFormat:@"MMM dd HH:mm"];
    NSString *convert = [formatter stringFromDate:temp];
    return convert;
}

-(NSString*)convertToTransactionDateFormat_V2:(NSString*)text{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *temp = [formatter dateFromString:text];
    
    [formatter setDateFormat:@"dd MMM yyyy"];
    NSString *convert = [formatter stringFromDate:temp];
    return convert;
}

-(NSString*)convertToTransactionTimeFormat_V2:(NSString*)text{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *temp = [formatter dateFromString:text];
    
    [formatter setDateFormat:@"hh:mm:ss"];
    NSString *convert = [formatter stringFromDate:temp];
    return convert;
}


-(NSString*)convertToTransactionDetailDateFormat:(NSString*)text{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *temp = [formatter dateFromString:text];
    
    [formatter setDateFormat:@"yyyy.MM.dd HH:mm:ss"];
    NSString *convert = [formatter stringFromDate:temp];
    return convert;
}

-(NSString*)convertToNotificationDateFormat:(NSString*)text{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *temp = [formatter dateFromString:text];
    
    [formatter setDateFormat:@"dd.M.yyyy"];
    NSString *convert = [formatter stringFromDate:temp];
    return convert;
}

-(NSString*)convertToNotificationDateFormat_V2:(NSString*)text{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *temp = [formatter dateFromString:text];
    
    [formatter setDateFormat:@"dd\nMMM ,yyyy"];
    NSString *convert = [formatter stringFromDate:temp];
    return convert;
}

#pragma mark - Convert to Black and Grey image
- (UIImage *)getBlackAndWhiteVersionOfImage:(UIImage *)anImage {
    UIImage *newImage;
    if (anImage){
        CGColorSpaceRef colorSapce = CGColorSpaceCreateDeviceGray();
        CGContextRef context = CGBitmapContextCreate(nil, anImage.size.width * anImage.scale, anImage.size.height * anImage.scale, 8, anImage.size.width * anImage.scale, colorSapce, (CGBitmapInfo)kCGImageAlphaNone);
        CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
        CGContextSetShouldAntialias(context, NO);
        CGContextDrawImage(context, CGRectMake(0, 0, anImage.size.width, anImage.size.height), [anImage CGImage]);
        
        CGImageRef bwImage = CGBitmapContextCreateImage(context);
        CGContextRelease(context);
        CGColorSpaceRelease(colorSapce);
        
        UIImage *resultImage = [UIImage imageWithCGImage:bwImage];
        CGImageRelease(bwImage);
        
        UIGraphicsBeginImageContextWithOptions(anImage.size, NO, anImage.scale);
        [resultImage drawInRect:CGRectMake(0.0, 0.0, anImage.size.width, anImage.size.height)];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return newImage;
}

#pragma mark - Rorate Image
-(UIImage*)rorateImagePortrait:(UIImage*)image{
    int kMaxResolution = 640;
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

#pragma mark - UIAlertController
-(UIAlertController*)setAndShowAlertController:(NSString*)title message:(NSString*)message cancelButton:(NSString*)buttonName{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *buttonAction = [UIAlertAction actionWithTitle:buttonName style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:buttonAction];
    
    return alertController;
}

#pragma mark - Prepare Statistics
-(void)addStatistics:(NSString*)action_cd itemID:(NSString*)itemID{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *english = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:english];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:MM:sss"];
    
    NSString *created_ts = [dateFormatter stringFromDate:[NSDate date]];
    
    NSString *userID = [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID];
    if(userID.length<=0){
        userID = @"0";
    }
    
    NSDictionary *temp = @{
                           @"action_cd"     : action_cd,
                           @"item_id"       : itemID,
                           @"user_id"       : userID,
                           @"cnt"           : @"1",
                           @"created_ts"    : created_ts
                           };
    
    NSMutableArray *temp2 = [[NSMutableArray alloc] init];
    [temp2 addObject:temp];
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    if([[NSUserDefaults standardUserDefaults] objectForKey:STATISTICS]){
        NSMutableArray *mutable = [[NSMutableArray alloc] init];
        mutable = [[NSUserDefaults standardUserDefaults] objectForKey:STATISTICS];
        tempArray = [mutable mutableCopy];
    }
    [tempArray addObjectsFromArray:temp2];
    [[NSUserDefaults standardUserDefaults] setObject:tempArray forKey:STATISTICS];
}

#pragma mark - Save to file
-(void)writeToFile:(NSString*)fileName andDictioanry:(NSDictionary*)dict{
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.txt",fileName]];
    [dict writeToFile:filePath atomically:YES];
}

#pragma mark - Read from file
-(NSDictionary*)readFromFile:(NSString*)fileName{
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.txt",fileName]];
    NSDictionary *dictFromFile = [NSDictionary dictionaryWithContentsOfFile:filePath];
    return dictFromFile;
}

#pragma mark - Check Device Token
-(NSString*)checkDeviceToken{
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN];
    if(deviceToken.length<=0){
        deviceToken=@"";
    }
    return deviceToken;
}

#pragma mark - Transition Animation for Navigation Controller
-(CATransition*)setupTransitionAnimation:(NSString*)animation{
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
    if([animation isEqualToString:@"toTop"]){
        transition.subtype = kCATransitionFromTop;
    } else if([animation isEqualToString:@"toBottom"]){
        transition.subtype = kCATransitionFromBottom;
    } else if([animation isEqualToString:@"toLeft"]){
        transition.subtype = kCATransitionFromLeft;
    } else if([animation isEqualToString:@"toRight"]){
        transition.subtype = kCATransitionFromRight;
    }
    return transition;
}

#pragma mark - Set Gradient
-(void)setGradient:(UIView*)imgView{
    CAGradientLayer *gradientMask = [CAGradientLayer layer];
    gradientMask.frame = imgView.bounds;
    gradientMask.colors = @[(id)[[SharedMethod new] colorWithHexString:BLACK_COLOR andAlpha:0.45f].CGColor,
                            (id)[UIColor clearColor].CGColor];
    [imgView.layer addSublayer:gradientMask];
}

-(void)setGradient2:(UIView*)imgView{
    CAGradientLayer *gradientMask = [CAGradientLayer layer];
    gradientMask.frame = imgView.bounds;
    gradientMask.colors = @[(id)[UIColor clearColor].CGColor,
                            (id)[UIColor blackColor].CGColor];
    [imgView.layer addSublayer:gradientMask];
}

#pragma mark - Loading Indicator
-(void)setLoadingIndicatorOnImage:(UIImageView *)imgView{
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.tag=100;
    [indicator setColor:[UIColor blackColor]];
    [indicator startAnimating];
    [indicator setCenter:CGPointMake((imgView.frame.size.width/2), (imgView.frame.size.height/2))];
    [indicator setHidesWhenStopped:YES];
    [imgView addSubview:indicator];
}

-(void)setupLoadingIndicator:(UIView*)view{
    UIView *bg = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [bg setTag:900];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [bg setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2]];
        [view.window setBackgroundColor:[UIColor clearColor]];
        [view bringSubviewToFront:bg];
    });
    
    hudView = [[UIView alloc] initWithFrame:CGRectMake((bg.frame.size.width/2)-50, (bg.frame.size.height/2)-50, 100, 100)];
    
    imgIndicator = [[UIImageView alloc]initWithFrame:CGRectMake(hudView.frame.size.width/2-30, 8, 60, 60)];
    
    [self setLoadingHUDView];
    [self setImageViewWithImages];
    [self setLoadingStatusWithText];
    
    [hudView addSubview:imgIndicator];
    [hudView addSubview:lblText];
    [bg addSubview:hudView];
    [view.window addSubview:bg];
}

-(void)setImageViewWithImages{
    dispatch_async(dispatch_get_main_queue(), ^{
        [imgIndicator setAnimationImages:[NSArray arrayWithObjects:
                                          [UIImage imageNamed:@"gif_1.png"],
                                          [UIImage imageNamed:@"gif_2.png"],
                                          [UIImage imageNamed:@"gif_3.png"],
                                          [UIImage imageNamed:@"gif_4.png"],
                                          [UIImage imageNamed:@"gif_5.png"],
                                          [UIImage imageNamed:@"gif_6.png"],
                                          [UIImage imageNamed:@"gif_7.png"],
                                          [UIImage imageNamed:@"gif_8.png"],
                                          [UIImage imageNamed:@"gif_9.png"],
                                          [UIImage imageNamed:@"gif_10.png"],
                                          [UIImage imageNamed:@"gif_11.png"],
                                          [UIImage imageNamed:@"gif_12.png"],
                                          [UIImage imageNamed:@"gif_13.png"],
                                          [UIImage imageNamed:@"gif_14.png"],
                                          [UIImage imageNamed:@"gif_15.png"],
                                          [UIImage imageNamed:@"gif_16.png"],
                                          [UIImage imageNamed:@"gif_17.png"],
                                          [UIImage imageNamed:@"gif_18.png"],
                                          [UIImage imageNamed:@"gif_19.png"],
                                          [UIImage imageNamed:@"gif_20.png"],
                                          [UIImage imageNamed:@"gif_21.png"],
                                          [UIImage imageNamed:@"gif_22.png"],
                                          [UIImage imageNamed:@"gif_23.png"],
                                          [UIImage imageNamed:@"gif_24.png"],
                                          [UIImage imageNamed:@"gif_25.png"],
                                          [UIImage imageNamed:@"gif_26.png"],
                                          nil]];
        
        [imgIndicator setAnimationDuration:3.0f];
        [imgIndicator setAnimationRepeatCount:0.0f];
        [imgIndicator startAnimating];
    });
}

-(void)setLoadingStatusWithText{
    lblText = [[UILabel alloc]initWithFrame:CGRectMake(8, 66, 84, 32)];
    [lblText setTextColor:[UIColor blackColor]];
    [lblText setFont:[UIFont systemFontOfSize:12]];
    [lblText setMinimumScaleFactor:0.5];
    [lblText setTextAlignment:NSTextAlignmentCenter];
    [lblText setText:NSLocalizedString(@"msg_Loading", nil)];
}

-(void)setLoadingHUDView{
    [hudView setBackgroundColor:[UIColor whiteColor]];
    [[hudView layer] setCornerRadius:10];
    [hudView setBackgroundColor:[[SharedMethod new] colorWithHexString:LOADING_BG_COLOR andAlpha:1.0f]];
}

-(void)removeLoadingIndicator:(UIView *)view{
    for(UIView *tempView in view.window.subviews){
        if(tempView.tag==900){
            [tempView removeFromSuperview];
        }
    }
}

#pragma mark - Process Flash
-(NSMutableArray*)processFlash:(NSArray*)flash andCurrentArray:(NSMutableArray*)flashArray{
    for(int count=0; count<[flash count]; count++){
        Flash *mFlash = [[Flash alloc] init];
        mFlash.ID = [NSString stringWithFormat:@"%d",[[[flash objectAtIndex:count] objectForKey:@"id"] intValue]];
        mFlash.type = [[flash objectAtIndex:count] objectForKey:@"type"];
        mFlash.name = [[flash objectAtIndex:count] objectForKey:@"name"];
        mFlash.start_date = [[flash objectAtIndex:count] objectForKey:@"start_date"];
        mFlash.expire_date = [[flash objectAtIndex:count] objectForKey:@"expire_date"];
        mFlash.publish_date = [[flash objectAtIndex:count] objectForKey:@"publish_date"];
        mFlash.status = [[flash objectAtIndex:count] objectForKey:@"status"];
        mFlash.trigger_frequency = [[flash objectAtIndex:count] objectForKey:@"trigger_frequency"];
        mFlash.payment_method = [[flash objectAtIndex:count] objectForKey:@"payment_method"];
        mFlash.applicable_to = [[flash objectAtIndex:count] objectForKey:@"applicable_to"];
        
        mFlash.uom = [[flash objectAtIndex:count] objectForKey:@"uom"];
        mFlash.voucher_type = [[flash objectAtIndex:count] objectForKey:@"voucher_type"];
        mFlash.voucher_limit = [[flash objectAtIndex:count] objectForKey:@"voucher_limit"];
        mFlash.total_grabbed = [[flash objectAtIndex:count] objectForKey:@"total_grabbed"];
        mFlash.post_id = [[flash objectAtIndex:count] objectForKey:@"post_id"];
        mFlash.outlet_id = [[flash objectAtIndex:count] objectForKey:@"outlet_id"];
        mFlash.is_public = [[flash objectAtIndex:count] objectForKey:@"is_public"];
        mFlash.voucher_expiry_type = [[flash objectAtIndex:count] objectForKey:@"voucher_expiry_type"];
        mFlash.voucher_expiry_value = [[flash objectAtIndex:count] objectForKey:@"voucher_expiry_value"];
        mFlash.created_at = [[flash objectAtIndex:count] objectForKey:@"created_at"];
        mFlash.updated_at = [[flash objectAtIndex:count] objectForKey:@"updated_at"];
        
        mFlash.deleted_at = [[flash objectAtIndex:count] objectForKey:@"deleted_at"];
        mFlash.expiry_millisecond = [[flash objectAtIndex:count] objectForKey:@"expiry_millisecond"];
        mFlash.expiry_date_millisecond = [[flash objectAtIndex:count] objectForKey:@"expiry_date_millisecond"];
        mFlash.terms = [[flash objectAtIndex:count] objectForKey:@"terms"];
        
        // Flash Price
        NSMutableArray *tempPrice = [[NSMutableArray alloc] init];
        [tempPrice addObject:[[flash objectAtIndex:count] objectForKey:@"price"]];
        NSMutableArray *price = [[NSMutableArray alloc] init];
        [price addObjectsFromArray: [self processFlashPrice:tempPrice andCurrentArray:price]];
        mFlash.price = [price objectAtIndex:0];
        
        // Flash Post
        NSMutableArray *tempPost = [[NSMutableArray alloc] init];
        [tempPost addObject:[[flash objectAtIndex:count] objectForKey:@"post"]];
        NSMutableArray *post = [[NSMutableArray alloc] init];
        [post addObjectsFromArray: [self processFlashPost:tempPost andCurrentArray:post]];
        mFlash.post = [post objectAtIndex:0];
        
        mFlash.hasOwn = [[flash objectAtIndex:count] objectForKey:@"hasOwn"];

        [flashArray addObject:mFlash];
    }
    
    return flashArray;
}

#pragma mark - Process Flash Price
-(NSMutableArray*)processFlashPrice:(NSArray*)price andCurrentArray:(NSMutableArray*)priceArray{
    for(int count=0; count<[price count]; count++){
        Price *mPrice = [[Price alloc] init];
        mPrice.e_guest_value = [[price objectAtIndex:count] objectForKey:@"e_guest_value"];
        mPrice.e_member_online = [[price objectAtIndex:count] objectForKey:@"e_member_online"];
        mPrice.e_member_wallet = [[price objectAtIndex:count] objectForKey:@"e_member_wallet"];
        mPrice.topup_value = [[price objectAtIndex:count] objectForKey:@"topup_value"];
        mPrice.ewallet_value = [[price objectAtIndex:count] objectForKey:@"ewallet_value"];
        
        [priceArray addObject:mPrice];
    }
    return priceArray;
}

#pragma mark - Process Flash Post
-(NSMutableArray*)processFlashPost:(NSArray*)posts andCurrentArray:(NSMutableArray*)postsArray{
    for(int count=0; count<[posts count]; count++){
        Post *mPost = [[Post alloc] init];
        mPost.ID = [NSString stringWithFormat:@"%d",[[[posts objectAtIndex:count] objectForKey:@"id"] intValue]];
        mPost.type = [[posts objectAtIndex:count] objectForKey:@"type"];
        mPost.category_id = [[posts objectAtIndex:count] objectForKey:@"category_id"];
        mPost.campaign_id = [[posts objectAtIndex:count] objectForKey:@"campaign_id"];
        mPost.title = [[posts objectAtIndex:count] objectForKey:@"title"];
        mPost.subtitle = [[posts objectAtIndex:count] objectForKey:@"subtitle"];
        mPost.image_id = [[posts objectAtIndex:count] objectForKey:@"image_id"];
        mPost.content = [[posts objectAtIndex:count] objectForKey:@"content"];
        mPost.url = [[posts objectAtIndex:count] objectForKey:@"url"];
        mPost.featured = [[posts objectAtIndex:count] objectForKey:@"featured"];
        
        mPost.featured_note = [[posts objectAtIndex:count] objectForKey:@"featured_note"];
        mPost.created_at = [[posts objectAtIndex:count] objectForKey:@"created_at"];
        mPost.updated_at = [[posts objectAtIndex:count] objectForKey:@"updated_at"];
        mPost.publish_at = [[posts objectAtIndex:count] objectForKey:@"publish_at"];
        mPost.priority = [[posts objectAtIndex:count] objectForKey:@"priority"];
        mPost.deleted_at = [[posts objectAtIndex:count] objectForKey:@"deleted_at"];
        mPost.status = [[posts objectAtIndex:count] objectForKey:@"status"];
        mPost.terms = [[posts objectAtIndex:count] objectForKey:@"terms"];
        mPost.image = [[posts objectAtIndex:count] objectForKey:@"image"];
        
        [postsArray addObject:mPost];
    }
    return postsArray;
}

#pragma mark - Process Category
-(NSMutableArray*)processCategory:(NSArray*)categories andCurrentArray:(NSMutableArray*)categoryArray{
    for(int count=0; count<[categories count]; count++){
        Category *category = [[Category alloc] init];
        category.ID = [NSString stringWithFormat:@"%d",[[[categories objectAtIndex:count] objectForKey:@"id"] intValue]];
        category.name = [[categories objectAtIndex:count] objectForKey:@"name"];
        category.content = [NSString stringWithFormat:@"%d",[[[categories objectAtIndex:count] objectForKey:@"description"] intValue]];
        
        if([[[categories objectAtIndex:count] objectForKey:@"image"] isKindOfClass:[NSDictionary class]]){
            category.image = [[[categories objectAtIndex:count] objectForKey:@"image"] objectForKey:@"src"];
        } else{
            category.image = @"";
        }
        
        category.created_at = [[categories objectAtIndex:count] objectForKey:@"title"];
        category.updated_at = [[categories objectAtIndex:count] objectForKey:@"subtitle"];
        category.deleted_at = [[categories objectAtIndex:count] objectForKey:@"content"];
        [categoryArray addObject:category];
    }
    return categoryArray;
}

#pragma mark - Process Posting
-(NSMutableArray*)processPosting:(NSArray*)posts andCurrentArray:(NSMutableArray*)promotionsArray{
    for(int count=0; count<[posts count]; count++){
        Promotion *posting = [[Promotion alloc] init];
        posting.ID = [NSString stringWithFormat:@"%d",[[[posts objectAtIndex:count] objectForKey:@"id"] intValue]];
        posting.type = [[posts objectAtIndex:count] objectForKey:@"type"];
        posting.category_id = [NSString stringWithFormat:@"%d",[[[posts objectAtIndex:count] objectForKey:@"category_id"] intValue]];
        posting.campaign_id = [NSString stringWithFormat:@"%d",[[[posts objectAtIndex:count] objectForKey:@"campaign_id"] intValue]];
        posting.title = [[posts objectAtIndex:count] objectForKey:@"title"];
        posting.subtitle = [[posts objectAtIndex:count] objectForKey:@"subtitle"];
        
        if([[[posts objectAtIndex:count] objectForKey:@"image"] isKindOfClass:[NSDictionary class]]){
            posting.image = [[[posts objectAtIndex:count] objectForKey:@"image"] objectForKey:@"src"];
        } else{
            posting.image = @"";
        }
        
        posting.content = [[posts objectAtIndex:count] objectForKey:@"content"];
        posting.url = [[posts objectAtIndex:count] objectForKey:@"url"];
        posting.featured = [[posts objectAtIndex:count] objectForKey:@"featured"];
        posting.featured_note = [[posts objectAtIndex:count] objectForKey:@"featured_note"];
        posting.created_at = [[posts objectAtIndex:count] objectForKey:@"created_at"];
        posting.updated_at = [[posts objectAtIndex:count] objectForKey:@"updated_at"];
        posting.publish_at = [[posts objectAtIndex:count] objectForKey:@"pulish_at"];
        posting.priority = [NSString stringWithFormat:@"%d",[[[posts objectAtIndex:count] objectForKey:@"priority"] intValue]];
        [promotionsArray addObject:posting];
    }
    return promotionsArray;
}

#pragma mark - Process Reward
-(NSMutableArray*)processReward:(NSArray*)rewards andCurrentArray:(NSMutableArray*)rewardsArray{
    for(int count=0; count<[rewards count]; count++){
        Reward *reward = [[Reward alloc] init];
        reward.ID = [NSString stringWithFormat:@"%d",[[[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"id"] intValue]];
        reward.hashKey = [[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"hash"];
        reward.userID = [NSString stringWithFormat:@"%d",[[[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"user_id"] intValue]];
        reward.type = [[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"type"];
        reward.serial = [[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"serial"];
        reward.value = [[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"type"];
        reward.campaign_id = [[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"campaign_id"];
        reward.from_transaction = [[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"from_transaction"];
        reward.created_at = [[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"created_at"];
        reward.updated_at = [[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"updated_at"];
        reward.expires_at = [[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"expires_at"];
        reward.deleted_at = [[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"deleted_at"];
        reward.redeemed_transaction = [[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"redeemed_transaction"];
        reward.redeem_at = [[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"redeemed_at"];
        reward.redeemed_outlet = [[[rewards objectAtIndex:count] objectAtIndex:0] objectForKey:@"redeemed_outlet"];
        
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        [temp addObject:[[rewards objectAtIndex:count] objectAtIndex:0]];
        
        NSMutableArray *campaigns = [[NSMutableArray alloc] init];
        [campaigns addObjectsFromArray: [self processCampaign:temp andCurrentArray:campaigns]];
        reward.campaign = [campaigns objectAtIndex:0];
        
        [rewardsArray addObject:reward];
    }
    return rewardsArray;
}

#pragma mark - Process Campaign
-(NSMutableArray*)processCampaign:(NSArray*)campaigns andCurrentArray:(NSMutableArray*)campaignsArray{
    for(int count=0; count<[campaigns count]; count++){
        Campaign *campaign = [[Campaign alloc] init];
        campaign.ID = [NSString stringWithFormat:@"%d",[[[campaigns objectAtIndex:count] objectForKey:@"id"] intValue]];
        campaign.type = [[campaigns objectAtIndex:count] objectForKey:@"type"];
        campaign.name = [[campaigns objectAtIndex:count] objectForKey:@"name"];
        campaign.start = [[campaigns objectAtIndex:count] objectForKey:@"start"];
        campaign.expire = [[campaigns objectAtIndex:count] objectForKey:@"expire"];
        campaign.trigger_frequence = [[campaigns objectAtIndex:count] objectForKey:@"trigger_frequence"];
        campaign.trigger_by = [[campaigns objectAtIndex:count] objectForKey:@"trigger_by"];
        campaign.trigger_value = [[campaigns objectAtIndex:count] objectForKey:@"trigger_value"];
        campaign.reward_type = [[campaigns objectAtIndex:count] objectForKey:@"reward_type"];
        campaign.trigger_limit = [[campaigns objectAtIndex:count] objectForKey:@"trigger_limit"];
        campaign.reward_value = [[campaigns objectAtIndex:count] objectForKey:@"reward_value"];
        campaign.reward_count = [[campaigns objectAtIndex:count] objectForKey:@"reward_count"];
        campaign.reward_limit = [[campaigns objectAtIndex:count] objectForKey:@"reward_limit"];
        campaign.sequence = [[campaigns objectAtIndex:count] objectForKey:@"sequence"];
        campaign.post_id = [[campaigns objectAtIndex:count] objectForKey:@"post_id"];
        campaign.outlet_id = [[campaigns objectAtIndex:count] objectForKey:@"outlet_id"];
        campaign.status = [[campaigns objectAtIndex:count] objectForKey:@"status"];
        
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        [temp addObject:[[campaigns objectAtIndex:count] objectForKey:@"post"]];
        
        NSMutableArray *voucher = [[NSMutableArray alloc] init];
        [voucher addObjectsFromArray: [self processVoucher:temp andCurrentArray:voucher]];
        campaign.voucher = [voucher objectAtIndex:0];
        
        [campaignsArray addObject:campaign];
    }
    return campaignsArray;
}

#pragma mark - Process Voucher
-(NSMutableArray*)processVoucher:(NSArray*)vouchers andCurrentArray:(NSMutableArray*)vouchersArray{
    for(int count=0; count<[vouchers count]; count++){
        Voucher *voucher = [[Voucher alloc] init];
        voucher.ID = [NSString stringWithFormat:@"%d",[[[vouchers objectAtIndex:count] objectForKey:@"id"] intValue]];
        voucher.type = [[vouchers objectAtIndex:count] objectForKey:@"type"];
        voucher.category_id = [NSString stringWithFormat:@"%d",[[[vouchers objectAtIndex:count] objectForKey:@"category_id"] intValue]];
        voucher.campaign_id = [NSString stringWithFormat:@"%d",[[[vouchers objectAtIndex:count] objectForKey:@"campaign_id"] intValue]];
        voucher.title = [[vouchers objectAtIndex:count] objectForKey:@"title"];
        voucher.subtitle = [[vouchers objectAtIndex:count] objectForKey:@"subtitle"];
        
        if([[[vouchers objectAtIndex:count] objectForKey:@"image"] isKindOfClass:[NSDictionary class]]){
            voucher.image = [[[vouchers objectAtIndex:count] objectForKey:@"image"] objectForKey:@"src"];
        } else{
            voucher.image = @"";
        }
        
        voucher.content = [[vouchers objectAtIndex:count] objectForKey:@"content"];
        voucher.terms = [[vouchers objectAtIndex:count] objectForKey:@"terms"];
        voucher.url = [[vouchers objectAtIndex:count] objectForKey:@"url"];
        voucher.featured = [[vouchers objectAtIndex:count] objectForKey:@"featured"];
        voucher.featured_note = [[vouchers objectAtIndex:count] objectForKey:@"featured_note"];
        voucher.created_at = [[vouchers objectAtIndex:count] objectForKey:@"created_at"];
        voucher.updated_at = [[vouchers objectAtIndex:count] objectForKey:@"updated_at"];
        voucher.publish_at = [[vouchers objectAtIndex:count] objectForKey:@"pulish_at"];
        voucher.priority = [NSString stringWithFormat:@"%d",[[[vouchers objectAtIndex:count] objectForKey:@"priority"] intValue]];
        [vouchersArray addObject:voucher];
    }
    return vouchersArray;
}

#pragma mark - Process Menu
-(NSMutableArray*)processMenu:(NSArray*)menus andCurrentArray:(NSMutableArray*)menuArray{
    for(int count=0; count<[menus count]; count++){
        Menu *menu = [[Menu alloc] init];
        menu.ID = [NSString stringWithFormat:@"%d",[[[menus objectAtIndex:count] objectForKey:@"id"] intValue]];
        menu.type = [[menus objectAtIndex:count] objectForKey:@"type"];
        menu.category_id = [NSString stringWithFormat:@"%d",[[[menus objectAtIndex:count] objectForKey:@"category_id"] intValue]];
        menu.campaign_id = [NSString stringWithFormat:@"%d",[[[menus objectAtIndex:count] objectForKey:@"campaign_id"] intValue]];
        menu.title = [[menus objectAtIndex:count] objectForKey:@"title"];
        menu.subtitle = [[menus objectAtIndex:count] objectForKey:@"subtitle"];
        
        if([[[menus objectAtIndex:count] objectForKey:@"image"] isKindOfClass:[NSDictionary class]]){
            menu.image = [[[menus objectAtIndex:count] objectForKey:@"image"] objectForKey:@"src"];
        } else{
            menu.image = @"";
        }
        
        menu.content = [[menus objectAtIndex:count] objectForKey:@"content"];
        menu.url = [[menus objectAtIndex:count] objectForKey:@"url"];
        menu.featured = [[menus objectAtIndex:count] objectForKey:@"featured"];
        menu.featured_note = [[menus objectAtIndex:count] objectForKey:@"featured_note"];
        menu.created_at = [[menus objectAtIndex:count] objectForKey:@"created_at"];
        menu.updated_at = [[menus objectAtIndex:count] objectForKey:@"updated_at"];
        menu.publish_at = [[menus objectAtIndex:count] objectForKey:@"pulish_at"];
        menu.priority = [NSString stringWithFormat:@"%d",[[[menus objectAtIndex:count] objectForKey:@"priority"] intValue]];
        [menuArray addObject:menu];
    }
    return menuArray;
}

#pragma mark - Process Outlet
-(NSMutableArray *)processOutlet:(NSArray *)outlets{
    NSMutableArray *outletsArray = [[NSMutableArray alloc] init];
    
    for(int count=0; count<[outlets count]; count++){
        Outlet *outlet = [[Outlet alloc] init];
        NSString *temp = [[outlets objectAtIndex:count] objectForKey:@"address"];
        outlet.address = [temp stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        outlet.address1 = [[outlets objectAtIndex:count] objectForKey:@"address1"];
        outlet.address2 = [[outlets objectAtIndex:count] objectForKey:@"address2"];
        outlet.address3 = [[outlets objectAtIndex:count] objectForKey:@"address3"];
        outlet.city = [[outlets objectAtIndex:count] objectForKey:@"city"];
        outlet.country = [[outlets objectAtIndex:count] objectForKey:@"country"];
        outlet.email = [[outlets objectAtIndex:count] objectForKey:@"email"];
        outlet.fax = [[outlets objectAtIndex:count] objectForKey:@"fax"];
        outlet.hours = [[outlets objectAtIndex:count] objectForKey:@"hours"];
        outlet.ID = [NSString stringWithFormat:@"%d",[[[outlets objectAtIndex:count] objectForKey:@"id"] intValue]];
        outlet.latitude = [[outlets objectAtIndex:count] objectForKey:@"latitude"];
        outlet.longitude = [[outlets objectAtIndex:count] objectForKey:@"longitude"];
        outlet.name = [[outlets objectAtIndex:count] objectForKey:@"name"];
        outlet.phone = [[outlets objectAtIndex:count] objectForKey:@"phone"];
        outlet.postcode = [[outlets objectAtIndex:count] objectForKey:@"postcode"];
        outlet.priority = [[outlets objectAtIndex:count] objectForKey:@"priority"];
        outlet.status = [[outlets objectAtIndex:count] objectForKey:@"status"];
        outlet.state = [[outlets objectAtIndex:count] objectForKey:@"state"];
        outlet.type = [[outlets objectAtIndex:count] objectForKey:@"type"];
        
        if([[[outlets objectAtIndex:count] objectForKey:@"image"] isKindOfClass:[NSDictionary class]]){
            outlet.imageString = [[[outlets objectAtIndex:count] objectForKey:@"image"] objectForKey:@"src"];
        } else{
            outlet.imageString = @"";
        }
        
        [outletsArray addObject:outlet];
    }
    return outletsArray;
}

#pragma mark - Process Transaction
-(NSMutableArray*)processTransactions:(NSArray*)transactions{
    NSMutableArray *transactionArray = [[NSMutableArray alloc] init];
    
    for(int count=0; count<[transactions count]; count++){
        Transaction *transaction = [[Transaction alloc] init];
        transaction.additional_text = [[transactions objectAtIndex:count] objectForKey:@"additional_text"];
        transaction.amount = [[transactions objectAtIndex:count] objectForKey:@"amount"];
        transaction.app_datetime = [[transactions objectAtIndex:count] objectForKey:@"app_datetime"];
        transaction.campaign_id = [[transactions objectAtIndex:count] objectForKey:@"campaign_id"];
        transaction.card_number = [[transactions objectAtIndex:count] objectForKey:@"card_number"];
        transaction.created_at = [[transactions objectAtIndex:count] objectForKey:@"created_at"];
        transaction.credit = [[transactions objectAtIndex:count] objectForKey:@"credit"];
        transaction.deleted_at = [[transactions objectAtIndex:count] objectForKey:@"deleted_at"];
        transaction.content = [[transactions objectAtIndex:count] objectForKey:@"description"];
        transaction.device_id = [[transactions objectAtIndex:count] objectForKey:@"device_id"];
        transaction.ID = [[transactions objectAtIndex:count] objectForKey:@"id"];
        transaction.item_id = [[transactions objectAtIndex:count] objectForKey:@"item_id"];
        transaction.latitude = [[transactions objectAtIndex:count] objectForKey:@"latitude"];
        transaction.longitude = [[transactions objectAtIndex:count] objectForKey:@"longitude"];
        transaction.message = [[transactions objectAtIndex:count] objectForKey:@"message"];
        transaction.nfc_tag_id = [[transactions objectAtIndex:count] objectForKey:@"nfc_tag_id"];
        transaction.outlet_id = [[transactions objectAtIndex:count] objectForKey:@"outlet_id"];
        transaction.reason = [[transactions objectAtIndex:count] objectForKey:@"reason"];
        transaction.reward_id = [[transactions objectAtIndex:count] objectForKey:@"reward_id"];
        transaction.staff_id = [[transactions objectAtIndex:count] objectForKey:@"staff_id"];
        transaction.status = [[transactions objectAtIndex:count] objectForKey:@"status"];
        transaction.terminal_id = [[transactions objectAtIndex:count] objectForKey:@"terminal_id"];
        transaction.terminal_sequence = [[transactions objectAtIndex:count] objectForKey:@"terminal_sequence"];
        transaction.type = [[transactions objectAtIndex:count] objectForKey:@"type"];
        transaction.type_id = [[transactions objectAtIndex:count] objectForKey:@"type_id"];
        transaction.updated_at = [[transactions objectAtIndex:count] objectForKey:@"updated_at"];
        transaction.user_id = [[transactions objectAtIndex:count] objectForKey:@"user_id"];
        [transactionArray addObject:transaction];
    }
    return transactionArray;
    
}

//--END OF GENERAL METHODS--//


//Validation Methods//
#pragma mark - Check Phone Validation
-(BOOL)validatePhone:(NSString *)checkString{
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return [checkString rangeOfCharacterFromSet:notDigits].location == NSNotFound ? YES : NO;
}

#pragma mark - Check Email Validation
-(BOOL)validateEmail:(NSString *)checkString{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
#ifdef DEBUG
    NSLog(@"Matching email result %d",[emailTest evaluateWithObject:checkString]);
#endif
    
    return [emailTest evaluateWithObject:checkString];
}
//--END OF VALIDATIONS METHODS--//


//Profile Methods//
#pragma mark - Save Profile Image
-(void)saveNewProfileImage:(UIImage*)image andIsSocial:(BOOL)isSocial{
    NSError *error;
    if (image!=nil){
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
        NSString *ptrsStr;// = IMG_PROFILE_PATH;//[NSString stringWithFormat:@"/Oasis/profile"];
        
        if(isSocial){
            ptrsStr = IMG_SOCIAL_PROFILE_PATH;
        } else{
            ptrsStr = IMG_PROFILE_PATH;
        }
        
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:ptrsStr];
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:nil error:&error]; //Create folder
        
        NSData *pngData = UIImagePNGRepresentation(image);
        NSString *filePath;
        
        if(isSocial){
            filePath = [dataPath stringByAppendingPathComponent:IMG_SOCIAL_NAME]; //Add the file name
        } else{
            filePath = [dataPath stringByAppendingPathComponent:IMG_NAME]; //Add the file name
        }
        [pngData writeToFile:filePath atomically:YES];
    } else{
#ifdef DEBUG
        NSLog(@"No image data was found.");
#endif
    }
}

#pragma mark - Remove Profile Image
-(void)removeProfileImage:(BOOL)isSocial{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *ptrsStr;// = IMG_PROFILE_PATH;//[NSString stringWithFormat:@"/Oasis/profile"];
    
    if(isSocial){
        ptrsStr = IMG_SOCIAL_PROFILE_PATH;
    } else{
        ptrsStr = IMG_PROFILE_PATH;
    }
    
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:ptrsStr];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:nil error:&error]; //Create folder
    } else{
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if([fileManager fileExistsAtPath:dataPath]){
            [fileManager removeItemAtPath:dataPath error:&error];
#ifdef DEBUG
            NSLog(@"Image removed!");
#endif
        }
    }
}

#pragma mark - Load Profile Image
-(UIImage *)loadProfileImage:(BOOL)isSocial{
    UIImage *image;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *ptrsStr;// = IMG_PROFILE_PATH;//[NSString stringWithFormat:@"/Oasis /profile"];
    
    if(isSocial){
        ptrsStr = IMG_SOCIAL_PROFILE_PATH;
    } else{
        ptrsStr = IMG_PROFILE_PATH;
    }
    
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:ptrsStr];
    NSString *filePath;// = [dataPath stringByAppendingPathComponent:IMG_NAME];
    
    if(isSocial){
        filePath = [dataPath stringByAppendingPathComponent:IMG_SOCIAL_NAME];
    } else{
        filePath = [dataPath stringByAppendingPathComponent:IMG_NAME];
    }
    
    image = [UIImage imageWithContentsOfFile:filePath];
    
    return image;
}
//--END OF PROFILE METHODS--//

//Convert UIImage to NSString (JSON)
#pragma mark - Convert UIImage to NSString (JSON)
-(NSData*)convertImageDataToJSON:(NSData*)imgData{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:imgData forKey:@"image:base64"];
    
    NSData *jsonData;
    
    if ([NSJSONSerialization isValidJSONObject:dict]){
        NSError *error = nil;
        jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
        
        if (error!=nil) {
            //            NSLog(@"Error creating JSON Data = %@",error);
        }
        else{
            NSLog(@"JSON Data created successfully.");
        }
    } else{
        NSLog(@"Not a valid object for JSON");
    }
    return jsonData;
}


//QR Code Methods//
#pragma mark - Generate QR Code
-(CIImage*)generateQRCode:(NSString*)string{
    NSData *QRData = [string dataUsingEncoding:NSISOLatin1StringEncoding];
    
    CIFilter *QRFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [QRFilter setValue:QRData forKey:@"inputMessage"];
    
    return [QRFilter outputImage];
}
//--END OF QR METHODS--//


//Convert Array to JSON//
#pragma mark - Convert Array to JSON
- (NSString*)convertArrayToString:(NSArray*)array{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    NSString *json;
    if (!jsonData){
        NSLog(@"Got an error: %@", error);
        json = @"";
    } else{
        json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return json;
}


//Convert JSON to Array//
#pragma mark - Convert JSON to Array
- (NSMutableArray*)convertJSONToArray:(NSString*)json{
    NSError *error;
    NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    temp = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    return temp;
}


//Dictionary Convert Nil to ""//
#pragma mark - Dictionary Convert Nil to ""
- (NSDictionary *) dictionaryByReplacingNullsWithNil:(NSDictionary*)sourceDictionary {
    NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:sourceDictionary];
    const id nul = [NSNull null];
    
    for(NSString *key in replaced) {
        const id object = [sourceDictionary objectForKey:key];
        if(object == nul) {
            [replaced setValue:nil forKey:key];
        }
    }
    return [NSDictionary dictionaryWithDictionary:replaced];
}

-(id) nestedDictionaryByReplacingNullsWithNil:(id)data{
    NSError* error;
    if (data == (id)[NSNull null]){
        return [[NSObject alloc] init];
    }
    id jsonObject;
    if ([data isKindOfClass:[NSData class]]){
        jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    } else {
        jsonObject = data;
    }
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSMutableArray *array = [jsonObject mutableCopy];
        for (int i = array.count-1; i >= 0; i--) {
            id a = array[i];
            if (a == (id)[NSNull null]){
                [array removeObjectAtIndex:i];
            } else {
                array[i] = [self nestedDictionaryByReplacingNullsWithNil:a];
            }
        }
        return array;
    } else if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *dictionary = [jsonObject mutableCopy];
        for(NSString *key in [dictionary allKeys]) {
            id d = dictionary[key];
            if (d == (id)[NSNull null]){
                dictionary[key] = @"";
            } else {
                dictionary[key] = [self nestedDictionaryByReplacingNullsWithNil:d];
            }
        }
        return dictionary;
    } else {
        return jsonObject;
    }
}

//END//


//Encryption Methods//
#pragma mark - SHA256
-(NSString*)sha256:(NSString *)clear{
    //    const char *s=[clear cStringUsingEncoding:NSUTF8StringEncoding];
    const char *s = [[clear precomposedStringWithCanonicalMapping] UTF8String];
    NSData *keyData=[NSData dataWithBytes:s length:strlen(s)];
    
    uint8_t digest[CC_SHA256_DIGEST_LENGTH]={0};
    CC_SHA256(keyData.bytes, (CC_LONG)keyData.length, digest);
    NSData *out=[NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];
    NSString *hash=[out description];
    hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
    return hash;
}
//--END OF ENCRYPTION METHODS--//


//AFNetworking Methods//
#pragma mark - Convert To Encrypted Data
-(NSMutableDictionary*)convertToEncryptedDictionary:(NSDictionary*)data{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    NSString *JSONString;
    if (!jsonData) {
        NSLog(@"JSON error: %@",error);
    } else {
        JSONString = [jsonData base64EncodedString];
    }
    
    NSString *strData = [FBEncryptorAES encryptBase64String:JSONString keyString:ENCRYPT_KEY IVString:SECRET_KEY separateLines:YES];
    
    strData=[strData stringByReplacingOccurrencesOfString:@"<" withString:@""];
    strData=[strData stringByReplacingOccurrencesOfString:@">" withString:@""];
    strData=[strData stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSMutableDictionary *encryptedDict = [[NSMutableDictionary alloc]init];
    [encryptedDict setObject:strData forKey:@"enc"];
    
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *currentVersion = [infoDict objectForKey:@"CFBundleVersion"];
    NSString *version = [NSString stringWithFormat:@"i%@",currentVersion];
    
    [encryptedDict setObject:version forKey:@"v"];
    return encryptedDict;
}


#pragma mark - Generate Token
+(NSString*) getTokenWithRoute:(NSString*) route apiKey:(NSString*)apiKey{
    NSString* expiry = [self timeStampExpiryHex];
    NSString* randNumber = [self randomNumberHex];
    NSString* random_token = [self sha1HashFor:[NSString stringWithFormat:@"%@%@",randNumber,apiKey]];
    NSString* route_secret = [self sha1HashFor:[NSString stringWithFormat:@"%@%@",route,apiKey]];
    NSString* hash = [self sha256HashFor:[NSString stringWithFormat:@"%@%@%@%@",route_secret,random_token,expiry,apiKey]];
    NSString* token = [NSString stringWithFormat:@"%@%@%@",random_token,hash,expiry];
    
#ifdef DEBUG
    NSLog(@"expiry - %@",expiry);
    NSLog(@"randomNumber - %@",randNumber);
    NSLog(@"randomToken - %@",random_token);
    NSLog(@"routeSecret - %@",route_secret);
    NSLog(@"hash - %@",hash);
#endif
    
    return token;
}

+(NSString*)sha256HashFor:(NSString*)input{
    const char* str = [input UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, (CC_LONG)strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
    {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

+(NSString*)sha1HashFor:(NSString*)input{
    const char* str = [input UTF8String];
    unsigned char result[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(str, (CC_LONG)strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_SHA1_DIGEST_LENGTH; i++)
    {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

+(NSString *) timeStampExpiryHex {
    NSString* timeStampString = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] + 3600*4];
#ifdef DEBUG
    NSLog(@"timeStampString-%d",[timeStampString intValue]);
#endif
    return [[NSString stringWithFormat:@"%lX",(long)[timeStampString integerValue]] lowercaseString];
}

+(NSString *) randomNumberHex{
    NSInteger randomNumber = random();
#ifdef DEBUG
    NSLog(@"Random-%ld",(long)randomNumber);
#endif
    return [[NSString stringWithFormat:@"%lx",randomNumber] lowercaseString];
}

#pragma mark - UIAlertView
-(UIAlertController*)setAndShowAlert:(NSString *)title message:(NSString *)message cancelButton:(NSString *)buttonName{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:NSLocalizedString(buttonName, nil) style:UIAlertActionStyleDefault handler:nil];
    
    [alertController addAction:alertAction];
    
    //    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:buttonName otherButtonTitles:nil];
    //    [alert show];
    return alertController;
}

#pragma mark - URL Request
-(NSMutableDictionary *)setupUrlRequest:(NSURL *)url timeOut:(CGFloat)timeout andEncryptParams:(NSMutableDictionary*)params{
    NSString *concatString = [self extractDictionaryKeysValues:params];
    NSError *error;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setTimeoutInterval:timeout];
    
    NSData *data = [concatString dataUsingEncoding:NSUTF8StringEncoding];
    
    [request setHTTPBody:data];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    
    NSString *requestReply = [[NSString alloc] initWithBytes:[responseData bytes]
                                                      length:[responseData length]
                                                    encoding:NSASCIIStringEncoding];
    
#ifdef DEBUG
    NSLog(@"Request Reply %@",requestReply);
#endif
    
    NSMutableDictionary *result = [requestReply JSONValue];
    
#ifdef DEBUG
    NSLog(@"Request Reply Result %@",[requestReply JSONValue]);
#endif
    
    return result;
}


#pragma mark - Extract Dictionary Keys & Values
-(NSString *)extractDictionaryKeysValues:(NSDictionary *)params{
    NSString *extractedString = @"";
    NSArray *keys = [params allKeys];
    NSInteger counter = 0;
    
    for(NSString *stringKey in keys){
        NSString *stringValue = [params objectForKey:stringKey];
        if(counter==0){
            extractedString = [extractedString stringByAppendingFormat:@"%@=%@",stringKey,stringValue];
        } else{
            extractedString = [extractedString stringByAppendingFormat:@"&%@=%@",stringKey,stringValue];
        }
        counter++;
    }
    extractedString = [extractedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
#ifdef DEBUG
    NSLog(@"extractedString - %@",extractedString);
#endif
    return extractedString;
}


#pragma mark - Setup AFNetworking
-(void)setupAFNetworkingAppSetting:(NSString *)url andParams:(NSMutableDictionary*)params andSuccess:(SuccessBlock)success{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [[manager requestSerializer] setTimeoutInterval:30.0f];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:@"google.com" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
#ifdef DEBUG
        NSLog(@"Retrieved App Setting Error - %@", error.localizedDescription);
#endif
    }];
}

-(void)setupAFNetworkingAppSetting:(NSString *)url  andSuccess:(SuccessBlock)success{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [[manager requestSerializer] setTimeoutInterval:10.0f];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *timeOut = @{
                                  @"errMsg" : NSLocalizedString(@"err_TimeOut", nil)
                                  };
        id newTimeOut = timeOut;
        success(newTimeOut);
        //        NSLog(@"error : %@",error);
    }];
}

-(void)setupAFNetworkingJSON:(NSString *)url andParams:(NSMutableDictionary *)params andSuccess:(SuccessBlock)success{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [[manager requestSerializer] setTimeoutInterval:10.0f];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSDictionary *timeOut = @{
                                  @"errMsg" : NSLocalizedString(@"err_TimeOut", nil)
                                  };
        id newTimeOut = timeOut;
        success(newTimeOut);
        //        NSLog(@"error : %@",error);
    }];
}

-(void)setupAFNetworkingPOST:(NSString *)url params:(NSMutableDictionary *)params andHeader:(NSMutableDictionary *)header andSuccess:(SuccessBlock)success{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [[manager requestSerializer] setTimeoutInterval:10.0f];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"application/json"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager.requestSerializer setValue:[header objectForKey:@"token"] forHTTPHeaderField:@"token"];
    
    if([header objectForKey:@"userId"]){
        [manager.requestSerializer setValue:[header objectForKey:@"userId"] forHTTPHeaderField:@"userId"];
    }
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
#ifdef DEBUG
        NSLog(@"Error - %@\nResponse - %@",error.localizedDescription,operation.responseString);
#endif
        
        if(operation.responseData.length>0){
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:operation.responseData
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:nil];
            //NSLog(@"ASDFX_code - %@", [response objectForKey:@"code"]);
            //NSLog(@"ASDFX_message - %@", [response objectForKey:@"message"]);
            //NSLog(@"ASDFX_errorcode - %@", error.code);
            
            NSString *errCode = [NSString stringWithFormat:@"%@", [response objectForKey:@"code"]];//[[response objectForKey:@"code"] stringValue];
            NSString *errMsg = [response objectForKey:@"message"];
            
            long nsErrorCode = 0;
            if(error.code != 0) {
                NSLog(@"error_code - %ld", (long)error.code);
                nsErrorCode = error.code;
            }
            
            if(nsErrorCode == -1009 || nsErrorCode == -1001 || nsErrorCode == 1011) {
                errCode = [NSString stringWithFormat:@"%ld", nsErrorCode];
                errMsg = NSLocalizedString(@"err_TimeOut", nil);
            
            } else if (nsErrorCode == 401) {
                errCode = [NSString stringWithFormat:@"%ld", nsErrorCode];
                errMsg = @"Unauthorized";//NSLocalizedString(@"err_Unauthorized", nil);
            }
            
            NSDictionary *err;
            err = @{@"errCode" : errCode,
                    @"errMsg"  : errMsg,
                    };

            id responseResult = err;
            success(responseResult);
        } else{
            NSDictionary *err = @{@"errCode" : @"-1009",
                                  @"errMsg" : NSLocalizedString(@"err_TimeOut", nil)
                                  };
            id responseResult = err;
            success(responseResult);
        }
    }];
}

-(void)setupAFNetworkingGET:(NSString *)url andParams:(NSMutableDictionary*)params andSuccess:(SuccessBlock)success{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [[manager requestSerializer] setTimeoutInterval:10.0f];
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"text/html"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"RESPONSE %@",responseObject);
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSInteger statusCode = [error code];
        NSLog(@"error : %@",error);
        if([error code] == NSURLErrorTimedOut){
            NSDictionary *result = @{
                                     @"msg" : NSLocalizedString(@"err_TimeOut", nil)
                                     };
            success(result);
        } else if(statusCode == -1009 || statusCode == -1004){
            NSDictionary *result = @{
                                     @"msg" : NSLocalizedString(@"err_NoInternet_Connection", nil)
                                     };
            success(result);
        }
    }];
}

-(void)setupAFNetworkingGET:(NSString *)url params:(NSMutableDictionary *)params andHeader:(NSMutableDictionary *)header andSuccess:(SuccessBlock)success{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [[manager requestSerializer] setTimeoutInterval:10.0f];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[header objectForKey:@"token"] forHTTPHeaderField:@"token"];
    
    if([header objectForKey:@"userId"]){
        [manager.requestSerializer setValue:[header objectForKey:@"userId"] forHTTPHeaderField:@"userId"];
    }
    
    if([header objectForKey:@"type"]){
        [manager.requestSerializer setValue:[header objectForKey:@"type"] forHTTPHeaderField:@"type"];
    }
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"application/json"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
#ifdef DEBUG
        NSLog(@"Error - %@\nResponse - %@\nCode - %d",error.localizedDescription,operation.responseString,(int)error.code);
#endif
        
        NSDictionary *response;
        if(operation.responseData){
            if(operation.responseData.length>0){
                response = [NSJSONSerialization JSONObjectWithData:operation.responseData
                                                           options:NSJSONReadingMutableContainers
                                                             error:nil];
            }
        }
        
        NSDictionary *err;
        
        if((int)error.code == -1009 || (int)error.code == -1001){
            err = @{@"errCode" : [NSString stringWithFormat:@"%d",(int)error.code],
                    @"errMsg"  : NSLocalizedString(@"err_TimeOut", nil)
                    };
        } else if((int)error.code == 401){
            err = @{@"errCode" : [NSString stringWithFormat:@"%d",(int)error.code],
                    @"errMsg"  : NSLocalizedString(@"err_Unauthorized", nil)
                    };
        } else{
            if([response isKindOfClass:[NSDictionary class]]){
                err = @{@"errCode" : [response objectForKey:@"code"],
                        @"errMsg"  : [response objectForKey:@"message"],
                        };
            } else{
                err = @{@"errCode" : @"-1009",
                        @"errMsg"  : NSLocalizedString(@"err_TimeOut", nil)
                        };
            }
        }
        
        id responseResult = err;
        success(responseResult);
    }];
}

-(void)setupAFNetworkingDELETE:(NSString *)url params:(NSMutableDictionary *)params andHeader:(NSMutableDictionary *)header andSuccess:(SuccessBlock)success{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [[manager requestSerializer] setTimeoutInterval:10.0f];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"application/json"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager.requestSerializer setValue:[header objectForKey:@"token"] forHTTPHeaderField:@"token"];
    
    if([header objectForKey:@"userId"]){
        [manager.requestSerializer setValue:[header objectForKey:@"userId"] forHTTPHeaderField:@"userId"];
    }
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [manager DELETE:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
#ifdef DEBUG
        NSLog(@"Error - %@\nResponse - %@",error.localizedDescription,operation.responseString);
#endif
        
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:operation.responseData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:nil];
        
        NSDictionary *err;
        
        err = @{@"errCode" : [response objectForKey:@"code"],
                @"errMsg"  : [response objectForKey:@"message"],
                };
        
        if((int)error.code == -1009 || (int)error.code == -1001){
            err = @{@"errCode" : [NSString stringWithFormat:@"%d",(int)error.code],
                    @"errMsg"  : NSLocalizedString(@"err_Time_Out", nil)
                    };
        } else if((int)error.code == 401){
            err = @{@"errCode" : [NSString stringWithFormat:@"%d",(int)error.code],
                    @"errMsg"  : NSLocalizedString(@"err_Unauthorized", nil)
                    };
        } else{
            err = @{@"errCode" : [response objectForKey:@"code"],
                    @"errMsg"  : [response objectForKey:@"message"],
                    };
        }
        
        id responseResult = err;
        success(responseResult);
    }];
}

-(void)setupAFNetworkingPUT:(NSString *)url params:(NSMutableDictionary *)params andHeader:(NSMutableDictionary *)header andSuccess:(SuccessBlock)success{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [[manager requestSerializer] setTimeoutInterval:10.0f];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"application/json"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager.requestSerializer setValue:[header objectForKey:@"token"] forHTTPHeaderField:@"token"];
    
    if([header objectForKey:@"userId"]){
        [manager.requestSerializer setValue:[header objectForKey:@"userId"] forHTTPHeaderField:@"userId"];
    }
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [manager PUT:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
#ifdef DEBUG
        NSLog(@"Error - %@\nResponse - %@",error.localizedDescription,operation.responseString);
#endif
        
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:operation.responseData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:nil];
        
        NSDictionary *err;
        
        err = @{@"errCode" : [response objectForKey:@"code"],
                @"errMsg"  : [response objectForKey:@"message"],
                };
        
        if((int)error.code == -1009 || (int)error.code == -1001){
            err = @{@"errCode" : [NSString stringWithFormat:@"%d",(int)error.code],
                    @"errMsg"  : NSLocalizedString(@"err_Time_Out", nil)
                    };
        } else if((int)error.code == 401){
            err = @{@"errCode" : [NSString stringWithFormat:@"%d",(int)error.code],
                    @"errMsg"  : NSLocalizedString(@"err_Unauthorized", nil)
                    };
        } else{
            err = @{@"errCode" : [response objectForKey:@"code"],
                    @"errMsg"  : [response objectForKey:@"message"],
                    };
        }
        
        id responseResult = err;
        success(responseResult);
    }];
}

-(void)setupAFNetworkingPATCH:(NSString *)url params:(NSMutableDictionary *)params andHeader:(NSMutableDictionary *)header andSuccess:(SuccessBlock)success{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [[manager requestSerializer] setTimeoutInterval:10.0f];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes=[NSSet setWithObject:@"application/json"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager.requestSerializer setValue:[header objectForKey:@"token"] forHTTPHeaderField:@"token"];
    
    if([header objectForKey:@"userId"]){
        [manager.requestSerializer setValue:[header objectForKey:@"userId"] forHTTPHeaderField:@"userId"];
    }
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [manager PATCH:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
#ifdef DEBUG
        NSLog(@"Error - %@\nResponse - %@",error.localizedDescription,operation.responseString);
#endif
        
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:operation.responseData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:nil];
        
        NSMutableDictionary *err = [[NSMutableDictionary alloc] init];
        
        [err setObject:[response objectForKey:@"code"] forKey:@"errCode"];
        [err setObject:[response objectForKey:@"message"] forKey:@"errMsg"];
        
        //        if([response objectForKey:@"errors"]){
        //            [err setObject:[response objectForKey:@"errors"] forKey:@"errErrors"];
        //        }
        NSLog(@"err -- %@",err);
        
        id responseResult = err;
        success(responseResult);
    }];
}

#pragma mark - Device sizes
-(ScreenType)currentDeviceType {
    CGFloat height = [UIScreen mainScreen].nativeBounds.size.height;
    
    switch ((int)height) {
        case 960:
            return iPhones35inch; // 320 x 480 pt
        case 1136:
            return iPhones40inch; // 320 x 568 pt
        case 1334:
            return iPhones47inch; // 375 x 667 pt
        case 1920:
        case 2208:
            return iPhones55inch; // 414 x 736 pt
        case 2436:
            return iPhone58inch; // 375 x 812 pt
        case 1792:
            return iPhone61inch; // 414 x 896 pt
        case 2688:
            return iPhone65inch; // 414 x 896 pt
        default:
            return unknown;
    }
}

-(BOOL)isSmallDevice {
    if ([[SharedMethod new] currentDeviceType] == iPhones35inch || [[SharedMethod new] currentDeviceType] == iPhones40inch) {
        return true;
    } else {
        return false;
    }
}

-(BOOL)isWideDevice {
    if ([[SharedMethod new] currentDeviceType] == iPhones55inch || [[SharedMethod new] currentDeviceType] == iPhone61inch || [[SharedMethod new] currentDeviceType] == iPhone65inch) {
        return true;
    } else {
        return false;
    }
}

-(BOOL)isWithNotch {
    if ([[SharedMethod new] currentDeviceType] == iPhone58inch || [[SharedMethod new] currentDeviceType] == iPhone61inch || [[SharedMethod new] currentDeviceType] == iPhone65inch) {
        return true;
    } else {
        return false;
    }
}

@end
