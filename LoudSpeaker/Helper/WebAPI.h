//
//  WebAPI.h
//  Oasis
//
//  Created by Wong Ryan on 13/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/Foundation.h>

typedef void(^SuccessBlocks)(id successBlock);
@interface WebAPI : NSObject

#pragma mark - Retrieve AppSettings
+(void)retrieveAppSettingsSuccess:(SuccessBlocks)successBlock;

#pragma mark - Retrieve App Settings With Params
+(void)retrieveAppSettingsWithParams:(NSDictionary *)inputParams andSuccess:(SuccessBlocks)successBlock;

#pragma mark - Geofence Setup
+(void)retrieveGeofence:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;
+(void)sendGeofenceData:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

#pragma mark - Users
//Sign Up
+(void)signUpNewUser:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;
+(void)loginUser:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Card Sign Up
+(void)cardSignUp:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Forgot Password
+(void)forgotPassword:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//FB
+(void)signUpFacebookNewUser:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;
+(void)loginFacebook:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Sign up Google New User
+(void)signUpGoogleNewUser:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;
+(void)loginGoogle:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Retrieve Profile
+(void)retrieveProfile:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Get All
+(void)retrieveAll:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Upload Profile Details
+(void)updateProfileDetail:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Retrieve Inbox
+(void)retrieveInbox:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//View Inbox Message
+(void)viewInboxMessage:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Delete Inbox Message
+(void)deleteInboxMessage:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Retrieve Campaign
+(void)retrieveCampaign:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Retrieve Campaign Detail
+(void)retrieveCampaignDetail:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Logout User
+(void)logoutUser:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;


//Upload Profile Picture Only
+(void)retrieveProfilePicture:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;
+(void)uploadProfilePicture:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;
+(void)deleteProfilePicture:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;


//Retrieve Outlets
+(void)retrieveOutlets:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Retrieve Home Detail
+(void)retrieveHomeDetail:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Retrieve Posting
+(void)retrievePosting:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Retrieve Posting Detail
+(void)retrievePostingDetail:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Retrieve Events
+(void)retrieveEvent:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Redeem Voucher
+(void)redeemVoucher:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;


//Send Enquiry//
+(void)sendEnquiry:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Retrieve Price
+(void)retrievePrice:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Flash Grab
+(void)retrieveFlashGrab:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Flash Spend
+(void)retrieveFlashSpend:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Booking E-wallet
+(void)bookingEwallet:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Retrieve Latest Order Detail
+(void)retrieveLatestOrderDetail:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Generate Order Id
+(void)generateOrderId:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Update Order Id
+(void)updateOrderStatus:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Virtual Card Registration
+(void)signUpVirtualCardUser:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Renew Membership
+(void)renewMembership:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Send Statistics//
+(void)sendStatistics:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

//Booking
+(void)retrieveBookingList:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;
+(void)retrieveBookingDetail:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;
+(void)retrieveTimeSlots:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;
+(void)retrieveAvailableSessions:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;
+(void)createBooking:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;
+(void)retrieveBookingPrice:(NSDictionary*)inputParams andSuccess:(SuccessBlocks)successBlock;

#pragma mark - Stop AFNetworking Request
+(void)stopAFNetworkingRequest;

#pragma mark - Stop NSURL Request
+(void)stopNSURLRequest;

@end
