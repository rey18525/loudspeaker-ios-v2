//
//  SQLManager.h
//  Oasis
//
//  Created by Wong Ryan on 13/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQLManager : NSObject

#pragma mark - Check Database
-(BOOL)checkDatabase;

#pragma mark - Check New Column in Table
-(void)checkTermsExist;

//Vouchers SQL//
#pragma mark - Insert/Update Promotion
-(void)insertAndUpdateVouchers:(NSMutableArray*)array;

#pragma mark - Delete Promotions
-(void)deleteVoucher:(NSString*)ID;
-(void)deleteVouchers:(NSMutableArray*)array;

#pragma mark - Retrieve Promotions
-(NSMutableArray*)retrieveVouchers;

#pragma mark - Search Promotions
-(NSMutableArray*)searchVouchers:(NSString*)searched;
//End of Vouchers SQL//

-(BOOL)isPromotionExists:(NSString*)ID;

//Promotions SQL//
#pragma mark - Insert/Update Promotion
-(void)insertAndUpdatePromotions:(NSMutableArray*)array;

#pragma mark - Delete Promotions
-(void)deletePromotion:(NSString*)ID;
-(void)deletePromotions:(NSMutableArray*)array;

#pragma mark - Retrieve Promotions
-(NSMutableArray*)retrievePromotions:(BOOL)isPromo;

#pragma mark - Search Promotions
-(NSMutableArray*)searchPromotions:(NSString*)searched;
//End of Promotion SQL//


//Menu SQL//
#pragma mark - Insert/Update Menu
-(void)insertAndUpdateMenu:(NSMutableArray*)array;

#pragma mark - Delete Menu
-(void)deleteMenu:(NSString*)ID;
-(void)deleteMenus:(NSMutableArray*)array;

#pragma mark - Retrieve Menu
-(NSMutableArray*)retrieveMenu:(NSString*)ID;

#pragma mark - Search Menu
-(NSMutableArray*)searchMenu:(NSString*)searched;
//End of Menu SQL//


//Outlets SQL//
#pragma mark - Insert/Update Outlet
-(void)insertAndUpdateOutlets:(NSMutableArray*)array;

#pragma mark - Check & Remove Records Not Exist In Latest Response
-(void)removeNotLatest:(NSMutableArray*)array;

#pragma mark - Delete Outlet
-(void)deleteOutlet:(NSMutableArray*)array;

#pragma mark - Retrieve Outlets
-(NSMutableArray*)retrieveOutlets;
//End of Outlets SQL//

@end
