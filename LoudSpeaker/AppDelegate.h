//
//  AppDelegate.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 25/11/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;
@end

