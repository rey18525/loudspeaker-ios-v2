//
//  BookingVC.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 23/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingVC.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "BookingCell.h"
#import "SVProgressHUD.h"
#import "WebAPI.h"
#import "BookingConfirmationVC.h"
#import "Outlet.h"
#import "BookingTimeModel.h"
#import "BookingModel.h"
#import "BookingSession.h"
#import "BookingParticipantsModel.h"
#import <PDKeychainBindingsController/PDKeychainBindings.h>

@interface BookingVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@end

@implementation BookingVC{
    BOOL bookingAvailabale;
    int selectedPax;
    UIStoryboard *storyboard;
    Outlet *selectedOutlet;
    BookingTimeModel *selectedTimeModel;
//    BookingSession *selectedSession;
    UIPickerView *timePickerView;
    UITextView *tvRemark;
    NSString *selectedDate, *selectedBigMouth, *selectedRegular, *selectedGuest, *selectedTime, *selectedSession, *priceEWallet, *priceMOL;
    UIDatePicker *datePicker, *timePicker;
    UIAlertController *alertController;
    UIAlertAction *cancelAction;
    NSDictionary *memberType;
    NSArray *iconArray;
    NSMutableArray *memberQuantityArray,*titleArray,*countArray,*outletArray,*timeArray, *timeKeyArray, *participantsArray, *sessionArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInitialization];
    [self setupData];
    [self setupDelegate];
    [self retrieveOutlets];
    [self.tableView reloadData];
}

-(void)setupInitialization{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.pickerView setHidden:YES];
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    titleArray = [[NSMutableArray alloc] init];
    countArray = [[NSMutableArray alloc] init];
    outletArray = [[NSMutableArray alloc] init];
    timeArray = [[NSMutableArray alloc] init];
    participantsArray = [[NSMutableArray alloc] init];
    sessionArray = [[NSMutableArray alloc] init];
    memberQuantityArray = [[NSMutableArray alloc] init];
}

-(void)setupData{
    [self.navigationItem setTitle:NSLocalizedString(@"nav_Booking", nil)];
    bookingAvailabale = NO;
    selectedPax = 0;
    memberType = [[NSUserDefaults standardUserDefaults] objectForKey:MEMBER_TYPES];
    
    iconArray = @[[UIImage imageNamed:@"ic_booking_outlet"],[UIImage imageNamed:@"ic_booking_big_member"],[UIImage imageNamed:@"ic_booking_regular_member"],[UIImage imageNamed:@"ic_booking_non_member"],[UIImage imageNamed:@"ic_booking_date"],[UIImage imageNamed:@"ic_booking_time"],[UIImage imageNamed:@"ic_booking_remark"]];
    
    NSArray *tempTitle = @[NSLocalizedString(@"phd_Booking_Outlet", nil),NSLocalizedString(@"phd_Booking_Big_Member", nil),NSLocalizedString(@"phd_Booking_Regular_Member", nil),NSLocalizedString(@"phd_Booking_Non_Member", nil),NSLocalizedString(@"phd_Booking_Date", nil),NSLocalizedString(@"phd_Booking_Time", nil),NSLocalizedString(@"phd_Booking_Remark", nil)];
    [titleArray addObjectsFromArray:tempTitle];
    
    for(int count=0; count<=40; count++){
        [memberQuantityArray addObject:[NSString stringWithFormat:@"%d",count]];
    }

    selectedBigMouth = @"0";
    selectedRegular = @"0";
    selectedGuest = @"0";
    
    NSArray *tempCount = @[@"",@"",@"",@"",@"",@"",@""];
    [countArray addObjectsFromArray:tempCount];
}

-(void)setupPickerView:(int)tag{
    if(tag==1){
        [self.pickerView selectRow:[selectedBigMouth integerValue] inComponent:0 animated:YES];
    } else if(tag==2){
        [self.pickerView selectRow:[selectedRegular integerValue] inComponent:0 animated:YES];
    } else{
        [self.pickerView selectRow:[selectedGuest integerValue] inComponent:0 animated:YES];
    }
    [self.pickerView setTag:tag];
    [self.pickerView setShowsSelectionIndicator:YES];
    [self.pickerView setHidden:NO];
    [self.pickerView reloadAllComponents];
}

-(void)setupDelegate{
    [self.pickerView setDelegate:self];
    [self.pickerView setDataSource:self];
    
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)setupDateKeyboard:(UITextField*)txt{
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setBarStyle:UIBarStyleBlackOpaque];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"btn_Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(bbtnDoneDidPressed:)];
    [btnDone setTintColor:[UIColor whiteColor]];
    [btnDone setTag:1];
    UIBarButtonItem *flexibleSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolbar.items = @[flexibleSeparator, btnDone];
    txt.inputAccessoryView = toolbar;
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [datePicker setBackgroundColor:[UIColor whiteColor]];
//    datePicker.maximumDate = [NSDate date];
//
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [datePicker setCalendar:gregorian];
    
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:+1];
//    [comps setYear:-100];
    NSDate *minDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    [comps setDay:0];
    [comps setMonth:+1];
//    [comps setYear:-18];
    NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];

//    datePicker.minimumDate = [NSDate date];
    datePicker.minimumDate = minDate;
    datePicker.maximumDate = maxDate;
    
//    [datePicker addTarget:self action:@selector(datePickerDidSelect:) forControlEvents:UIControlEventValueChanged];
    txt.inputView = datePicker;
}

-(void)setupTimeKeyboard:(UITextField*)txt{
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setBarStyle:UIBarStyleBlackOpaque];

    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"btn_Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(bbtnDoneDidPressed:)];
    [btnDone setTintColor:[UIColor whiteColor]];
    [btnDone setTag:2];
    UIBarButtonItem *flexibleSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolbar.items = @[flexibleSeparator, btnDone];
    txt.inputAccessoryView = toolbar;
    
    timePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    timePicker.datePickerMode = UIDatePickerModeTime;
    [timePicker setLocale:[NSLocale localeWithLocaleIdentifier:@"en_GB"]];
    [timePicker setBackgroundColor:[UIColor whiteColor]];
    [timePicker setMinuteInterval:30];
    
//    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDate *currentDate = [timePicker date];
//    NSDateComponents *comps = [[NSDateComponents alloc] init];
////    [comps setHour:3];
//    NSDate *minDate = [gregorian dateByAddingComponents:comps toDate:currentDate options:0];
//    [timePicker setMinimumDate:minDate];
    
    if(selectedTime.length>0){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_GB"]];
        dateFormatter.dateFormat = @"HH:mm";
        NSDate *date = [dateFormatter dateFromString:selectedTime];
        [timePicker setDate:date];
    }
    
//    [timePicker addTarget:self action:@selector(timePickerDidSelect:) forControlEvents:UIControlEventValueChanged];
    txt.inputView = timePicker;
}

-(BOOL)validateMemberQuantity{
    int numberOfPax = [selectedBigMouth intValue] + [selectedRegular intValue] + [selectedGuest intValue];
    if(numberOfPax > 0){
        return YES;
    } else{
        return NO;
    }
}

-(BOOL)validateSelectedOption{
    selectedPax = [selectedBigMouth intValue] + [selectedRegular intValue] + [selectedGuest intValue];
//    if(selectedOutlet && selectedPax>0 && selectedDate.length>0 && selectedTime.length>0 && selectedSession.session_id){
    if(selectedOutlet && selectedPax>0 && selectedDate.length>0 && selectedTime.length>0){
        return YES;
    }
    return NO;
}

-(NSMutableDictionary*)constructBookingModelDictionary{
    NSMutableDictionary *bookingDict = [[NSMutableDictionary alloc] init];
    [bookingDict setObject:selectedOutlet.ID forKey:@"outlet_id"];
    [bookingDict setObject:selectedDate forKey:@"booking_date"];
    [bookingDict setObject:selectedTime forKey:@"time_start"];
    [bookingDict setObject:@"" forKey:@"time_end"];
    [bookingDict setObject:[self constructParticipants:NO] forKey:@"participants"];
    
    return bookingDict;
}

-(NSMutableArray*)constructParticipants:(BOOL)isEWallet{
    selectedPax = [selectedBigMouth intValue] + [selectedRegular intValue] + [selectedGuest intValue];
    NSMutableArray *participants = [[NSMutableArray alloc] init];
    if([selectedBigMouth intValue] > 0){
        for(int count=0; count< [selectedBigMouth intValue]; count++){
            if(isEWallet){
                [self addParticipants:@"e_wallet" andArray:participants];
            }else{
                [self addParticipants:@"big_mouth" andArray:participants];
            }
        }
    }
    
    if([selectedRegular intValue] > 0){
        for(int count=0; count< [selectedRegular intValue]; count++){
            [self addParticipants:@"regular" andArray:participants];
        }
    }
    
    if([selectedGuest intValue] > 0){
        for(int count=0; count< [selectedGuest intValue]; count++){
            [self addParticipants:@"guest" andArray:participants];
        }
    }
    return participants;
}

-(void)addParticipants:(NSString*)type andArray:(NSMutableArray*)participants{
    NSDictionary *temp = @{
                           @"user_id"     : @"",
                           @"member_type" : type,
                           @"fullname"    : @"",
                           @"contact"     : @""
                           };
    [participants addObject:temp];
}

-(NSMutableDictionary*)constructSimplifiedParticipants{
    NSMutableDictionary *participants = [[NSMutableDictionary alloc] init];
    [participants setObject:selectedBigMouth forKey:@"big_mouth"];
    [participants setObject:selectedRegular forKey:@"regular"];
    [participants setObject:selectedGuest forKey:@"guest"];
    return participants;
}

-(void)retrieveOutlets{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSDictionary *params = @{@"userId"           : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"token"            : [SharedMethod getTokenWithRoute:@"user/outlets" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
#ifdef DEBUG
        NSLog(@"Request Outlets - %@",params);
#endif
        
        [WebAPI retrieveOutlets:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.tabBarController setSelectedIndex:0];
                }
            } else{
                NSString *code = [result objectForKey:@"code"];
                if([code isEqualToString:@"000"]){
                    if([[result objectForKey:@"outlets"] count]>0){
                        result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                        [[SharedMethod new] writeToFile:OUTLET_FILE andDictioanry:result];
                        [self retrieveLocalOutlets];
                    }
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"Message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    [self retrieveLocalOutlets];
                }
            }
        }];
    } else{
        [SVProgressHUD dismiss];
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        [self retrieveLocalOutlets]; 
    }
}

-(void)retrieveLocalOutlets{
    NSDictionary *tempDict = [[SharedMethod new] readFromFile:OUTLET_FILE];
    outletArray = [[SharedMethod new] processOutlet:[tempDict objectForKey:@"outlets"]];
}

-(void)updateData{
    selectedPax = [selectedBigMouth intValue] + [selectedRegular intValue] + [selectedGuest intValue];
//    if(selectedOutlet.ID && selectedPax>0 && selectedDate.length>0 && selectedTime.length>0){
    if(selectedOutlet.ID && selectedPax>0 && selectedDate.length>0){
        bookingAvailabale = NO;
        [self retrieveTimeSlots];
        
    }
}

-(void)retrieveTimeSlots{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
//        selectedPax = [selectedBigMouth intValue] + [selectedRegular intValue] + [selectedGuest intValue];
//        participantsArray = [[NSMutableArray alloc] init];
//        participantsArray = [self constructSimplifiedParticipants];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:selectedDate forKey:@"booking_date"];
        [params setObject:selectedOutlet.ID forKey:@"outlet_id"];
//        [params setObject:[NSString stringWithFormat:@"%d",selectedPax] forKey:@"pax"];
        [params setObject:[self constructSimplifiedParticipants] forKey:@"participants"];
//        [params setObject:selectedTime forKey:@"time_start"];
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        }
        [params setObject:[SharedMethod getTokenWithRoute:@"user/booking/timeslots" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        
#ifdef DEBUG
        NSLog(@"Retrieve Time Slots Params - %@",params);
#endif
        NSLog(@"Booking available - %d",bookingAvailabale);
        [WebAPI retrieveTimeSlots:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                timeArray = [[NSMutableArray alloc] init];
                sessionArray = [[NSMutableArray alloc] init];
                selectedTime = @"";
                selectedSession = @"";
                
                if([[result objectForKey:@"sessions"] count]>0){
                    for(NSDictionary *session in [result objectForKey:@"sessions"]){
                        BookingSession *bookingSession = [[BookingSession alloc] initWithAttributes:session];
                        [sessionArray addObject:bookingSession];
                    }
                    bookingAvailabale = YES;
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
            [self.tableView reloadData];
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)retrieveBookingPrice:(BOOL)isEwallet{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        selectedPax = [selectedBigMouth intValue] + [selectedRegular intValue] + [selectedGuest intValue];
        participantsArray = [[NSMutableArray alloc] init];
        participantsArray = [self constructParticipants:isEwallet];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:selectedDate forKey:@"booking_date"];
        [params setObject:selectedOutlet.ID forKey:@"outlet_id"];
//        [params setObject:selectedSession.session_id forKey:@"booking_session_id"];
        [params setObject:selectedSession forKey:@"booking_session_id"];
        [params setObject:selectedTime forKey:@"time_start"];
        [params setObject:@"" forKey:@"time_end"];
        [params setObject:participantsArray forKey:@"participants"];
//        [params setObject:tvRemark.text forKey:@"remark"];
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        }
        [params setObject:[SharedMethod getTokenWithRoute:@"user/booking/pricing" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        
#ifdef DEBUG
        NSLog(@"Retrieve Booking Pricing Params - %@",params);
#endif
        
        [WebAPI retrieveBookingPrice:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    BookingModel *bookingModel = [[BookingModel alloc] initWithAttributes:[self constructBookingModelDictionary]];
                    
                    NSMutableArray *participantsArray = [[NSMutableArray alloc] init];
                    for(NSDictionary *temp in [result objectForKey:@"participants"]){
                        BookingParticipantsModel *participantsModel = [[BookingParticipantsModel alloc] initWithAttributes:temp];
                        [participantsArray addObject:participantsModel];
                    }                    
                    bookingModel.participants = participantsArray;
                    bookingModel.total = [result objectForKey:@"total"];
                    if(isEwallet){
                        priceEWallet = [result objectForKey:@"total"];
                    }else{
                        priceMOL = [result objectForKey:@"total"];
                    }
                    
                    bookingModel.remark = tvRemark.text;
                    
                    if(priceEWallet != nil && priceMOL != nil){
                        BookingConfirmationVC *bookingConfirmationVC = [storyboard instantiateViewControllerWithIdentifier:@"bookingConfirmationVC"];
                        bookingConfirmationVC.selectedBookingModel = bookingModel;
                        bookingConfirmationVC.selectedOutlet = selectedOutlet;
                        bookingConfirmationVC.selectedSession = selectedSession;
                        bookingConfirmationVC.priceEWallet = priceEWallet;
                        bookingConfirmationVC.priceMOL = priceMOL;
                        //bookingConfirmationVC.selectedBookingSession = selectedSession;
                        
                        [self.navigationController pushViewController:bookingConfirmationVC animated:YES];
                    }

                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

#pragma mark - UIPickerView Delegate
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [memberQuantityArray count];
}

#pragma mark - UIPickerView Data Source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 30.0f;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return 100.0f;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [memberQuantityArray objectAtIndex:row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView.tag==1){
        selectedBigMouth = [memberQuantityArray objectAtIndex:row];
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [self updateData];
    } else if(pickerView.tag==2){
        selectedRegular = [memberQuantityArray objectAtIndex:row];
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [self updateData];
    } else{
        selectedGuest = [memberQuantityArray objectAtIndex:row];
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [self updateData];
    }
    [pickerView setHidden:YES];
}


#pragma mark - UITextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField.tag==4 && textField.text.length<=0){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
        NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",datePicker.date]];
        
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSString *myDate = [dateFormatter stringFromDate:date];
        selectedDate = myDate;
        [titleArray replaceObjectAtIndex:4 withObject:myDate];
        
        BookingCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
        [cell.lblTitle setText:[[SharedMethod new] convertDateTimeFormat:myDate andInputFormat:@"yyyy-MM-dd" andOutputFormat:@"dd MMM yyyy"]];
        [self updateData];
    } else{
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
        NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",timePicker.date]];
        
        dateFormatter.dateFormat = @"HH:mm";
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
        
        NSInteger hour = [components hour];
        NSInteger minute = [components minute];
        NSString *timeString;
        if(minute<30){
            minute = 30;
            timeString = [NSString stringWithFormat:@"%ld:%ld",hour,minute];
        } else{
            hour +=1;
            minute = 00;
            timeString = [NSString stringWithFormat:@"%ld:00",hour];
        }
        [timePicker setDate:[dateFormatter dateFromString:timeString]];
        NSString *myTime = timeString;
        selectedTime = myTime;

        BookingCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
        [cell.lblTitle setText:selectedTime];
        [self updateData];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    [self.pickerView setHidden:YES];
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(!bookingAvailabale){
//        return 6;
        return 5;
    } else{
//        return [titleArray count]+1;
        return [titleArray count];
    }
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BookingCell *cell = (BookingCell*)[tableView dequeueReusableCellWithIdentifier:@"bookingCell"];
    if(cell==nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BookingCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if(indexPath.row < ([titleArray count]-1)){
        if(indexPath.row==4){
            [cell setupTitleViewAndHideTxt:NO];
            [self setupDateKeyboard:cell.txtTitle];
        } else{
            [cell setupTitleViewAndHideTxt:YES];
        }
        
        if(indexPath.row==0){
            //Outlet//
            if(selectedOutlet){
                [cell setupTitleDetails:[iconArray objectAtIndex:indexPath.row] title:selectedOutlet.name andCount:@""];
            } else{
                [cell setupTitleDetails:[iconArray objectAtIndex:indexPath.row] title:[titleArray objectAtIndex:indexPath.row] andCount:@""];
            }
        } else if(indexPath.row==1){
            //Big Mouth//
            [cell setupTitleDetails:[iconArray objectAtIndex:indexPath.row] title:[titleArray objectAtIndex:indexPath.row] andCount:selectedBigMouth];
        } else if(indexPath.row==2){
            //Regular//
            [cell setupTitleDetails:[iconArray objectAtIndex:indexPath.row] title:[titleArray objectAtIndex:indexPath.row] andCount:selectedRegular];
        } else if(indexPath.row==3){
            //Guest//
            [cell setupTitleDetails:[iconArray objectAtIndex:indexPath.row] title:[titleArray objectAtIndex:indexPath.row] andCount:selectedGuest];
        } else if(indexPath.row==4){
            //Date//
            [cell.txtTitle setTag:indexPath.row];
//            [cell.txtTitle setDelegate:self];
            [cell.btnQuestion setHidden:NO];
            [cell.btnQuestion addTarget:self action:@selector(displayBookingDateDialog:) forControlEvents:UIControlEventTouchUpInside];
            if(selectedDate.length>0){
                [cell setupTitleDetails:[iconArray objectAtIndex:indexPath.row] title:[[SharedMethod new] convertDateTimeFormat:selectedDate andInputFormat:@"yyyy-MM-dd" andOutputFormat:@"dd MMM yyyy"] andCount:@""];
            } else{
                [cell setupTitleDetails:[iconArray objectAtIndex:indexPath.row] title:[titleArray objectAtIndex:indexPath.row] andCount:@""];
            }
        } else if(indexPath.row==5){
            //Time//
            [cell.txtTitle setTag:indexPath.row];
//            [cell.txtTitle setDelegate:self];
            if(selectedTime.length>0){
                [cell setupTitleDetails:[iconArray objectAtIndex:indexPath.row] title:[NSString stringWithFormat:@"%@",[[SharedMethod new] convertDateTimeFormat:selectedTime andInputFormat:@"HH:mm:ss" andOutputFormat:@"hh:mm a"]] andCount:@""];
            } else{
                [cell setupTitleDetails:[iconArray objectAtIndex:indexPath.row] title:[titleArray objectAtIndex:indexPath.row] andCount:@""];
            }
        }
//        else if(indexPath.row==6){
//            //Session//
//            if(selectedSession){
//                [cell setupTitleDetails:[iconArray objectAtIndex:indexPath.row] title:[NSString stringWithFormat:@"%@",selectedSession.name] andCount:@""];
//            } else{
//                [cell setupTitleDetails:[iconArray objectAtIndex:indexPath.row] title:[titleArray objectAtIndex:indexPath.row] andCount:@""];
//            }
//        }
    }
//    else if(indexPath.row == ([titleArray count]-1)){
//        [cell setupRemarkView];
//        [cell setupRemarkDetails:[iconArray objectAtIndex:indexPath.row] title:[titleArray objectAtIndex:indexPath.row] andRemark:@""];
//        tvRemark = cell.tvRemark;
//    }
    else{
        [cell setupButtonView];
        [cell.btnNext removeTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
        [cell.btnNext addTarget:self action:@selector(btnNextDidPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.pickerView setHidden:YES];
    
    switch (indexPath.row) {
        case 0:
            [self outletSelectionDidPressed];
            break;
        case 1:
            [self bigMouthMemberSelectionDidPressed];
            break;
        case 2:
            [self regularMemberSelectionDidPressed];
            break;
        case 3:
            [self nonMemberSelectionDidPressed];
            break;
        case 4:
            [self dateSelectionDidPressed];
            break;
        case 5:
            [self timeSelectionDidPressed];
            break;
        case 6:
//            [self sessionSelectionDidPressed];
            break;
        default:
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!bookingAvailabale){
        return 40.0f;
    } else{
        if(indexPath.row<6){
            return 40.0f;
        }
//        else if(indexPath.row==7) {
//            return 140.0f;
//        }
        else {
            return 56.0f;
        }
    }
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)bbtnDoneDidPressed:(UIButton*)sender {
    [self.pickerView setHidden:YES];
    if(sender.tag==1){
        //Date Complete//
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
        NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",datePicker.date]];

        dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSString *myDate = [dateFormatter stringFromDate:date];
        selectedDate = myDate;

        BookingCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
        [cell.lblTitle setText:[[SharedMethod new] convertDateTimeFormat:myDate andInputFormat:@"yyyy-MM-dd" andOutputFormat:@"dd MMM yyyy"]];
    } else{
        //Time Complete//
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_GB"]];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
        NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",timePicker.date]];

        dateFormatter.dateFormat = @"HH:mm";
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];

        NSInteger hour = [components hour];
        NSInteger minute = [components minute];
        NSString *timeString;
        
        
        NSDate *currentDate = [NSDate date];
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        [comps setDay:6];
        NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
        
//        if(selectedTime.length<=0){
//            if(minute<30){
//                minute = 30;
//                timeString = [NSString stringWithFormat:@"%ld:%ld",hour,minute];
//            } else{
//                hour +=1;
//                minute = 00;
//                timeString = [NSString stringWithFormat:@"%ld:00",hour];
//            }
//        } else{
            timeString = [dateFormatter stringFromDate:[timePicker date]];
//        }
        [timePicker setDate:[dateFormatter dateFromString:timeString]];
        NSString *myTime = timeString;
        selectedTime = myTime;

        BookingCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
        [cell.lblTitle setText:selectedTime];
    }
    [self dismissKeyboard];
    [self updateData];
}

-(void)datePickerDidSelect:(UIDatePicker*)sender{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
    NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",datePicker.date]];
    
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSString *myDate = [dateFormatter stringFromDate:date];
    selectedDate = myDate;
    [titleArray replaceObjectAtIndex:4 withObject:myDate];
    
    BookingCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
    [cell.lblTitle setText:[[SharedMethod new] convertDateTimeFormat:myDate andInputFormat:@"yyyy-MM-dd" andOutputFormat:@"dd MMM yyyy"]];
    [self updateData];
}

-(void)timePickerDidSelect:(UIDatePicker*)sender{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_GB"]];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
    NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",sender.date]];
    
    dateFormatter.dateFormat = @"HH:mm";
    NSString *myTime = [dateFormatter stringFromDate:date];
    selectedTime = myTime;
    [titleArray replaceObjectAtIndex:5 withObject:myTime];
    
    BookingCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
    [cell.lblTitle setText:selectedTime];
    [self updateData];
}

-(void)outletSelectionDidPressed{
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    for(Outlet *outlet in outletArray){
        UIAlertAction *action = [UIAlertAction actionWithTitle:outlet.name style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            selectedOutlet = outlet;
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            [self updateData];
        }];
        [alertController addAction:action];
    }
    
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)dateSelectionDidPressed{
    [self.pickerView setHidden:YES];
    BookingCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
    [cell.txtTitle becomeFirstResponder];
}

-(void)timeSelectionDidPressed{
    [self.pickerView setHidden:YES];
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for(BookingSession *session in sessionArray){
        if([session.slots count]>0){
            for(NSString *time in session.slots){
                UIAlertAction *action = [UIAlertAction actionWithTitle:[[SharedMethod new] convertDateTimeFormat:time andInputFormat:@"HH:mm:ss" andOutputFormat:@"hh:mm a"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    selectedTime = time;
                    selectedSession = session.session_id;
                    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:5 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                }];
                [alertController addAction:action];
            }
        }
    }
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)bigMouthMemberSelectionDidPressed{
    [self setupPickerView:1];
}

-(void)regularMemberSelectionDidPressed{
    [self setupPickerView:2];
}

-(void)nonMemberSelectionDidPressed{
    [self setupPickerView:3];
}

-(void)sessionSelectionDidPressed{
//    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//
//    for(BookingSession *session in sessionArray){
//        UIAlertAction *action = [UIAlertAction actionWithTitle:session.name style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            selectedSession = session;
//            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:6 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
//        }];
//        [alertController addAction:action];
//    }
//
//    [alertController addAction:cancelAction];
//    [self presentViewController:alertController animated:YES completion:nil];
}

-(IBAction)btnNextDidPressed:(id)sender{
    [self.pickerView setHidden:YES];
    if([[SharedMethod new] checkNetworkConnectivity]){
        if([self validateSelectedOption]){
            [self retrieveBookingPrice:NO];
            [self retrieveBookingPrice:YES];
            
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Booking_Selection", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        }
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(IBAction)displayBookingDateDialog:(id)sender{
    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"lbl_Date_Question", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
}

@end
