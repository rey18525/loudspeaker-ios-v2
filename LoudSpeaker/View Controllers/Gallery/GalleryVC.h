//
//  GalleryVC.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 19/12/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryVC : UIViewController
@property (nonatomic, strong) UIImage *image;
@end
