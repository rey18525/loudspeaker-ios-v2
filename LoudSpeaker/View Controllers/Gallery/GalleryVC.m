//
//  GalleryVC.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 19/12/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "GalleryVC.h"
#import "SharedMethod.h"
#import "Constant.h"

@interface GalleryVC ()<UIScrollViewDelegate>
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIButton *btnClose;
@property (nonatomic, strong) IBOutlet UIImageView *imgGallery;
@end

@implementation GalleryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBar];
    [self setupDelegates];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupUI];
//    [self setupSwipe];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)setupNavigationBar{
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationItem setTitle:@""];
}

-(void)setupDelegates{
    [self.scrollView setDelegate:self];
    [self.scrollView setBackgroundColor:[[SharedMethod new] colorWithHexString:BLACK_COLOR andAlpha:0.5f]];
    self.scrollView.minimumZoomScale=1.0;
    self.scrollView.maximumZoomScale=6.0;
    //    self.scrollView.contentSize=CGSizeMake(1280, 960);
}

-(void)setupUI{
    [self.view setBackgroundColor:[[SharedMethod new] colorWithHexString:GREY_COLOR andAlpha:0.5f]];
    [[self.btnClose titleLabel] setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [[self.btnClose titleLabel] setFont:[UIFont fontWithName:LIGHT_GREY_COLOR size:15.0f]];
    
    [self.imgGallery setContentMode:UIViewContentModeScaleAspectFit];
    if(self.image){
        [self.imgGallery setImage:self.image];
    } else{
        NSLog(@"NO IMG");
    }
    
}

/*
-(void)setupSwipe{
    if([self.images count]>0){
        [self.imgGallery setUserInteractionEnabled:YES];
        UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
        UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
        
        // Setting the swipe direction.
        [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
        [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
        
        // Adding the swipe gesture on image view
        [self.imgGallery addGestureRecognizer:swipeLeft];
        [self.imgGallery addGestureRecognizer:swipeRight];
    }
}
*/

#pragma mark - Actions
- (IBAction)btnCloseDidPressed:(id)sender {
    for(UIView *temp in self.parentViewController.view.window.subviews){
        if(temp.tag == 100){
            [temp removeFromSuperview];
        }
    }
    [self removeFromParentViewController];
}

/*
- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    NSLog(@"Index - %d",self.index);
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"Left Swipe");
        if(self.index<[self.images count]-1){
            self.index+=1;
            [self.imgGallery setImage:[self.images objectAtIndex:self.index]];
        } else{
            //Maximum
        }
    }
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        NSLog(@"Right Swipe");
        if(self.index!=0){
            self.index-=1;
            [self.imgGallery setImage:[self.images objectAtIndex:self.index]];
        } else{
            
        }
    }
    
}
*/

#pragma mark - UIScrollView Delegates
-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.imgGallery;
}

-(void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale{
    if(scale==1.0){
        [self.imgGallery setUserInteractionEnabled:YES];
    } else if(scale>1.0){
        [self.imgGallery setUserInteractionEnabled:NO];
    }
}

@end
