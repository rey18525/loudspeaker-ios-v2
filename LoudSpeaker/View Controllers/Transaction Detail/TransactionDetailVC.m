//
//  TransactionDetailVC.m
//  Oasis
//
//  Created by Wong Ryan on 28/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "TransactionDetailVC.h"
#import "TransactionDetailCell.h"
#import "Constant.h"
#import "SharedMethod.h"

@interface TransactionDetailVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *imgVoid;

@end

@implementation TransactionDetailVC{

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBar];
    [self setupDelegates];
    [self setupInitialization];
    if([self.selectedTransaction.status isEqualToString:@"voided"]){
        [self setupVoidedImage];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView sizeToFit];
}

#pragma mark - Setup Initialization
-(void)setupNavigationBar{
    [self.navigationItem setTitle:NSLocalizedString(@"nav_TransactionDetail_V2", nil)];
}

-(void)setupInitialization{
    [self.tableView setEstimatedRowHeight:100];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [[SharedMethod new] setBorder:self.tableView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
}

-(void)setupDelegates{
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
}

-(void)setupVoidedImage{
    [self.imgVoid setHidden:NO];
}

-(void)setupAttributedText:(NSString*)text andLabel:(UILabel*)label{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_BOLD size:28] range:NSMakeRange(0, 2)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_REGULAR size:15] range:NSMakeRange(2, 9)];
    [label setAttributedText:attributedString];
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
//    return 4;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 200)];
    [view setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];

    NSString *date = [[SharedMethod new] convertToTransactionDateFormat_V2:self.selectedTransaction.created_at];
    
    UILabel *lblDate = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, 160, 80)];
    [lblDate setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [lblDate setFont:[UIFont fontWithName:FONT_BOLD size:27.0f]];
    
    [lblDate setText:[date substringToIndex:2]];
//    [self setupAttributedText:lblDate.text andLabel:lblDate];
    [lblDate sizeToFit];
    
    UILabel *lblDate2 = [[UILabel alloc] initWithFrame:CGRectMake(lblDate.frame.origin.x+lblDate.frame.size.width+4, 12, 80, 50)];
    [lblDate2 setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [lblDate2 setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
    [lblDate2 setText:[date substringFromIndex:2]];
    [lblDate2 sizeToFit];
    
    UILabel *lblTime = [[UILabel alloc] initWithFrame:CGRectMake(self.tableView.frame.size.width-80-8, 11, 80, 20)];
    [lblTime setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [lblTime setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
    [lblTime setText:[[SharedMethod new] convertDateTimeFormat:self.selectedTransaction.created_at andInputFormat:@"yyyy-MM-dd HH:mm:ss" andOutputFormat:@"HH:mm:ss"]];
    [lblTime setTextAlignment:NSTextAlignmentRight];
    
    UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(8, lblDate.frame.origin.y+lblDate.frame.size.height+8, CGRectGetWidth(self.tableView.frame)-18, 48)];
    [lblHeader setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [lblHeader setFont:[UIFont fontWithName:FONT_REGULAR size:18.0f]];
    [lblHeader setText:self.selectedTransaction.content];
    [lblHeader setTextAlignment:NSTextAlignmentCenter];
    [lblHeader setNumberOfLines:2];
    
    [view addSubview:lblDate];
    [view addSubview:lblDate2];
    [view addSubview:lblTime];
    [view addSubview:lblHeader];
    
    return view;
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TransactionDetailCell *cell = (TransactionDetailCell*)[tableView dequeueReusableCellWithIdentifier:@"transactionDetailCell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
//    [[SharedMethod new] setBorder:cell borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    

//    if(indexPath.row==0){
//        //Member ID//
//        [cell.lblTitle setText:NSLocalizedString(@"lbl_Transaction_ID", nil)];
//        [cell.lblDescription setText:[NSString stringWithFormat:@"%d",[self.selectedTransaction.user_id intValue]]];
//    } else
    if(indexPath.row==0){
        //Reference No.//
        [cell.lblTitle setText:NSLocalizedString(@"lbl_Transaction_Ref", nil)];
        [cell.lblDescription setText:[NSString stringWithFormat:@"%d",[self.selectedTransaction.ID intValue]]];
    } else if(indexPath.row==1){
        //Action//
        NSString *text = [NSString stringWithFormat:@"%@%@",[[self.selectedTransaction.type substringToIndex:1] uppercaseString],[self.selectedTransaction.type substringFromIndex:1]];
        if([text containsString:@"_"]){
            text = [text stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        }
        [cell.lblTitle setText:NSLocalizedString(@"lbl_Transaction_Action", nil)];
        [cell.lblDescription setText:text];
    } else if(indexPath.row==2){
        //Status//
        NSString *text = self.selectedTransaction.status;
        [cell.lblTitle setText:NSLocalizedString(@"lbl_Transaction_Status", nil)];
        if(text.length>0){
            text = [NSString stringWithFormat:@"%@%@",[[self.selectedTransaction.status substringToIndex:1] uppercaseString],[self.selectedTransaction.status substringFromIndex:1]];
            [cell.lblDescription setText:text];
        } else{
            [cell.lblDescription setText:@"-"];
        }
    }
    /*
    else if(indexPath.row==4){
        //Date/Time//
        NSString *date = [[SharedMethod new] convertToTransactionDetailDateFormat:self.selectedTransaction.created_at];
        [cell.lblTitle setText:NSLocalizedString(@"lbl_Transaction_Date", nil)];
        [cell.lblDescription setText:date];
    }
     */
    else if(indexPath.row==3){
        //Transaction Amount//
        [cell.lblTitle setText:NSLocalizedString(@"lbl_Transaction_Amount", nil)];
        [cell.lblDescription setText:[NSString stringWithFormat:@"RM %@",self.selectedTransaction.credit]];
    }
    
    else if(indexPath.row==4){
        //Reason//
        [cell.lblTitle setText:NSLocalizedString(@"lbl_Transaction_Reason", nil)];
        [cell.lblDescription setText:self.selectedTransaction.reason];
        if(self.selectedTransaction.reason.length>0){
            [cell.lblDescription setText:[NSString stringWithFormat:@"%@%@",[cell.lblDescription.text substringToIndex:1],[cell.lblDescription.text substringFromIndex:1]]];
        } else{
            [cell.lblDescription setText:@"-"];
        }
    }
    
    /*else if(indexPath.row==6){
        //Credit//
        [cell.lblTitle setText:NSLocalizedString(@"lbl_Transaction_Credit", nil)];
        [cell.lblDescription setText:[NSString stringWithFormat:@"$%@",self.selectedTransaction.credit]];
    } else if(indexPath.row==7){
        //Reason//
        [cell.lblTitle setText:NSLocalizedString(@"lbl_Transaction_Description", nil)];
        [cell.lblDescription setText:self.selectedTransaction.content];
    } else if(indexPath.row==8){
        //Reason//
        [cell.lblTitle setText:NSLocalizedString(@"lbl_Transaction_Reason", nil)];
        [cell.lblDescription setText:self.selectedTransaction.reason];
        if(self.selectedTransaction.reason.length>0){
            [cell.lblDescription setText:[NSString stringWithFormat:@"%@%@",[cell.lblDescription.text substringToIndex:1],[cell.lblDescription.text substringFromIndex:1]]];
        }
    }*/
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"Selected row %d",(int)indexPath.row);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 130;
}



@end
