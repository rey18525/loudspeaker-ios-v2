//
//  CardExpireVC.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 26/04/2017.
//  Copyright © 2017 Ryan. All rights reserved.
//

#import "CardExpireVC.h"
#import "Constant.h"
#import "SharedMethod.h"

@interface CardExpireVC ()
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;

@end

@implementation CardExpireVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self enlargeImage];
}

-(void)enlargeImage{
    [UIView animateWithDuration:1.0 animations:^{
        self.imgIcon.transform = CGAffineTransformMakeScale(1.5, 1.5);
    } completion:^(BOOL finished){
        [self reduceImage];
    }];
}

-(void)reduceImage{
    [UIView animateWithDuration:1.0 animations:^{
        self.imgIcon.transform = CGAffineTransformMakeScale(1.0, 1.0);
    } completion:^(BOOL finished){
        [self enlargeImage];
    }];
}

#pragma mark - Custom Functions
-(void)setupUI{
    [self.lblDescription setText:self.promptMessage];
    [self.lblDescription setTextColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
    [self.lblDescription setFont:[UIFont fontWithName:FONT_REGULAR size:17.0f]];
    
    [[SharedMethod new] setBorder:self.btnOk borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    [[self.btnOk titleLabel] setFont:[UIFont fontWithName:FONT_REGULAR size:15.0f]];
    [self.btnOk setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
}

#pragma mark - Actions
- (IBAction)btnOkDidPressed:(id)sender {
    [self.cardExpireDelegate closeView];
}

@end
