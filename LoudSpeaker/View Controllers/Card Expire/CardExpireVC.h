//
//  CardExpireVC.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 26/04/2017.
//  Copyright © 2017 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CardExpire_Delegate <NSObject>
@optional
-(void)closeView;
@end


@interface CardExpireVC : UIViewController
@property (nonatomic, assign) NSString *promptMessage;
@property (nonatomic) id <CardExpire_Delegate> cardExpireDelegate;
@end
