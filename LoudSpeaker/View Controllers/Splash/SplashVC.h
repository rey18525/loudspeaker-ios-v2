//
//  SplashVC.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 02/03/2017.
//  Copyright © 2017 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashVC : UIViewController

-(void)performOpeningMusic;

@end
