//
//  SplashVC.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 02/03/2017.
//  Copyright © 2017 Ryan. All rights reserved.
//

#import "SplashVC.h"
#import "Constant.h"
#import <AVFoundation/AVFoundation.h>

@interface SplashVC ()<AVAudioPlayerDelegate>

@end

@implementation SplashVC{
    AVAudioPlayer *player;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if([ENABLE_STARTUP_MUSIC isEqualToString:@"1"]){
        [self performOpeningMusic];
    } else{
        [self performSelector:@selector(nextScreen) withObject:nil afterDelay:0.1];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(void)nextScreen{
    [self performSegueWithIdentifier:@"testSegue" sender:nil];
}

-(void)performOpeningMusic{
    NSString* resourcePath = [[NSBundle mainBundle] resourcePath];
    resourcePath = [resourcePath stringByAppendingString:@"/startup.mp3"];
    NSError* err;
    
    //Initialize our player pointing to the path to our resource
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:
              [NSURL fileURLWithPath:resourcePath] error:&err];
    
    if( err ){
        NSLog(@"Failed with reason: %@", [err localizedDescription]);
        [self nextScreen];
    }
    else{
        //set our delegate and begin playback
        player.delegate = self;
        [player play];
        player.numberOfLoops = 0;
        player.currentTime = 0;
        player.volume = 1.0;
    }}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    if(flag){
        [self nextScreen];
    }
}


@end
