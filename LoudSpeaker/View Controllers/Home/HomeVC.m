//
//  HomeVC.m
//  Oasis
//
//  Created by Wong Ryan on 21/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "HomeVC.h"
#import "Constant.h"
#import "SharedMethod.h"
#import "WebAPI.h"
#import "AnnouncementCell.h"
#import "PromotionCell.h"
#import "VoucherHomeCell.h"
#import "HomeHeaderCell.h"
#import "VoucherListVC.h"
#import "VoucherDetailVC.h"
#import "PromotionListVC.h"
#import "FlashListVC.h"
#import "SQLManager.h"
#import "SVProgressHUD.h"
#import "Promotion.h"
#import "PromotionDetailVC.h"
#import "AFNetworking.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "NotificationVC.h"
#import <BBBadgeBarButtonItem.h>
#import "IntroVC.h"
#import <KIImagePager.h>
#import "BannerCell.h"
#import "PromotionHomeCell.h"
#import "FlashHomeCell.h"
#import "PrivilegeVC.h"
#import "Category.h"
#import "Flash.h"
#import "WebVC.h"
#import "AdvertisementVC.h"
#import "BookingVC.h"
#import <PDKeychainBindings.h>

@interface HomeVC ()<UITableViewDelegate,UITableViewDataSource,Voucher_Delegate,Promotion_Delegate,Flash_Delegate,KIImagePagerDelegate,KIImagePagerDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbtnInbox;
@property (weak, nonatomic) IBOutlet UIButton *btnInbox;
@property (weak, nonatomic) IBOutlet UIImageView *imgInbox;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (weak, nonatomic) IBOutlet UIImageView *imgPrivilege;
@property (weak, nonatomic) IBOutlet UIButton *btnPrivilege;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *myNavigationBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *myNavigationBarHeight;
@property (weak, nonatomic) IBOutlet UIButton *bbtnBooking;
@property (weak, nonatomic) IBOutlet UIImageView *imgBooking;
@property (assign) int REQUEST_CONFIG;
@end

@import Firebase;

@implementation HomeVC{
    UIStoryboard *storyboard;
    BOOL hasAnnouncement,performOnce;
    int tableRows;
    NSMutableArray *campaignsArray, *bannerArray, *bannerImagesArray, *flashCategoryArray, *promotionCategoryArray, *eventCategoryArray, *menuArray;
    AdvertisementVC *advertisementVC;
    NSString *myAppID;
//    BBBadgeBarButtonItem *bbtnInbox;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.REQUEST_CONFIG = 1;
    
    if(SYSTEM_VERSION_LESS_THAN(@"10")){
        [self updateTabBarController];
    }
    
    [self setupInitialization];
    [self setupUI];
    [self setupDelegates];
    [self setupData];
    [self setupRefreshControl];
    
    if([DISPLAY_INTRODUCTION isEqualToString:@"1"]){
        if(![[NSUserDefaults standardUserDefaults] boolForKey:IS_SECOND_RUN]){
            //If NOT second run means FIRST run//
            IntroVC *introVC = [storyboard instantiateViewControllerWithIdentifier:@"introVC"];
            [introVC setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:introVC animated:YES];
        }
    } else{
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self.navigationItem setTitle:NSLocalizedString(@"nav_Home_V2", nil)];
    [self.lblTitle setText:NSLocalizedString(@"nav_Home_V2", nil)];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.tableView setContentOffset:CGPointMake(0, 0) animated:NO];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"displayReward"]){
        [self.tabBarController setSelectedIndex:2];
    }
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setupTabBar];
        [self setupBarButton];
        
        if([UIScreen mainScreen].bounds.size.height == 812){
            [self.myNavigationBarHeight setConstant:50.0f];
        } else{
            [self.myNavigationBarHeight setConstant:46.0f];
        }
    });
    //[self checkRequestConfig];
    [self setAPIParameters];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:COMPLETE_BOOKING]){
        [self.tabBarController setSelectedIndex:2];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self checkToken];
    performOnce = NO;
}


#pragma mark - Custom Functions
-(void)setupTabBar{
    [self.tabBarController setHidesBottomBarWhenPushed:NO];
}

-(void)updateTabBarController{
    UITabBar *tabBar = self.tabBarController.tabBar;
    UITabBarItem *tab1 = [[tabBar items] objectAtIndex:0];
    [tab1 setImage:[[UIImage imageNamed:@"ic_bottombar_home"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab1 setSelectedImage:[[UIImage imageNamed:@"ic_bottombar_home_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
}

-(void)setupInitialization{
    [self.navigationItem setTitle:NSLocalizedString(@"nav_Home_V2", nil)];
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    bannerArray = [[NSMutableArray alloc] init];
    bannerImagesArray = [[NSMutableArray alloc] init];
    flashCategoryArray = [[NSMutableArray alloc] init];
    promotionCategoryArray = [[NSMutableArray alloc] init];
    eventCategoryArray = [[NSMutableArray alloc] init];
    campaignsArray = [[NSMutableArray alloc] init];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    
    advertisementVC = [storyboard instantiateViewControllerWithIdentifier:@"advertisementVC"];
    [advertisementVC.view setTag:901];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnCloseAdvertisementDidPressed:)];
    [advertisementVC.view addGestureRecognizer:tapGesture];
    [advertisementVC.btnClose addTarget:self action:@selector(btnCloseAdvertisementDidPressed:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setupUI{
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
}

-(void)setupBarButton{
    [self.lblCount setBackgroundColor:[UIColor clearColor]];
    [self.lblCount setFont:[UIFont fontWithName:FONT_REGULAR size:10.0f]];
    [[SharedMethod new] setCornerRadius:self.lblCount andCornerRadius:self.lblCount.frame.size.width/2];
    [[self.lblCount layer] setBorderColor:[[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] CGColor]];
    [[self.lblCount layer] setBorderWidth:1.0f];
    [self.lblCount setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
        NSString *temp = [NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults] integerForKey:INBOX_COUNT]];
        [self.lblCount setText:temp];
    } else{
        [self.lblCount setText:@""];
        [self.lblCount setBackgroundColor:[UIColor clearColor]];
        [[self.lblCount layer] setBorderWidth:0.0f];
    }
    
    [self.btnInbox addTarget:self action:@selector(bbtnInboxDidPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.btnInbox.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(10));
    
    if([ENABLE_ANIMATION_NOTIFICATION isEqualToString:@"1"]){
        if([[NSUserDefaults standardUserDefaults] integerForKey:INBOX_COUNT] >0){
            self.imgInbox.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(10));
            [UIView animateKeyframesWithDuration:0.5 delay:0.0 options:UIViewKeyframeAnimationOptionAutoreverse | UIViewKeyframeAnimationOptionRepeat animations:^{
                [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1.0 animations:^{
                    self.imgInbox.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-10));
                }];
            } completion:^(BOOL finished) {
                //Do Nothing//
            }];
        } else{
            self.imgInbox.transform = CGAffineTransformIdentity;
        }
    }
    
    self.imgBooking.transform = CGAffineTransformIdentity;
    [UIView animateKeyframesWithDuration:0.5 delay:0.0 options:UIViewKeyframeAnimationOptionAutoreverse | UIViewKeyframeAnimationOptionRepeat animations:^{
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1.0 animations:^{
            self.imgBooking.transform = CGAffineTransformMakeScale(1.5, 1.5);
        }];
    } completion:^(BOOL finished) {
        //Do Nothing//
    }];
    
//    UIBarButtonItem *bbtnPrivilege = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"ic_action_privellege"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(bbtnPrivilegeDidPressed:)];

//    UIImageView *imgPrivilege = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_action_privellege"]];
//    [imgPrivilege setAutoresizingMask:UIViewAutoresizingNone];
//    [imgPrivilege setContentMode:UIViewContentModeScaleAspectFit];
//
//    UIButton *btnPrivilege = [[UIButton alloc] initWithFrame:CGRectMake(0, -5, 40, 40)];
//    [btnPrivilege addSubview:imgPrivilege];
//    [btnPrivilege addTarget:self action:@selector(bbtnPrivilegeDidPressed:) forControlEvents:UIControlEventTouchUpInside];
//    [imgPrivilege setCenter:btnPrivilege.center];
//
//    UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 33)];
//    [tempView addSubview:imgPrivilege];
//    [tempView addSubview:btnPrivilege];
//
//    UIBarButtonItem *bbtnPrivilege = [[UIBarButtonItem alloc] initWithCustomView:tempView];
    
    self.imgPrivilege.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(10));
    [UIView animateKeyframesWithDuration:0.5 delay:0.0 options:UIViewKeyframeAnimationOptionAutoreverse | UIViewKeyframeAnimationOptionRepeat animations:^{
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1 animations:^{
            self.imgPrivilege.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-10));
        }];
    } completion:^(BOOL finished) {
        //Do Nothing//
    }];
//    self.navigationItem.rightBarButtonItems = @[self.bbtnInbox, bbtnPrivilege];
}

-(void)setupDelegates{
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
}

-(void)setupData{
    tableRows=7;
    hasAnnouncement = NO;
    performOnce = NO;
}

-(void)setupRefreshControl{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl setTintColor:[UIColor blackColor]];
    [refreshControl addTarget:self action:@selector(refreshAction:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

-(void)checkToken{
    if(![[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN]){
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:DEVICE_TOKEN];
    }
}

- (void)setAPIParameters{
    /*
     NSString *webservice_url = @"http://api.loudspeakerktv.my/";
     NSString *share_base = @"http://loudspeakerktv.my/";
     NSString *company_website = @"http://www.loudspeakerktv.my";
     NSString *initial_key = @"H0dko5m67wFMPFfUuHG5NaGoDoPLilZLGgqo+mwtOHE=";
     NSString *merchant_key = @"PinZkN3MSC";
     NSString *merchant_code = @"M12911_S0001";
     NSString *payment_callback = @"https://payment.apppay.tech/loudspeaker/ipay88/backend.php";
     */
    
    NSString *webservice_url = API_APP_SERVER;
    NSString *share_base = API_APP_SERVER_SHARE;
    NSString *company_website = API_COMPANY_WEBSITE;
    NSString *initial_key = API_INITIAL_KEY;
    NSString *merchant_key = IPAY_MERCHANT_KEY;
    NSString *merchant_code = IPAY_MERCHANT_CODE;
    NSString *payment_callback = IPAY_CALLBACK_URL;
    
    NSLog(@"TEST_webservice_url - %@", webservice_url);
    NSLog(@"TEST_share_base - %@", share_base);
    NSLog(@"TEST_company_website - %@", company_website);
    NSLog(@"TEST_initial_key - %@", initial_key);
    NSLog(@"TEST_merchant_key - %@", merchant_key);
    NSLog(@"TEST_merchant_code - %@", merchant_code);
    NSLog(@"TEST_payment_callback - %@", payment_callback);
    
    [[NSUserDefaults standardUserDefaults] setObject:webservice_url forKey:WEBSERVICE_URL];
    [[NSUserDefaults standardUserDefaults] setObject:share_base forKey:SHARE_BASE];
    [[NSUserDefaults standardUserDefaults] setObject:company_website forKey:COMPANY_WEBSITE];
    [[NSUserDefaults standardUserDefaults] setObject:merchant_key forKey:MERCHANT_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:merchant_code forKey:MERCHANT_CODE];
    [[NSUserDefaults standardUserDefaults] setObject:payment_callback forKey:PAYMENT_CALLBACK];
    [[PDKeychainBindings sharedKeychainBindings] setObject:initial_key forKey:INITIAL_KEY];
 
    if(![[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
        [[PDKeychainBindings sharedKeychainBindings] removeObjectForKey:SECRET_KEY];
        [[PDKeychainBindings sharedKeychainBindings] setObject:initial_key forKey:SECRET_KEY];
    }
    [self retrieveHomeDetails];
}

- (void)checkRequestConfig{
    if(self.REQUEST_CONFIG == 1){
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_SECOND_RUN]){
            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        }
        
        FIRRemoteConfig *remoteConfig = [FIRRemoteConfig remoteConfig];
        FIRRemoteConfigSettings *remoteConfigSettings = [[FIRRemoteConfigSettings alloc] initWithDeveloperModeEnabled:YES];
        remoteConfig.configSettings = remoteConfigSettings;
        [remoteConfig setDefaultsFromPlistFileName:@"RemoteConfig"];
        
        // cacheExpirationSeconds is set to cacheExpiration here, indicating that any previously
        // fetched and cached config would be considered expired because it would have been fetched
        // more than cacheExpiration seconds ago. Thus the next fetch would go to the server unless
        // throttling is in progress. The default expiration duration is 43200 (12 hours).
        [remoteConfig fetchWithExpirationDuration:1 completionHandler:^(FIRRemoteConfigFetchStatus status, NSError *error) {
            if (status == FIRRemoteConfigFetchStatusSuccess) {
                [remoteConfig activateFetched];
                
                NSString *is_debug = remoteConfig[@"is_debug"].stringValue;
                NSString *webservice_url;
                NSString *share_base;
                NSString *company_website;
                NSString *initial_key;
                NSString *merchant_key;
                NSString *merchant_code;
                NSString *payment_callback;
                
                //is_debug = @"1";
                // Override Firebase Remote Config
                // Note: This is not part of original code, is a hack to allow more code level control to DEV or PROD endpoints
                is_debug = IS_DEBUG_FIREBASE_OVERRIDE;
                
                if([is_debug isEqualToString:@"0"]){ //Should be 1//
                    //IF DEBUG//
                    webservice_url = remoteConfig[@"webservice_url_uat"].stringValue;
                    share_base = remoteConfig[@"share_base_uat"].stringValue;
                    company_website = remoteConfig[@"company_website_uat"].stringValue;
                    initial_key = remoteConfig[@"initial_key_uat"].stringValue;
                    merchant_key = remoteConfig[@"merchant_key"].stringValue;
                    merchant_code = remoteConfig[@"merchant_code"].stringValue;
                    payment_callback = remoteConfig[@"payment_callback_uat"].stringValue;
                } else{
                    //IF PRODUCTION//
                    webservice_url = remoteConfig[@"webservice_url"].stringValue;
                    share_base = remoteConfig[@"share_base"].stringValue;
                    company_website = remoteConfig[@"company_website"].stringValue;
                    initial_key = remoteConfig[@"initial_key"].stringValue;   
                    merchant_key = remoteConfig[@"merchant_key"].stringValue;
                    merchant_code = remoteConfig[@"merchant_code"].stringValue;
                    payment_callback = remoteConfig[@"payment_callback"].stringValue;
                }
                self.REQUEST_CONFIG = 0;
                
                [[NSUserDefaults standardUserDefaults] setObject:webservice_url forKey:WEBSERVICE_URL];
                [[NSUserDefaults standardUserDefaults] setObject:share_base forKey:SHARE_BASE];
                [[NSUserDefaults standardUserDefaults] setObject:company_website forKey:COMPANY_WEBSITE];
                [[NSUserDefaults standardUserDefaults] setObject:merchant_key forKey:MERCHANT_KEY];
                [[NSUserDefaults standardUserDefaults] setObject:merchant_code forKey:MERCHANT_CODE];
                [[NSUserDefaults standardUserDefaults] setObject:payment_callback forKey:PAYMENT_CALLBACK];
                [[PDKeychainBindings sharedKeychainBindings] setObject:initial_key forKey:INITIAL_KEY];
                
                if(![[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
                    [[PDKeychainBindings sharedKeychainBindings] removeObjectForKey:SECRET_KEY];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:initial_key forKey:SECRET_KEY];
                }
                
                [self retrieveHomeDetails];
            } else {
                //NSLog(@"Config not fetched");
                //NSLog(@"Error %@", error.localizedDescription);
//                [self retrieveLocalHomeDetails];
                
                NSString *is_debug = @"0";
                NSString *webservice_url = @"http://api.loudspeakerktv.my/";
                NSString *share_base = @"http://loudspeakerktv.my/";
                NSString *company_website = @"http://loudspeakerktv.my/";
                NSString *initial_key = @"H0dko5m67wFMPFfUuHG5NaGoDoPLilZLGgqo+mwtOHE=";
                NSString *merchant_key = @"PinZkN3MSC";
                NSString *merchant_code = @"M12911_S0001";
                NSString *payment_callback = @"https://payment.apppay.tech/loudspeaker/ipay88/backend.php";
                
                [[NSUserDefaults standardUserDefaults] setObject:webservice_url forKey:WEBSERVICE_URL];
                [[NSUserDefaults standardUserDefaults] setObject:share_base forKey:SHARE_BASE];
                [[NSUserDefaults standardUserDefaults] setObject:company_website forKey:COMPANY_WEBSITE];
                [[NSUserDefaults standardUserDefaults] setObject:merchant_key forKey:MERCHANT_KEY];
                [[NSUserDefaults standardUserDefaults] setObject:merchant_code forKey:MERCHANT_CODE];
                [[NSUserDefaults standardUserDefaults] setObject:payment_callback forKey:PAYMENT_CALLBACK];
                [[PDKeychainBindings sharedKeychainBindings] setObject:initial_key forKey:INITIAL_KEY];
                
                if(![[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
                    [[PDKeychainBindings sharedKeychainBindings] removeObjectForKey:SECRET_KEY];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:initial_key forKey:SECRET_KEY];
                }
                [self retrieveHomeDetails];
            }
        }];
        
    } else{
        [self retrieveHomeDetails];
    }
}

-(void)retrieveLocalHomeDetails{
    NSDictionary *result = [[NSUserDefaults standardUserDefaults] objectForKey:HOME_DETAIL]
    ;

    if([campaignsArray count]>0){
        [campaignsArray removeAllObjects];
    }
    
    NSMutableArray *tempCampaign = [[NSMutableArray alloc] init];
    if([[result objectForKey:@"campaigns"] count]>0){
        for(int count=0;count<[[result objectForKey:@"campaigns"] count];count++){
            [tempCampaign addObject:[[result objectForKey:@"campaigns"] objectAtIndex:count]];
        }
    }
    
    campaignsArray = [[SharedMethod new] processCampaign:tempCampaign andCurrentArray:campaignsArray];
    int maximumVoucher = [VOUCHER_LIMIT intValue];
    if([campaignsArray count]>0){
        for(int count=0; count<[campaignsArray count]; count++){
            if(count>maximumVoucher-1){
                [campaignsArray removeObjectAtIndex:count];
            }
        }
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[[result objectForKey:@"categories"] objectForKey:@"menu"] forKey:MENU_PARENTS];
    
//    if(hasAnnouncement){
//        tableRows = (int)[promotionsArray count]+4;//+4 = Voucher + Announcement + 2 Titles
//    } else{
//        tableRows = (int)[promotionsArray count]+3;//+3 = Voucher + 2 Titles
//    }
    
    [self.tableView reloadData];
}

-(BOOL)connected{
    //    return [AFNetworkReachabilityManager sharedManager].reachable;
    NSURL *scriptUrl = [NSURL URLWithString:@"http://www.google.com/m"];
    NSData *data = [NSData dataWithContentsOfURL:scriptUrl];
    if (data){
        NSLog(@"Device is connected to the Internet");
        return YES;
    }
    else{
        NSLog(@"Device is not connected to the Internet");
        return NO;
    }
}

-(void)retrieveHomeDetails{
    if([self connected]){
        performOnce = YES;
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_SECOND_RUN]){
            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        }
        NSString *deviceToken = [[SharedMethod new] checkDeviceToken];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:PLATFORM forKey:@"platform"];
        [params setObject:deviceToken forKey:@"push_token"];
        [params setObject:[SharedMethod getTokenWithRoute:@"user/home" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        }
        [params setObject:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"] forKey:@"update_ios"];
        
        [WebAPI retrieveHomeDetail:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.tabBarController setSelectedIndex:0];
                    [self retrieveHomeDetails];
                }
            } else{
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:HAS_CONFIG];
                    result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                    [[NSUserDefaults standardUserDefaults] setObject:result forKey:HOME_DETAIL];
                    
                    if([bannerArray count]>0){
                        [bannerArray removeAllObjects];
                    }
                    
                    if([bannerImagesArray count]>0){
                        [bannerImagesArray removeAllObjects];
                    }
                    
                    if([flashCategoryArray count]>0){
                        [flashCategoryArray removeAllObjects];
                    }
                    
                    if([promotionCategoryArray count]>0){
                        [promotionCategoryArray removeAllObjects];
                    }
                    
                    if([eventCategoryArray count]>0){
                        [eventCategoryArray removeAllObjects];
                    }
                    
                    if([campaignsArray count]>0){
                        [campaignsArray removeAllObjects];
                    }
                    
                    if([[result objectForKey:@"banners"] count]>0){
                        bannerArray = [[NSMutableArray alloc] initWithArray:[result objectForKey:@"banners"]];
                    }
                    
                    NSMutableArray *tempFlashCategory = [[NSMutableArray alloc] init];
                    if([[[result objectForKey:@"categories"] objectForKey:@"flash_deal"] count]>0){
                        for(int count=0;count<[[[result objectForKey:@"categories"] objectForKey:@"flash_deal"] count];count++){
                            [tempFlashCategory addObject:[[[result objectForKey:@"categories"] objectForKey:@"flash_deal"] objectAtIndex:count]];
                        }
                        tempFlashCategory = [[SharedMethod new] processFlash:tempFlashCategory andCurrentArray:flashCategoryArray];
                    }
                    
                    NSMutableArray *tempPromoCategory = [[NSMutableArray alloc] init];
                    if([[[result objectForKey:@"categories"] objectForKey:@"promo"] count]>0){
                        for(int count=0;count<[[[result objectForKey:@"categories"] objectForKey:@"promo"] count];count++){
                            [tempPromoCategory addObject:[[[result objectForKey:@"categories"] objectForKey:@"promo"] objectAtIndex:count]];
                        }
                        promotionCategoryArray = [[SharedMethod new] processCategory:tempPromoCategory andCurrentArray:promotionCategoryArray];
                    }
                    
                    NSMutableArray *tempEventCategory = [[NSMutableArray alloc] init];
                    if([[[result objectForKey:@"categories"] objectForKey:@"event"] count]>0){
                        for(int count=0;count<[[[result objectForKey:@"categories"] objectForKey:@"event"] count];count++){
                            [tempEventCategory addObject:[[[result objectForKey:@"categories"] objectForKey:@"event"] objectAtIndex:count]];
                        }
                        eventCategoryArray = [[SharedMethod new] processCategory:tempEventCategory andCurrentArray:eventCategoryArray];
                    }
  
                    NSMutableArray *tempCampaign = [[NSMutableArray alloc] init];
                    if([[result objectForKey:@"campaigns"] count]>0){
                        for(int count=0;count<[[result objectForKey:@"campaigns"] count];count++){
                            [tempCampaign addObject:[[result objectForKey:@"campaigns"] objectAtIndex:count]];
                        }
                        campaignsArray = [[SharedMethod new] processCampaign:tempCampaign andCurrentArray:campaignsArray];
                    }
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[[result objectForKey:@"categories"] objectForKey:@"menu"] forKey:MENU_PARENTS];
                    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"gender"] forKey:GENDER];
                    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"race"] forKey:RACE];
                    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"states"] forKey:STATES];
                    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"countries"] forKey:COUNTRIES];
                    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"topup"] forKey:TOPUP_LIST];
                    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"enquiry_categories"] forKey:TOPICS];
                    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"member_types"] forKey:MEMBER_TYPES];
                    [[NSUserDefaults standardUserDefaults] setBool:[[result objectForKey:@"booking_guest_allowed"] boolValue] forKey:ALLOW_GUEST_BOOKING];
                    [[NSUserDefaults standardUserDefaults] setBool:[[result objectForKey:@"isCardExpired"] boolValue] forKey:IS_CARD_EXPIRED];
                    
                    if([self checkAppVersion]){
                        if([[[result objectForKey:@"versions"] objectForKey:@"force_ios"] isEqualToString:@"1"]){
                            [self displayForceUpdate];
                        } else{
                            [self displayUpdate];
                        }
                    }
                    
                    if([[result objectForKey:@"pages"] isKindOfClass:[NSDictionary class]]){
                        NSString *terms = [[result objectForKey:@"pages"] objectForKey:@"terms"];
                        NSString *privacy = [[result objectForKey:@"pages"] objectForKey:@"privacy"];
                        NSString *faq = [[result objectForKey:@"pages"] objectForKey:@"faq"];
                        NSString *booking_terms = [[result objectForKey:@"pages"] objectForKey:@"booking_terms"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:terms forKey:TERMS_CONDITION];
                        [[NSUserDefaults standardUserDefaults] setObject:privacy forKey:PRIVACY];
                        [[NSUserDefaults standardUserDefaults] setObject:faq forKey:FAQ];
                        [[NSUserDefaults standardUserDefaults] setObject:booking_terms forKey:BOOKING_TERMS];
                    }
//
//                    if(hasAnnouncement){
//                        tableRows = (int)[promotionsArray count]+4;//+4 = Voucher + Announcement + 2 Titles
//                    } else{
//                        tableRows = (int)[promotionsArray count]+3;//+3 = Voucher + 2 Titles
//                    }
                    
                    if(![[NSUserDefaults standardUserDefaults] boolForKey:PROCESS_FIRST_INSTALL]){
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:PROCESS_FIRST_INSTALL];
                        [self sendStatistics];
                    }
                    
                    if([[NSUserDefaults standardUserDefaults] boolForKey:DISPLAY_ADVERTISEMENT] && [[result objectForKey:@"ads"] count]>0){
                        
                        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_SECOND_RUN]){
                            [self.view.window addSubview:advertisementVC.view];
                            [self addChildViewController:advertisementVC];
                            
                            [advertisementVC.imgAdvertisement sd_setImageWithURL:[NSURL URLWithString:[[[[result objectForKey:@"ads"] objectAtIndex:0] objectForKey:@"image"] objectForKey:@"src"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                
                            }];
                        }
                    }
                    
                    if([campaignsArray count]>0){
                        tableRows = 7;
                    } else{
                        tableRows = 5;
                    }
                    
                    [self.tableView reloadData];
                }
            }
        }];
    } else{
        if([[NSUserDefaults standardUserDefaults] boolForKey:HAS_CONFIG] == NO){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_No_Config", nil) preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *exitAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Exit", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                exit(1);
            }];
            
            UIAlertAction *retryAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Config_Retry", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self retrieveHomeDetails];
            }];
            
            [alertController addAction:exitAction];
            [alertController addAction:retryAction];
            [self presentViewController:alertController animated:YES completion:nil];
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            NSDictionary *result = [[NSUserDefaults standardUserDefaults] objectForKey:HOME_DETAIL];
            if([bannerArray count]>0){
                [bannerArray removeAllObjects];
            }
            
            if([bannerImagesArray count]>0){
                [bannerImagesArray removeAllObjects];
            }
            
            if([flashCategoryArray count]>0){
                [flashCategoryArray removeAllObjects];
            }
            
            if([promotionCategoryArray count]>0){
                [promotionCategoryArray removeAllObjects];
            }
            
            if([eventCategoryArray count]>0){
                [eventCategoryArray removeAllObjects];
            }
            
            if([campaignsArray count]>0){
                [campaignsArray removeAllObjects];
            }
            
            if([[result objectForKey:@"banners"] count]>0){
                bannerArray = [[NSMutableArray alloc] initWithArray:[result objectForKey:@"banners"]];
            }
            
            if([[result objectForKey:@"banners"] count]>0){
                bannerArray = [[NSMutableArray alloc] initWithArray:[result objectForKey:@"banners"]];
            }
            
            NSMutableArray *tempFlashCategory = [[NSMutableArray alloc] init];
            if([[[result objectForKey:@"categories"] objectForKey:@"flash_deal"] count]>0){
                for(int count=0;count<[[[result objectForKey:@"categories"] objectForKey:@"flash_deal"] count];count++){
                    [tempFlashCategory addObject:[[[result objectForKey:@"categories"] objectForKey:@"flash_deal"] objectAtIndex:count]];
                }
                tempFlashCategory = [[SharedMethod new] processFlash:tempFlashCategory andCurrentArray:flashCategoryArray];
            }
            
            NSMutableArray *tempPromoCategory = [[NSMutableArray alloc] init];
            if([[[result objectForKey:@"categories"] objectForKey:@"promo"] count]>0){
                for(int count=0;count<[[[result objectForKey:@"categories"] objectForKey:@"promo"] count];count++){
                    [tempPromoCategory addObject:[[[result objectForKey:@"categories"] objectForKey:@"promo"] objectAtIndex:count]];
                }
                promotionCategoryArray = [[SharedMethod new] processCategory:tempPromoCategory andCurrentArray:promotionCategoryArray];
            }
            
            NSMutableArray *tempEventCategory = [[NSMutableArray alloc] init];
            if([[[result objectForKey:@"categories"] objectForKey:@"event"] count]>0){
                for(int count=0;count<[[[result objectForKey:@"categories"] objectForKey:@"event"] count];count++){
                    [tempEventCategory addObject:[[[result objectForKey:@"categories"] objectForKey:@"event"] objectAtIndex:count]];
                }
                
                eventCategoryArray = [[SharedMethod new] processCategory:tempEventCategory andCurrentArray:eventCategoryArray];
            }
                
            NSMutableArray *tempCampaign = [[NSMutableArray alloc] init];
            if([[result objectForKey:@"campaigns"] count]>0){
                for(int count=0;count<[[result objectForKey:@"campaigns"] count];count++){
                    [tempCampaign addObject:[[result objectForKey:@"campaigns"] objectAtIndex:count]];
                }
                
                campaignsArray = [[SharedMethod new] processCampaign:tempCampaign andCurrentArray:campaignsArray];
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:[[result objectForKey:@"categories"] objectForKey:@"menu"] forKey:MENU_PARENTS];
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"gender"] forKey:GENDER];
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"race"] forKey:RACE];
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"states"] forKey:STATES];
            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"enquiry_categories"] forKey:TOPICS];
            
            if([[result objectForKey:@"pages"] isKindOfClass:[NSDictionary class]]){
                NSString *terms = [[result objectForKey:@"pages"] objectForKey:@"terms"];
                NSString *privacy = [[result objectForKey:@"pages"] objectForKey:@"privacy"];
                NSString *faq = [[result objectForKey:@"pages"] objectForKey:@"faq"];
                
                [[NSUserDefaults standardUserDefaults] setObject:terms forKey:TERMS_CONDITION];
                [[NSUserDefaults standardUserDefaults] setObject:privacy forKey:PRIVACY];
                [[NSUserDefaults standardUserDefaults] setObject:faq forKey:FAQ];
            }
            
            if([campaignsArray count]>0){
                tableRows = 7;
            } else{
                tableRows = 5;
            }
            
            [self.tableView reloadData];
        }
    }
}

-(BOOL)checkAppVersion{
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
//    NSString *appID = @"com.appandus.loudspeaker";
//    NSLog(@"appID - %@",appID);
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
    NSData* data = [NSData dataWithContentsOfURL:url];
    NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//    NSLog(@"lookup - %@",lookup);
    if ([lookup[@"resultCount"] integerValue] == 1){
        
        if([[[lookup objectForKey:@"results"] objectAtIndex:0] objectForKey:@"trackId"]){
            myAppID = [[[lookup objectForKey:@"results"] objectAtIndex:0] objectForKey:@"trackId"];
        }
        
        NSString *appStoreVersion = lookup[@"results"][0][@"version"];
        NSString *currentVersion = infoDictionary[@"CFBundleShortVersionString"];
        
        NSArray *store = [appStoreVersion componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
        NSArray *app = [currentVersion componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
        int tempA = 0;
        int tempB = 0;
        NSLog(@"tesx_store = %lu", (unsigned long)[store count]);
        NSLog(@"tesx_app = %lu", (unsigned long)[app count]);
        
        if([store count] == 3 && [app count] == 3) {
            if([[store objectAtIndex:0] isEqualToString:[app objectAtIndex:0]]){
                if([[store objectAtIndex:1] isEqualToString:[app objectAtIndex:1]]){
                    tempA = [[store objectAtIndex:2] intValue];
                    tempB = [[app objectAtIndex:2] intValue];
                    
                    if(tempB < tempA){
                        NSLog(@"Please update to latest version");
                        return YES;
                    }
                } else{
                    tempA = [[store objectAtIndex:1] intValue];
                    tempB = [[app objectAtIndex:1] intValue];
                    
                    //NSLog(@"tesx_tempA = %d", tempA);
                    //NSLog(@"tesx_tempB = %d", tempB);
                    tempB = 2; // Override Please Update checking, comment this out for Production
                    
                    if(tempB < tempA){
                        NSLog(@"Please update to latest version");
                        return YES;
                    }
                }
            } else {
                tempA = [[store objectAtIndex:0] intValue];
                tempB = [[app objectAtIndex:0] intValue];
                if(tempB < tempA){
                    NSLog(@"Please update to latest version");
                    return YES;
                }
            }
        } else if([store count] == 3 && [app count] == 2){
            tempA = [[store objectAtIndex:0] intValue];
            tempB = [[app objectAtIndex:0] intValue];
            if(tempB < tempA){
                NSLog(@"Please update to latest version");
                return YES;
            } else if([[store objectAtIndex:0] isEqualToString:[app objectAtIndex:0]] && [[store objectAtIndex:1] isEqualToString:[app objectAtIndex:1]]){
                NSLog(@"Please update to latest version");
                return YES;
            }
            
        } else if([store count] == 2 && [app count] == 3){
            if([[store objectAtIndex:0] isEqualToString:[app objectAtIndex:0]]){
                if([[store objectAtIndex:1] isEqualToString:[app objectAtIndex:1]]){
                    NSLog(@"Please update to latest version");
                    return YES;
                }
            }
        }
    }
    return NO;
}

-(void)displayUpdate{
    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"msg_Update_App", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"msg_Update_App", nil) message:NSLocalizedString(@"msg_Update_App", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)displayForceUpdate{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"msg_Update_App", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *updateAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Update", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        NSString *simple = @"itms-apps://itunes.apple.com/app/id1209322067";
        NSString *appStoreUrl = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@",myAppID];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appStoreUrl]];
        exit(0);
    }];
    
    UIAlertAction *exitAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Exit", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        exit(0);
    }];
    
    [alertController addAction:exitAction];
    [alertController addAction:updateAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)sendStatistics{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    NSMutableArray *temp = [[NSUserDefaults standardUserDefaults] objectForKey:STATISTICS];
    if([temp count]>0){
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        [params setObject:[SharedMethod getTokenWithRoute:@"user/stats/action" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        [params setObject:temp forKey:@"data"];
        
        [WebAPI sendStatistics:params andSuccess:^(id successBlock) {
            NSDictionary *result = successBlock;
            if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:STATISTICS];
            }
        }];
    }
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableRows;
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        BannerCell *cell = (BannerCell*)[tableView dequeueReusableCellWithIdentifier:@"bannerCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BannerCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.imagePager setDelegate:self];
        [cell.imagePager setDataSource:self];
        
        [cell.imagePager setImageCounterDisabled:YES];
        [cell.imagePager setSlideshowTimeInterval:3.0f];

        SDWebImageManager *manager = [SDWebImageManager sharedManager];

        bannerImagesArray = [[NSMutableArray alloc] init];
//        if([bannerImagesArray count]>0){
//            [bannerImagesArray removeAllObjects];
//        }
//        dispatch_async(dispatch_get_main_queue(), ^{
            NSMutableArray *tempArray = [bannerArray mutableCopy];
            for(int count=0; count<[bannerArray count]; count++){
                NSDictionary *temp = [bannerArray objectAtIndex:count];
                NSObject *tempObject = [temp objectForKey:@"image"];
            
            
                if([temp objectForKey:@"image"] && [tempObject isKindOfClass:[NSDictionary class]]){
                    if([[temp objectForKey:@"image"] objectForKey:@"src"]){
                        [[manager imageDownloader] downloadImageWithURL:[NSURL URLWithString:[[temp objectForKey:@"image"] objectForKey:@"src"]]
                                                                options:0
                                                               progress:nil
                                                              completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                                                                  [bannerImagesArray addObject:image];
                                                                  [cell.imagePager reloadData];
                                                                  [cell.imagePager reloadInputViews];
                                                              }];
                    }
                } else{
                    tempArray = [bannerArray mutableCopy];
                    [tempArray removeObject:temp];
                }
            }
            [bannerArray removeAllObjects];
            [bannerArray addObjectsFromArray:tempArray];    
//        });
        
//        dispatch_async(dispatch_get_main_queue(), ^{
            [cell.imagePager.pageControl setCurrentPageIndicatorTintColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
            [cell.imagePager.pageControl setPageIndicatorTintColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
            [cell.imagePager.pageControl setCenter:CGPointMake(CGRectGetWidth(cell.imagePager.frame)/2, CGRectGetHeight(cell.imagePager.frame)-14)];
            //[cell.imagePager.pageControl setBackgroundColor:[[SharedMethod new] colorWithHexString:BLACK_COLOR andAlpha:0.7f]];
		[cell.imagePager.pageControl setBackgroundColor:[UIColor clearColor]];
//        });
        
        return cell;
    } else if(indexPath.row==1){
        HomeHeaderCell *cell = (HomeHeaderCell*)[tableView dequeueReusableCellWithIdentifier:@"homeHeaderCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeHeaderCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.lblTitle setText:NSLocalizedString(@"lbl_Flash", nil)];
        
        return cell;
    } else if(indexPath.row==2){
        FlashHomeCell *cell = (FlashHomeCell*)[tableView dequeueReusableCellWithIdentifier:@"flashHomeCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FlashHomeCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //cell.isFlash = YES;
            cell.flashArray = flashCategoryArray;
            [cell setupCollectionView];
            cell.flashDelegate = self;
            [cell.collectionView reloadData];
        });
        
        return cell;
    } else if(indexPath.row==3){
        HomeHeaderCell *cell = (HomeHeaderCell*)[tableView dequeueReusableCellWithIdentifier:@"homeHeaderCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeHeaderCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.lblTitle setText:NSLocalizedString(@"lbl_Promotion", nil)];
        
        return cell;
    } else if(indexPath.row==4){
        PromotionHomeCell *cell = (PromotionHomeCell*)[tableView dequeueReusableCellWithIdentifier:@"promotionHomeCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PromotionHomeCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            cell.isPromo = YES;
            cell.promotionsArray = promotionCategoryArray;
            [cell setupCollectionView];
            cell.promotionDelegate = self;
            [cell.collectionView reloadData];
        });

        
        return cell;
    } else if(indexPath.row==5){
        HomeHeaderCell *cell = (HomeHeaderCell*)[tableView dequeueReusableCellWithIdentifier:@"homeHeaderCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeHeaderCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.lblTitle setText:NSLocalizedString(@"lbl_Events", nil)];
        
        return cell;
    } else if(indexPath.row==6){
        PromotionHomeCell *cell = (PromotionHomeCell*)[tableView dequeueReusableCellWithIdentifier:@"promotionHomeCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PromotionHomeCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.isPromo = NO;
            cell.promotionsArray = eventCategoryArray;
            [cell setupCollectionView];
            cell.promotionDelegate = self;
            [cell.collectionView reloadData];
        });
        
        return cell;
    } else if(indexPath.row==7){
        HomeHeaderCell *cell = (HomeHeaderCell*)[tableView dequeueReusableCellWithIdentifier:@"homeHeaderCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeHeaderCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.lblTitle setText:NSLocalizedString(@"lbl_Vouchers_V2", nil)];
        
        return cell;
    } else if(indexPath.row==8){
        VoucherHomeCell *cell = (VoucherHomeCell*)[tableView dequeueReusableCellWithIdentifier:@"voucherHomeCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VoucherHomeCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.campaignsArray = campaignsArray;
            [cell setupCollectionView];
            cell.voucherDelegate = self;
            [cell.collectionView reloadData];
        });
        return cell;
    }

    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==2){
        PromotionListVC *promotionListVC = [storyboard instantiateViewControllerWithIdentifier:@"promotionListVC"];
        promotionListVC.title = NSLocalizedString(@"nav_Promotion_V2", nil);
        promotionListVC.isPromo = YES;
        [self.navigationController pushViewController:promotionListVC animated:YES]; 
    }
    
    /*
    int maximum = [PROMOTION_LIMIT intValue];
    if(hasAnnouncement){
        if((indexPath.row==2) || (indexPath.row==0)){
            //Perform Nothing//
        } else if(indexPath.row>2 && indexPath.row-3 < maximum){
            PromotionDetailVC *promotionDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"promotionDetailVC"];
            promotionDetailVC.selectedPromotion = [promotionsArray objectAtIndex:indexPath.row-4];
            [self.navigationController pushViewController:promotionDetailVC animated:YES];
        } else{
            PromotionListVC *promotionListVC = [storyboard instantiateViewControllerWithIdentifier:@"promotionListVC"];
            [self.navigationController pushViewController:promotionListVC animated:YES];
        }
    } else{
        if((indexPath.row==2) || (indexPath.row==0)){
            //Perform Nothing//
        } else if(indexPath.row>2 && indexPath.row-2 < maximum){
            PromotionDetailVC *promotionDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"promotionDetailVC"];
            promotionDetailVC.selectedPromotion = [promotionsArray objectAtIndex:indexPath.row-3];
            [self.navigationController pushViewController:promotionDetailVC animated:YES];
        } else{
            PromotionListVC *promotionListVC = [storyboard instantiateViewControllerWithIdentifier:@"promotionListVC"];
            [self.navigationController pushViewController:promotionListVC animated:YES];
        }
    }
     */
}

/*
-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(hasAnnouncement){
        if(indexPath.row>=4){
            PromotionCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.imgView.layer.sublayers = nil;
            cell.contentsView.layer.sublayers = nil;
        }
    } else{
        if(indexPath.row>2){
            PromotionCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.imgView.layer.sublayers = nil;
            cell.contentsView.layer.sublayers = nil;
        }
    }
}
*/

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        return [UIScreen mainScreen].bounds.size.width/2;
    } else if(indexPath.row==1 || indexPath.row==3 || indexPath.row==5){
        return 42;
    } else if(indexPath.row==2 || indexPath.row==4){
        return 200;
    } else if(indexPath.row==6){
        return 170;
    }
    
    /*
    int maximum = [PROMOTION_LIMIT intValue];
    
    if(indexPath.row==0){
        return 58;
    } else if(indexPath.row==1){
        return 128;//158//228
    }
    
    if(hasAnnouncement){
        if(indexPath.row==2){
            return [UIScreen mainScreen].bounds.size.width/2;
        } else if(indexPath.row==3){
            return 58;
        } else if(indexPath.row>2 && indexPath.row-3 < maximum){
            return ([UIScreen mainScreen].bounds.size.width/3)+80;
        } else{
            return ([UIScreen mainScreen].bounds.size.width/3)+20;
        }
    } else{
        if(indexPath.row==2){
            return 58;
        } else if(indexPath.row>2 && indexPath.row-2 < maximum){
            return ([UIScreen mainScreen].bounds.size.width/3)+80;
        } else{
            return ([UIScreen mainScreen].bounds.size.width/3)+20;
        }
    }
     */
    return UITableViewAutomaticDimension;
}

//Hide navigation bar when scroll//
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    if(scrollView.contentOffset.y <=100){
//        [self.navigationController setHidesBarsOnSwipe:NO];
//        [self.navigationController setNavigationBarHidden:NO animated:YES];
//    } else{
//        [self.navigationController setHidesBarsOnSwipe:YES];
//        [self.navigationController setNavigationBarHidden:YES animated:YES];
//    }
//}


#pragma mark - KIImagePager Delegate
-(NSArray *)arrayWithImages:(KIImagePager *)pager{
    if(bannerImagesArray && [bannerImagesArray count]>0){
		//NSArray *tmpArr = [NSArray arrayWithObject:[bannerImagesArray firstObject]];
		return bannerImagesArray;;
    } else{
        return @[];
    }
}

#pragma mark - KIImagePager Data Source
-(void)imagePager:(KIImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index{
//    NSLog(@"Selected %ld",index);
    if([bannerImagesArray count]>0){
        NSDictionary *selectedBanner = [bannerArray objectAtIndex:index];
        NSString *url = [selectedBanner objectForKey:@"url"];
        
        if(url.length>0){
            url = [url stringByReplacingOccurrencesOfString:@"\\" withString:@""];
            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]]){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:nil];
            }
        } else{
            WebVC *webVC = [storyboard instantiateViewControllerWithIdentifier:@"webVC"];
            webVC.normalContent = [selectedBanner objectForKey:@"content"];
            webVC.fromMenu=NO;
            webVC.navigationTitle = @"";
            [webVC setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:webVC animated:YES];
        }
    }
}

-(UIImage *)placeHolderImageForImagePager:(KIImagePager *)pager{
    return [UIImage imageNamed:@"bg_promotion"];
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(KIImagePager*)pager{
    return UIViewContentModeScaleAspectFit;
}

#pragma mark - Actions
-(void)refreshAction:(UIRefreshControl *)refreshControl {
    [refreshControl endRefreshing];
    [self retrieveHomeDetails];
}

-(IBAction)bbtnInboxDidPressed:(id)sender{
    /*
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
        NotificationVC *notificationVC = [storyboard instantiateViewControllerWithIdentifier:@"notificationVC"];
        [notificationVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:notificationVC animated:YES];
    } else{
        [self.tabBarController setSelectedIndex:2];
    }
     */
}

-(IBAction)bbtnBookingDidPressed:(id)sender{
    if([[NSUserDefaults standardUserDefaults] boolForKey:ALLOW_GUEST_BOOKING] == 0){
        BookingVC *bookingVC = [storyboard instantiateViewControllerWithIdentifier:@"bookingVC"];
        [bookingVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:bookingVC animated:YES];
        /*
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            BookingVC *bookingVC = [storyboard instantiateViewControllerWithIdentifier:@"bookingVC"];
            [bookingVC setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:bookingVC animated:YES];
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"msg_Booking_Guest_Not_Allow", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        }*/
    } else{
        BookingVC *bookingVC = [storyboard instantiateViewControllerWithIdentifier:@"bookingVC"];
        [bookingVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:bookingVC animated:YES];
    }
}

-(IBAction)bbtnPrivilegeDidPressed:(id)sender{
    PrivilegeVC *privilegeVC = [storyboard instantiateViewControllerWithIdentifier:@"privilegeVC"];
    [privilegeVC setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:privilegeVC animated:YES];
}

-(void)viewMoreVouchers{
    VoucherListVC *voucherListVC = [storyboard instantiateViewControllerWithIdentifier:@"voucherListVC"];
    [self.navigationController pushViewController:voucherListVC animated:YES];
}

-(void)selectVoucher:(int)index{
    Campaign *campaign = [campaignsArray objectAtIndex:index];
    Voucher *selectedVoucher = campaign.voucher;
    
    [[SharedMethod new] addStatistics:@"post_view" itemID:selectedVoucher.ID];
    VoucherDetailVC *voucherDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"voucherDetailVC"];
    voucherDetailVC.selectedCampaign = campaign;
    voucherDetailVC.selectedVoucher = selectedVoucher;
    [self.navigationController pushViewController:voucherDetailVC animated:YES];
}

-(void)selectFlash:(int)index{
    
    Flash *flash = [[Flash alloc] init];
    flash = [flashCategoryArray objectAtIndex:index];
    
    FlashListVC *flashListVC = [storyboard instantiateViewControllerWithIdentifier:@"flashListVC"];
    flashListVC.flash = flash;
    //flashListVC.myTitle = flash.name;
    
    //Category *category = [[Category alloc] init];
    //category = [promotionCategoryArray objectAtIndex:index];
    //flashListVC.categoryID = category.ID;
    
    [self.navigationController pushViewController:flashListVC animated:YES];
}

-(void)selectPromotion:(int)index isPromo:(BOOL)result{
    Category *category = [[Category alloc] init];
    PromotionListVC *promotionListVC = [storyboard instantiateViewControllerWithIdentifier:@"promotionListVC"];
    
    if(result){
        category = [promotionCategoryArray objectAtIndex:index];
        promotionListVC.myTitle = category.name;//NSLocalizedString(@"nav_Promotion_V2", nil);
        promotionListVC.isPromo = YES;
    } else{
        category = [eventCategoryArray objectAtIndex:index];
        promotionListVC.myTitle = category.name;//NSLocalizedString(@"nav_Event_V2", nil);
        promotionListVC.isPromo = NO;
    }
    
    promotionListVC.categoryID = category.ID;
    [self.navigationController pushViewController:promotionListVC animated:YES];
}

-(IBAction)btnCloseAdvertisementDidPressed:(id)sender{
    for(UIView *temp in self.view.window.subviews){
        if(temp.tag == 901){
            [temp removeFromSuperview];
        }
    }
    [advertisementVC removeFromParentViewController];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:DISPLAY_ADVERTISEMENT];
}

@end
