//
//  MenuDetailVC.m
//  Oasis
//
//  Created by Wong Ryan on 30/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "MenuDetailVC.h"
#import "MenuDetailCell.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "WebVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <UIView+Toast.h>
#import "GalleryVC.h"
#import "SVProgressHUD.h"
#import <MessageUI/MessageUI.h>

@interface MenuDetailVC ()<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate,MFMessageComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MenuDetailVC{
    UIStoryboard *storyboard;
    UIAlertController *alertController;
    int cellSize;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBar];
    [self setupUI];
    [self setupData];
    [self setupDelegates];
}

-(void)setupNavigationBar{
    [self.navigationItem setTitle:self.navigationTitle];
    
    if(self.navigationTitle.length<=0){
        [self.navigationItem setTitle:self.selectedMenu.title];
    }
}

-(void)setupUI{
    [self.tableView setEstimatedRowHeight:45];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    [self.view setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.tableView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

-(void)setupData{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

-(void)setupDelegates{
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
//    [self.navigationController.view.layer addAnimation:[[SharedMethod new] setupTransitionAnimation:TRANSITION_BOTTOM] forKey:kCATransition];
//    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)bbtnShareDidPressed:(id)sender {
    NSURL *myWebsite;
    NSArray *objectsToShare;
//    NSLog(@"selectedMenu.url = %@",self.selectedMenu.url);
    if(self.selectedMenu.url.length>0){
        myWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"%@",self.selectedMenu.url]];
    } else{
        myWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"%@p/%@",[[NSUserDefaults standardUserDefaults] objectForKey:SHARE_BASE],self.selectedMenu.ID]];
//        NSLog(@"myWebsite = %@",myWebsite.absoluteString);
    }
    objectsToShare = @[myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)btnGalleryDidPressed:(id)sender{
    GalleryVC *galleryVC = [storyboard instantiateViewControllerWithIdentifier:@"galleryVC"];
    galleryVC.image = self.selectedMenu.cachedImage;
    [galleryVC.view setTag:100];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view.window addSubview:galleryVC.view];
        [self addChildViewController:galleryVC];
        [galleryVC didMoveToParentViewController:self];
    });
}

#pragma mark - WebView Delegate
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss];
    
    if(webView.tag==100){
        CGRect frame = webView.frame;
        frame.size.height = 1;
        webView.frame = frame;
        CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
        frame.size = fittingSize;
        webView.frame = frame;
        
        cellSize = fittingSize.height+8;
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
    }
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
//    NSLog(@"url is %@",request.URL.absoluteString);
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        NSURL *url = request.URL;
        
        if([url.absoluteString containsString:@"www"] || [url.absoluteString containsString:@"http"]){
            if([[UIApplication sharedApplication] canOpenURL:url]){
                WebVC *webVC = [storyboard instantiateViewControllerWithIdentifier:@"webVC"];
                webVC.selectedURL = url;
                [self.navigationController pushViewController:webVC animated:YES];
            }
        } else if([url.absoluteString hasPrefix:@"tel:"]){
            if([[UIApplication sharedApplication] canOpenURL:url]){
                NSString *tel = [[url.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                alertController = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"%@ %@?",NSLocalizedString(@"msg_MakeCall", nil),[tel substringFromIndex:4]] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Call", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [[UIApplication sharedApplication] openURL:url];
                }];
                
                UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_No", nil) style:UIAlertActionStyleDefault handler:nil];
                
                [alertController addAction:noAction];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        } else if([[url scheme] isEqual:@"mailto"]){
            if ([MFMailComposeViewController canSendMail]) {
                [[UIApplication sharedApplication] openURL:url];
            } else{
                alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Send_Mail_Fail", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [alertController removeFromParentViewController];
                }];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        
        return NO;
    }
    return YES;
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MenuDetailCell *cell = (MenuDetailCell*)[tableView dequeueReusableCellWithIdentifier:@"menuDetailCell"];
    if(cell==nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MenuDetailCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if(indexPath.row==0){
        [cell.imgView setHidden:NO];
        [cell.lblTitle setHidden:YES];
        [cell.separatorView setHidden:YES];
        [cell.webView setHidden:YES];
        
        if(self.selectedMenu.image.length>0){
            [[SharedMethod new] setLoadingIndicatorOnImage:cell.imgView];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [cell.imgView setContentMode:UIViewContentModeScaleAspectFit];
                    [cell.imgView setClipsToBounds:YES];
                    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:self.selectedMenu.image] placeholderImage:[UIImage imageNamed:@"bg_menu"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        self.selectedMenu.cachedImage = image;
                        for(UIView *view in cell.imgView.subviews){
                            if(view.tag==100){
                                [view removeFromSuperview];
                            }
                        }
                    }];
                });
            });
        } else{
            [cell.imgView setImage:[UIImage imageNamed:@"bg_menu"]];
        }
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnGalleryDidPressed:)];
        [cell addGestureRecognizer:tapGesture];
        
        return cell;
    }
    /*else if(indexPath.row==1){
        [cell.imgView setHidden:YES];
        [cell.lblTitle setHidden:NO];
        [cell.separatorView setHidden:NO];
        [cell.webView setHidden:YES];
        
        [cell.lblTitle setText:self.selectedMenu.title];
        
        return cell;
    }*/
    else{
        [cell.imgView setHidden:YES];
        [cell.lblTitle setHidden:YES];
        [cell.separatorView setHidden:YES];
        
        [cell.webView setBackgroundColor:[UIColor whiteColor]];
        [cell.webView setOpaque:NO];
        [cell.webView setTag:100];
        [cell.webView.scrollView setScrollEnabled:NO];
        
//        NSString *finalString = [NSString stringWithFormat:@"<html><head><meta name='viewport' http-equiv='Content-Type' content='text/html;charset=UTF-8;width=device-width,initial-scale=0.85'></head><body><font face='%@' color=#474747>%@</body></html>", FONT_REGULAR, self.selectedMenu.content];
//
//        [cell.webView loadHTMLString:finalString baseURL:nil];
        [cell.webView setDelegate:self];
        
        NSString *myBase = [NSString stringWithFormat:@"<html><head><script>function nav(url){document.location.href = url;}</script><meta content=\"html; charset=utf-8\" http-equiv=\"Content-Type\"/><meta name=\"viewport\" content=\"width=device-width\"/><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/></head><body><font face='%@' color=#474747>%@</body></html>", FONT_REGULAR, self.selectedMenu.content];
        
        //                NSLog(@"mybase - %@",myBase);
        [cell.webView loadHTMLString:myBase baseURL:[[NSBundle mainBundle] URLForResource:@"style" withExtension:@"css"]];

        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"Selected row %d",(int)indexPath.row);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        return [UIScreen mainScreen].bounds.size.width;//-50;
    }
    /*else if(indexPath.row==1){
        return UITableViewAutomaticDimension;//return 60;
    }*/
    else{
        if(cellSize>44){
            return cellSize;
        } else{
            return 44;
        }
    }
}

@end
