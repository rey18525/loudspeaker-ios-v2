//
//  MenuDetailVC.h
//  Oasis
//
//  Created by Wong Ryan on 30/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Menu.h"

@interface MenuDetailVC : UIViewController
@property (nonatomic) NSString *navigationTitle;
@property (nonatomic) Menu *selectedMenu;
@end
