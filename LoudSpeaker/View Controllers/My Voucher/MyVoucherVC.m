//
//  MyVoucherVC.m
//  Oasis
//
//  Created by Wong Ryan on 26/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "MyVoucherVC.h"
#import "VoucherTableCell.h"
#import "SharedMethod.h"
#import "HomeVC.h"
#import "Constant.h"
#import "Reward.h"
#import "Campaign.h"
#import "Voucher.h"
#import "WebAPI.h"
#import "SVProgressHUD.h"
#import "VoucherDetailVC.h"
#import "EmptyTableCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <PDKeychainBindings.h>

@interface MyVoucherVC ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@import Firebase;

@implementation MyVoucherVC{
    UIStoryboard *storyboard;
    BOOL isEmpty;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setupInitialization];
    [self setupData];
    [self setupDelegates];
    [self setupRefreshControl];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:FROM_QR_VOUCHER_DETAIL]){
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:FROM_QR_VOUCHER_DETAIL];
        [self retrieveAll];
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:DISPLAY_REWARD]){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:DISPLAY_REWARD];
    }
}

#pragma mark - Setup Initialization
-(void)setupUI{
    [self.view setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.tableView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

-(void)setupInitialization{
    [self.navigationItem setTitle:NSLocalizedString(@"nav_MyVoucher_V2", nil)];
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

-(void)setupData{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:FROM_QR_VOUCHER_DETAIL];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:MY_VOUCHER];
//    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"popToRoot"];
    if([self.myVouchers count]>0){
        isEmpty = NO;
    } else{
        isEmpty = YES;
    }
}

-(void)setupDelegates{
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
}

-(void)setupRefreshControl{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl setTintColor:[UIColor blackColor]];
    [refreshControl addTarget:self action:@selector(refreshAction:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}


-(NSMutableArray*)filterVouchers{
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    NSMutableArray *tempReward = [[NSMutableArray alloc] init];
    NSMutableArray *tempRewards = [[NSMutableArray alloc] init];
    NSMutableArray *vouchers = [[NSMutableArray alloc] init];
    temp = [[[NSUserDefaults standardUserDefaults] objectForKey:USER_POINT_DETAIL] objectForKey:@"vouchers"];
    
    for(NSDictionary *dict in temp){
        NSMutableArray *_reward = [[NSMutableArray alloc] init];
        [_reward addObject:dict];
        [tempReward addObject:_reward];
    }
    
    tempRewards = [[SharedMethod new] processReward:tempReward andCurrentArray:tempRewards];
    for(Reward *r in tempRewards){
        if([r.type isEqualToString:@"voucher"]){
            [vouchers addObject:r];
        }
    }
    return vouchers;
}

-(void)retrieveAll{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSString *deviceToken = [[SharedMethod new] checkDeviceToken];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        [params setObject:PLATFORM forKey:@"platform"];
        [params setObject:deviceToken forKey:@"push_token"];
        [params setObject:[SharedMethod getTokenWithRoute:@"user/all" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];

        #ifdef DEBUG
        NSLog(@"Request All - %@",params);
        #endif
        
        [WebAPI retrieveAll:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.tabBarController setSelectedIndex:0];
                }
            } else{
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%.2f",[[result objectForKey:@"balance"] floatValue]] forKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] setObject:result forKey:USER_POINT_DETAIL];
                    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"user"] forKey:USER_DETAIL];
                    
                    int badgeNo = [[result objectForKey:@"inbox_unread"] intValue];
                    [[NSUserDefaults standardUserDefaults] setInteger:badgeNo forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeNo];
                  
                    if([self.myVouchers count]>0){
                        [self.myVouchers removeAllObjects];
                    }
                    
                    self.myVouchers = [self filterVouchers];
                    [self.tableView reloadData];
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"Message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender{
    if(self.fromProfile){
        [self.navigationController popViewControllerAnimated:YES];
    } else{
        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:0] animated:YES];
    }
}

-(void)refreshAction:(UIRefreshControl *)refreshControl {
    [refreshControl endRefreshing];
    [self retrieveAll];
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([self.myVouchers count]>0){
        return [self.myVouchers count];
    } else{
        return 1;
    }
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!isEmpty){
        VoucherTableCell *cell = (VoucherTableCell*)[tableView dequeueReusableCellWithIdentifier:@"voucherCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VoucherTableCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        Reward *reward = [self.myVouchers objectAtIndex:indexPath.row];
        Campaign *campaign = reward.campaign;
        Voucher *voucher = campaign.voucher;
        
        NSString *temp = reward.expires_at;
        
        if(temp.length<=0){
            [cell.expireView setHidden:YES];
        } else{
            [cell.expireView setHidden:NO];
            [cell.lblExpireDate setText:[[SharedMethod new] convertToNotificationDateFormat:reward.expires_at]];
        }
        
        [cell.lblDescription setText:voucher.title];
        
        if(voucher.image.length>0){
//            [[SharedMethod new] setLoadingIndicatorOnImage:cell.imgVoucher];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [cell.imgVoucher setContentMode:UIViewContentModeScaleAspectFit];
                    [cell.imgVoucher setClipsToBounds:YES];
                    [cell.imgVoucher sd_setImageWithURL:[NSURL URLWithString:voucher.image] placeholderImage:[UIImage imageNamed:@"bg_voucher"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        
                        NSString *redeemed_at = reward.redeem_at;
                        NSLog(@"redeemed_at value = %@",redeemed_at);
                        if(redeemed_at.length>0){
                            UIImage *temp = [[SharedMethod new] getBlackAndWhiteVersionOfImage:image];
                            [cell.imgVoucher setImage:temp];
                            voucher.cachedImage = temp;
                        } else{
                            voucher.cachedImage = image;
                        }
//                        for(UIView *view in cell.imgVoucher.subviews){
//                            if(view.tag==100){
//                                [view removeFromSuperview];
//                            }
//                        }
                    }];
                });
            });
            
        } else{
            [cell.imgVoucher setImage:[UIImage imageNamed:@"bg_voucher"]];
        }
        
        NSString *redeemed_at = reward.redeem_at;
        if(redeemed_at.length>0){
            [cell.imgUsed setHidden:NO];
            [cell.imgUsed setImage:[UIImage imageNamed:@"img_voucher_used"]];
        } else{
            [cell.imgUsed setHidden:YES];
        }

        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [[cell.expireView layer] setCornerRadius:cell.expireView.frame.size.width/2];
////            [[SharedMethod new] setGradient:cell.imgVoucher];
//        });
        
        return cell;
    } else{
        EmptyTableCell *cell = (EmptyTableCell*)[tableView dequeueReusableCellWithIdentifier:@"emptyCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EmptyTableCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.imgIcon setImage:[UIImage imageNamed:@"ic_empty"]];
        [cell.lblDescription setText:NSLocalizedString(@"lbl_Empty", nil)];
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!isEmpty){
        VoucherTableCell *myCell = [tableView dequeueReusableCellWithIdentifier:@"voucherCell"];
        myCell.imgVoucher.layer.sublayers = nil;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!isEmpty && [self.myVouchers count]>0){
        Reward *reward = [self.myVouchers objectAtIndex:indexPath.row];
        Campaign *campaign = reward.campaign;
        Voucher *voucher = campaign.voucher;
        
//        [[SharedMethod new] addStatistics:@"post_view" itemID:voucher.ID];
        
        VoucherDetailVC *voucherDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"voucherDetailVC"];
        [voucherDetailVC setHidesBottomBarWhenPushed:YES];
        voucherDetailVC.selectedReward = reward;
        voucherDetailVC.selectedCampaign = reward.campaign;
        voucherDetailVC.selectedVoucher = voucher;
        voucherDetailVC.isMyVoucher = YES;
        
        if(reward.redeem_at.length>0){
            voucherDetailVC.isRedemeed = YES;
        }
        [self.navigationController pushViewController:voucherDetailVC animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isEmpty){
        return self.tableView.frame.size.height+64;
    } else{
        return ([UIScreen mainScreen].bounds.size.width/2)+50;
    }
}

@end
