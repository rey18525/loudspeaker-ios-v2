//
//  AboutVC.m
//  Oasis
//
//  Created by Wong Ryan on 15/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "AboutVC.h"
#import "ContactVC.h"
#import "Constant.h"
#import "WebVC.h"
#import "SharedMethod.h"
#import "HomeVC.h"
#import <CoreLocation/CoreLocation.h>

@import MapKit;

@interface AboutVC ()
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (weak, nonatomic) IBOutlet UILabel *lblVersion;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet UIButton *btnExpand;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *panGesture;
@property (weak, nonatomic) IBOutlet UITextView *tvDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsViewBottom;
@end

@implementation AboutVC{
    UIStoryboard *storyboard;
    UIAlertController *alertController;
    NSDictionary *homeDetail;
    NSArray *labelsArray;
    BOOL isOpen;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    
    if(SYSTEM_VERSION_LESS_THAN(@"10")){
        [self updateTabBarController];
    }
    
    [self setupUI];
    [self setupData];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.navigationItem setTitle:NSLocalizedString(@"nav_About_V2", nil)];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
//    [self.panGesture addTarget:self action:@selector(panRecognizer:)];
    
//    [self.buttonsView setBackgroundColor:[UIColor clearColor]];
    [self.buttonsView setBackgroundColor:[[SharedMethod new] colorWithHexString:BUTTONS_BG_COLOR andAlpha:0.7]];
//    [self.buttonsView setAlpha:0.7f];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.btnExpand setImage:[UIImage imageNamed:@"ic_about_up"] forState:UIControlStateNormal];
    [self.buttonsViewBottom setConstant:-110];
    isOpen = NO;
}

#pragma mark - Setup Initialization
-(void)updateTabBarController{
    UITabBar *tabBar = self.tabBarController.tabBar;
    UITabBarItem *tab1 = [[tabBar items] objectAtIndex:4];
    [tab1 setImage:[[UIImage imageNamed:@"ic_bottombar_about"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab1 setSelectedImage:[[UIImage imageNamed:@"ic_bottombar_about_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
}

-(void)setupUI{
    [self.tvDescription setShowsVerticalScrollIndicator:NO];
    [self.btnExpand setImage:[UIImage imageNamed:@"ic_about_up"] forState:UIControlStateNormal];
    [self.lblVersion setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
}

-(void)setupData{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    isOpen = NO;
    
    labelsArray = @[NSLocalizedString(@"lbl_About_Email", nil),
                    NSLocalizedString(@"lbl_About_Web", nil),
                    NSLocalizedString(@"lbl_About_FB", nil),
                    NSLocalizedString(@"lbl_About_Instagram", nil),
                    NSLocalizedString(@"lbl_About_Youtube", nil),
                    NSLocalizedString(@"lbl_About_Faq", nil),
                    NSLocalizedString(@"lbl_About_TnC", nil),
                    NSLocalizedString(@"lbl_About_Privacy", nil)];
    
    for(UILabel *lbl in self.labels){
        [lbl setFont:[UIFont fontWithName:FONT_REGULAR size:11.2f]];
        [lbl setTextColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
        [lbl setText:[labelsArray objectAtIndex:lbl.tag-1]];
    }   
    
    [self.tvDescription setHidden:YES];
    
    [self.lblDescription setText:COMPANY_DESCRIPTION];
    [self.lblDescription setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblDescription setFont:[UIFont fontWithName:FONT_REGULAR size:17.0f]];
    [self.lblDescription setTextAlignment:NSTextAlignmentJustified];
    
    [self.view bringSubviewToFront:self.buttonsView];
    
    homeDetail = [[NSUserDefaults standardUserDefaults] objectForKey:HOME_DETAIL];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    [self.lblVersion setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"lbl_Version", nil),version]];
}

- (void)displayRegionCenteredOnMapItem:(MKMapItem*)from {
    CLLocation* fromLocation = from.placemark.location;
    
    // Create a region centered on the starting point with a 10km span
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(fromLocation.coordinate, 10000, 10000);
    
    // Open the item in Maps, specifying the map region to display.
    [MKMapItem openMapsWithItems:[NSArray arrayWithObject:from]
                   launchOptions:[NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSValue valueWithMKCoordinate:region.center], MKLaunchOptionsMapCenterKey,
                                  [NSValue valueWithMKCoordinateSpan:region.span], MKLaunchOptionsMapSpanKey, nil]];
    
}

-(void)panRecognizer:(UIPanGestureRecognizer*)gesture{
    CGPoint velocity = [gesture velocityInView:self.buttonsView];
    if(velocity.x <0 && self.buttonsViewBottom.constant == -70){
        [UIView animateWithDuration:0.5 animations:^{
            [self.buttonsViewBottom setConstant:0];
        } completion:^(BOOL finished) {
            isOpen = YES;
        }];
    } else if(velocity.x >0 && self.buttonsViewBottom.constant == 0){
        [UIView animateWithDuration:0.5 animations:^{
            [self.buttonsViewBottom setConstant:-70];
        } completion:^(BOOL finished) {
            isOpen = NO;
        }];
    }
}

- (IBAction)btnExtendButtonDidPressed:(id)sender {
    if(self.buttonsViewBottom.constant == -110){
        [UIView animateWithDuration:2.0 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self.buttonsViewBottom setConstant:0];
        } completion:^(BOOL finished) {
            isOpen = YES;
            [self.btnExpand setImage:[UIImage imageNamed:@"ic_about_down"] forState:UIControlStateNormal];
        }];
    } else if(self.buttonsViewBottom.constant == 0){
        [UIView animateWithDuration:2.0 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self.buttonsViewBottom setConstant:-110];
        } completion:^(BOOL finished) {
            isOpen = NO;
            [self.btnExpand setImage:[UIImage imageNamed:@"ic_about_up"] forState:UIControlStateNormal];
        }];
    }

}

#pragma mark - Actions
- (IBAction)bbtnShareDidPressed:(id)sender {
    NSURL *myWebsite;
    NSArray *objectsToShare;
    
    myWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:COMPANY_WEBSITE]]];
    objectsToShare = @[myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)btnExpandDidPressed:(id)sender {

}

- (IBAction)btnEmailDidPressed:(id)sender {
    ContactVC *contactVC = [storyboard instantiateViewControllerWithIdentifier:@"contactVC"];
    [contactVC setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:contactVC animated:YES];
}

- (IBAction)btnWebsiteDidPressed:(id)sender {
    WebVC *webVC = [storyboard instantiateViewControllerWithIdentifier:@"webVC"];
    webVC.urlString = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:COMPANY_WEBSITE]];
    webVC.fromMenu=NO;
    webVC.navigationTitle = COMPANY_TITLE;
    [webVC setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:webVC animated:YES];
//    [self.navigationController.view.layer addAnimation:[[SharedMethod new] setupTransitionAnimation:TRANSITION_TOP] forKey:kCATransition];
//    [self.navigationController pushViewController:webVC animated:NO];
}

- (IBAction)btnFacebookDidPressed:(id)sender {
    NSURL *facebookURL = [NSURL URLWithString:COMPANY_FACEBOOK];
    if ([[UIApplication sharedApplication] canOpenURL:facebookURL]){
        [[UIApplication sharedApplication] openURL:facebookURL];
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:COMPANY_FACEBOOK_WEB]];
    }
}

- (IBAction)btnInstagramDidPressed:(id)sender {
    NSURL *instagramURL = [NSURL URLWithString:COMPANY_INSTAGRAM];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]){
        [[UIApplication sharedApplication] openURL:instagramURL];
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:COMPANY_INSTAGRAM_WEB]];
    }
}

- (IBAction)btnYoutubeDidPressed:(id)sender {
    NSURL *youtubeURL = [NSURL URLWithString:COMPANY_YOUTUBE];
    if ([[UIApplication sharedApplication] canOpenURL:youtubeURL]){
        [[UIApplication sharedApplication] openURL:youtubeURL];
    }
    else{
        [[UIApplication sharedApplication] openURL:youtubeURL];
    }
}

- (IBAction)btnPdpaDidPressed:(id)sender {
    WebVC *webVC = [storyboard instantiateViewControllerWithIdentifier:@"webVC"];
    
    NSString *urlString = [[NSUserDefaults standardUserDefaults] objectForKey:FAQ];
    if(urlString.length<=0){
        urlString = [[NSUserDefaults standardUserDefaults] objectForKey:COMPANY_WEBSITE];
    }
    webVC.urlString = urlString;
    webVC.fromMenu=NO;
    [webVC setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:webVC animated:YES];
}

- (IBAction)btnTermsDidPressed:(id)sender {
    WebVC *webVC = [storyboard instantiateViewControllerWithIdentifier:@"webVC"];
//    webVC.urlString = [NSString stringWithFormat:@"%@",COMPANY_WEBSITE];
    
    NSString *urlString = [[NSUserDefaults standardUserDefaults] objectForKey:TERMS_CONDITION];
    if(urlString.length<=0){
        urlString = [[NSUserDefaults standardUserDefaults] objectForKey:COMPANY_WEBSITE];
    }
    webVC.urlString = urlString;
    webVC.fromMenu=NO;
//    webVC.navigationTitle = COMPANY_TITLE;
    [webVC setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:webVC animated:YES];
}

- (IBAction)btnPrivacyDidPressed:(id)sender {
    WebVC *webVC = [storyboard instantiateViewControllerWithIdentifier:@"webVC"];
//    webVC.urlString = [NSString stringWithFormat:@"%@",COMPANY_WEBSITE];
    NSString *urlString = [[NSUserDefaults standardUserDefaults] objectForKey:PRIVACY];
    if(urlString.length<=0){
        urlString = [[NSUserDefaults standardUserDefaults] objectForKey:COMPANY_WEBSITE];
    }
    webVC.urlString = urlString;
    webVC.fromMenu=NO;
//    webVC.navigationTitle = COMPANY_TITLE;
    [webVC setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:webVC animated:YES];
}

@end
