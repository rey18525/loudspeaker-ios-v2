#import "FlashListVC.h"
#import "PromotionCell.h"
#import "SharedMethod.h"
#import "SVProgressHUD.h"
#import "Constant.h"
#import "WebAPI.h"
#import "SQLManager.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "PromotionDetailVC.h"
#import "SearchTableVC.h"
#import <PDKeychainBindings.h>
#import "LoginVC.h"
#import "PaymentGatewayVC.h"
#import <WebKit/WebKit.h>

@interface FlashListVC ()<DismissPaymentGatewayDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *flashImage;
@property (weak, nonatomic) IBOutlet UILabel *lblFlashName;
@property (weak, nonatomic) IBOutlet UIWebView *wvFlashContent;
@property (weak, nonatomic) IBOutlet WKWebView *wkFlashContent;


@property (weak, nonatomic) IBOutlet UIWebView *wvFlashTerms;
@property (weak, nonatomic) IBOutlet UILabel *lblFlashType;
@property (weak, nonatomic) IBOutlet UILabel *lblFlashDays;
@property (weak, nonatomic) IBOutlet UILabel *lblFlashHours;
@property (weak, nonatomic) IBOutlet UILabel *lblFlashMinutes;
@property (weak, nonatomic) IBOutlet UILabel *lblFlashSeconds;
@property (weak, nonatomic) IBOutlet UIButton *btnFlash;
@end

@import Firebase;

@implementation FlashListVC{
    UIStoryboard *storyboard;
    NSMutableArray *flashArray;
    Post *post;
    Price *price;
    bool isOnline;
    bool isTopup;
    bool isGuest;
    bool isMember;
    bool isCash;
    bool isEGuest;
    NSString *flashPriceValue;
    NSDictionary *userDetail;
    
    bool isSoldOut;
    int voucherLimit;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    post = self.flash.post;
    price = self.flash.price;
    userDetail = [[NSUserDefaults standardUserDefaults] objectForKey:USER_DETAIL];
    
    [self setupInitialization];
    [self setupDelegates];
    [self setupData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - Setup Initialization

-(void)setupInitialization{
    if([self.flash.payment_method  isEqual: @"online"]){
        isOnline = YES;
    } else{
        isOnline = NO;
    }
    
    if([self.flash.type isEqual: @"topup"]){
        isTopup = YES;
    } else {
        isTopup = NO;
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_GUEST]){
        isGuest = YES;
    } else {
        isGuest = NO;
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_MEMBER]){
        isMember = YES;
    } else {
        isMember = NO;
    }
    
    if([self.flash.payment_method  isEqual: @"cash"]){
        isCash = YES;
    } else {
        isCash = NO;
    }
    
    if([self.flash.applicable_to isEqual: @"e-guest"]){
        isEGuest = YES;
    } else {
        isEGuest = NO;
    }
    
    voucherLimit = self.flash.voucher_limit;
    
    
    if([self.flash.applicable_to isEqual: @"all"]){
        if(isGuest){
            flashPriceValue = price.e_guest_value;
        }else if(isOnline){
            flashPriceValue = price.e_member_online;
        }else{
            flashPriceValue = price.e_member_wallet;
        }
        
    }else if([self.flash.applicable_to  isEqual: @"e-guest"]) {
        flashPriceValue = price.e_guest_value;
        
    }else if([self.flash.applicable_to isEqual: @"e-member"]) {
        if(isTopup){
            flashPriceValue = price.topup_value;
        }else{
            if(isOnline){
                flashPriceValue = price.e_member_online;
            }else{
                flashPriceValue = price.e_member_wallet;
            }
        }
        
    }
    
    if(isOnline){
        [_btnFlash setTitle:[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"lbl_Flash_Purchase", nil), flashPriceValue] forState:UIControlStateNormal];
    }else if(isCash){
        [_btnFlash setTitle:[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"lbl_Flash_Grab", nil), flashPriceValue] forState:UIControlStateNormal];
    }
    
    [_flashImage sd_setImageWithURL:[NSURL URLWithString:post.image] placeholderImage:[UIImage imageNamed:@"bg_menu"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    [_lblFlashName setText:self.flash.name];
    
    [_lblFlashType setText:[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"lbl_Flash_Type", nil), self.flash.applicable_to]];
    
    NSMutableString *contentString = [post.content mutableCopy];
    if([contentString length] != 0){
        if ([contentString rangeOfString:@"table class"].location == NSNotFound) {
            NSLog(@"ASDFX - string does not contain table class");
        } else {
            NSLog(@"ASDFX - string contains table class!");
            
            long newIndex = [contentString rangeOfString:@"table class"].location + 6; // table + " "
            [contentString insertString:@"border=1 " atIndex:newIndex];
            
            post.content = contentString;
        }
        
        NSLog(@"ASDFX_FLASH_content - %@", post.content);
        [_wvFlashContent loadHTMLString:[NSString stringWithFormat:@"%@", post.content] baseURL:nil];
    }
    
    [_wvFlashTerms loadHTMLString:[NSString stringWithFormat:@"%@", post.terms] baseURL:nil];
    
    double remainingTime = [self.flash.expiry_date_millisecond doubleValue] - ([[NSDate date] timeIntervalSince1970]);
    
    if(remainingTime > 0) {
        remainingTime =  remainingTime/1000;
        NSInteger mSeconds = fmod(remainingTime, 60);
        NSInteger mMinutes = fmod((remainingTime / 60), 60);
        NSInteger mHours = fmod((remainingTime / (60*60)), 60);
        NSInteger mDays = fmod(remainingTime / ((60*60)*24), 24);
        
        if(mDays < 10) {
            [_lblFlashDays setText:[NSString stringWithFormat:@"0%ld", mDays]];
        } else {
            [_lblFlashDays setText:[NSString stringWithFormat:@"%ld", mDays]];
        }
        
        if(mHours < 10) {
            [_lblFlashHours setText:[NSString stringWithFormat:@"0%ld", mHours]];
        } else {
            [_lblFlashHours setText:[NSString stringWithFormat:@"%ld", mHours]];
        }
        
        if(mMinutes < 10) {
            [_lblFlashMinutes setText:[NSString stringWithFormat:@"0%ld", mMinutes]];
        } else {
            [_lblFlashMinutes setText:[NSString stringWithFormat:@"%ld", mMinutes]];
        }
        
        if(mSeconds < 10) {
            [_lblFlashSeconds setText:[NSString stringWithFormat:@"0%ld", mSeconds]];
        } else {
            [_lblFlashSeconds setText:[NSString stringWithFormat:@"%ld", mSeconds]];
        }
    }
}

-(void)setupDelegates{
    
}

-(void)setupData{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self.navigationItem setTitle:NSLocalizedString(@"lbl_Flash_Details", nil)];
    
}

-(void)showNonRefundableMessage:(NSString*)price eWallet:(bool)ewallet eWalletAmount:(NSString*)eAmount {
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle: @""
                                message:[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"lbl_Flash_Non_Refundable", nil), price]
                                preferredStyle:UIAlertControllerStyleAlert
                                ];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if([[SharedMethod new] checkNetworkConnectivity]){
            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
            
            NSDictionary *params;
            
            NSString *userBalance = [[NSUserDefaults standardUserDefaults] objectForKey:USER_BALANCE];
            userBalance = @"0.00";
            
            if(ewallet){
                if(userBalance < flashPriceValue){
                    [SVProgressHUD dismiss];
                    [self presentViewController:[[SharedMethod new]
                                                setAndShowAlertController:NSLocalizedString(@"App_Name", nil)
                                                message:NSLocalizedString(@"msg_Insufficient_Balance", nil)
                                                cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
                    ];
                    
                }else if([[NSUserDefaults standardUserDefaults] boolForKey:IS_CARD_EXPIRED] == 0){
                    params = @{@"userId"            : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                               @"flashdeal_id"      : self.flash.ID,
                               @"amount"            : eAmount,
                               @"credit"            : [NSString stringWithFormat:@"%@%@", @"-", eAmount],
                               @"payment_method_id" : @"0",
                               //@"is_retry"          : @"1",
                               @"qr_code"           : @"",
                               @"message"           : @"",
                               @"type"              : @"flashdeal_wallet",
                               @"description"       : [NSString stringWithFormat:@"%@%@%@", @"Grab FlashDeal ", eAmount, @" with E-Wallet"],
                               @"token"             : [SharedMethod getTokenWithRoute:@"user/transaction/spend/flashdeal" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                               };
                    
                    [WebAPI retrieveFlashSpend:params andSuccess:^(id successBlock) {
                        [SVProgressHUD dismiss];
                        
                        NSDictionary *result = successBlock;
                        if([result objectForKey:@"errMsg"]){
                            [self presentViewController:[[SharedMethod new]
                                                         setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil)
                                                         message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                            
                        } else{
                            [self presentViewController:[[SharedMethod new]
                                                         setAndShowAlertController:NSLocalizedString(@"App_Name", nil)
                                                         message:NSLocalizedString(@"msg_Flash_Ewallet_success", nil)
                                                         cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
                             ];
                            
                        }
                    }];
                }else if([[NSUserDefaults standardUserDefaults] boolForKey:IS_CARD_EXPIRED] == 1){
                    [self presentViewController:[[SharedMethod new]
                                                setAndShowAlertController:NSLocalizedString(@"App_Name", nil)
                                                message:NSLocalizedString(@"msg_Card_Expired", nil)
                                                cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
                    ];
                }
                
            }else{
                params = @{@"userId"     : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                           @"fullname"   : [userDetail objectForKey:@"fullname"],
                           @"contact"    : [userDetail objectForKey:@"contact"],
                           @"email"      : [userDetail objectForKey:@"email"],
                           @"description": @"Flash Deal",
                           @"amount"     : price,
                           @"type"       : @"flashdeal",
                           @"flashdeal_id" : self.flash.ID,
                           @"push_token" : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                           @"token"      : [SharedMethod getTokenWithRoute:@"user/order/create" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                           };
                
                [WebAPI generateOrderId:params andSuccess:^(id successBlock) {
                    [SVProgressHUD dismiss];
                    NSDictionary *result = successBlock;
                    if([result objectForKey:@"errMsg"]){
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    } else{
                        result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                        if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                            if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"pending"]){
                                PaymentGatewayVC *paymentGatewayVC = [storyboard instantiateViewControllerWithIdentifier:@"paymentGatewayVC"];
                                paymentGatewayVC.dismissPaymentGatewayDelegate = self;
                                paymentGatewayVC.userId = [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID];
                                paymentGatewayVC.dict = [result objectForKey:@"order"];
                                paymentGatewayVC.type = @"flashdeal";
                                [paymentGatewayVC setHidesBottomBarWhenPushed:YES];
                                [self.navigationController pushViewController:paymentGatewayVC animated:YES];
                            } else{
                                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                            }
                        } else{
                            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                        }
                    }
                }];
                
            }
            
        } else{
            [self presentViewController:[[SharedMethod new]
                                         setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil)
                                         message:NSLocalizedString(@"msg_Internet", nil)
                                         cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
             ];
            
        }
        
    }];
    [alert addAction:ok];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:NO completion:nil];
}

-(void)molOrEwalletMessage{
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle: @""
                                message:[NSString stringWithFormat:@"%@%@\n%@%@",
                                         NSLocalizedString(@"lbl_Flash_MOL", nil),
                                         price.e_member_online,
                                         NSLocalizedString(@"lbl_Flash_Wallet", nil),
                                         price.e_member_wallet
                                         ]
                                preferredStyle:UIAlertControllerStyleAlert
                                ];
    
    UIAlertAction *mol = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Flash_MOL", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showNonRefundableMessage:price.e_member_online eWallet:NO eWalletAmount:price.e_member_online];
    }];
    [alert addAction:mol];
    
    UIAlertAction *wallet = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Flash_Wallet", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showNonRefundableMessage:price.e_member_wallet eWallet:YES eWalletAmount:price.e_member_wallet];
    }];
    [alert addAction:wallet];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:NO completion:nil];
}

-(void)flashGrab{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        
        NSDictionary *params = @{@"userId"      : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"flashdealId" : self.flash.ID,
                                 @"token"       : [SharedMethod getTokenWithRoute:@"user/flashdeal/grab" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
        NSLog(@"ASDFX_params - %@", params);
        
        [WebAPI retrieveFlashGrab:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new]
                                             setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil)
                                             message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                
            } else{
                [self presentViewController:[[SharedMethod new]
                                             setAndShowAlertController:NSLocalizedString(@"App_Name", nil)
                                             message:[result objectForKey:@"message"]
                                             cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
                 ];
                
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new]
                                     setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil)
                                     message:NSLocalizedString(@"msg_Internet", nil)
                                     cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
         ];
        
    }
}

#pragma mark - Actions
- (IBAction)bbtnCloseDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnFlashDidPressed:(id)sender {
    
    [self setupInitialization];
    
     NSLog(@"Test Flash");
     
     NSLog(@"ASDFX_FLASH_multiple - %d", [self.flash.trigger_frequency  isEqual: @"multiple"]);
     NSLog(@"ASDFX_FLASH_hasOwn - %@", self.flash.hasOwn);
     NSLog(@"ASDFX_FLASH_isGuest - %d", isGuest);
     NSLog(@"ASDFX_FLASH_isMember - %d", isMember);
     NSLog(@"ASDFX_FLASH_applicable_emember - %d", [self.flash.applicable_to isEqual: @"e-member"]);
     NSLog(@"ASDFX_FLASH_applicable_eguest - %d", [self.flash.applicable_to isEqual: @"e-guest"]);
     NSLog(@"ASDFX_FLASH_applicable_all - %d", [self.flash.applicable_to isEqual: @"all"]);
     NSLog(@"ASDFX_FLASH_isTopup - %d", isTopup);
     NSLog(@"ASDFX_FLASH_isCash - %d", isCash);
     NSLog(@"ASDFX_FLASH_type_deal - %d", [self.flash.type isEqual: @"deal"]);
     NSLog(@"ASDFX_FLASH_type_outlet - %d", [self.flash.type isEqual: @"outlet"]);
     NSLog(@"ASDFX_FLASH_payment_online - %d", [self.flash.payment_method  isEqual: @"online"]);
     NSLog(@"ASDFX_FLASH_isEguest - %d", isEGuest);
     
     NSLog(@"ASDFX_FLASH_not_multiple_hasOwn - %d", ![self.flash.trigger_frequency  isEqual: @"multiple"] && self.flash.hasOwn);
     NSLog(@"ASDFX_FLASH_not_logged_in - %d", ![[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]);
     NSLog(@"ASDFX_FLASH_isGuest_applicableTo_emember - %d", (isGuest && [self.flash.applicable_to isEqual: @"e-member"]));
     NSLog(@"ASDFX_FLASH_isTopup - %d", isTopup);
     NSLog(@"ASDFX_FLASH_applicableTo_all_cash - %d", ([self.flash.applicable_to isEqual: @"all"] && isCash));
     NSLog(@"ASDFX_FLASH_type_deal_outlet_payment_online - %d", ([self.flash.type isEqual: @"deal"] || [self.flash.type isEqual: @"outlet"]) &&  [self.flash.payment_method  isEqual: @"online"]);
     NSLog(@"ASDFX_FLASH_applicableTo_emember_isCash - %d", [self.flash.applicable_to isEqual: @"e-member"] && isCash);
     NSLog(@"ASDFX_FLASH_isEGuest_isOnline - %d", (isEGuest && isOnline));
     NSLog(@"ASDFX_FLASH_isEGuest_isCash - %d", (isEGuest && isCash));
     
    if(![[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
        LoginVC *loginVC = [storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
        [loginVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController.view.layer addAnimation:[[SharedMethod new] setupTransitionAnimation:TRANSITION_TOP] forKey:kCATransition];
        [self.navigationController pushViewController:loginVC animated:NO];
        
    }else if([self.flash.trigger_frequency isEqual: @"once"] && [self.flash.hasOwn isEqual: @"true"]){
        // (flashItem.getTriggerFrequency().equals(AppConstant.FLASH_TRIGGER_FREQUENCY_ONCE) && flashItem.getHasOwn())
        [self presentViewController:[[SharedMethod new]
                                     setAndShowAlertController:NSLocalizedString(@"ttl_Flash_Deal", nil)
                                     message:NSLocalizedString(@"lbl_Flash_Already_Own", nil)
                                     cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
         ];
        
    }else if(![self.flash.trigger_frequency isEqual: @"multiple"] && [self.flash.hasOwn isEqual: @"false"]){
        
        [self presentViewController:[[SharedMethod new]
                                     setAndShowAlertController:NSLocalizedString(@"ttl_Flash_Deal", nil)
                                     message:NSLocalizedString(@"lbl_Flash_Already_Own", nil)
                                     cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
         ];
        
    }else if(isGuest && [self.flash.applicable_to isEqual: @"e-member"]){
        [self presentViewController:[[SharedMethod new]
                                     setAndShowAlertController:NSLocalizedString(@"ttl_Flash_Deal", nil)
                                     message:NSLocalizedString(@"lbl_Flash_Emember_Only", nil)
                                     cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
         ];
    }else if(isTopup){
        if(isGuest){
            [self presentViewController:[[SharedMethod new]
                                         setAndShowAlertController:NSLocalizedString(@"ttl_Flash_Deal", nil)
                                         message:NSLocalizedString(@"lbl_Flash_Emember_Only", nil)
                                         cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
             ];
            
        }else if(isMember){
            [self showNonRefundableMessage:flashPriceValue eWallet:NO eWalletAmount:nil];
            
        }
    }else if([self.flash.applicable_to isEqual: @"all"] && isCash){
        // ASDFX TODO user/flashdeal/grab
        
        //        VolleyCardVerify(
        //         jobj,
        //         URLConstant.APP_FLASH_GRAB,
        //         Request.Method.POST,
        //         REQ_GRAB
        //        );
        
        [self flashGrab];
        
    } else if(([self.flash.type isEqual: @"deal"] || [self.flash.type isEqual: @"outlet"]) &&  [self.flash.payment_method  isEqual: @"online"]){
        if(isMember && [self.flash.applicable_to isEqual: @"e-member"]){
            //         ASDFX TODO MOL molButton showNonRefundableMessage
            //         ASDFX TODO WALLET walletButton showNonRefundableMessage
            
            [self molOrEwalletMessage];
            
        }else if(!isMember && [self.flash.applicable_to isEqual: @"e-member"]){
            [self presentViewController:[[SharedMethod new]
                                         setAndShowAlertController:NSLocalizedString(@"ttl_Flash_Deal", nil)
                                         message:NSLocalizedString(@"lbl_Flash_Emember_Only", nil)
                                         cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
             ];
            
        }else if(isGuest && [self.flash.applicable_to isEqual: @"e-guest"]){
            // ASDFX TODO showNonRefundableMessage(flashPriceValue, false, "");
            NSLog(@"ASDFX_price - %@", price.e_member_online);
            [self showNonRefundableMessage:flashPriceValue eWallet:NO eWalletAmount:nil];
            
        }else if(!isGuest && [self.flash.applicable_to isEqual: @"e-guest"]){
            [self presentViewController:[[SharedMethod new]
                                         setAndShowAlertController:NSLocalizedString(@"ttl_Flash_Deal", nil)
                                         message:NSLocalizedString(@"lbl_Flash_Eguest_Only", nil)
                                         cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
             ];
            
        }else if([self.flash.applicable_to isEqual: @"all"]){
            // ASDFX TODO showNonRefundableMessage(flashPriceValue, false, "");
            //[self showNonRefundableMessage:price.e_member_online eWallet:NO eWalletAmount:nil];
            if (isMember) {
                [self molOrEwalletMessage];
            }else {
                [self showNonRefundableMessage:price.e_guest_value eWallet:NO eWalletAmount:nil];
            }
        }
        
    } else if([self.flash.applicable_to isEqual: @"e-member"] && isOnline){
        if(isMember){
            // ASDFX TODO 441 Activity_FlashDetails MOL
            // ASDFX TODO 455 Activity_FlashDetails Wallet
            [self molOrEwalletMessage];
            
        }else{
            [self presentViewController:[[SharedMethod new]
                                         setAndShowAlertController:NSLocalizedString(@"ttl_Flash_Deal", nil)
                                         message:NSLocalizedString(@"lbl_Flash_Emember_Only", nil)
                                         cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
             ];
            
        }
        
    }else if([self.flash.applicable_to isEqual: @"e-member"] && isCash){
        if(isMember){
            // ASDFX TODO
            //        VolleyCardVerify(
            //         jobj,
            //         URLConstant.APP_FLASH_GRAB,
            //         Request.Method.POST,
            //         REQ_GRAB
            //        );
            
            [self flashGrab];
            
        }else{
            [self presentViewController:[[SharedMethod new]
                                         setAndShowAlertController:NSLocalizedString(@"ttl_Flash_Deal", nil)
                                         message:NSLocalizedString(@"lbl_Flash_Emember_Only", nil)
                                         cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
             ];
            
        }
    }else if(isEGuest && isOnline){
        if(isGuest){
            [self showNonRefundableMessage:flashPriceValue eWallet:NO eWalletAmount:nil];
            
        }else{
            [self presentViewController:[[SharedMethod new]
                                         setAndShowAlertController:NSLocalizedString(@"ttl_Flash_Deal", nil)
                                         message:NSLocalizedString(@"lbl_Flash_Emember_Only", nil)
                                         cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
             ];
            
        }
        
    }else if(isEGuest && isCash){        
        if(isGuest){
            // ASDFX TODO
            //        VolleyCardVerify(
            //         jobj,
            //         URLConstant.APP_FLASH_GRAB,
            //         Request.Method.POST,
            //         REQ_GRAB
            //        );
            
            [self flashGrab];
            
        }else{
            [self presentViewController:[[SharedMethod new]
                                         setAndShowAlertController:NSLocalizedString(@"ttl_Flash_Deal", nil)
                                         message:NSLocalizedString(@"lbl_Flash_Eguest_Only", nil)
                                         cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
             ];
            
        }
    }
    
    if(voucherLimit > 0){
        if([self.flash.total_grabbed intValue] >= voucherLimit){
            isSoldOut = YES;
            // TODO SOLD OUT
        }
    }
    
}
@end
