//
//  CardlessRegistrationVC.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 28/12/2017.
//  Copyright © 2017 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DismissCardlessLoginDelegate
@optional
-(void)dismissCardlessLoginVC;
@end

@interface CardlessRegistrationVC : UIViewController<DismissCardlessLoginDelegate>
@property (nonatomic) NSDictionary *socialDetails;
@property (nonatomic) NSString *socialToken;
@property (nonatomic) BOOL isFB;
@property (nonatomic) NSDictionary *basicDetail;
@property (nonatomic) id <DismissCardlessLoginDelegate> dismissCardlessLoginDelegate;
@end

