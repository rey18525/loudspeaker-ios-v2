//
//  TopUpVC.m
//  Oasis
//
//  Created by Wong Ryan on 26/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "TopUpVC.h"
#import "TopUpCell.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "WebAPI.h"
#import "SVProgressHUD.h"
#import "PaymentGatewayVC.h"
#import <PDKeychainBindingsController/PDKeychainBindingsController.h>

@interface TopUpVC ()<UICollectionViewDelegate, UICollectionViewDataSource, DismissPaymentGatewayDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbtnBack;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblPowered;
@property (weak, nonatomic) IBOutlet UILabel *lblNote;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;
@end

@implementation TopUpVC{
    UIStoryboard *storyboard;
    NSMutableArray *topUpArray, *topUpImageArray;
    NSDictionary *userDetail;
    float total;
    int selectedIndex;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInitialization];
    [self setupNavigationBar];
    [self setupCollectionView];
    [self setupUI];
    [self setupData];
}

#pragma mark - Setup Initialization
-(void)setupNavigationBar{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationItem setTitle:NSLocalizedString(@"nav_TopUp", nil)];
}

-(void)setupInitialization{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    topUpArray = [[NSMutableArray alloc] init];
    topUpImageArray = [[NSMutableArray alloc] init];
}

-(void)setupData{
    total = 0;
    selectedIndex = -1;
    [self.lblTotal setText:@"RM 0"];
    topUpArray = [[NSUserDefaults standardUserDefaults] objectForKey:TOPUP_LIST];
    
    [topUpImageArray addObject:[UIImage imageNamed:@"ic_topup_10"]];
    [topUpImageArray addObject:[UIImage imageNamed:@"ic_topup_20"]];
    [topUpImageArray addObject:[UIImage imageNamed:@"ic_topup_50"]];
    [topUpImageArray addObject:[UIImage imageNamed:@"ic_topup_100"]];
    
    userDetail = [[NSUserDefaults standardUserDefaults] objectForKey:USER_DETAIL];
    [self.collectionView reloadData];
    
    float height = ((([UIScreen mainScreen].bounds.size.width-32)/2)-32)*0.7;
    [self.collectionViewHeight setConstant:([topUpArray count])*height];
    
    NSString *note = NSLocalizedString(@"lbl_Topup_Note", nil);
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:note];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_REGULAR size:13.0f] range:NSMakeRange(0, note.length-5)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_BOLD size:13.0f] range:NSMakeRange(note.length-5, 5)];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[[SharedMethod new] colorWithHexString:LABEL_COLOR andAlpha:1.0f] range:NSMakeRange(0, note.length)];
    [self.lblNote setAttributedText:attributedString];
    
    [self.lblPowered setText:NSLocalizedString(@"poweredBy", nil)];
    [self.btnClear setTitle:NSLocalizedString(@"btn_Clear", nil) forState:UIControlStateNormal];
    [self.btnConfirm setTitle:NSLocalizedString(@"btn_Proceed", nil) forState:UIControlStateNormal];
}

-(void)setupUI{
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    
    [self.lblTotal setTextColor:[[SharedMethod new] colorWithHexString:LABEL_COLOR andAlpha:1.0f]];
    [self.lblTotal setFont:[UIFont fontWithName:FONT_REGULAR size:20.0f]];
    
    [self.lblPowered setTextColor:[[SharedMethod new] colorWithHexString:LABEL_COLOR andAlpha:1.0f]];
    [self.lblPowered setFont:[UIFont fontWithName:FONT_REGULAR size:13.0f]];
    
    [self.lblNote setTextColor:[[SharedMethod new] colorWithHexString:LABEL_COLOR andAlpha:1.0f]];
    [self.lblNote setFont:[UIFont fontWithName:FONT_REGULAR size:13.0f]];
    
    [self.btnConfirm setTitleColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
    [self.btnConfirm setBackgroundColor:[[SharedMethod new] colorWithHexString:MEDIUM_GREEN_COLOR andAlpha:1.0f]];
//    [self.btnConfirm setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
    [[self.btnConfirm titleLabel] setFont:[UIFont fontWithName:FONT_REGULAR size:15.0f]];
//    [[SharedMethod new] setBorder:self.btnConfirm borderColor:GREEN_COLOR andBackgroundColor:GREEN_COLOR];
    
    
    [self.btnClear setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
    [[self.btnClear titleLabel] setFont:[UIFont fontWithName:FONT_REGULAR size:15.0f]];
    [[SharedMethod new] setBorder:self.btnClear borderColor:THEME_COLOR andBackgroundColor:@""];
//    [self.btnClear setTitleColor:[[SharedMethod new] colorWithHexString:LABEL_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
//    [self.btnClear setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

-(void)setupCollectionView{
    [self.collectionView registerNib:[UINib nibWithNibName:@"TopUpCell" bundle:nil] forCellWithReuseIdentifier:@"topUpCell"];
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
}

-(void)retrieveOrderId:(NSString*)price{
    //FOR UAT PURPOSE//
    //    price = @"1.00";
    
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSDictionary *params = @{@"userId"     : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"fullname"   : [userDetail objectForKey:@"fullname"],
                                 @"contact"    : [userDetail objectForKey:@"contact"],
                                 @"email"      : [userDetail objectForKey:@"email"],
                                 @"description": NSLocalizedString(@"ipay_Topup", nil),
                                 @"amount"     : price,
                                 @"type"       : @"topup",
                                 @"push_token" : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                 @"token"      : [SharedMethod getTokenWithRoute:@"user/order/create" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
#ifdef DEBUG
        NSLog(@"Retrieve Order Id Param - %@",params);
#endif
        
        [WebAPI generateOrderId:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"pending"]){
                        PaymentGatewayVC *paymentGatewayVC = [storyboard instantiateViewControllerWithIdentifier:@"paymentGatewayVC"];
                        paymentGatewayVC.dismissPaymentGatewayDelegate = self;
                        paymentGatewayVC.userId = [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID];
                        paymentGatewayVC.dict = [result objectForKey:@"order"];
                        paymentGatewayVC.type = @"topup";
                        [paymentGatewayVC setHidesBottomBarWhenPushed:YES];
                        [self.navigationController pushViewController:paymentGatewayVC animated:YES];
                    } else{
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    }
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)processTopup:(NSDictionary *)basicDetails{
//    NSLog(@"basicDetails - %@",basicDetails);
    if([basicDetails count]>0){
        if([basicDetails objectForKey:@"status"] && ([[basicDetails objectForKey:@"status"] isEqualToString:@"cancelled"] || [[basicDetails objectForKey:@"status"] isEqualToString:@"rejected"])){
            //Do nothing//
        } else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(void)displayPaymentGatewayMessage:(NSString*)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"App_Name", nil) message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)dismissPaymentGatewayVC:(NSString*)message{
    if(message.length>0){
        [self performSelector:@selector(displayPaymentGatewayMessage:) withObject:message afterDelay:0.2];
    }
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnConfirmDidPressed:(id)sender {
    //Call topup API//
    if(total<=0){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"err_Invalid_Topup", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    } else{
        [self retrieveOrderId:[NSString stringWithFormat:@"%.0f",total]];
    }
}

- (IBAction)btnClearDidPressed:(id)sender {
    selectedIndex = -1;
    total = 0;
    [self.lblTotal setText:@"RM 0"];
}

#pragma mark - UICollectionView Data Source
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [topUpArray count];
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(8, 16, 8, 8);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    TopUpCell *cell = (TopUpCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"topUpCell" forIndexPath:indexPath];
    if(cell==nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TopUpCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    //For 1 selection only//
//    if(selectedIndex==(int)indexPath.row){
//        [cell.lblTitle setBackgroundColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
//        [cell.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
//    } else{
//        [cell.lblTitle setBackgroundColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
//        [cell.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
//
//    }
    
    //For accumulate//
//    [cell.lblTitle setBackgroundColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
//    [[cell layer] setBorderColor:[[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] CGColor]];
//    [[cell layer] setBorderWidth:1.0f];
//    [cell.lblTitle setText:[NSString stringWithFormat:@"RM %@",[topUpArray objectAtIndex:indexPath.row]]];
    
    
    NSString *imgString = [NSString stringWithFormat:@"ic_topup_%@",[topUpArray objectAtIndex:indexPath.row]];
    UIImage *imgTopup = [UIImage imageNamed:imgString];
    
    if(imgTopup){
        [[cell.contentView layer] setBorderWidth:0.0f];
        [cell.imgBackground setHidden:NO];
        [cell.imgBackground setImage:imgTopup];
    } else{
        [[cell.contentView layer] setBorderWidth:1.0f];
        [cell.imgBackground setHidden:YES];
        [cell.lblTitle setText:[NSString stringWithFormat:@"RM %@",[topUpArray objectAtIndex:indexPath.row]]];
    }
    
    return cell;
}

#pragma mark - UICollectionView Delegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    total += [[topUpArray objectAtIndex:indexPath.row] floatValue];
    selectedIndex = (int)indexPath.row;
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
//    total = [[topUpArray objectAtIndex:indexPath.row] floatValue];
    [self.lblTotal setText:[NSString stringWithFormat:@"RM %.0f",total]];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float width = ([UIScreen mainScreen].bounds.size.width-56)/2;
    return CGSizeMake(width, width);
//    return CGSizeMake(width*0.7, width*0.7);
}

@end
