//
//  RewardPopVC.m
//  Oasis
//
//  Created by Wong Ryan on 28/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "RewardPopVC.h"

@interface RewardPopVC ()
@property (weak, nonatomic) IBOutlet UIImageView *imgBG;
@property (weak, nonatomic) IBOutlet UIView *popView;
@end

@implementation RewardPopVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    
    [[self.popView layer] setCornerRadius:10.0f];
    [self.popView setClipsToBounds:YES];
    [self.lblOk setText:NSLocalizedString(@"btn_Ok", nil)];
}
- (IBAction)btnOkDidPressed:(id)sender {
    for(UIView *temp in self.parentViewController.view.window.subviews){
        if(temp.tag == 901){
            [temp removeFromSuperview];
        }
    }
    [self removeFromParentViewController];
}

@end
