//
//  NotificationVC.m
//  Oasis
//
//  Created by Wong Ryan on 26/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "NotificationVC.h"
#import "NotificationCell.h"
#import "Constant.h"
#import "SharedMethod.h"
#import "WebAPI.h"
#import "SVProgressHUD.h"
#import "EmptyTableCell.h"
#import <PDKeychainBindings.h>

@interface NotificationVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbtnBack;

@end

@import Firebase;

@implementation NotificationVC{
    NSMutableArray *notificationArray;
    UIAlertController *alertController;
    UIStoryboard *storyboard;
    int paging, totalCount;
    BOOL isEmpty, isTimeOut;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInitialization];
    [self setupDelegates];
    
    [self setupData];
    [self setupNavigationBar];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self retrieveNotification];
}

#pragma mark - Setup Initialization
-(void)setupDelegates{
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
}

-(void)setupNavigationBar{
    [self.navigationItem setTitle:NSLocalizedString(@"nav_Notification_V2", nil)];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.bbtnBack setTarget:self];
    [self.bbtnBack setAction:@selector(bbtnBackDidPressed:)];
}

-(void)setupInitialization{
    notificationArray = [[NSMutableArray alloc] init];
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self.tableView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.tableView setAllowsMultipleSelectionDuringEditing:NO];
}

-(void)setupData{
    isEmpty = NO;
    isTimeOut = NO;
    paging = 1;
    totalCount = 0;
//    [self.tableView reloadData];
}

-(void)retrieveNotification{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        [FIRAnalytics logEventWithName:@"Retrieve Notifications" parameters:nil];
        
        /*
        NSDictionary *params = @{@"userId"           : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"platform"         : PLATFORM,
                                 @"push_token"       : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                 @"token"            : [SharedMethod getTokenWithRoute:@"user/inbox" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]],
                                 @"page"             : [NSString stringWithFormat:@"%d",paging]
                                 };
         */
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        FIRCrashMessage([NSString stringWithFormat:@"UserId:*Sensitive*"]);
        
        [params setObject:PLATFORM forKey:@"platform"];
        FIRCrashMessage([NSString stringWithFormat:@"Platform:%@",PLATFORM]);
        
        [params setObject:[[SharedMethod new] checkDeviceToken] forKey:@"push_token"];
        FIRCrashMessage([NSString stringWithFormat:@"Push Token:%@",PLATFORM]);
        
        [params setObject:[SharedMethod getTokenWithRoute:@"user/inbox" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        
        [params setObject:[NSString stringWithFormat:@"%d",paging] forKey:@"page"];
        FIRCrashMessage([NSString stringWithFormat:@"Page:%d",paging]);
        
        #ifdef DEBUG
        NSLog(@"Request Inbox - %@",params);
        #endif
        
        [WebAPI retrieveInbox:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                isEmpty = YES;
                isTimeOut = YES;
                [self.tableView reloadData];
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                totalCount = [[result objectForKey:@"total_items"] intValue];
                [notificationArray addObjectsFromArray:[result objectForKey:@"messages"]];
                
                if([notificationArray count]<=0){
                    isEmpty = YES;
                    isTimeOut = NO;
                }
                
                [self.tableView reloadData];    
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        isEmpty = YES;
        isTimeOut = YES;
        [self.tableView reloadData];
    }
}

-(void)readMessage:(NSString*)msgID andIndex:(int)index{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSString *deviceToken = [[SharedMethod new] checkDeviceToken];
        NSString *route = [NSString stringWithFormat:@"user/inbox/message/%@",msgID];
        NSDictionary *params = @{@"userId"           : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"platform"         : PLATFORM,
                                 @"msgID"            : msgID,
                                 @"push_token"       : deviceToken,
                                 @"token"            : [SharedMethod getTokenWithRoute:route apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
        #ifdef DEBUG
        NSLog(@"View Inbox Message - %@",params);
        #endif
        
        [WebAPI viewInboxMessage:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                
                NSInteger tempCount = [[NSUserDefaults standardUserDefaults] integerForKey:INBOX_COUNT];
                tempCount-=1;
                [[NSUserDefaults standardUserDefaults] setInteger:tempCount forKey:INBOX_COUNT];
                NSString *read_at = [[result objectForKey:@"message"] objectForKey:@"read_at"];
                
                [[notificationArray objectAtIndex:index] setObject:read_at forKey:@"read_at"];
                [self.tableView reloadData];
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(isEmpty){
        return 1;
    } else{
        return [notificationArray count];
    }
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isEmpty){
        EmptyTableCell *cell = (EmptyTableCell*)[tableView dequeueReusableCellWithIdentifier:@"emptyCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EmptyTableCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.imgIcon setImage:[UIImage imageNamed:@"ic_empty"]];
        [cell.lblDescription setText:NSLocalizedString(@"lbl_Empty", nil)];
        
        return cell;
    } else{
        NotificationCell *cell = (NotificationCell*)[tableView dequeueReusableCellWithIdentifier:@"notificationCell"];
        
        [cell.lblTitle setText:[[notificationArray objectAtIndex:indexPath.row] objectForKey:@"title"]];
        [cell.lblDescription setText:[[notificationArray objectAtIndex:indexPath.row] objectForKey:@"message"]];

        NSString *read_at = [[notificationArray objectAtIndex:indexPath.row] objectForKey:@"read_at"];
        
        if(read_at.length<=0){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setupAttributedText:[[SharedMethod new] convertToNotificationDateFormat_V2:[[notificationArray objectAtIndex:indexPath.row] objectForKey:@"send_at"]] andLabel:cell.lblDate];
                [cell.imgIndicator_Width setConstant:25];
            });
        } else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setupAttributedText:[[SharedMethod new] convertToNotificationDateFormat_V2:[[notificationArray objectAtIndex:indexPath.row] objectForKey:@"send_at"]] andLabel:cell.lblDate];
                [cell.imgIndicator_Width setConstant:0];
            });
        }
        
        if(indexPath.row == [notificationArray count] - 1 && [notificationArray count] < totalCount){
            if([[SharedMethod new] checkNetworkConnectivity]){
                paging+=1;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self retrieveNotification];
                    });
                });
            } else{
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"err_Error", nil) message:NSLocalizedString(@"msg_No_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            }
        }
        return cell;
    }
}

-(void)setupAttributedText:(NSString*)text andLabel:(UILabel*)label{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_REGULAR size:25] range:NSMakeRange(0, 2)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_REGULAR size:12] range:NSMakeRange(2, 10)];
    [label setAttributedText:attributedString];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!isEmpty){
        NSString *msgID = [NSString stringWithFormat:@"%d",[[[notificationArray objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]];
        NSString *read_at = [[notificationArray objectAtIndex:indexPath.row] objectForKey:@"read_at"];
        
        if(read_at.length<=0){
            [self readMessage:msgID andIndex:(int)indexPath.row];
        }
        
        NSString *title = [[notificationArray objectAtIndex:indexPath.row] objectForKey:@"title"];
        NSString *message = [NSString stringWithFormat:@"%@\n\n%@",[[notificationArray objectAtIndex:indexPath.row] objectForKey:@"message"],[[notificationArray objectAtIndex:indexPath.row] objectForKey:@"send_at"]];
        NSString *postID = [[notificationArray objectAtIndex:indexPath.row] objectForKey:@"post_id"];
        NSString *type = [[notificationArray objectAtIndex:indexPath.row] objectForKey:@"type"];
        
        alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:nil];
        
        if(type.length>0 && postID.length>0){
            UIAlertAction *showAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Show", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [alertController addAction:showAction];
        }
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isEmpty){
        return self.tableView.frame.size.height+64;
    } else{
        return 78;
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        if([[SharedMethod new] checkNetworkConnectivity]){
            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
            NSString *msgID = [NSString stringWithFormat:@"%d",[[[notificationArray objectAtIndex:indexPath.row] objectForKey:@"id"] intValue]];
            NSString *route = [NSString stringWithFormat:@"user/inbox/message/%@",msgID];
            
            NSDictionary *params = @{@"userId"           : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                     @"platform"         : PLATFORM,
                                     @"msgID"            : msgID,
                                     @"push_token"       : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                     @"token"            : [SharedMethod getTokenWithRoute:route apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                     };

            
            [WebAPI deleteInboxMessage:params andSuccess:^(id successBlock) {
                [SVProgressHUD dismiss];
                NSDictionary *result = successBlock;
                if([result objectForKey:@"errMsg"]){
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                } else{
                    [notificationArray removeObjectAtIndex:indexPath.row];
                    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                }
            }];
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        }
    }
}

@end
