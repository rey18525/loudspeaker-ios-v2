//
//  LoginVC.m
//  Oasis
//
//  Created by Wong Ryan on 21/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "LoginVC.h"
#import "Constant.h"
#import "SharedMethod.h"
#import "UIView+Toast.h"
#import "HomeVC.h"
#import "SignUpVC.h"
#import "CardRegistrationVC.h"
#import "WebAPI.h"
#import "SVProgressHUD.h"
#import "ProfileVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "TextFieldValidator.h"
#import "ForgotPasswordVC.h"
#import <PDKeychainBindings.h>
#import <TPKeyboardAvoidingScrollView.h>
#import <Crashlytics/Crashlytics.h>

@interface LoginVC ()<GIDSignInDelegate,GIDSignInUIDelegate,DismissLoginDelegate,DismissForgotPasswordDelegate,DismissCardRegistrationDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContent;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtEmail;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnForgot;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnGoogle;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UILabel *lblNotice;

@end

@import Firebase;

@implementation LoginVC{
    UIStoryboard *storyboard;
    UIAlertController *alertController;
    ForgotPasswordVC *forgotPasswordVC;
    CardRegistrationVC *cardRegistration;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //    [self.txtEmail setText:@"ryan@appandus.com"];
    //    [self.txtPassword setText:@"123456"];
    
    /*
    UIButton* button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(20, 50, 100, 30);
    [button setTitle:@"Crash" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(crashButtonTapped:)
        forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    */
    
    [self setupDelegates];
    [self setupTextFieldValidation];
    [self setupUI];
    [self setupInitialization];
    [self setupDismissKeyboard];
    [self setupGoogleSignIn];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationItem setTitle:NSLocalizedString(@"nav_Login_V2", nil)];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self setHidesBottomBarWhenPushed:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.txtEmail setText:@""];
    [self.txtPassword setText:@""];
}

#pragma mark - Setup UI
-(void)setupDelegates{
    [self.txtEmail setDelegate:self];
    [self.txtPassword setDelegate:self];
}

-(void)setupTextFieldValidation{
    [self.txtEmail setIsMandatory:YES];
    [self.txtEmail setValidateOnCharacterChanged:NO];
    [self.txtEmail addRegx:@"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" withMsg:NSLocalizedString(@"err_Invalid_Email", nil)];
    [self.txtEmail updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    
    [self.txtPassword setIsMandatory:YES];
    [self.txtPassword setValidateOnCharacterChanged:NO];
    [self.txtPassword updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
}

-(void)setupUI{
    dispatch_async(dispatch_get_main_queue(), ^{
//        NSLog(@"Height - %f",[UIScreen mainScreen].bounds.size.height);
        
//        if([UIScreen mainScreen].bounds.size.height == 480){
//            [self.scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, 667)];
//            [self.scrollContent setFrame:CGRectMake(0, 0, self.scrollContent.frame.size.width, 667)];
//            [self.btnSkip setFrame:CGRectMake(self.btnSkip.frame.origin.x, self.btnFacebook.frame.origin.y + self.btnFacebook.frame.size.height+32, 160, 40)];
//        }
        
        [self.txtEmail setFont:[UIFont fontWithName:FONT_REGULAR size:14.0]];
        [self.txtPassword setFont:[UIFont fontWithName:FONT_REGULAR size:14.0]];
        [self.btnForgot.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:14.0]];
        [self.btnLogin.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:14.0]];
        [self.btnSignUp.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:14.0]];
        [self.btnFacebook.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:14.0]];
        [self.lblNotice setFont:[UIFont fontWithName:FONT_REGULAR size:10.0f]];
        [self.btnSkip.titleLabel setFont:[UIFont fontWithName:FONT_BOLD size:15.0]];
        
        [[SharedMethod new] setPaddingToTextField:self.txtEmail];
        [[SharedMethod new] setPaddingToTextField:self.txtPassword];
        
        [self.txtEmail setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.txtPassword setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        
        [[SharedMethod new] setBorder:self.txtEmail borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [[SharedMethod new] setBorder:self.txtPassword borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [self.btnForgot setTitleColor:[[SharedMethod new] colorWithHexString:GREY_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
        
        [[SharedMethod new] setCustomButtonStyle:self.btnLogin image:@"" borders:YES borderWidth:1.0f cornerRadius:0.0f borderColor:THEME_COLOR textColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
        [[SharedMethod new] setCustomButtonStyle:self.btnSignUp image:@"" borders:YES borderWidth:1.0f cornerRadius:0.0f borderColor:THEME_COLOR textColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
        [[SharedMethod new] setCustomButtonStyle:self.btnFacebook image:@"Facebook" borders:NO borderWidth:0.0f cornerRadius:0.0f borderColor:@"" textColor:THEME_COLOR andBackgroundColor:FACEBOOK_BUTTON_COLOR];
        [[SharedMethod new] setCustomButtonStyle:self.btnGoogle image:@"Google" borders:NO borderWidth:0.0f cornerRadius:0.0f borderColor:@"" textColor:THEME_COLOR andBackgroundColor:GOOGLE_BUTTON_COLOR];
    });
    [self.lblNotice setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.btnSkip setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
    [self.txtEmail setPlaceholder:NSLocalizedString(@"phd_Email", nil)];
    [self.txtPassword setPlaceholder:NSLocalizedString(@"phd_Password", nil)];
    [self.btnLogin setTitle:NSLocalizedString(@"btn_Login", nil) forState:UIControlStateNormal];
    [self.btnForgot setTitle:NSLocalizedString(@"btn_Forgot_Password", nil) forState:UIControlStateNormal];
    [self.btnSkip setTitle:NSLocalizedString(@"btn_Skip_Top", nil) forState:UIControlStateNormal];
    [self.lblNotice setText:NSLocalizedString(@"lbl_Sign_Up_Notice", nil)];
}

-(void)setupInitialization{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

-(void)setupDismissKeyboard{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)setupGoogleSignIn{
    [GIDSignIn sharedInstance].uiDelegate = self;
    [GIDSignIn sharedInstance].delegate = self;
    //    [[GIDSignIn sharedInstance] signInSilently];
}

-(void)fetchUserFBInfo{
    if ([FBSDKAccessToken currentAccessToken]){
#ifdef DEBUG
        NSLog(@"Facebook Token - %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
#endif
        
        [[NSUserDefaults standardUserDefaults] setObject:[[FBSDKAccessToken currentAccessToken]tokenString] forKey:FB_TOKEN];
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday, about ,location ,friends ,hometown , friendlists"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error){
#ifdef DEBUG
                 NSLog(@"Facebook Response Result - %@",result);
#endif
                 
                 NSDictionary *fbResult = result;
                 
                 if(![fbResult objectForKey:@"email"]){
                     NSString *portrait = [[[fbResult objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
                     
                     if(portrait.length>0 || [portrait isEqual:[NSNull null]]){
                         NSString *portrait = [[[fbResult objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
                         NSURL *url = [NSURL URLWithString:portrait];
                         UIImage *img = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
                         [[SharedMethod new] saveNewProfileImage:img andIsSocial:YES];
                     }
                     
                     CardRegistrationVC *cardRegistrationVC = [storyboard instantiateViewControllerWithIdentifier:@"cardRegistrationVC"];
                     cardRegistrationVC.socialToken = [[FBSDKAccessToken currentAccessToken]tokenString];
                     cardRegistrationVC.socialDetails = fbResult;
                     cardRegistrationVC.dismissCardRegistrationDelegate = self;
                     cardRegistrationVC.isFB = YES;
                     [self presentViewController:cardRegistrationVC animated:YES completion:^{
                         //Nothing//
                     }];
                 } else{
                     if([[SharedMethod new] checkNetworkConnectivity]){
                         NSString *portrait = [[[fbResult objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
                         
                         if(portrait.length>0 || [portrait isEqual:[NSNull null]]){
                             NSString *portrait = [[[fbResult objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
                             NSURL *url = [NSURL URLWithString:portrait];
                             UIImage *img = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
                             [[SharedMethod new] saveNewProfileImage:img andIsSocial:YES];
                         }
                         [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                         [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
                         
                         //Login Using FB//
                         NSDictionary *params = @{@"email"            : [fbResult objectForKey:@"email"],
                                                  @"provider_id"      : [fbResult objectForKey:@"id"],
                                                  @"socialToken"      : [[FBSDKAccessToken currentAccessToken]tokenString],
                                                  @"platform"         : PLATFORM,
                                                  @"push_token"       : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                                  @"token"            : [SharedMethod getTokenWithRoute:@"user/login/facebook" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                                  };
                         
                        #ifdef DEBUG
                        NSLog(@"Prepare Login With Facebook Params - %@",params);
                        #endif
                         
                         [WebAPI loginFacebook:params andSuccess:^(id successBlock) {
                             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                             NSDictionary *result = successBlock;
                             if([result objectForKey:@"errMsg"]){
                                 if([[result objectForKey:@"errCode"] isEqualToString:NOT_FOUND]){
                                     [SVProgressHUD dismiss];
                                     CardRegistrationVC *cardRegistrationVC = [storyboard instantiateViewControllerWithIdentifier:@"cardRegistrationVC"];
                                     cardRegistrationVC.socialToken = [[FBSDKAccessToken currentAccessToken]tokenString];
                                     cardRegistrationVC.socialDetails = fbResult;
                                     cardRegistrationVC.dismissCardRegistrationDelegate = self;
                                     cardRegistrationVC.isFB = YES;
                                     [self presentViewController:cardRegistrationVC animated:YES completion:^{
                                         //Nothing//
                                     }];
                                 } else if([[result objectForKey:@"errCode"] isEqualToString:@"U00"]){
                                     NSDictionary *newParams = @{@"email"            : [fbResult objectForKey:@"email"],
                                                                 @"provider_id"      : [fbResult objectForKey:@"id"],
                                                                 @"socialToken"      : [[FBSDKAccessToken currentAccessToken]tokenString],
                                                                 @"platform"         : PLATFORM,
                                                                 @"push_token"       : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                                                 @"token"            : [SharedMethod getTokenWithRoute:@"user/register/facebook" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                                              };
                                     [self updateSocialToken:newParams];
                                 } else{
                                     [SVProgressHUD dismiss];
                                     [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                                 }
                             } else{
                                 [SVProgressHUD dismiss];
                                 result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_SOCIAL];
                                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
                                [[PDKeychainBindings sharedKeychainBindings] setObject:[result objectForKey:@"secret"] forKey:SECRET_KEY];
                                 [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"user"] forKey:USER_DETAIL];
                                 
                                 NSString *userID = [NSString stringWithFormat:@"%d",[[[result objectForKey:@"user"] objectForKey:@"id"] intValue]];
                                 [[NSUserDefaults standardUserDefaults] setObject:userID forKey:USER_ID];
                                 
                                 [self.navigationController setNavigationBarHidden:NO animated:YES];
                                 [self.navigationController popViewControllerAnimated:NO];
                             }
                         }];
                     } else{
                         [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                         FBSDKLoginManager *manager = [[FBSDKLoginManager alloc]init];
                         [manager logOut];
                     }
                 }
             } else{
                 NSLog(@"error - %@",error.userInfo[FBSDKErrorDeveloperMessageKey]);
                 [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"err_Title", nil) message:NSLocalizedString(@"err_Login_Facebook", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                 FBSDKLoginManager *manager = [[FBSDKLoginManager alloc]init];
                 [manager logOut];
             }
         }];
    } else{
        NSLog(@"No token");
    }
}

-(void)updateSocialToken:(NSDictionary*)params{
    if([[SharedMethod new] checkNetworkConnectivity]){
        #ifdef DEBUG
        NSLog(@"Update Social Token Params - %@",params);
        #endif
        
        [WebAPI signUpFacebookNewUser:params andSuccess:^(id successBlock) {
            NSDictionary *result = successBlock;
            [SVProgressHUD dismiss];
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_SOCIAL];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
                [[PDKeychainBindings sharedKeychainBindings] setObject:[result objectForKey:@"secret"] forKey:SECRET_KEY];
                [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"user"] forKey:USER_DETAIL];
                
                NSString *userID = [NSString stringWithFormat:@"%d",[[[result objectForKey:@"user"] objectForKey:@"id"] intValue]];
                [[NSUserDefaults standardUserDefaults] setObject:userID forKey:USER_ID];
                
                [self.navigationController setNavigationBarHidden:NO animated:YES];
                [self.navigationController popViewControllerAnimated:NO];
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        FBSDKLoginManager *manager = [[FBSDKLoginManager alloc]init];
        [manager logOut];
    }
}

-(void)dismissLoginVC{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.tabBarController setSelectedIndex:2];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)dismissCardRegistrationVC{
    [cardRegistration removeFromParentViewController];
    [cardRegistration.view removeFromSuperview];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.tabBarController setSelectedIndex:2];
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)dismissForgotPasswordVC{
    [forgotPasswordVC removeFromParentViewController];
    [forgotPasswordVC.view removeFromSuperview];
}

#pragma mark - UITextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self dismissKeyboard];
    return YES;
}

#pragma mark - Actions
/*
- (IBAction)crashButtonTapped:(id)sender {
    [[Crashlytics sharedInstance] crash];
}*/

- (IBAction)btnForgotPasswordDidPressed:(id)sender {
    forgotPasswordVC = [storyboard instantiateViewControllerWithIdentifier:@"forgotPasswordVC"];
    forgotPasswordVC.dismissForgotPassword = self;
    [self addChildViewController:forgotPasswordVC];
    [self.view addSubview:forgotPasswordVC.view];
    [self.view bringSubviewToFront:forgotPasswordVC.view];
}

- (IBAction)btnLoginDidPressed:(id)sender {
    [self dismissKeyboard];
    if([[SharedMethod new] checkNetworkConnectivity]){
        //        if(self.txtEmail.text.length>0 && self.txtPassword.text.length>0 && [[SharedMethod new] validateEmail:[self.txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        if([self.txtEmail validate] && [self.txtPassword validate]){
            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            //Process Login API//
//            NSDictionary *params;
            /*
            NSDictionary *params = @{@"email"            : [self.txtEmail.text stringByTrimmingCharactersInSet:
                                                            [NSCharacterSet whitespaceCharacterSet]],
                                     @"password"         : self.txtPassword.text,
                                     @"platform"         : PLATFORM,
                                     @"push_token"       : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                     @"token"            : [SharedMethod getTokenWithRoute:@"user/login" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                     };
            */
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            [FIRAnalytics logEventWithName:@"Login" parameters:nil];
            [params setObject:[self.txtEmail.text stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceCharacterSet]] forKey:@"email"];
            FIRCrashMessage([NSString stringWithFormat:@"Email:%@",[self.txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]);
            
            [params setObject:self.txtPassword.text forKey:@"password"];
            FIRCrashMessage([NSString stringWithFormat:@"Password:*Sensitive*"]);
            
            [params setObject:PLATFORM forKey:@"platform"];
            FIRCrashMessage([NSString stringWithFormat:@"Platform:%@",PLATFORM]);
            
            [params setObject:[[SharedMethod new] checkDeviceToken] forKey:@"push_token"];
            FIRCrashMessage([NSString stringWithFormat:@"Push Token:%@",[[SharedMethod new] checkDeviceToken]]);
            
            [params setObject:[SharedMethod getTokenWithRoute:@"user/login" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
            
            [WebAPI loginUser:params andSuccess:^(id successBlock) {
                [SVProgressHUD dismiss];
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                NSDictionary *result = successBlock;
                
                if([result objectForKey:@"errMsg"]){
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                } else{
                    result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                    NSDictionary *userResult = [result objectForKey:@"user"];
                    
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[result objectForKey:@"secret"] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setObject:userResult forKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_SOCIAL];
                    NSString *userID = [NSString stringWithFormat:@"%d",[[userResult objectForKey:@"id"] intValue]];
                    [[NSUserDefaults standardUserDefaults] setObject:userID forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"register_rate"] forKey:UPGRADE_REGISTER_RATE];
                
                    NSString *userType = [userResult objectForKey:@"type"];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_GUEST];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_MEMBER];
                    if([userType isEqual: @"e-member"]) {
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_MEMBER];
                    } else if([userType isEqual: @"e-guest"]) {
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_GUEST];
                    }
                    
                    [self.navigationController setNavigationBarHidden:NO animated:YES];
                    //[self.navigationController popViewControllerAnimated:NO];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }];
            
        }
        
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

- (IBAction)btnSignUpDidPressed:(id)sender {
    cardRegistration = [storyboard instantiateViewControllerWithIdentifier:@"cardRegistrationVC"];
    cardRegistration.dismissCardRegistrationDelegate = self;
    [self presentViewController:cardRegistration animated:YES completion:nil];
    
//    SignUpVC *signUpVC = [storyboard instantiateViewControllerWithIdentifier:@"signUpVC"];
//    signUpVC.dismissLoginDelegate = self;
//    [self presentViewController:signUpVC animated:YES completion:nil];
}

- (IBAction)btnFacebookDidPressed:(id)sender {
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logInWithReadPermissions:@[@"public_profile",@"user_birthday",@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if(error){
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:error.localizedDescription cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        } else if(result.isCancelled){
            //Cancelled//
            NSLog(@"Cancelled");
        } else {
            //            if([result.grantedPermissions containsObject:@"email"]){
            [self fetchUserFBInfo];
            //            }
        }
    }];
}

- (IBAction)btnGoogleDidPressed:(id)sender {
    [[GIDSignIn sharedInstance] signIn];
}

- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController{
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if(error==nil){
        NSMutableDictionary *googleResult = [[NSMutableDictionary alloc] init];
        [googleResult setObject:user.profile.name forKey:@"name"];
        [googleResult setObject:user.profile.email forKey:@"email"];
        [googleResult setObject:user.userID forKey:@"id"];
        
        if([[SharedMethod new] checkNetworkConnectivity]){
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
            
            NSDictionary *params = @{@"fullname"         : user.profile.name,
                                     @"email"            : user.profile.email,
                                     @"provider_id"      : user.userID,
                                     @"socialToken"      : user.authentication.idToken,
                                     @"platform"         : PLATFORM,
                                     @"push_token"       : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                     @"token"            : [SharedMethod getTokenWithRoute:@"user/login/google" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                     };
            
#ifdef DEBUG
            NSLog(@"Prepare Login With Google Params - %@",params);
#endif
            
            [WebAPI loginGoogle:params andSuccess:^(id successBlock) {
                NSDictionary *result = successBlock;
                [SVProgressHUD dismiss];
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                if([result objectForKey:@"errMsg"]){
                    if([[result objectForKey:@"errCode"] isEqualToString:NOT_FOUND]){
                        SignUpVC *signUpVC = [storyboard instantiateViewControllerWithIdentifier:@"signUpVC"];
                        signUpVC.socialToken = user.authentication.idToken;
                        signUpVC.socialDetails = googleResult;
                        signUpVC.dismissLoginDelegate = self;
                        signUpVC.isFB = NO;
                        [self presentViewController:signUpVC animated:YES completion:nil];
                    }
                } else{
                    result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_SOCIAL];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
                   [[PDKeychainBindings sharedKeychainBindings] setObject:[result objectForKey:@"secret"] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"user"] forKey:USER_DETAIL];
                    
                    NSString *userID = [NSString stringWithFormat:@"%d",[[[result objectForKey:@"user"] objectForKey:@"id"] intValue]];
                    [[NSUserDefaults standardUserDefaults] setObject:userID forKey:USER_ID];
                    
                    [self.navigationController setNavigationBarHidden:NO animated:YES];
                    [self.navigationController popViewControllerAnimated:NO];
                }
            }];
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            FBSDKLoginManager *manager = [[FBSDKLoginManager alloc]init];
            [manager logOut];
        }
        
    } else{
        NSLog(@"Error Signing in");
    }
}

- (IBAction)btnSkipDidPressed:(id)sender {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.tabBarController setSelectedIndex:0];
}

@end
