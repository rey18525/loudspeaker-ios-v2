    //
//  VoucherListVC.m
//  Oasis
//
//  Created by Wong Ryan on 04/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "VoucherListVC.h"
#import "VoucherTableCell.h"
#import "SharedMethod.h"
#import "VoucherTableCell.h"
#import "VoucherDetailVC.h"
#import "SQLManager.h"
#import "SVProgressHUD.h"
#import "WebAPI.h"
#import "Constant.h"
#import "Voucher.h"
#import "Campaign.h"
#import "SearchTableVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <PDKeychainBindings.h>

@interface VoucherListVC ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchResultsUpdating>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbtnSearch;
@property (strong, nonatomic) UISearchController *searchController;
@end

@implementation VoucherListVC{
    UIStoryboard *storyboard;
    NSMutableArray *voucherArray, *campaignsArray;
    NSString *searchText;
    SearchTableVC *searchTableVC;
    int paging, totalCount;
    BOOL isSearch;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInitialization];
    [self setupData];
    [self setupDelegates];
    [self setupRefreshControl];
    [self setupSearchController];
    [self retrieveVouchers];
}

#pragma mark - Setup Initialization
-(void)setupInitialization{
    [self.navigationItem setTitle:NSLocalizedString(@"nav_Voucher_V2", nil)];
    voucherArray = [[NSMutableArray alloc] init];
    campaignsArray = [[NSMutableArray alloc] init];
    [self.tableView setBackgroundColor:[[SharedMethod new] colorWithHexString:GREY_COLOR andAlpha:1.0f]];
}

-(void)setupData{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    paging=1;
    totalCount=0;
    isSearch=NO;
}

-(void)setupRefreshControl{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl setTintColor:[UIColor blackColor]];
    [refreshControl addTarget:self action:@selector(refreshAction:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

-(void)setupDelegates{
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
}

-(void)setupSearchController{
    UINavigationController *searchResultController = [storyboard instantiateViewControllerWithIdentifier:@"searchTableVC"];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultController];
    [self.searchController setSearchResultsUpdater:self];
    [self.searchController setDimsBackgroundDuringPresentation:YES];
    [[self.searchController searchBar] setDelegate:self];
    [self setDefinesPresentationContext:YES];
    [self.searchController.searchBar sizeToFit];
    
    [self.searchController.searchBar setTintColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
    [self.searchController.searchBar setBarTintColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
    self.searchController.searchBar.frame = CGRectMake(self.searchController.searchBar.frame.origin.x, self.searchController.searchBar.frame.origin.y, self.searchController.searchBar.frame.size.width, 44.0);
}

-(void)retrieveVouchers{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSString *deviceToken = [[SharedMethod new] checkDeviceToken];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:PLATFORM forKey:@"platform"];
        [params setObject:deviceToken forKey:@"push_token"];
        [params setObject:[SharedMethod getTokenWithRoute:@"user/campaigns" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        [params setObject:PROMOTION_LOAD forKey:@"limit"];
        [params setObject:[NSString stringWithFormat:@"%d",paging] forKey:@"page"];
        [params setObject:@[@"start",@"desc"] forKey:@"order_by"];
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        }
        
        #ifdef DEBUG
        NSLog(@"Retrieve Campaigns - %@",params);
        #endif
        
        [WebAPI retrieveCampaign:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.tabBarController setSelectedIndex:0];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            } else{
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    totalCount = [[result objectForKey:@"total_items"] intValue];
                    NSMutableArray *tempCampaign = [[NSMutableArray alloc] init];
                    for(int count=0;count<[[result objectForKey:@"campaigns"] count];count++){
                        [tempCampaign addObject:[[result objectForKey:@"campaigns"] objectAtIndex:count]];
                    }
                    
                    campaignsArray = [[SharedMethod new] processCampaign:tempCampaign andCurrentArray:campaignsArray];
                    
                    [self.tableView reloadData];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

#pragma mark - Actions
- (IBAction)bbtnCloseDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)bbtnSearchDidPressed:(id)sender {
    if(!isSearch){
        [self.tableView setTableHeaderView:self.searchController.searchBar];
        isSearch = YES;
    } else{
        [self.tableView setTableHeaderView:nil];
        isSearch = NO;
    }
}

-(void)refreshAction:(UIRefreshControl *)refreshControl {
    [refreshControl endRefreshing];
    paging=1;
    if([campaignsArray count]>0){
        [campaignsArray removeAllObjects];
        [self.tableView reloadData];
    }
    [self retrieveVouchers];
}

#pragma mark - UISearchController Delegate
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    //Do nothing//
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    searchText = searchBar.text;
    searchTableVC = (SearchTableVC *)self.searchController.searchResultsController;
    [self addChildViewController:searchTableVC];
    
    searchTableVC.view.frame = CGRectMake(searchTableVC.view.frame.origin.x, searchTableVC.view.frame.origin.y, searchTableVC.view.frame.size.width, searchTableVC.view.frame.size.height);
    searchTableVC.isCampaign = YES;
    searchTableVC.searchText = searchText;
    searchTableVC.campaignsArray = [[NSMutableArray alloc] init];
    [searchTableVC.tableView reloadData];
    [searchTableVC searchCampaign];
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchText = @"";
    [self.searchController.searchBar setText:@""];
    
    searchTableVC = (SearchTableVC *)self.searchController.searchResultsController;
    searchTableVC.promotionsArray = [[NSMutableArray alloc] init];
    [searchTableVC.tableView reloadData];
    
    isSearch = NO;
    
    [self.tableView setTableHeaderView:nil];
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [campaignsArray count];
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    VoucherTableCell *cell = (VoucherTableCell*)[tableView dequeueReusableCellWithIdentifier:@"voucherCell"];
    if(cell==nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VoucherTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Campaign *campaign = [campaignsArray objectAtIndex:indexPath.row];
    Voucher *voucher = campaign.voucher;
    
    [cell.imgUsed setHidden:YES];
    [cell.expireView setHidden:YES];
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[SharedMethod new] setGradient:cell.imgVoucher];
//    });
    
    if(voucher.image.length>0){
//        [[SharedMethod new] setLoadingIndicatorOnImage:cell.imgVoucher];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [cell.imgVoucher setContentMode:UIViewContentModeScaleAspectFit];
                [cell.imgVoucher setClipsToBounds:YES];
                [cell.imgVoucher sd_setImageWithURL:[NSURL URLWithString:voucher.image] placeholderImage:[UIImage imageNamed:@"bg_voucher"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    voucher.cachedImage = image;
//                    for(UIView *view in cell.imgVoucher.subviews){
//                        if(view.tag==100){
//                            [view removeFromSuperview];
//                        }
//                    }
                }];
            });
        });
    } else{
        [cell.imgVoucher setImage:[UIImage imageNamed:@"bg_voucher"]];
    }
    
    [cell.lblDescription setText:voucher.title];
    [cell.lblDescription setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [cell.lblDescription setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
    
    if(indexPath.row == [campaignsArray count] - 1 && [campaignsArray count] < totalCount){
        if([[SharedMethod new] checkNetworkConnectivity]){
            paging+=1;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self retrieveVouchers];
                });
            });
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"err_Error", nil) message:NSLocalizedString(@"msg_No_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        }
    }

    return cell;
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    VoucherTableCell *myCell = [tableView dequeueReusableCellWithIdentifier:@"voucherCell"];
    myCell.imgVoucher.layer.sublayers = nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Campaign *campaign = [campaignsArray objectAtIndex:indexPath.row];
    Voucher *selectedVoucher = campaign.voucher;
    
    [[SharedMethod new] addStatistics:@"post_view" itemID:selectedVoucher.ID];
    
    VoucherDetailVC *voucherDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"voucherDetailVC"];
    voucherDetailVC.isMyVoucher = NO;
    voucherDetailVC.selectedCampaign = campaign;
    voucherDetailVC.selectedVoucher = selectedVoucher;
    [self.navigationController pushViewController:voucherDetailVC animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ([UIScreen mainScreen].bounds.size.width/2)+44;//+60;
}

@end
