//
//  EditProfileVC.m
//  Oasis
//
//  Created by Wong Ryan on 26/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "EditProfileVC.h"
#import "TPKeyboardAvoidingCollectionView.h"
#import "SharedMethod.h"
#import "WebAPI.h"
#import "Constant.h"
#import "Base64.h"
#import "UIView+Toast.h"
#import "SVProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
#import "TextFieldValidator.h"
#import <PDKeychainBindings.h>

@interface EditProfileVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (strong, nonatomic) IBOutletCollection(TextFieldValidator) NSArray *textfields;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingCollectionView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imgPortrait;
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtName;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtIC;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtEmail;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtGender;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtRace;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtDob;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtCity;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtState;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtCountry;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtPhone;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtAddress;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtAddress2;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtAddress3;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtPostcode;

@property (weak, nonatomic) IBOutlet UILabel *lblChangePassword;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtCurrentPassword;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtNewPassword;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UISwitch *passwordSwitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailView_Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContent_Height;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@end

@implementation EditProfileVC{
    UIStoryboard *storyboard;
    UIAlertController *alertController;
    UIImagePickerController *imagePicker;
    NSInteger detailHeight;
    UIDatePicker *datePicker;
    NSData *convertedData;
    NSArray *labelsArray;
    NSMutableArray *statesArray;
    NSString *selectedGender, *selectedRace, *selectedState, *selectedCountry;
    NSDictionary *myDetail;
    UIImage *previousImage, *selectedImage;
    BOOL isChangePwd, deleteProfilePicture, profileChanged, contactChanged;
    UIAlertAction *cancelAction;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInitialization];
    [self setupTextFieldValidation];
    [self setupDoBKeyboard];
    [self setupData];
    [self setupUI];
    [self setupDismissKeyboard];
}

#pragma mark - Initialization
-(void)setupUI{
    for(UILabel *lbl in self.labels){
        [lbl setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [lbl setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
        [lbl setText:[labelsArray objectAtIndex:lbl.tag-1]];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        for(UITextField *txt in self.textfields){
            [[SharedMethod new] setupTextfield:txt fontType:FONT_REGULAR fontSize:14.0f textColor:THEME_COLOR textAlpha:1.0f addPadding:YES andAddBorder:YES];
            [txt setDelegate:self];
        }
//        [[SharedMethod new] setCornerRadius:self.imgPortrait andCornerRadius:self.imgPortrait.frame.size.width/2];
        [[self.imgPortrait layer] setBorderColor:[[[SharedMethod new] colorWithHexString:BLACK_COLOR andAlpha:1.0f] CGColor]];
        [[self.imgPortrait layer] setBorderWidth:2.0f];
    });
    
    [[SharedMethod new] setCustomButtonStyle:self.btnSave image:@"" borders:YES borderWidth:1.0f cornerRadius:0.0f borderColor:THEME_COLOR textColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    
    [self.txtEmail setEnabled:NO];
    [self.txtIC setEnabled:NO];
    [self.txtDob setEnabled:NO];
    [self.txtName setEnabled:YES];
    
    [self.txtEmail setBackgroundColor:[[SharedMethod new] colorWithHexString:NON_EDITABLE_COLOR andAlpha:1.0f]];
    [self.txtIC setBackgroundColor:[[SharedMethod new] colorWithHexString:NON_EDITABLE_COLOR andAlpha:1.0f]];
//    [self.txtName setBackgroundColor:[[SharedMethod new] colorWithHexString:NON_EDITABLE_COLOR andAlpha:1.0f]];
    [self.txtDob setBackgroundColor:[[SharedMethod new] colorWithHexString:NON_EDITABLE_COLOR andAlpha:1.0f]];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_SOCIAL]){
        [self.passwordSwitch setHidden:YES];
        [self.passwordView setHidden:YES];
        [self.lblChangePassword setHidden:YES];
    } else{
        [self.passwordSwitch setHidden:NO];
        [self.passwordView setHidden:NO];
        [self.lblChangePassword setHidden:NO];
    }
}

-(void)setupInitialization{
    [self.navigationItem setTitle:NSLocalizedString(@"nav_EditProfile_V2", nil)];
    [self.navigationController.navigationItem setHidesBackButton:YES animated:YES];
    
    statesArray = [[NSMutableArray alloc] init];
    detailHeight = self.detailView_Height.constant;
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [self.txtIC setKeyboardType:UIKeyboardTypeNumberPad];
    [self setupDoneToolbar:self.txtIC];
    [self setupDoneToolbar:self.txtPhone];
    [self setupDoneToolbar:self.txtPostcode];
    
    [self.txtCurrentPassword setPlaceholder:NSLocalizedString(@"phd_Password_V2", nil)];
    [self.txtNewPassword setPlaceholder:NSLocalizedString(@"phd_Password_V2", nil)];
    [self.txtConfirmPassword setPlaceholder:NSLocalizedString(@"phd_Password_V2", nil)];
}

-(void)setupData{
    isChangePwd = NO;
    selectedGender = @"";
    selectedRace = @"";
    selectedState = @"";
    
    labelsArray = @[NSLocalizedString(@"lbl_Name", nil),
                    NSLocalizedString(@"lbl_IC", nil),
                    NSLocalizedString(@"lbl_Mobile", nil),
                    NSLocalizedString(@"lbl_Email", nil),
                    NSLocalizedString(@"lbl_Gender", nil),
                    NSLocalizedString(@"lbl_Race", nil),
                    NSLocalizedString(@"lbl_Dob", nil),
                    NSLocalizedString(@"lbl_Address1", nil),
                    NSLocalizedString(@"lbl_Address2", nil),
                    NSLocalizedString(@"lbl_Address3", nil),
                    NSLocalizedString(@"lbl_City", nil),
                    NSLocalizedString(@"lbl_Country", nil),
                    NSLocalizedString(@"lbl_State", nil),
                    NSLocalizedString(@"lbl_Postcode", nil),
                    NSLocalizedString(@"lbl_Current_Password", nil),
                    NSLocalizedString(@"lbl_New_Password", nil),
                    NSLocalizedString(@"lbl_Confirm_New_Password", nil)];
    
    myDetail = [[NSUserDefaults standardUserDefaults] objectForKey:USER_DETAIL];
//    NSLog(@"myDetail - %@",myDetail);
    [self.imgPortrait setImage:self.currentImage];
    [self.txtName setText:[myDetail objectForKey:@"fullname"]];
    [self.txtEmail setText:[myDetail objectForKey:@"email"]];
    
    NSString *temp = [myDetail objectForKey:@"birthday"];
    if(temp.length>0){
//        [self.txtDob setText:[[SharedMethod new] convertDateTimeFormat:[myDetail objectForKey:@"birthday"] andInputFormat:@"yyyy-MM-dd" andOutputFormat:@"dd.M.yyyy"]];
        [self.txtDob setText:[[SharedMethod new] convertDateTimeFormat:[myDetail objectForKey:@"birthday"] andInputFormat:@"yyyy-MM-dd" andOutputFormat:@"dd MMM yyyy"]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSDate *tempDate = [dateFormatter dateFromString:self.txtDob.text];
        [datePicker setDate:tempDate];
    }
    
    [self.txtPhone setText:[myDetail objectForKey:@"contact"]];
    [self.txtIC setText:[myDetail objectForKey:@"nric"]];
    [self.txtCity setText:[myDetail objectForKey:@"city"]];
    [self.txtAddress setText:[myDetail objectForKey:@"address1"]];
    [self.txtAddress2 setText:[myDetail objectForKey:@"address2"]];
    [self.txtAddress3 setText:[myDetail objectForKey:@"address3"]];
    
    selectedGender = [myDetail objectForKey:@"gender"];
    selectedRace = [myDetail objectForKey:@"race"];
    selectedState = [myDetail objectForKey:@"state_code"];
    selectedCountry = [myDetail objectForKey:@"country_code"];
    
    [self.txtPostcode setText:[myDetail objectForKey:@"postcode"]];
    
    NSMutableArray *genderArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:GENDER]];
    if([genderArray count]>0){
        for(int count=0;count<[genderArray count];count++){
            if([[[genderArray objectAtIndex:count] objectForKey:@"code"] isEqualToString:selectedGender]){
                [self.txtGender setText:[[genderArray objectAtIndex:count] objectForKey:@"label"]];
                selectedGender = [[genderArray objectAtIndex:count] objectForKey:@"code"];
                break;
            }
        }
    }

    NSMutableArray *raceArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:RACE]];
    if([raceArray count]>0){
        for(int count=0;count<[raceArray count];count++){
            if([[[raceArray objectAtIndex:count] objectForKey:@"code"] isEqualToString:selectedRace]){
                [self.txtRace setText:[[raceArray objectAtIndex:count] objectForKey:@"label"]];
                selectedRace = [[raceArray objectAtIndex:count] objectForKey:@"code"];
                break;
            }
        }
    }

//    NSMutableArray *stateArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:STATES]];
//    if([stateArray count]>0){
//        for(int count=0;count<[stateArray count];count++){
//            if([[[stateArray objectAtIndex:count] objectForKey:@"code"] isEqualToString:selectedState]){
//                [self.txtState setText:[[stateArray objectAtIndex:count] objectForKey:@"name"]];
//                selectedState = [[stateArray objectAtIndex:count] objectForKey:@"code"];
//                break;
//            }
//        }
//    }
    
    NSMutableArray *countryArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:COUNTRIES]];
    if([countryArray count]>0){
        for(int count=0;count<[countryArray count];count++){
            if([[[countryArray objectAtIndex:count] objectForKey:@"iso"] isEqualToString:selectedCountry]){
                [self.txtCountry setText:[[countryArray objectAtIndex:count] objectForKey:@"name"]];
                selectedCountry = [[countryArray objectAtIndex:count] objectForKey:@"iso"];
                
                statesArray = [[countryArray objectAtIndex:count] objectForKey:@"states"];
                
                for(int count2=0;count2<[[[countryArray objectAtIndex:count] objectForKey:@"states"] count];count2++){
                    if([[[[[countryArray objectAtIndex:count] objectForKey:@"states"] objectAtIndex:count2] objectForKey:@"code"] isEqualToString:selectedState]){
                        [self.txtState setText:[[[[countryArray objectAtIndex:count] objectForKey:@"states"] objectAtIndex:count2] objectForKey:@"name"]];
                    }
                }
                break;
            }
        }
    }
}

#pragma mark - Custom Functions
-(void)setupTextFieldValidation{
    [self.txtName setValidateOnCharacterChanged:NO];
    [self.txtName updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtName setIsMandatory:YES];
    
    [self.txtIC setValidateOnCharacterChanged:NO];
//    [self.txtIC addRegx:@"[0-9]{12}" withMsg:NSLocalizedString(@"err_Invalid_IC", nil)];
    [self.txtIC updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtIC setIsMandatory:YES];
    
    [self.txtCurrentPassword setValidateOnCharacterChanged:NO];
    [self.txtCurrentPassword updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtCurrentPassword setIsMandatory:YES];
    
    [self.txtNewPassword setValidateOnCharacterChanged:NO];
    [self.txtNewPassword updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtNewPassword setIsMandatory:YES];
    
    [self.txtConfirmPassword setValidateOnCharacterChanged:NO];
    [self.txtConfirmPassword updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtConfirmPassword setIsMandatory:YES];
    
    [self.txtConfirmPassword addConfirmValidationTo:self.txtNewPassword withMsg:NSLocalizedString(@"err_Invalid_Password", nil)];
    
    [self.txtEmail setValidateOnCharacterChanged:NO];
    [self.txtEmail addRegx:@"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" withMsg:NSLocalizedString(@"err_Invalid_Email", nil)];
    [self.txtEmail updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtEmail setIsMandatory:YES];
    
    [self.txtCity setValidateOnCharacterChanged:NO];
    [self.txtCity updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtCity setIsMandatory:YES];
    
    [self.txtPhone setValidateOnCharacterChanged:NO];
    [self.txtPhone addRegx:@"[0-9]{10,13}" withMsg:NSLocalizedString(@"err_Invalid_Phone", nil)];
    [self.txtPhone updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtPhone setIsMandatory:YES];
    
    [self.txtAddress setIsMandatory:NO];
    [self.txtAddress2 setIsMandatory:NO];
    [self.txtAddress3 setIsMandatory:NO];
    
    [self.txtPostcode setValidateOnCharacterChanged:NO];
    [self.txtPostcode updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtPostcode setIsMandatory:YES];}

-(void)setupDismissKeyboard{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)adjustScrollContent{
    if(isChangePwd){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.passwordView setAlpha:1.0f];
            [UIView animateWithDuration:0.5 animations:^{
                [self.detailView_Height setConstant:(self.detailView_Height.constant+self.passwordView.frame.size.height+8)];
            }];
        });

    } else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.passwordView setAlpha:0.0f];
            [UIView animateWithDuration:0.5 animations:^{
                [self.detailView_Height setConstant:(self.detailView_Height.constant-self.passwordView.frame.size.height-8)];
            }];
        });
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.5 delay:0.3 options:UIViewAnimationOptionTransitionNone animations:^{
            [self.scrollContent_Height setConstant:(self.detailView_Height.constant+self.detailView.frame.origin.y+8)];
        } completion:nil];
    });
}

-(void)setupDoneToolbar:(UITextField*)selectedTextfield{
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setBarStyle:UIBarStyleBlackOpaque];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"btn_Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(btnDoneDidPressed:)];
    [doneButton setTag:2];
    [doneButton setTintColor:[UIColor whiteColor]];
    UIBarButtonItem *flexibleSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolbar.items = @[flexibleSeparator, doneButton];
    selectedTextfield.inputAccessoryView = toolbar;
}

-(void)setupDoBKeyboard{
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setBarStyle:UIBarStyleBlackOpaque];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"btn_Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(bbtnDoneDidPressed:)];
    [btnDone setTintColor:[UIColor whiteColor]];    
    [btnDone setTag:1];
    UIBarButtonItem *flexibleSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolbar.items = @[flexibleSeparator, btnDone];
    self.txtDob.inputAccessoryView = toolbar;
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [datePicker setBackgroundColor:[UIColor whiteColor]];
    //    datePicker.maximumDate = [NSDate date];
    //
    //    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    //    NSDate *currentDate = [NSDate date];
    //    NSDateComponents *comps = [[NSDateComponents alloc] init];
    //    [comps setYear:-100];
    //    NSDate *minDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    //    [comps setYear:-18];
    //    NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    //
    //    datePicker.minimumDate = minDate;
    //    datePicker.maximumDate = maxDate;
    
    self.txtDob.inputView = datePicker;
}

-(void)takePhoto{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        imagePicker = [[UIImagePickerController alloc]init];
        [imagePicker setDelegate:self];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        
        [imagePicker setAllowsEditing:NO];
        [imagePicker setModalPresentationStyle:UIModalPresentationFullScreen];
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:@"Fail to open camera" message:@"Camera is not available on your device" cancelButton:@"Ok"] animated:YES completion:nil];
    }
}

-(void)chooseFromPhotos{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
        imagePicker = [[UIImagePickerController alloc]init];
        [imagePicker setDelegate:self];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [imagePicker setAllowsEditing:NO];
        [imagePicker setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

-(BOOL)validatePasswords{
    if(isChangePwd){
        if(self.txtCurrentPassword.text.length<=0){
            [self.view makeToast:NSLocalizedString(@"err_Empty_Current_Password", nil)];
            return NO;
        } else if(self.txtNewPassword.text.length<=0){
            [self.view makeToast:NSLocalizedString(@"err_Empty_New_Password", nil)];
            return NO;
        } else if(self.txtConfirmPassword.text.length<=0){
            [self.view makeToast:NSLocalizedString(@"err_Empty_Confirm_Password", nil)];
            return NO;
        } else if(![self.txtNewPassword.text isEqualToString:self.txtConfirmPassword.text]){
            [self.view makeToast:NSLocalizedString(@"err_Invalid_Password", nil)];
            return NO;
        }
    } 
    return YES;
}

-(NSString*)formatDate:(NSString*)text{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [formatter dateFromString:text];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *convert = [formatter stringFromDate:date];
    return convert;
}

-(void)uploadPortrait{
    if(deleteProfilePicture){
        if([[SharedMethod new] checkNetworkConnectivity]){
            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
            [params setObject:PLATFORM forKey:@"platform"];
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN] forKey:@"push_token"];
            [params setObject:[SharedMethod getTokenWithRoute:@"user/profile/image" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
            
            #ifdef DEBUG
            NSLog(@"Prepare Delete Profile Picture Params - %@",params);
            #endif
            
            [WebAPI deleteProfilePicture:params andSuccess:^(id successBlock) {
                [SVProgressHUD dismiss];
                NSDictionary *result = successBlock;
                if([result objectForKey:@"errMsg"]){
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                        [[SharedMethod new] removeProfileImage:YES];
                        [[SharedMethod new] removeProfileImage:NO];
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                        [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                        [self.navigationController popViewControllerAnimated:YES];
                        [self.tabBarController setSelectedIndex:0];
                    }
                } else{
                    NSString *code = [result objectForKey:@"code"];
                    if([code isEqualToString:@"000"]){
                        #ifdef DEBUG
                        NSLog(@"Done removing portrait");
                        #endif
                        
                        [self.imgPortrait setImage:[UIImage imageNamed:@"ic_profile_placeholder"]];
                        [[SharedMethod new] removeProfileImage:YES];
                        [[SharedMethod new] removeProfileImage:NO];
                    }
                }
            }];
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        }
    } else{
        if([[SharedMethod new] checkNetworkConnectivity]){
            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
            [params setObject:PLATFORM forKey:@"platform"];
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN] forKey:@"push_token"];
            [params setObject:[SharedMethod getTokenWithRoute:@"user/profile/image" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
            [params setObject:[convertedData base64EncodedString] forKey:@"image:base64"];
            
            #ifdef DEBUG
            NSLog(@"Prepare Upload Profile Picture Params - %@",params);
            #endif
            
            [WebAPI uploadProfilePicture:params andSuccess:^(id successBlock) {
                [SVProgressHUD dismiss];
                NSDictionary *result = successBlock;
                if([result objectForKey:@"errMsg"]){
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                        [[SharedMethod new] removeProfileImage:YES];
                        [[SharedMethod new] removeProfileImage:NO];
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                        [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                        [self.navigationController popViewControllerAnimated:YES];
                        [self.tabBarController setSelectedIndex:0];
                    }
                } else{
                    NSString *code = [result objectForKey:@"code"];
                    if([code isEqualToString:@"000"]){
                        #ifdef DEBUG
                        NSLog(@"Done uploading portrait");
                        #endif
                    }
                }
            }];
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        }
    }
}

-(void)searchInvalidInput{
    if(self.txtGender.text.length<=0){
        [self.scrollView setContentOffset:CGPointMake(0, (self.txtGender.frame.origin.y+self.txtGender.frame.size.height) - (self.txtGender.frame.origin.y/2)) animated:YES];
        [self.view makeToast:NSLocalizedString(@"err_Empty_Gender", nil)];
    } else if(self.txtRace.text.length<=0){
        [self.scrollView setContentOffset:CGPointMake(0, (self.txtRace.frame.origin.y+self.txtRace.frame.size.height) - (self.txtRace.frame.origin.y/2)) animated:YES];
        [self.view makeToast:NSLocalizedString(@"err_Empty_Race", nil)];
    } else if(self.txtState.text.length<=0){
        [self.scrollView setContentOffset:CGPointMake(0, (self.txtState.frame.origin.y+self.txtState.frame.size.height) - (self.txtState.frame.origin.y/2)) animated:YES];
        [self.view makeToast:NSLocalizedString(@"err_Empty_State", nil)];
    } else if(![self.txtName validate]){
        [self.txtName becomeFirstResponder];
    } else if(![self.txtIC validate]){
        [self.txtIC becomeFirstResponder];
    } else if(![self.txtPhone validate]){
        [self.txtPhone becomeFirstResponder];
    } else if(![self.txtEmail validate]){
        [self.txtEmail becomeFirstResponder];
    } else if(![self.txtDob validate]){
        [self.txtDob becomeFirstResponder];
    } else if(![self.txtCity validate]){
        [self.txtCity becomeFirstResponder];
    } else if(![self.txtPostcode validate]){
        [self.txtPostcode becomeFirstResponder];
    }
}

#pragma mark - UIImagePicker Delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary*)info{
//    UIImage *initialImage = [[SharedMethod new] rorateImagePortrait:[info objectForKey:UIImagePickerControllerOriginalImage]];
    UIImage *initialImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *image = [self squareImageFromImage:initialImage scaledToSize:320];
    
    convertedData = UIImageJPEGRepresentation(image, 0.4f);//Double check the compression values
    
    #ifdef DEBUG
    NSLog(@"Image size(bytes) - %d",(int)convertedData.length);
    #endif
    
    selectedImage = [UIImage imageWithData:convertedData];
    
    [self.imgPortrait setImage:selectedImage];
    
    profileChanged = YES;
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [self uploadPortrait];
    }];
}

- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark - UITextField Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField.tag==8 || textField.tag==9 || textField.tag==10 || textField.tag==11){
        if(textField.text.length>=40 && range.length==0){
            return NO;
        }
    } else if(textField.tag==14){
        if(textField.text.length>=6 && range.length==0){
            return NO;
        }
    }
//    if(textField.tag==2){
//        if(textField.text.length>12 && range.length==0){
//            return NO;
//        }
//        
//        if([string rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet].invertedSet].location != NSNotFound){
//            return NO;
//        }
//    }
    
    /*else if(textField.tag==11){
     if([string rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet].invertedSet].location != NSNotFound){
     return NO;
     }
     }*/
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self dismissKeyboard];
    return YES;
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSaveDidPressed:(id)sender {
    //Process Save API//
    [self dismissKeyboard];
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_SOCIAL]){
        if([self.txtName validate] && [self.txtEmail validate] && [self.txtIC validate] && [self.txtPhone validate] && [self.txtDob validate] && self.txtGender.text.length>0 && self.txtRace.text.length>0 && [self.txtCity validate] && self.txtCountry.text.length>0 && self.txtState.text.length>0 && [self.txtPostcode validate]){
            if([[SharedMethod new] checkNetworkConnectivity]){
                [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
                
                [params setObject:[self.txtName.text stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]] forKey:@"fullname"];
                [params setObject:[myDetail objectForKey:@"nric"] forKey:@"nric"];
//                [params setObject:[myDetail objectForKey:@"email"] forKey:@"email"];
                [params setObject:[[SharedMethod new] convertToSendDateFormat:self.txtDob.text] forKey:@"birthday"];
                [params setObject:selectedGender forKey:@"gender"];
                [params setObject:selectedRace forKey:@"race"];
                [params setObject:selectedCountry forKey:@"country_code"];
                [params setObject:self.txtCity.text forKey:@"city"];
                [params setObject:selectedState forKey:@"state_code"];
                
                if(![self.txtPhone.text isEqualToString:[myDetail objectForKey:@"contact"]]){
                    [params setObject:self.txtPhone.text forKey:@"contact"];
                }
                
                [params setObject:self.txtAddress.text forKey:@"address1"];
                [params setObject:self.txtAddress2.text forKey:@"address2"];
                [params setObject:self.txtAddress3.text forKey:@"address3"];
                [params setObject:self.txtPostcode.text forKey:@"postcode"];

                [params setObject:[myDetail objectForKey:@"fullname"] forKey:@"fullname"];
                [params setObject:PLATFORM forKey:@"platform"];
                [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN] forKey:@"push_token"];
                [params setObject:[SharedMethod getTokenWithRoute:@"user/profile" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
                
                if(![self.txtPhone.text isEqualToString:[myDetail objectForKey:@"contact"]]){
                    [params setObject:self.txtPhone.text forKey:@"contact"];
                }
                
#ifdef DEBUG
                NSLog(@"Prepare Edit Profile Params - %@",params);
#endif
                
                [WebAPI updateProfileDetail:params andSuccess:^(id successBlock) {
                    [SVProgressHUD dismiss];
                    NSDictionary *result = successBlock;
                    if([result objectForKey:@"errMsg"]){
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                        if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                            [[SharedMethod new] removeProfileImage:YES];
                            [[SharedMethod new] removeProfileImage:NO];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                            [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                            [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                            [self.navigationController popViewControllerAnimated:YES];
                            [self.tabBarController setSelectedIndex:0];
                        }
                    } else{
                        NSString *code = [result objectForKey:@"code"];
                        if([code isEqualToString:@"000"]){
                            result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"user"] forKey:USER_PROFILE];
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                    }
                }];
            } else{
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            }
        } else{
            [self searchInvalidInput];
        }
    } else{
        if(isChangePwd){
//            if([self validatePasswords]){
            if([self.txtEmail validate] && [self.txtIC validate] && [self.txtPhone validate] && [self.txtDob validate] && self.txtGender.text.length>0 && self.txtRace.text.length>0 && [self.txtCity validate] && self.txtCountry.text.length>0 && self.txtState.text.length>0 && [self.txtPostcode validate] && [self.txtCurrentPassword validate] && [self.txtNewPassword validate] && [self.txtConfirmPassword validate]){
                if([[SharedMethod new] checkNetworkConnectivity]){
                    [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
                    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
                    
                    [params setObject:[self.txtName.text stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceCharacterSet]] forKey:@"fullname"];
                    [params setObject:[myDetail objectForKey:@"nric"] forKey:@"nric"];
//                    [params setObject:[myDetail objectForKey:@"email"] forKey:@"email"];
                    [params setObject:[[SharedMethod new] convertToSendDateFormat:self.txtDob.text] forKey:@"birthday"];
                    [params setObject:selectedGender forKey:@"gender"];
                    [params setObject:selectedRace forKey:@"race"];
                    [params setObject:self.txtCity.text forKey:@"city"];
                    [params setObject:selectedCountry forKey:@"country_code"];
                    [params setObject:selectedState forKey:@"state_code"];
                    
                    if(![self.txtPhone.text isEqualToString:[myDetail objectForKey:@"contact"]]){
                        [params setObject:self.txtPhone.text forKey:@"contact"];
                    }
                    
                    [params setObject:self.txtAddress.text forKey:@"address1"];
                    [params setObject:self.txtAddress2.text forKey:@"address2"];
                    [params setObject:self.txtPostcode.text forKey:@"postcode"];
                    
                    [params setObject:self.txtCurrentPassword.text forKey:@"current_password"];
                    [params setObject:self.txtNewPassword.text forKey:@"password"];
                    [params setObject:self.txtConfirmPassword.text forKey:@"password_confirm"];
                    
                    [params setObject:PLATFORM forKey:@"platform"];
                    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN] forKey:@"push_token"];
                    [params setObject:[SharedMethod getTokenWithRoute:@"user/profile" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
                    
                    #ifdef DEBUG
                    NSLog(@"Prepare Edit Profile Params - %@",params);
                    #endif
                    
                    [WebAPI updateProfileDetail:params andSuccess:^(id successBlock) {
                        [SVProgressHUD dismiss];
                        NSDictionary *result = successBlock;
                        if([result objectForKey:@"errMsg"]){
                            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                        } else{
                            NSString *code = [result objectForKey:@"code"];
                            if([code isEqualToString:@"000"]){
                                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                                [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"user"] forKey:USER_PROFILE];
                                [self.navigationController popViewControllerAnimated:YES];
                            }
                        }
                    }];
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            } else{
                [self searchInvalidInput];
            }
        } else{
//            NSLog(@"isProfileChanged - %d",profileChanged);
            if(profileChanged){
                //Upload Picture Only
                if([[SharedMethod new] checkNetworkConnectivity]){
                    [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
                    NSDictionary *params = @{@"userId"           : [NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] intValue]],
                                             @"image:base64"     : [convertedData base64EncodedString],
                                             @"platform"         : PLATFORM,
                                             @"push_token"       : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                             @"token"            : [SharedMethod getTokenWithRoute:@"user/profile/image" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                             };
                    
                    #ifdef DEBUG
                    NSLog(@"Request Profile - %@",params);
                    #endif
                    
                    [WebAPI uploadProfilePicture:params andSuccess:^(id successBlock) {
                        NSDictionary *result = successBlock;
                        if([result objectForKey:@"errMsg"]){
                            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                        } else{
                            NSString *code = [result objectForKey:@"code"];
                            if([code isEqualToString:@"000"]){
                                [self.navigationController popViewControllerAnimated:YES];
                            }
                        }
                    }];
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            } else{
                if([self.txtEmail validate] && [self.txtIC validate] && [self.txtPhone validate] && [self.txtDob validate] && self.txtGender.text.length>0 && self.txtRace.text.length>0 && [self.txtCity validate] && self.txtCountry.text.length>0 && self.txtState.text.length>0 && [self.txtPostcode validate]){
                    if([[SharedMethod new] checkNetworkConnectivity]){
                        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
                        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
                        
                        if(![[[SharedMethod new] convertToDefaultDateFormat:[self formatDate:[myDetail objectForKey:@"birthday"]]] isEqualToString:self.txtDob.text]){
                            [params setObject:[[SharedMethod new] convertToSendDateFormat:self.txtDob.text] forKey:@"birthday"];
                        }
                        
                        [params setObject:[self.txtName.text stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceCharacterSet]] forKey:@"fullname"];
                        [params setObject:[myDetail objectForKey:@"nric"] forKey:@"nric"];
//                    [params setObject:[myDetail objectForKey:@"email"] forKey:@"email"];
                        [params setObject:[[SharedMethod new] convertToSendDateFormat:self.txtDob.text] forKey:@"birthday"];
                        [params setObject:selectedGender forKey:@"gender"];
                        [params setObject:selectedRace forKey:@"race"];
                        [params setObject:self.txtCity.text forKey:@"city"];
                        [params setObject:selectedCountry forKey:@"country_code"];
                        [params setObject:selectedState forKey:@"state_code"];
                        
                        if(![self.txtPhone.text isEqualToString:[myDetail objectForKey:@"contact"]]){
                            [params setObject:self.txtPhone.text forKey:@"contact"];
                        }
                        
                        [params setObject:self.txtAddress.text forKey:@"address1"];
                        [params setObject:self.txtAddress2.text forKey:@"address2"];
                        [params setObject:self.txtAddress3.text forKey:@"address3"];
                        [params setObject:self.txtPostcode.text forKey:@"postcode"];

                        [params setObject:PLATFORM forKey:@"platform"];
                        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN] forKey:@"push_token"];
                        [params setObject:[SharedMethod getTokenWithRoute:@"user/profile" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
                        
                        #ifdef DEBUG
                        NSLog(@"Prepare Edit Profile Params - %@",params);
                        #endif
                        
                        [WebAPI updateProfileDetail:params andSuccess:^(id successBlock) {
                            [SVProgressHUD dismiss];
                            NSDictionary *result = successBlock;
                            if([result objectForKey:@"errMsg"]){
                                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"]cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                            } else{
                                NSString *code = [result objectForKey:@"code"];
                                if([code isEqualToString:@"000"]){
                                    result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                                    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"user"] forKey:USER_PROFILE];
                                    [self.navigationController popViewControllerAnimated:YES];
                                }
                            }
                        }];
                    } else{
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    }
                } else{
                    [self searchInvalidInput];
                }
            }
        }
    }
}

- (IBAction)btnGenderDidPressed:(id)sender {
    [self dismissKeyboard];
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:NSLocalizedString(@"msg_Gender", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSMutableArray *genderArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:GENDER]];
    
    if([genderArray count]>0){
        for(int count=0;count<[genderArray count];count++){
            UIAlertAction *action = [UIAlertAction actionWithTitle:[[genderArray objectAtIndex:count] objectForKey:@"label"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.txtGender setText:[[genderArray objectAtIndex:count] objectForKey:@"label"]];
                selectedGender = [[genderArray objectAtIndex:count] objectForKey:@"code"];
            }];
            
            [alertController addAction:action];
        }
    }
    
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:^{
        [self dismissKeyboard];
    }];
}

- (IBAction)btnRaceDidPressed:(id)sender {
    [self dismissKeyboard];
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:NSLocalizedString(@"msg_Race", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSMutableArray *raceArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:RACE]];
    
    if([raceArray count]>0){
        for(int count=0;count<[raceArray count];count++){
            UIAlertAction *action = [UIAlertAction actionWithTitle:[[raceArray objectAtIndex:count] objectForKey:@"label"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.txtRace setText:[[raceArray objectAtIndex:count] objectForKey:@"label"]];
                selectedRace = [[raceArray objectAtIndex:count] objectForKey:@"code"];
            }];
            
            [alertController addAction:action];
        }
    }
    
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:^{
        [self dismissKeyboard];
    }];
}

- (IBAction)btnCountryDidPressed:(id)sender {
    [self dismissKeyboard];
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:NSLocalizedString(@"msg_Country", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSMutableArray *countryArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:COUNTRIES]];
    
    if([countryArray count]>0){
        for(int count=0;count<[countryArray count];count++){
            UIAlertAction *action = [UIAlertAction actionWithTitle:[[countryArray objectAtIndex:count] objectForKey:@"name"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.txtCountry setText:[[countryArray objectAtIndex:count] objectForKey:@"name"]];
                selectedCountry = [[countryArray objectAtIndex:count] objectForKey:@"iso"];
                
                statesArray = [[countryArray objectAtIndex:count] objectForKey:@"states"];
                [self.txtState setText:[[statesArray objectAtIndex:0] objectForKey:@"name"]];
                selectedState = [[statesArray objectAtIndex:0] objectForKey:@"code"];
            }];
            
            [alertController addAction:action];
        }
    }
    
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:^{
        [self dismissKeyboard];
    }];
}

- (IBAction)btnStateDidPressed:(id)sender {
    [self dismissKeyboard];
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:NSLocalizedString(@"msg_State", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSMutableArray *stateArray = [[NSMutableArray alloc] initWithArray:statesArray];
    
    if([stateArray count]>0){
        for(int count=0;count<[stateArray count];count++){
            UIAlertAction *action = [UIAlertAction actionWithTitle:[[stateArray objectAtIndex:count] objectForKey:@"name"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.txtState setText:[[stateArray objectAtIndex:count] objectForKey:@"name"]];
                selectedState = [[stateArray objectAtIndex:count] objectForKey:@"code"];
            }];
            
            [alertController addAction:action];
        }
    }
    
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:^{
        [self dismissKeyboard];
    }];
}

-(void)checkCameraPermission{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized){
        [self takePhoto];
    } else if(authStatus == AVAuthorizationStatusDenied){
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            if(granted){
                [self takePhoto];
            } else{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                NSLog(@"Camera Permission Denied");
            }
        }];
    } else{
         [self takePhoto];   
    }
}

-(void)checkGalleryPermission{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    
    if(status == PHAuthorizationStatusAuthorized) {
        [self chooseFromPhotos];
    } else{
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if(status == PHAuthorizationStatusAuthorized) {
                [self chooseFromPhotos];
            } else if(status == PHAuthorizationStatusDenied){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                NSLog(@"Gallery Permission Denied");
            }
        }];
    }
}

- (IBAction)btnPortraitDidPressed:(id)sender {
    [self dismissKeyboard];
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_TakePhoto", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self checkCameraPermission];
    }];
    
    UIAlertAction *galleryAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Gallery", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self checkGalleryPermission];
    }];
    
    UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Delete_Portrait", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        deleteProfilePicture=YES;
        [self.imgPortrait setImage:[UIImage imageNamed:@"temp_profile.png"]];
        [self uploadPortrait];
    }];
    
    [alertController addAction:cameraAction];
    [alertController addAction:galleryAction];
    [alertController addAction:deleteAction];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)bbtnDoneDidPressed:(UIButton*)sender {
    if(sender.tag == 1){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
        NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",datePicker.date]];
        
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSString *myDate = [dateFormatter stringFromDate:date];
        [self.txtDob setText:[[SharedMethod new] convertToDefaultDateFormat:myDate]];
    }
    [self dismissKeyboard];
}

- (IBAction)btnDoneDidPressed:(id)sender{
    [self dismissKeyboard];
}

- (IBAction)btnSwitchDidPressed:(UISwitch*)sender {
    isChangePwd=!isChangePwd;
    [sender setOn:isChangePwd];
    [self adjustScrollContent];
}


@end
