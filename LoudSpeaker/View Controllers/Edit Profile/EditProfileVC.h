//
//  EditProfileVC.h
//  Oasis
//
//  Created by Wong Ryan on 26/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileVC : UIViewController
@property (nonatomic) UIImage *currentImage;
@end
