//
//  MenuVC.m
//  Oasis
//
//  Created by Wong Ryan on 21/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "MenuVC.h"
#import "MenuCell.h"
#import "MenuDetailVC.h"
#import "SharedMethod.h"
#import "SQLManager.h"
#import "Constant.h"
#import "Menu.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SVProgressHUD.h"
#import "WebAPI.h"
#import "EmptyCollectionCell.h"
#import <PDKeychainBindings.h>

@interface MenuVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContent_Width;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *indicatorView;

@end

@import Firebase;

@implementation MenuVC{
    int selectedButton, totalCount, paging, searchTotalCount, searchPaging;
    UIStoryboard *storyboard;
    NSMutableArray *buttons, *menuArray, *searchMenuArray, *titleArray;
    BOOL isSearch,isEmpty,isTimeOut, isDetail;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(SYSTEM_VERSION_LESS_THAN(@"10")){
        [self updateTabBarController];
    }
    
    [self setupDelegates];
    [self setupInitialization];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupNavigationBar];
    [self.collectionView setContentOffset:CGPointMake(0, 0)];
    [self setupSwipe];
    [self setupData];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setupTabButtons];
    });
    
    if(!isSearch){
        if([menuArray count]>0){
            [menuArray removeAllObjects];
        }
        [self retrieveMenu];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(!self.searchBar.hidden && !isDetail){
        isSearch = NO;
        [self.searchBar setHidden:YES];
        
        if([searchMenuArray count]>0){
            [searchMenuArray removeAllObjects];
        }
        
        searchTotalCount = 0;
        searchPaging = 1;
        selectedButton = 0;
        [self.collectionView reloadData];
    }
}

#pragma mark - Custom Functions
-(void)setupNavigationBar{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationItem setTitle:NSLocalizedString(@"nav_Price_List", nil)];
    self.navigationItem.rightBarButtonItems = nil;
}

-(void)updateTabBarController{
    UITabBar *tabBar = self.tabBarController.tabBar;
    UITabBarItem *tab1 = [[tabBar items] objectAtIndex:1];
    [tab1 setImage:[[UIImage imageNamed:@"ic_bottombar_menu"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab1 setSelectedImage:[[UIImage imageNamed:@"ic_bottombar_menu_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
}

-(void)setupInitialization{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self.collectionView registerNib:[UINib nibWithNibName:@"MenuCell" bundle:nil] forCellWithReuseIdentifier:@"menuCell"];
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    [self.scrollView setPagingEnabled:NO];
    
    buttons = [[NSMutableArray alloc] init];
    menuArray = [[NSMutableArray alloc] init];
    searchMenuArray = [[NSMutableArray alloc] init];
    titleArray = [[NSMutableArray alloc] init];
    
    [self.indicatorView setBackgroundColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.scrollContent setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.scrollView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.view setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

-(void)setupSwipe{
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(leftSwipe:)];
    leftSwipe.direction=UISwipeGestureRecognizerDirectionRight;
    [self.collectionView addGestureRecognizer:leftSwipe];
    
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(rightSwipe:)];
    rightSwipe.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.collectionView addGestureRecognizer:rightSwipe];
}

-(void)setupDelegates{
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
    [self.searchBar setDelegate:self];
}

-(void)setupData{
    totalCount = 0;
    paging = 1;
    searchTotalCount = 0;
    searchPaging = 1;
    isEmpty = NO;
    isTimeOut = NO;
    
    if([titleArray count]>0){
        [titleArray removeAllObjects];
    }
    
    NSArray *temp = [[NSUserDefaults standardUserDefaults] objectForKey:MENU_PARENTS];
    for(int loop=0;loop<[temp count];loop++){
        NSString *tempParent = [NSString stringWithFormat:@"%d",[[[temp objectAtIndex:loop] objectForKey:@"parent"] intValue]];
        if([tempParent isEqualToString:@"0"]){
            [titleArray addObject:[temp objectAtIndex:loop]];
        }
    }
    
    [self.collectionView setShowsVerticalScrollIndicator:NO];
    [self.collectionView reloadData];
}

-(void)setupTabButtons{
    int xAxis = 0;
    for(int number=0;number<[titleArray count];number++){
//        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(xAxis, 0, self.scrollView.frame.size.width/[titleArray count], self.scrollView.frame.size.height)];
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(xAxis, 0, self.scrollView.frame.size.width/3, self.scrollView.frame.size.height)];
        
        [btn.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:15]];
        btn.tag=number;
        xAxis = btn.frame.size.width+btn.frame.origin.x+1;
        
        [btn setTitle:[[titleArray objectAtIndex:number] objectForKey:@"name"] forState:UIControlStateNormal];
        [btn setTitleColor:[[SharedMethod new] colorWithHexString:GREY_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
        [btn setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateSelected];
        [btn setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
        
        [btn addTarget:self action:@selector(btnTabDidPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if(!isDetail){
            if(btn.tag==0){
                [btn setSelected:YES];
                [btn setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
                [btn setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
                [self updateSelectedItem:btn];
            }
        } else{
            if(number == selectedButton){
                [btn setSelected:YES];
                [btn setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
                [btn setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
                [self updateSelectedItem:btn];
            }
        }
        
        [buttons addObject:btn];
        [self.scrollContent addSubview:btn];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.scrollContent_Width setConstant:xAxis];
        [self.scrollView setContentSize:CGSizeMake(xAxis, self.scrollView.frame.size.height)];
        [self.scrollContent bringSubviewToFront:self.indicatorView];
    });
//    [self.scrollContent bringSubviewToFront:self.searchBar];
}

-(void)updateSelectedItem:(UIButton*)btn{
    [UIView animateWithDuration:0.3f animations:^{
        [self.indicatorView setFrame:CGRectMake(btn.frame.origin.x, btn.frame.size.height-4, btn.frame.size.width, 4)];
    }];
}

-(void)searchMenu:(NSString*)searchText{
    NSString *deviceToken = [[SharedMethod new] checkDeviceToken];
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        [FIRAnalytics logEventWithName:@"Menu Search" parameters:nil];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        
        [params setObject:PLATFORM forKey:@"platform"];
        FIRCrashMessage([NSString stringWithFormat:@"Platform:%@",PLATFORM]);
        
        [params setObject:deviceToken forKey:@"push_token"];
        FIRCrashMessage([NSString stringWithFormat:@"Push Token:%@",deviceToken]);
        
        [params setObject:[SharedMethod getTokenWithRoute:@"user/posts/menu" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        
        [params setObject:POST_TYPE_3 forKey:@"type"];
        FIRCrashMessage([NSString stringWithFormat:@"Type:%@",POST_TYPE_3]);
        
        [params setObject:MENU_LOAD forKey:@"limit"];
        FIRCrashMessage([NSString stringWithFormat:@"Limit:%@",MENU_LOAD]);
        
        [params setObject:[NSString stringWithFormat:@"%d",searchPaging] forKey:@"page"];
        FIRCrashMessage([NSString stringWithFormat:@"Page:%d",searchPaging]);
        
        [params setObject:[NSString stringWithFormat:@"%d",[[[titleArray objectAtIndex:selectedButton] objectForKey:@"id"] intValue]] forKey:@"category_id"];
        FIRCrashMessage([NSString stringWithFormat:@"CategoryID:%d",[[[titleArray objectAtIndex:selectedButton] objectForKey:@"id"] intValue]]);
        
        [params setObject:searchText forKey:@"q"];
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
            FIRCrashMessage([NSString stringWithFormat:@"UserID:*Sensitive*"]);
        }
        
        #ifdef DEBUG
        NSLog(@"Retrieve Search Menu - %@",params);
        #endif
        
        [WebAPI retrievePosting:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.tabBarController setSelectedIndex:0];
                }
                isEmpty = YES;
                isTimeOut = YES;
                
                [self.collectionView reloadData];
            } else{
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    if([searchMenuArray count]>0){
                        [searchMenuArray removeAllObjects];
                    }
                    
                    searchMenuArray = [[SharedMethod new] processMenu:[result objectForKey:@"posts"] andCurrentArray:searchMenuArray];
                    
                    searchTotalCount = [[result objectForKey:@"total_items"] intValue];
                    if([searchMenuArray count]<=0){
                        isEmpty = YES;
                        isTimeOut = NO;
                    } else{
                        isEmpty = NO;
                        isTimeOut = NO;
                    }
                    
                    [self.collectionView reloadData];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        menuArray = [[SQLManager new] retrieveMenu:[[titleArray objectAtIndex:selectedButton] objectForKey:@"id"]];
        if([menuArray count]<=0){
            isEmpty = YES;
            isTimeOut = YES;
        }
        [self.collectionView reloadData];
    }
}

-(void)retrieveMenu{
    //    [[SQLManager new] retrievePromotions];
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        [FIRAnalytics logEventWithName:@"Retrieve Menu" parameters:nil];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        
        [params setObject:PLATFORM forKey:@"platform"];
        FIRCrashMessage([NSString stringWithFormat:@"Platform:%@",PLATFORM]);

        [params setObject:[[SharedMethod new] checkDeviceToken] forKey:@"push_token"];
        FIRCrashMessage([NSString stringWithFormat:@"Push Token:%@",[[SharedMethod new] checkDeviceToken]]);
        
        [params setObject:[SharedMethod getTokenWithRoute:@"user/posts/menu" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        
        [params setObject:POST_TYPE_3 forKey:@"type"];
        FIRCrashMessage([NSString stringWithFormat:@"Type:%@",POST_TYPE_3]);
        
        [params setObject:MENU_LOAD forKey:@"limit"];
        FIRCrashMessage([NSString stringWithFormat:@"Limit:%@",MENU_LOAD]);
        
        [params setObject:[NSString stringWithFormat:@"%d",paging] forKey:@"page"];
        FIRCrashMessage([NSString stringWithFormat:@"Page:%d",paging]);
        
        [params setObject:[NSString stringWithFormat:@"%d",[[[titleArray objectAtIndex:selectedButton] objectForKey:@"id"] intValue]] forKey:@"category_id"];
        FIRCrashMessage([NSString stringWithFormat:@"CategoryID:%d",[[[titleArray objectAtIndex:selectedButton] objectForKey:@"id"] intValue]]);
        
        [params setObject:@"order_by[0][0]=priority&order_by[0][1]=asc&order_by[1][0]=publish_at&order_by[1][1]=desc" forKey:@"order_by"];
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
            FIRCrashMessage([NSString stringWithFormat:@"UserID:*Sensitive*"]);
        }
        
        #ifdef DEBUG
        NSLog(@"Retrieve Menu - %@",params);
        #endif
        
        [WebAPI retrievePosting:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.tabBarController setSelectedIndex:0];
                }
                isEmpty = YES;
                isTimeOut = YES;
                
                [self.collectionView reloadData];
            } else{
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    menuArray = [[SharedMethod new] processMenu:[result objectForKey:@"posts"] andCurrentArray:menuArray];
                    
                    [[SQLManager new] insertAndUpdateMenu:menuArray];
                    totalCount = [[result objectForKey:@"total_items"] intValue];
                    
                    if([menuArray count]<=0){
                        isEmpty = YES;
                        isTimeOut = NO;
                    } else{
                        isEmpty = NO;
                        isTimeOut = NO;
                    }
                    
                    [self.collectionView reloadData];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        menuArray = [[SQLManager new] retrieveMenu:[[titleArray objectAtIndex:selectedButton] objectForKey:@"id"]];
        if([menuArray count]<=0){
            isEmpty = YES;
            isTimeOut = YES;
        }
        [self.collectionView reloadData];
    }
}

#pragma mark - Actions
- (IBAction)bbtnSearchDidPressed:(id)sender {
    [self.searchBar setHidden:isSearch];
    isSearch = !isSearch;
    [self.searchBar resignFirstResponder];
    [self.searchBar setText:@""];
    isEmpty = NO;
    isTimeOut = NO;
    
    if([searchMenuArray count]>0){
        [searchMenuArray removeAllObjects];
        searchTotalCount = 0;
        searchPaging = 1;
    }
    
    [self.collectionView reloadData];
    [self.collectionView setContentOffset:CGPointMake(0, 0) animated:NO];
    
    if(!self.searchBar.hidden){
        [self.scrollView setScrollEnabled:NO];
        [self.searchBar becomeFirstResponder];
    } else{
        [self.scrollView setScrollEnabled:YES];
        [self.searchBar resignFirstResponder];
    }
}

- (IBAction)btnTabDidPressed:(UIButton*)sender{
    for(UIButton *button in buttons){
        if(button.tag == sender.tag){
            selectedButton = (int)sender.tag;
            [button setSelected:YES];
            [button setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
            [button setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.scrollView scrollRectToVisible:button.frame animated:YES];
                [self updateSelectedItem:button];
            });
            
        } else{
            [button setSelected:NO];
            [button setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
            [button setTitleColor:[[SharedMethod new] colorWithHexString:GREY_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
        }
    }
    
    if([menuArray count]>0){
        [menuArray removeAllObjects];
    }
    
    if([searchMenuArray count]>0){
        [searchMenuArray removeAllObjects];
    }
    
    [self.collectionView reloadData];
    [self.collectionView setContentOffset:CGPointMake(0, 0)];
    
    totalCount = 0;
    paging = 1;
    searchTotalCount = 0;
    searchPaging = 1;
    [self retrieveMenu];
}

#pragma mark - UISwipeGestureRecognizer Delegate
-(void)leftSwipe:(UISwipeGestureRecognizer*)gesture{
    if(selectedButton>0){
        UIButton *temp = [[UIButton alloc] init];
        [temp setTag:selectedButton-1];
        [self btnTabDidPressed:temp];
    }
}

-(void)rightSwipe:(UISwipeGestureRecognizer*)gesture{
    if(selectedButton<[titleArray count]-1){
        UIButton *temp = [[UIButton alloc] init];
        [temp setTag:selectedButton+1];
        [self btnTabDidPressed:temp];
    }
}

#pragma mark - UISearchBar Delegate
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

//-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
//    if(searchText.length<=0){
//        if([searchMenuArray count]>0){
//            [searchMenuArray removeAllObjects];
//        }
//        searchMenuArray = [menuArray mutableCopy];
//    } else{
//        [searchMenuArray removeAllObjects];
//        for(Menu *temp in menuArray){
//            NSRange range = [temp.title rangeOfString:searchText options:NSCaseInsensitiveSearch];
//            if(range.location!=NSNotFound){
//                [searchMenuArray addObject:temp];
//            }
//        }
//    }
//    
//    if([searchMenuArray count]<=0){
//        isEmpty = YES;
//        isTimeOut = NO;
//    } else{
//        isEmpty = NO;
//        isTimeOut = NO;
//    }
//    
//    [self.collectionView reloadData];
//}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [self searchMenu:searchBar.text];
}

#pragma mark - UICollectionView Data Source
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(isSearch){
        if(isEmpty){
            return 1;
        } else{
            return [searchMenuArray count];
        }
    } else{
        if(isEmpty){
            return 1;
        } else{
            return [menuArray count];
        }
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(isEmpty){
        [collectionView registerNib:[UINib nibWithNibName:@"EmptyCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"emptyCell"];
        EmptyCollectionCell *cell = (EmptyCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"emptyCell" forIndexPath:indexPath];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EmptyCollectionCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        if(isTimeOut){
            [cell.imgIcon setImage:[UIImage imageNamed:@"ic_no_internet"]];
            [cell.lblDescription setText:NSLocalizedString(@"lbl_No_Internet", nil)];
        } else{
            [cell.imgIcon setImage:[UIImage imageNamed:@"ic_empty"]];
            [cell.lblDescription setText:NSLocalizedString(@"lbl_Empty", nil)];
        }
        
        return cell;
        
    } else{
        MenuCell *cell = (MenuCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"menuCell" forIndexPath:indexPath];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MenuCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        Menu *menu;
        
        if(isSearch){
            menu = [searchMenuArray objectAtIndex:indexPath.row];
        } else{
            menu = [menuArray objectAtIndex:indexPath.row];
        }
        
        [cell.lblTitle setText:menu.title];
        
        if(menu.image.length>0){
            [[SharedMethod new] setLoadingIndicatorOnImage:cell.imgView];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [cell.imgView setContentMode:UIViewContentModeScaleAspectFit];
                    [cell.imgView setClipsToBounds:YES];
                    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:menu.image] placeholderImage:[UIImage imageNamed:@"bg_menu"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        menu.cachedImage = image;
                        for(UIView *view in cell.imgView.subviews){
                            if(view.tag==100){
                                [view removeFromSuperview];
                            }
                        }
                    }];
                });
            });
        } else{
            [cell.imgView setImage:[UIImage imageNamed:@"bg_menu"]];
        }
        
        if(isSearch){
            if(indexPath.row == [searchMenuArray count] - 1 && [searchMenuArray count] < searchTotalCount){
                if([[SharedMethod new] checkNetworkConnectivity]){
                    searchPaging+=1;
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self searchMenu:self.searchBar.text];
                        });
                    });
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"err_Error", nil) message:NSLocalizedString(@"msg_No_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        } else{
            if(indexPath.row == [menuArray count] - 1 && [menuArray count] < totalCount && !isSearch){
                if([[SharedMethod new] checkNetworkConnectivity]){
                    paging+=1;
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self retrieveMenu];
                        });
                    });
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"err_Error", nil) message:NSLocalizedString(@"msg_No_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }
        
        return cell;
    }
}

#pragma mark - UICollectionView Delegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(!isEmpty){
        [self.searchBar resignFirstResponder];
        MenuCell *cell = (MenuCell*)[collectionView cellForItemAtIndexPath:indexPath];
        Menu *selectedMenu;
        
        isDetail = YES;
        if(isSearch){
            selectedMenu = [searchMenuArray objectAtIndex:indexPath.row];
        } else{
            selectedMenu = [menuArray objectAtIndex:indexPath.row];
        }
        
        MenuDetailVC *menuDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"menuDetailVC"];
        menuDetailVC.navigationTitle = cell.lblTitle.text;
        menuDetailVC.selectedMenu = selectedMenu;
        
        [self.navigationController pushViewController:menuDetailVC animated:YES];
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(isEmpty){
        return CGSizeMake(collectionView.frame.size.width, collectionView.frame.size.height);
    } else{
        return CGSizeMake(([UIScreen mainScreen].bounds.size.width/2)-20, ([UIScreen mainScreen].bounds.size.width/2)+20);
    }
}

@end
