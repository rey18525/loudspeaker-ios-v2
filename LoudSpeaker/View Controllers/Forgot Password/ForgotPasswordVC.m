//
//  ForgotPasswordVC.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 02/12/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "WebAPI.h"
#import <UIView+Toast.h>
#import "SVProgressHUD.h"
#import "TextFieldValidator.h"
#import <PDKeychainBindings.h>

@interface ForgotPasswordVC ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@end

@implementation ForgotPasswordVC{
    UIStoryboard *storyboard;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setupData];
    [self setupDelegates];
    [self setupTextFieldValidation];
}

#pragma mark - Custom Functions
-(void)setupDelegates{
    [self.txtEmail setDelegate:self];
}

-(void)setupTextFieldValidation{
    [self.txtEmail setValidateOnCharacterChanged:NO];
    [self.txtEmail addRegx:@"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" withMsg:NSLocalizedString(@"err_Invalid_Email", nil)];
    [self.txtEmail updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
}

-(void)setupUI{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SharedMethod new] setPaddingToTextField:self.txtEmail];
        [[SharedMethod new] setBorder:self.txtEmail borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [[SharedMethod new] setCustomButtonStyle:self.btnSend image:@"" borders:YES borderWidth:1.0f cornerRadius:0.0f borderColor:THEME_COLOR textColor:THEME_COLOR andBackgroundColor:TRANSPARENT];
        [[SharedMethod new] setCustomButtonStyle:self.btnCancel image:@"" borders:YES borderWidth:1.0f cornerRadius:0.0f borderColor:WHITE_COLOR textColor:WHITE_COLOR andBackgroundColor:TRANSPARENT];
    });
    
    [self.lblTitle setFont:[UIFont fontWithName:FONT_BOLD size:20.0f]];
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
    [self.txtEmail setTextColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
    
    [self.lblTitle setText:NSLocalizedString(@"lbl_Forgot_Password", nil)];
    [self.txtEmail setPlaceholder:NSLocalizedString(@"phd_Forgot_Email", nil)];
    [self.btnCancel setTitle:NSLocalizedString(@"btn_Cancel", nil) forState:UIControlStateNormal];
    [self.btnSend setTitle:NSLocalizedString(@"btn_Send", nil) forState:UIControlStateNormal];
}

-(void)setupData{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

-(void)setupDismissKeyboard{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - UITextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self dismissKeyboard];
    return YES;
}

#pragma mark - Actions
-(IBAction)btnSendDidPressed:(id)sender{
    if(self.txtEmail.text.length>0 && [[SharedMethod new] validateEmail:self.txtEmail.text]){
        //Process Forgot Password API//
        if([[SharedMethod new] checkNetworkConnectivity]){
            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
            NSDictionary *params = @{
                                     @"email"    : self.txtEmail.text,
                                     @"token"    : [SharedMethod getTokenWithRoute:@"user/password/forgot" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                     };
            
#ifdef DEBUG
            NSLog(@"Forgot Password Params - %@",params);
#endif
            
            [WebAPI forgotPassword:params andSuccess:^(id successBlock) {
                [SVProgressHUD dismiss];
                NSDictionary *result = successBlock;
                if([result objectForKey:@"errMsg"]){
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    [self.dismissForgotPassword dismissForgotPasswordVC];
                }
            }];
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        }
    } else if(self.txtEmail.text.length==0 || [[SharedMethod new] validateEmail:self.txtEmail.text]){
        [self.view makeToast:NSLocalizedString(@"err_Invalid_Email", nil)];
    }
}

-(IBAction)btnCancelDidPressed:(id)sender{
    [self.dismissForgotPassword dismissForgotPasswordVC];
}

@end
