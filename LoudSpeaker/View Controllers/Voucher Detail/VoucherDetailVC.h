//
//  VoucherDetailVC.h
//  Oasis
//
//  Created by Wong Ryan on 04/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Voucher.h"
#import "Campaign.h"
#import "Reward.h"

@interface VoucherDetailVC : UIViewController
@property (nonatomic) Voucher *selectedVoucher;
@property (nonatomic) Campaign *selectedCampaign;
@property (nonatomic) Reward *selectedReward;
@property (nonatomic) BOOL isMyVoucher;
@property (nonatomic) BOOL isRedemeed;
@end
