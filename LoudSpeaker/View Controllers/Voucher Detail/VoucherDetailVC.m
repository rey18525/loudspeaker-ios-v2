//
//  VoucherDetailVC.m
//  Oasis
//
//  Created by Wong Ryan on 04/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "VoucherDetailVC.h"
#import "VoucherBannerCell.h"
#import "VoucherDetailCell.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "SVProgressHUD.h"
#import "WebVC.h"
#import "WebAPI.h"
#import "QRCodeVC.h"
#import "GalleryVC.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <MessageUI/MessageUI.h>
#import <PDKeychainBindings.h>

@interface VoucherDetailVC ()<UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate,MFMessageComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnAction;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableView_btm_btnAction;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webView_btm_superView;

@end

@implementation VoucherDetailVC{
    UIImage *imageQR;
    QRCodeVC *qrCodeVC;
    UIAlertController *alertController;
    UIStoryboard *storyboard;
    int cellSize,cellSize2;
    BOOL hasTerms;
    float brightness;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setupUI];
        [self setupInitialization];
        [self setupData];
        [self setupDelegates];
        [self.tableView reloadData];
    });
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationItem setTitle:NSLocalizedString(@"nav_Voucher_Detail_V2", nil)];
    if(!self.isMyVoucher){
        if([[NSUserDefaults standardUserDefaults] boolForKey:POP_BACK]){
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:POP_BACK];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
}

#pragma mark - Setup Initialization
-(void)setupUI{
    [self.view setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.tableView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SharedMethod new] setBorder:self.btnAction borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
        [[self.btnAction titleLabel] setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    });
    
    [self.btnAction setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
    [self.btnAction.titleLabel setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
}

-(void)setupInitialization{
    if(self.isMyVoucher){
        if(self.isRedemeed){
            [self.btnAction setHidden:YES];
            [self.tableView_btm_btnAction setConstant:-40];
        } else{
            [self.btnAction setHidden:YES];
            [self.btnAction setTitle:NSLocalizedString(@"btn_Use_Here", nil) forState:UIControlStateNormal];
            [self.btnAction addTarget:self action:@selector(btnUseDidPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
    } else{
        if([self.selectedCampaign.trigger_by isEqualToString:@"user"]){
            [self.btnAction setHidden:NO];
            [self.tableView_btm_btnAction setConstant:58];
            
            [self.btnAction setTitle:NSLocalizedString(@"btn_Redeem", nil) forState:UIControlStateNormal];
            [self.btnAction addTarget:self action:@selector(btnRedeemDidPressed:) forControlEvents:UIControlEventTouchUpInside];
        } else{
            [self.btnAction setHidden:YES];
            [self.tableView_btm_btnAction setConstant:-40];
        }
    }
}

-(void)setupData{
    cellSize = 0;
    cellSize2 = 0;
    if(self.selectedVoucher.terms.length>0){
        hasTerms = YES;
    } else{
        hasTerms = NO;
    }
    
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

-(void)setupDelegates{
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
}

-(void)performRedeemVoucher{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
    NSString *route = [NSString stringWithFormat:@"user/reward/claim/%@",self.selectedVoucher.campaign_id];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    [params setObject:PLATFORM forKey:@"platform"];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN] forKey:@"push_token"];
    [params setObject:[SharedMethod getTokenWithRoute:route apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
    [params setObject:self.selectedVoucher.campaign_id forKey:@"campaign_id"];
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
    }
    
    #ifdef DEBUG
    NSLog(@"Redeem Voucher Params - %@",params);
    #endif

    [WebAPI redeemVoucher:params andSuccess:^(id successBlock) {
        [SVProgressHUD dismiss];
        NSDictionary *result = successBlock;
        if([result objectForKey:@"errMsg"]){
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        } else{
            if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"App_Name", nil) message:[result objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromVoucherDetail"];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"popToRoot"];
                    [self.tabBarController setSelectedIndex:2];
                }];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
                 
            } else{
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            }
        }
    }];
}

-(void)generateQR{
    NSTimeZone *MsiaTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [dateFormatter setTimeZone:MsiaTimeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *currentDate = [NSDate date];
    NSString *tempDate = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:currentDate]];
    
    NSDate *formattedDate = [dateFormatter dateFromString:tempDate];
    
    double timeStamp = [formattedDate timeIntervalSince1970];
    
    #ifdef DEBUG
    NSLog(@"timeStamp - %.0f",timeStamp);
    #endif
    
    NSString *encryptResult = [[SharedMethod new] sha256:[NSString stringWithFormat:@"%@%@%@",[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY],self.selectedReward.hashKey,[NSString stringWithFormat:@"%.0f",timeStamp]]];
    
    #ifdef DEBUG
    NSLog(@"EncryptResult - %@",encryptResult);
    NSLog(@"Secret Key - %@",[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]);
    #endif
    
    NSString *QRData = [NSString stringWithFormat:@"%@%@/%@-%@/%@%@",LOUDSPEAKER_URL,LOUDSPEAKER_USE_VOUCHER,[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],self.selectedReward.ID,[NSString stringWithFormat:@"%.0f",timeStamp],[encryptResult substringToIndex:6]];
    
    #ifdef DEBUG
    NSLog(@"QRData - %@",QRData);
    #endif
    
    UIImageView *temp = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    
    // QRCode
    CIImage *imgQR = [[SharedMethod new] generateQRCode:QRData];
    CGFloat scaleX = temp.frame.size.width / imgQR.extent.size.width;
    CGFloat scaleY = temp.frame.size.height / imgQR.extent.size.height;
    
    CIImage *imgTransformed = [imgQR imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    imageQR = [UIImage imageWithCIImage:imgTransformed];
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnGalleryDidPressed:(id)sender{
    GalleryVC *galleryVC = [storyboard instantiateViewControllerWithIdentifier:@"galleryVC"];
    galleryVC.image = self.selectedVoucher.cachedImage;
    [galleryVC.view setTag:100];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view.window addSubview:galleryVC.view];
        [self addChildViewController:galleryVC];
        [galleryVC didMoveToParentViewController:self];
    });
}

- (IBAction)btnUseDidPressed:(id)sender{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromVoucherDetailQR"];
    [self generateQR];
    qrCodeVC = [storyboard instantiateViewControllerWithIdentifier:@"qrCodeVC"];
    qrCodeVC.imgQR = imageQR;
    
    brightness = [[UIScreen mainScreen] brightness];
    [[UIScreen mainScreen] setBrightness:1.0f];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnDismissDidPressed:)];
    [qrCodeVC.view addGestureRecognizer:tapGesture];
    [qrCodeVC.view setTag:100];
    [self.view.window addSubview:qrCodeVC.view];
    [self addChildViewController:qrCodeVC];
}

- (IBAction)btnDismissDidPressed:(id)sender {
    [[UIScreen mainScreen] setBrightness:brightness];
    
    for(UIView *temp in self.view.window.subviews){
        if(temp.tag == 100){
            [temp removeFromSuperview];
        }
    }
    [qrCodeVC removeFromParentViewController];
    [self.navigationController popViewControllerAnimated:YES];
//    [self retrieveAll];
}

- (IBAction)btnRedeemDidPressed:(id)sender{
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
        if([[SharedMethod new] checkNetworkConnectivity]){
            
            //For TouchID//
            /*
            LAContext *myContext = [[LAContext alloc] init];
            NSError *authError = nil;
            NSString *myLocalizedReasonString = @"By verifying the finger print to agree to the terms and conditions.";
            
            if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
                [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                          localizedReason:myLocalizedReasonString
                                    reply:^(BOOL success, NSError *error) {
                                        if (success) {
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                [self performRedeemVoucher];
                                            });
                                        } else {
                                            if(kLAErrorAuthenticationFailed){
                                                [myContext evaluatePolicy:kLAPolicyDeviceOwnerAuthentication localizedReason:myLocalizedReasonString reply:^(BOOL success, NSError * _Nullable error) {
                                                    if(success){
                                                        [self performRedeemVoucher];
                                                    } else{
                                                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"msg_Authentication_Fail", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                                                    }
                                                }];
                                            } else if(kLAErrorUserCancel){
                                                
                                            } else if(kLAErrorUserFallback){
                                                [myContext evaluatePolicy:kLAPolicyDeviceOwnerAuthentication localizedReason:myLocalizedReasonString reply:^(BOOL success, NSError * _Nullable error) {
                                                    if(success){
                                                        [self performRedeemVoucher];
                                                    } else{
                                                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"msg_Authentication_Fail", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                                                    }
                                                }];
                                            }
                                        }
                                    }];
            } else{
                alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"msg_Voucher_Redeem", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *agreeAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Agree", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self performRedeemVoucher];
                }];
                
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleDefault handler:nil];
                
                [alertController addAction:cancelAction];
                [alertController addAction:agreeAction];
                
                [self presentViewController:alertController animated:YES completion:nil];
            }
             */
            
            alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"msg_Voucher_Redeem", nil) preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *agreeAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Agree", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self performRedeemVoucher];
            }];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleDefault handler:nil];
            
            [alertController addAction:cancelAction];
            [alertController addAction:agreeAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        }
    } else{
        [self.tabBarController setSelectedIndex:2];
    }
}

#pragma mark - WebView Delegate
-(void)webViewDidStartLoad:(UIWebView *)webView{
//    [self.view.window addSubview:loadingVC.view];
//    [self addChildViewController:loadingVC];
//    [loadingVC startLoad];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss];
    
    if(webView.tag==100 && cellSize==0){
        CGRect frame = webView.frame;
        frame.size.height = 1;
        webView.frame = frame;
        CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
        frame.size = fittingSize;
        webView.frame = frame;
        
        cellSize = fittingSize.height+8;
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
    } else if(webView.tag==101 && cellSize2==0){
        CGRect frame = webView.frame;
        frame.size.height = 1;
        webView.frame = frame;
        CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
        frame.size = fittingSize;
        webView.frame = frame;
        
        cellSize2 = fittingSize.height+8;
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
    }

}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    //    NSLog(@"url is %@",request.URL.absoluteString);
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        NSURL *url = request.URL;
        
        if([url.absoluteString containsString:@"www"] || [url.absoluteString containsString:@"http"]){
            if([[UIApplication sharedApplication] canOpenURL:url]){
                WebVC *webVC = [storyboard instantiateViewControllerWithIdentifier:@"webVC"];
                webVC.selectedURL = url;
                [self.navigationController pushViewController:webVC animated:YES];
            }
        } else if([url.absoluteString hasPrefix:@"tel:"]){
            if([[UIApplication sharedApplication] canOpenURL:url]){
                NSString *tel = [[url.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                alertController = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"%@ %@?",NSLocalizedString(@"msg_MakeCall", nil),[tel substringFromIndex:4]] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Call", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [[UIApplication sharedApplication] openURL:url];
                }];
                
                UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_No", nil) style:UIAlertActionStyleDefault handler:nil];
                
                [alertController addAction:noAction];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        } else if([[url scheme] isEqual:@"mailto"]){
            if ([MFMailComposeViewController canSendMail]) {
                [[UIApplication sharedApplication] openURL:url];
            } else{
                alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Send_Mail_Fail", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [alertController removeFromParentViewController];
                }];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        
        return NO;
    }
    return YES;
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(hasTerms){
        return 3;
    } else{
        return 2;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.isMyVoucher){
        if(hasTerms){
            if(section==0){
                return 3;
            } else if(section==1){
                return 1;
            } else{
                return 2;
            }
        } else{
            if(section==0){
                return 3;
            } else if(section==1){
                return 1;
            }else{
                return 0;
            }
        }
    } else{
        if(section==1){
            return 1;
        } else{
            return 2;
        }
    }

}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*
    if(indexPath.row==0){
        VoucherBannerCell *cell = (VoucherBannerCell*)[tableView dequeueReusableCellWithIdentifier:@"voucherBannerCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VoucherBannerCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.imgBanner setContentMode:UIViewContentModeScaleAspectFit];
        if(self.selectedVoucher.image.length>0 && self.selectedVoucher.cachedImage){
            [cell.imgBanner setImage:self.selectedVoucher.cachedImage];
            
            UIButton *btn = [[UIButton alloc] initWithFrame:cell.imgBanner.frame];
            [btn setTitle:@"" forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(btnGalleryDidPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell addSubview:btn];
            [cell bringSubviewToFront:btn];
        } else if(self.selectedVoucher.image.length>0 && !self.selectedVoucher.cachedImage){
            [[SharedMethod new] setLoadingIndicatorOnImage:cell.imgBanner];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [cell.imgBanner setClipsToBounds:YES];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [cell.imgBanner sd_setImageWithURL:[NSURL URLWithString:self.selectedVoucher.image] placeholderImage:[UIImage imageNamed:@"bg_voucher"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        
                        NSString *redeemed_at = self.selectedReward.redeem_at;
                        if(redeemed_at.length>0){
                            UIImage *temp = [[SharedMethod new] getBlackAndWhiteVersionOfImage:image];
                            [cell.imgBanner setImage:temp];
                            [cell.imgUsed setHidden:NO];
                            [cell.imgUsed setImage:[UIImage imageNamed:@"img_voucher_used"]];
                        } else{
                            [cell.imgUsed setHidden:YES];
                        }
                        
                        for(UIView *view in cell.imgBanner.subviews){
                            if(view.tag==100){
                                [view removeFromSuperview];
                            }
                        }
                    }];
                });
            });
            UIButton *btn = [[UIButton alloc] initWithFrame:cell.imgBanner.frame];
            [btn setTitle:@"" forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(btnGalleryDidPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell addSubview:btn];
            [cell bringSubviewToFront:btn];
        } else{
            [cell.imgBanner setImage:[UIImage imageNamed:@"bg_voucher"]];
        }
        
        if(self.isMyVoucher){
            if(self.selectedReward.expires_at.length<=0){
                [cell.expireView setHidden:YES];
            } else{
                [cell.expireView setHidden:NO];
                [cell.lblExpireDate setText:[[SharedMethod new] convertToNotificationDateFormat:self.selectedReward.expires_at]];
            }
        } else{
            [cell.expireView setHidden:YES];
        }
        
        if(self.selectedReward.redeem_at.length>0){
            [cell.imgUsed setHidden:NO];
            [cell.imgUsed setImage:[UIImage imageNamed:@"img_voucher_used"]];
        }
        
        return cell;
    } else{
        VoucherDetailCell *cell = (VoucherDetailCell*)[tableView dequeueReusableCellWithIdentifier:@"voucherDetailCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VoucherDetailCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        if(self.isMyVoucher){
            if(indexPath.row==1){
                if(self.selectedReward.redeem_at.length<=0){
                    [cell.webView setHidden:YES];
                    [cell.lblTitle setHidden:YES];
                    [cell.separatorView setHidden:YES];
                    [cell.separatorView2 setHidden:YES];
                    [cell.expireView setHidden:NO];
                    
                    [cell.lblLeft setText:NSLocalizedString(@"lbl_Left", nil)];
                    [cell.lblExpireDayTitle setText:NSLocalizedString(@"lbl_Expire_Day", nil)];
                    [cell.lblExpireHourTitle setText:NSLocalizedString(@"lbl_Expire_Hour", nil)];
                    
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
                    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    
                    NSDate *expireDate = [formatter dateFromString:self.selectedReward.expires_at];
                    NSDate *currentDate = [NSDate date];
                    
                    NSInteger day = [[[NSCalendar currentCalendar] components: NSCalendarUnitDay
                                                                     fromDate: currentDate
                                                                       toDate: expireDate
                                                                      options: 0] day];
                    
                    NSInteger hour = [[[NSCalendar currentCalendar] components: NSCalendarUnitHour
                                                                      fromDate: currentDate
                                                                        toDate: expireDate
                                                                       options: 0] hour];
                    
                    hour = hour%24;
                    
                    [cell.lblExpireDayValue setText:[NSString stringWithFormat:@"%d",(int)day]];
                    [cell.lblExpireHourValue setText:[NSString stringWithFormat:@"%d",(int)hour]];
                    
                    return cell;
                } else{
                    [cell.webView setHidden:YES];
                    [cell.lblTitle setHidden:NO];
                    [cell.separatorView setHidden:NO];
                    [cell.separatorView2 setHidden:YES];
                    [cell.expireView setHidden:YES];
                    [cell.lblLeft setHidden:YES];
                    
                    [cell.lblTitle setText:[NSString stringWithFormat:@"%@\n%@",NSLocalizedString(@"lbl_Redeemed_On", nil),self.selectedReward.redeem_at]];
                    
                    return cell;
                }
            } else if(indexPath.row==2){
                [cell.webView setHidden:YES];
                [cell.expireView setHidden:YES];
                [cell.separatorView2 setHidden:YES];
                [cell.lblTitle setText:self.selectedVoucher.title];
                [cell.separatorView setHidden:YES];
                return cell;
            }
            else if(indexPath.row==3){
                [cell.webView setHidden:NO];
                [cell.lblTitle setHidden:YES];
                [cell.expireView setHidden:YES];
                [cell.separatorView2 setHidden:YES];
                [cell.separatorView setHidden:YES];
                
                [cell.webView setTag:100];
                [cell.webView.scrollView setScrollEnabled:NO];
                
                NSString *finalString = [NSString stringWithFormat:@"<html><head><meta name='viewport' http-equiv='Content-Type' content='text/html;charset=UTF-8;width=device-width,initial-scale=0.85'></head><body><font face='%@' color=#474747>%@</body></html>", FONT_REGULAR, self.selectedVoucher.content];
                
                [cell.webView loadHTMLString:finalString baseURL:nil];
                [cell.webView setDelegate:self];
                
                return cell;
            }
            else{
                return nil;
            }
        } else{
            if(indexPath.row==1){
                [cell.webView setHidden:YES];
                [cell.expireView setHidden:YES];
                [cell.separatorView setHidden:YES];
                [cell.separatorView2 setHidden:NO];
                [cell.lblTitle setText:self.selectedVoucher.title];

                return cell;
            }
            else if(indexPath.row==2){
                [cell.webView setHidden:NO];
                [cell.lblTitle setHidden:YES];
                [cell.expireView setHidden:YES];
                [cell.separatorView2 setHidden:YES];
                
                [cell.webView setTag:100];
                [cell.webView.scrollView setScrollEnabled:NO];
                
                NSString *finalString = [NSString stringWithFormat:@"<html><head><meta name='viewport' http-equiv='Content-Type' content='text/html;charset=UTF-8;width=device-width,initial-scale=0.85'></head><body><font face='%@' color=#474747>%@</body></html>", FONT_REGULAR, self.selectedVoucher.content];
                
                [cell.webView loadHTMLString:finalString baseURL:nil];
                [cell.webView setDelegate:self];
                
                return cell;
            }
            else{
                return nil;
            }
        }
    }
     */
    if(indexPath.section==0){
        if(indexPath.row==0){
            VoucherBannerCell *cell = (VoucherBannerCell*)[tableView dequeueReusableCellWithIdentifier:@"voucherBannerCell"];
            if(cell==nil){
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VoucherBannerCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            if(self.selectedVoucher.image.length>0 && self.selectedVoucher.cachedImage){
                [cell.imgBanner setImage:self.selectedVoucher.cachedImage];
            } else if(self.selectedVoucher.image.length>0 && !self.selectedVoucher.cachedImage){
                [[SharedMethod new] setLoadingIndicatorOnImage:cell.imgBanner];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [cell.imgBanner setClipsToBounds:YES];
                        [cell.imgBanner setContentMode:UIViewContentModeScaleAspectFit];
                        [cell.imgBanner sd_setImageWithURL:[NSURL URLWithString:self.selectedVoucher.image] placeholderImage:[UIImage imageNamed:@"bg_voucher"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            for(UIView *view in cell.imgBanner.subviews){
                                if(view.tag==100){
                                    [view removeFromSuperview];
                                }
                            }
                        }];
                    });
                });
            } else{
                [cell.imgBanner setImage:[UIImage imageNamed:@"bg_voucher"]];
            }
            
            if(self.isMyVoucher){
                [cell.expireView setHidden:NO];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[cell.expireView layer] setCornerRadius:cell.expireView.frame.size.width/2];
                });
                [cell.lblExpireDate setText:[[SharedMethod new] convertToNotificationDateFormat:self.selectedReward.expires_at]];
            } else{
                [cell.expireView setHidden:YES];
            }
            
            if(self.selectedReward.redeem_at.length>0){
                [cell.imgUsed setHidden:NO];
                [cell.imgUsed setImage:[UIImage imageNamed:@"img_voucher_used"]];
            }
            
            return cell;
        } else{
            VoucherDetailCell *cell = (VoucherDetailCell*)[tableView dequeueReusableCellWithIdentifier:@"voucherDetailCell"];
            if(cell==nil){
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VoucherDetailCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            if(self.isMyVoucher){
                if(indexPath.row==1){
                    if(self.selectedReward.redeem_at.length<=0){
                        [cell.webView setHidden:YES];
                        [cell.lblTitle setHidden:YES];
                        [cell.separatorView setHidden:YES];
                        [cell.separatorView2 setHidden:YES];
                        [cell.expireView setHidden:NO];
                        [cell.lblLeft setText:NSLocalizedString(@"lbl_Left", nil)];
                        [cell.lblExpireDayTitle setText:NSLocalizedString(@"lbl_Expire_Day", nil)];
                        [cell.lblExpireHourTitle setText:NSLocalizedString(@"lbl_Expire_Hour", nil)];
                        
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
                        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        
                        NSDate *expireDate = [formatter dateFromString:self.selectedReward.expires_at];
                        NSDate *currentDate = [NSDate date];
                        
                        NSInteger day = [[[NSCalendar currentCalendar] components: NSCalendarUnitDay
                                                                         fromDate: currentDate
                                                                           toDate: expireDate
                                                                          options: 0] day];
                        
                        NSInteger hour = [[[NSCalendar currentCalendar] components: NSCalendarUnitHour
                                                                          fromDate: currentDate
                                                                            toDate: expireDate
                                                                           options: 0] hour];
                        
                        hour = hour%24;
                        
                        [cell.lblExpireDayValue setText:[NSString stringWithFormat:@"%d",(int)day]];
                        [cell.lblExpireHourValue setText:[NSString stringWithFormat:@"%d",(int)hour]];
                        
                        return cell;
                    } else{
                        [cell.webView setHidden:YES];
                        [cell.lblTitle setHidden:NO];
                        [cell.separatorView setHidden:NO];
                        [cell.separatorView setHidden:YES];
                        [cell.expireView setHidden:YES];
                        [cell.lblLeft setHidden:YES];
                        [cell.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
                        [cell.lblTitle setTextAlignment:NSTextAlignmentCenter];
                        [cell.lblTitle setText:[NSString stringWithFormat:@"%@\n%@",NSLocalizedString(@"lbl_Redeemed_On", nil),self.selectedReward.redeem_at]];
                        return cell;
                    }
                } else if(indexPath.row==2){
                    [cell.webView setHidden:YES];
                    [cell.expireView setHidden:YES];
                    [cell.lblTitle setHidden:NO];
                    [cell.lblTitle setTextAlignment:NSTextAlignmentCenter];
                    [cell.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
                    [cell.lblTitle setText:self.selectedVoucher.title];
                    [cell.separatorView setHidden:YES];
                    return cell;
                } else{
                    return nil;
                }
            } else{
                if(indexPath.row==1){
                    [cell.webView setHidden:YES];
                    [cell.expireView setHidden:YES];
                    [cell.separatorView setHidden:YES];
                    [cell.lblTitle setHidden:NO];
                    [cell.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
                    [cell.lblTitle setTextAlignment:NSTextAlignmentCenter];
                    [cell.lblTitle setText:self.selectedVoucher.title];
                    
                    return cell;
                } else{
                    return nil;
                }
            }
        }
    } else if(indexPath.section==1){
        VoucherDetailCell *cell = (VoucherDetailCell*)[tableView dequeueReusableCellWithIdentifier:@"voucherDetailCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VoucherDetailCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.webView setHidden:NO];
        [cell.lblTitle setHidden:YES];
        [cell.expireView setHidden:YES];
        [cell.separatorView setHidden:YES];
        [cell.separatorView2 setHidden:YES];
        
        [cell.webView setBackgroundColor:[UIColor clearColor]];
        [cell.webView setOpaque:NO];
        [cell.webView setTag:100];
        [cell.webView.scrollView setScrollEnabled:NO];
        [cell.webView.scrollView setBounces:NO];
        
//        NSString *finalString = [NSString stringWithFormat:@"<html><head><meta name='viewport' http-equiv='Content-Type' content='text/html;charset=UTF-8;width=device-width,initial-scale=0.85'></head><body><font face='%@' color=#474747>%@</body></html>", FONT_REGULAR, self.selectedVoucher.content];
//
//        [cell.webView loadHTMLString:finalString baseURL:nil];
        [cell.webView setDelegate:self];
        
        NSString *myBase = [NSString stringWithFormat:@"<html><head><script>function nav(url){document.location.href = url;}</script><meta content=\"html; charset=utf-8\" http-equiv=\"Content-Type\"/><meta name=\"viewport\" content=\"width=device-width\"/><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/></head><body><font face='%@' color=#474747>%@</body></html>", FONT_REGULAR, self.selectedVoucher.content];
        
//                NSLog(@"mybase - %@",myBase);
        [cell.webView loadHTMLString:myBase baseURL:[[NSBundle mainBundle] URLForResource:@"style" withExtension:@"css"]];
        
        return cell;
    } else{
        VoucherDetailCell *cell = (VoucherDetailCell*)[tableView dequeueReusableCellWithIdentifier:@"voucherDetailCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VoucherDetailCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        if(indexPath.row==0){
            [cell.webView setHidden:YES];
            [cell.lblTitle setHidden:NO];
            [cell.expireView setHidden:YES];
            [cell.separatorView setHidden:YES];
            [cell.separatorView2 setHidden:YES];
            
            [cell.lblTitle setText:NSLocalizedString(@"lbl_Terms_Condition_V2", nil)];
            [cell.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:16.0f]];
            [cell.lblTitle setTextAlignment:NSTextAlignmentLeft];
            return cell;
        } else{
            [cell.webView setHidden:NO];
            [cell.lblTitle setHidden:YES];
            [cell.expireView setHidden:YES];
            [cell.separatorView setHidden:YES];
            [cell.separatorView2 setHidden:YES];
            
            [cell.webView setBackgroundColor:[UIColor clearColor]];
            [cell.webView setOpaque:NO];
            [cell.webView setTag:101];
            [cell.webView.scrollView setScrollEnabled:NO];
            [cell.webView.scrollView setBounces:NO];
            
            NSString *finalString = [NSString stringWithFormat:@"<html><head><script>function nav(url){document.location.href = url;}</script><meta content=\"html; charset=utf-8\" http-equiv=\"Content-Type\"/><meta name=\"viewport\" content=\"width=device-width\"/><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/></head><body><font face='%@' color=#474747>%@</body></html>", FONT_REGULAR, self.selectedVoucher.terms];
            
            [cell.webView loadHTMLString:finalString baseURL:nil];
            [cell.webView setDelegate:self];
            return cell;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    NSLog(@"Selected row %d",(int)indexPath.row);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0 && indexPath.section==0){
        return tableView.frame.size.width/2;
    } else{
        if(self.isMyVoucher){
            if(indexPath.row == 1 && indexPath.section == 0){
                if(self.selectedReward.expires_at.length<=0){
                    return 0;
                } else{
                    return 90;
                }
            }
            
            if(indexPath.row == 0 && indexPath.section == 1){
                return cellSize+10;
            } else if(indexPath.row == 1 && indexPath.section == 2){
                return cellSize2+10;
            }
            else{
                return 48;
            }
        } else{
            if(indexPath.row == 0 && indexPath.section == 1){
                return cellSize+10;
            } else if(indexPath.row == 1 && indexPath.section == 2){
                return cellSize2+10;
            }
            else{
                return 48;
            }
        }
    }
}

@end
