//
//  BookingHistoryVC.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 20/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingHistoryVC.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "BookingHistoryCell.h"
#import "BookingDetailVC.h"
#import "WebAPI.h"
#import "EmptyTableCell.h"
#import "BookingModel.h"
#import "SVProgressHUD.h"
#import <PDKeychainBindingsController/PDKeychainBindings.h>

@interface BookingHistoryVC ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation BookingHistoryVC{
    UIStoryboard *storyboard;
    NSMutableArray *bookingArray, *outletArray;
    BOOL isEmpty;
    int paging, totalCount;
    UIRefreshControl *refreshControl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInitialization];
    [self setupDelegate];
    [self setupRefreshControl];
    [self retrieveLocalOutlets];
    [self checkOutlets];
    [self retrieveBookingList];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.tabBarController.tabBar setHidden:YES];
}

-(void)setupInitialization{
    [self.navigationItem setTitle:NSLocalizedString(@"nav_Booking_History", nil)];
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    outletArray = [[NSMutableArray alloc] init];
    bookingArray = [[NSMutableArray alloc] init];
}

-(void)setupDelegate{
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
}

-(void)setupRefreshControl{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl setTintColor:[UIColor blackColor]];
    [refreshControl addTarget:self action:@selector(refreshAction:) forControlEvents:UIControlEventValueChanged];
    bookingArray = [[NSMutableArray alloc] init];
    [self.tableView addSubview:refreshControl];
}

-(void)checkOutlets{
    if([outletArray count]<=0){
        [self retrieveOutlets];
    }
}

-(void)retrieveOutlets{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSDictionary *params = @{@"userId"           : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"token"            : [SharedMethod getTokenWithRoute:@"user/outlets" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
#ifdef DEBUG
        NSLog(@"Request Outlets - %@",params);
#endif
        
        [WebAPI retrieveOutlets:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.tabBarController setSelectedIndex:0];
                }
            } else{
                NSString *code = [result objectForKey:@"code"];
                if([code isEqualToString:@"000"]){
                    if([[result objectForKey:@"outlets"] count]>0){
                        result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                        [[SharedMethod new] writeToFile:OUTLET_FILE andDictioanry:result];
                        [self retrieveLocalOutlets];
                    }
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"Message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    [self retrieveLocalOutlets];
                }
            }
        }];
    } else{
        [SVProgressHUD dismiss];
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        [self retrieveLocalOutlets];
    }
}

-(void)retrieveLocalOutlets{
    NSDictionary *tempDict = [[SharedMethod new] readFromFile:OUTLET_FILE];
    outletArray = [[SharedMethod new] processOutlet:[tempDict objectForKey:@"outlets"]];
}

-(void)retrieveBookingList {
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:PLATFORM forKey:@"platform"];
        [params setObject:[[SharedMethod new] checkDeviceToken] forKey:@"push_token"];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        [params setObject:[SharedMethod getTokenWithRoute:@"user/bookings" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        [params setObject:PROMOTION_LOAD forKey:@"limit"];
        [params setObject:[NSString stringWithFormat:@"%d",paging] forKey:@"page"];
//        NSLog(@"Retrieve Booking List Param - %@",params);
        [WebAPI retrieveBookingList:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    totalCount = [[result objectForKey:@"total_items"] intValue];
                    if([[result objectForKey:@"bookings"] count]>0){
                        isEmpty = NO;
                        for(NSDictionary *temp in [result objectForKey:@"bookings"]){
                            BookingModel *bookingModel = [[BookingModel alloc] initWithAttributes:temp];
                            [bookingArray addObject:bookingModel];
                        }
                    } else{
                        isEmpty = YES;
                    }
                } else{
                    isEmpty = YES;
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
                
             [self.tableView reloadData];
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(isEmpty){
        return 1;
    } else{
        return [bookingArray count];
    }
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isEmpty){
        EmptyTableCell *cell = (EmptyTableCell*)[tableView dequeueReusableCellWithIdentifier:@"emptyCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EmptyTableCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.imgIcon setImage:[UIImage imageNamed:@"ic_empty"]];
        [cell.lblDescription setText:NSLocalizedString(@"lbl_Empty", nil)];
        return cell;
    } else{
        BookingHistoryCell *cell = (BookingHistoryCell*)[tableView dequeueReusableCellWithIdentifier:@"bookingHistoryCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BookingHistoryCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        if([bookingArray count]>0){
            BookingModel *bookingModel = [bookingArray objectAtIndex:indexPath.row];
            [cell setupBookingHistoryWithData:bookingModel];
        
            if(indexPath.row == [bookingArray count] - 1 && [bookingArray count] < totalCount){
                if([[SharedMethod new] checkNetworkConnectivity]){
                    paging+=1;
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self retrieveBookingList];
                        });
                    });
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"err_Error", nil) message:NSLocalizedString(@"msg_No_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!isEmpty){
        BookingModel *bookingModel = [bookingArray objectAtIndex:indexPath.row];
        BookingDetailVC *bookingDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"bookingDetailVC"];
        bookingDetailVC.bookingId = bookingModel.booking_id;
        [self.navigationController pushViewController:bookingDetailVC animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isEmpty){
        return self.tableView.frame.size.height+64;
    } else{
        return 92.0f;
    }
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)refreshAction:(UIRefreshControl *)refreshControl {
    [refreshControl endRefreshing];
    paging=1;
    if([bookingArray count]>0){
        [bookingArray removeAllObjects];
        [self.tableView reloadData];
    }
    [self retrieveBookingList];
}

@end
