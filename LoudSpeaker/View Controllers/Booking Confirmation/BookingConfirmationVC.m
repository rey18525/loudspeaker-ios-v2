//
//  BookingConfirmationVC.m
//  LoudSpeaker
//
//  Created by Appandus on 30/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingConfirmationVC.h"
#import "Constant.h"
#import "SharedMethod.h"
#import "WebAPI.h"
#import "TextFieldValidator.h"
#import "SVProgressHUD.h"
#import "BookingConfirmCell.h"
#import "BookingPriceCell.h"
#import "BookingPersonalDetailCell.h"
#import "BookingParticipantsModel.h"
#import <PDKeychainBindingsController/PDKeychainBindings.h>
#import <UIView+Toast.h>
#import "BookingHistoryModel.h"
#import "PaymentGatewayVC.h"
#import "ProfileVC.h"
#import "WebVC.h"
#import "HomeVC.h"

@interface BookingConfirmationVC ()<UITableViewDelegate,UITableViewDataSource,DismissPaymentGatewayDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation BookingConfirmationVC{
    UIStoryboard *storyboard;
    NSDictionary *userDetail;
    NSMutableDictionary *eWalletParams;
    int bigMouthQuantity, regularQuantity, guestQuantity;
    float bigMouthPrice, regularPrice, guestPrice;
    BOOL isAgreed;
    TextFieldValidator *txtName, *txtEmail, *txtContact;
    BookingHistoryModel *bookingHistoryModel;
    BOOL isMOL, isEWallet, isEGuest;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInitialization];
    [self setupDismissKeyboard];
    [self setupDelegates];
    [self setupUI];
    [self setupData];
}

#pragma mark - Setup Initialization
-(void)setupInitialization{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
}

-(void)setupDelegates{
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
}

-(void)setupUI{
    [self.tableView setEstimatedRowHeight:32.0f];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

-(void)setupData{
    isMOL = NO;
    isEGuest = NO;
    isEWallet = NO;
    
    [self.navigationItem setTitle:NSLocalizedString(@"nav_Booking_Confirmation", nil)];
    userDetail = [[NSUserDefaults standardUserDefaults] objectForKey:USER_DETAIL];
    
    bigMouthQuantity = [self calculateQuantityOfType:@"big_mouth"];
    regularQuantity = [self calculateQuantityOfType:@"regular"];
    guestQuantity = [self calculateQuantityOfType:@"guest"];
    
    bigMouthPrice = [self retrievePriceOfType:@"big_mouth"];
    regularPrice = [self retrievePriceOfType:@"regular"];
    guestPrice = [self retrievePriceOfType:@"guest"];
    
    isAgreed = NO;
}

-(int)calculateQuantityOfType:(NSString*)memberType{
    int total = 0;
    for(BookingParticipantsModel *temp in self.selectedBookingModel.participants){
        if([[temp.member_type lowercaseString] isEqualToString:memberType]){
            total+=1;
        }
    }
    return total;
}

-(float)retrievePriceOfType:(NSString*)memberType{
    float total = 0.00;
    for(BookingParticipantsModel *temp in self.selectedBookingModel.participants){
        if([[temp.member_type lowercaseString] isEqualToString:memberType]){
            total += [temp.price floatValue];
        }
    }
    return total;
}

-(void)setupDismissKeyboard{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(NSMutableArray*)constructParticipantsArray:(NSArray*)model{
    NSMutableArray *participants = [[NSMutableArray alloc] init];
    
    for(BookingParticipantsModel *temp in model){
        NSDictionary *modelDict = @{
                                    @"user_id" : temp.user_id,
                                    @"member_type" : temp.member_type,
                                    @"fullname" : temp.fullname,
                                    @"contact" : temp.contact
                                    };
        [participants addObject:modelDict];
    }
    return participants;
}

-(void)confirmPayment{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        
        BookingParticipantsModel *tempModel = [self.selectedBookingModel.participants objectAtIndex:0];
        tempModel.fullname = txtName.text;
        tempModel.email = txtEmail.text;
        tempModel.contact = txtContact.text;
        [self.selectedBookingModel.participants replaceObjectAtIndex:0 withObject:tempModel];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        eWalletParams = [[NSMutableDictionary alloc] init];
        [params setObject:self.selectedBookingModel.booking_date forKey:@"booking_date"];
        [params setObject:self.selectedBookingModel.outlet_id forKey:@"outlet_id"];
//        [params setObject:self.selectedBookingSession.session_id forKey:@"booking_session_id"];
        [params setObject:self.selectedSession forKey:@"booking_session_id"];
        [params setObject:self.selectedBookingModel.time_start forKey:@"time_start"];
        [params setObject:self.selectedBookingModel.time_end forKey:@"time_end"];
        [params setObject:[self constructParticipantsArray:self.selectedBookingModel.participants] forKey:@"participants"];
//        [params setObject:self.selectedBookingModel.remark forKey:@"remark"];
        [params setObject:txtName.text forKey:@"fullname"];
        [params setObject:txtEmail.text forKey:@"email"];
        [params setObject:txtContact.text forKey:@"contact"];
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        }
        
        if(isEWallet){
            [params setObject:@"Booking_Wallet" forKey:@"description"];
            [params setObject:@"booking_wallet" forKey:@"type"];
        }
        
        [params setObject:[SharedMethod getTokenWithRoute:@"user/booking/create" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        
#ifdef DEBUG
        NSLog(@"Confirm Booking Payment Params - %@",params);
#endif
        
        [WebAPI createBooking:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    if([[result objectForKey:@"booking"] count]>0 && [[result objectForKey:@"order"] count]>0){
                        bookingHistoryModel = [[BookingHistoryModel alloc] initWithAttributes:[result objectForKey:@"booking"]];
                        
                        if(isEWallet){
                            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
                            
                            NSDictionary *params = @{@"payWallet"   : @"Pay using E-Wallet",
                                                     @"amount"      : self.priceEWallet,
                                                     @"token"       : [SharedMethod getTokenWithRoute:@"user/flashdeal/grab" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                                     };
                            
                            [WebAPI bookingEwallet:params andSuccess:^(id successBlock) {
                                [SVProgressHUD dismiss];
                                
                                NSDictionary *result = successBlock;
                                if([result objectForKey:@"errMsg"]){
                                    [self presentViewController:[[SharedMethod new]
                                                                 setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil)
                                                                 message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                                    
                                } else{
                                    [self presentViewController:[[SharedMethod new]
                                                                 setAndShowAlertController:NSLocalizedString(@"App_Name", nil)
                                                                 message:[result objectForKey:@"message"]
                                                                 cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
                                     ];
                                    
                                }
                            }];
                        }
                        
                        if(isMOL || isEGuest){
                            PaymentGatewayVC *paymentGatewayVC = [storyboard instantiateViewControllerWithIdentifier:@"paymentGatewayVC"];
                            paymentGatewayVC.dismissPaymentGatewayDelegate = self;
                            if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
                                paymentGatewayVC.userId = [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID];
                            } else{
                                paymentGatewayVC.userId = @"";
                            }
                            paymentGatewayVC.dict = [result objectForKey:@"order"];
                            paymentGatewayVC.type = [[result objectForKey:@"order"] objectForKey:@"type"];
                            [paymentGatewayVC setHidesBottomBarWhenPushed:YES];
                            [self.navigationController pushViewController:paymentGatewayVC animated:YES];
                        }
                    }
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)processBooking:(NSDictionary *)basicDetails{
    if([[basicDetails objectForKey:@"status"] isEqualToString:@"paid"] || [[basicDetails objectForKey:@"status"] isEqualToString:@"completed"]){
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            [self.navigationController popToRootViewControllerAnimated:YES];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:COMPLETE_BOOKING];
//            [self.tabBarController setSelectedIndex:2];
//            ProfileVC *profileVC = [storyboard instantiateViewControllerWithIdentifier:@"profileVC"];
//            profileVC.completeBooking = YES;
//            [self.navigationController setHidesBottomBarWhenPushed:NO];
//            [self setHidesBottomBarWhenPushed:NO];
//            [profileVC setHidesBottomBarWhenPushed:NO];
//            [self.navigationController pushViewController:profileVC animated:YES];
        } else{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"msg_Guest_Booking_Success", nil) preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                HomeVC *homeVC = [storyboard instantiateViewControllerWithIdentifier:@"homeVC"];
                homeVC.hidesBottomBarWhenPushed = NO;
                [self.navigationController pushViewController:homeVC animated:YES];
            }];
            
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    } else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)dismissPaymentGatewayVC:(NSString*)message{
    if(message.length>0){
        [self performSelector:@selector(displayPaymentGatewayMessage:) withObject:message afterDelay:0.2];
    }
}

-(void)displayPaymentGatewayMessage:(NSString*)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"App_Name", nil) message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UITableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 13;
}

#pragma mark - UITableView Data Source
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row<3){
        BookingConfirmCell *cell = (BookingConfirmCell*)[tableView dequeueReusableCellWithIdentifier:@"bookingConfirmCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BookingConfirmCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        switch (indexPath.row) {
            case 0:
                [cell setupBookingView:NSLocalizedString(@"phd_Booking_Outlet", nil) andValue:self.selectedOutlet.name];
                break;
            case 1:
                [cell setupBookingView:NSLocalizedString(@"phd_Booking_Date", nil) andValue:self.selectedBookingModel.booking_date];
                break;
            case 2:
                [cell setupBookingView:NSLocalizedString(@"phd_Booking_Time", nil) andValue:[NSString stringWithFormat:@"%@",[[SharedMethod new] convertDateTimeFormat:self.selectedBookingModel.time_start andInputFormat:@"HH:mm:ss" andOutputFormat:@"hh:mm a"]]];
                break;
//            case 3:
//                [cell setupBookingView:NSLocalizedString(@"phd_Booking_Session", nil) andValue:self.selectedBookingSession.name];
//                break;
//            case 4:
//                [cell setupBookingView:NSLocalizedString(@"phd_Booking_Remark", nil) andValue:self.selectedBookingModel.remark];
//                break;
        }
        return cell;
    } else if(indexPath.row>=3 && indexPath.row<7){
        BookingPriceCell *cell = (BookingPriceCell*)[tableView dequeueReusableCellWithIdentifier:@"bookingPriceCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BookingPriceCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        switch(indexPath.row){
            case 3:
                [cell setupPriceView:NSLocalizedString(@"lbl_Big_Mouth", nil) quantity:[NSString stringWithFormat:@"%d",bigMouthQuantity] value:[NSString stringWithFormat:@"%.2f",bigMouthPrice] andIsTotal:NO];
                [cell.topSeparatorView setHidden:NO];
                break;
            case 4:
                [cell setupPriceView:NSLocalizedString(@"lbl_Regular", nil) quantity:[NSString stringWithFormat:@"%d",regularQuantity] value:[NSString stringWithFormat:@"%.2f",regularPrice] andIsTotal:NO];
                break;
            case 5:
                [cell setupPriceView:NSLocalizedString(@"lbl_Guest", nil) quantity:[NSString stringWithFormat:@"%d",guestQuantity] value:[NSString stringWithFormat:@"%.2f",guestPrice] andIsTotal:NO];
                break;
            case 6:
                [cell setupPriceView:NSLocalizedString(@"lbl_Total", nil) quantity:@"" value:self.selectedBookingModel.total andIsTotal:YES];
                break;
        }
        return cell;
    } else{
        BookingPersonalDetailCell *cell = (BookingPersonalDetailCell*)[tableView dequeueReusableCellWithIdentifier:@"bookingPersonalDetailCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BookingPersonalDetailCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        switch (indexPath.row) {
            case 7:
                [cell setupTitleView];
                break;
            case 8:
                [cell.txtValue setKeyboardType:UIKeyboardTypeDefault];
                if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
                    [cell.txtValue setEnabled:NO];
                    [cell setupPersonalName:NSLocalizedString(@"lbl_Booking_Name", nil) andValue:[userDetail objectForKey:@"fullname"]];
                } else{
                    [cell.txtValue setEnabled:YES];
                    [cell setupPersonalName:NSLocalizedString(@"lbl_Booking_Name", nil) andValue:@""];
                }
                
                txtName = cell.txtValue;
                break;
            case 9:
                [cell.txtValue setKeyboardType:UIKeyboardTypeEmailAddress];
                if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
                    [cell.txtValue setEnabled:NO];
                    [cell setupPersonalEmail:NSLocalizedString(@"lbl_Booking_Email", nil) andValue:[userDetail objectForKey:@"email"]];
                } else{
                    [cell.txtValue setEnabled:YES];
                    [cell setupPersonalEmail:NSLocalizedString(@"lbl_Booking_Email", nil) andValue:@""];
                }
                
                txtEmail = cell.txtValue;
                break;
            case 10:
                [cell.txtValue setKeyboardType:UIKeyboardTypePhonePad];
                if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
                    [cell.txtValue setEnabled:NO];
                    [cell setupPersonalContact:NSLocalizedString(@"lbl_Booking_Contact", nil) andValue:[userDetail objectForKey:@"contact"]];
                } else{
                    [cell.txtValue setEnabled:YES];
                    [cell setupPersonalContact:NSLocalizedString(@"lbl_Booking_Contact", nil) andValue:@""];
                }
                txtContact = cell.txtValue;
                break;
            case 11:
                [cell setupButtons];
                [cell.btnAgreement addTarget:self action:@selector(btnAgreementDidPressed:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnPay addTarget:self action:@selector(btnPayDidPressed:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnTermsCondition addTarget:self action:@selector(btnTermsConditionDidPressed:) forControlEvents:UIControlEventTouchUpInside];
                break;
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"Clicked - %d",(int)indexPath.row);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row<=10){
        if(indexPath.row==8 || indexPath.row==9 || indexPath.row==10){
            return 40.0f;
        } else{
            return UITableViewAutomaticDimension;
        }
    } else{
        return 100.0f;
    }
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnAgreementDidPressed:(id)sender {
    BookingPersonalDetailCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:11 inSection:0]];
    if(!isAgreed){
        [cell.imgAgreement setImage:[UIImage imageNamed:@"ic_check"]];
        [cell.imgAgreement setBackgroundColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    } else{
        [cell.imgAgreement setImage:nil];
        [cell.imgAgreement setBackgroundColor:[UIColor clearColor]];
    }
    isAgreed = !isAgreed;
}

- (IBAction)btnTermsConditionDidPressed:(id)sender {
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
//    [alertController addAction:cancelAction];
//
//    [self presentViewController:alertController animated:YES completion:nil];
//
//
//    UIAlertAction *termsAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"lbl_Terms_Condition", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        WebVC *webVC = [storyboard instantiateViewControllerWithIdentifier:@"webVC"];
        NSString *urlString = [[NSUserDefaults standardUserDefaults] objectForKey:BOOKING_TERMS];
        if(urlString.length<=0){
            urlString = [[NSUserDefaults standardUserDefaults] objectForKey:COMPANY_WEBSITE];
        }
        webVC.urlString = urlString;
        webVC.fromMenu=NO;
        webVC.fromSignUp=YES;
        [webVC setHidesBottomBarWhenPushed:YES];
        UINavigationController *temp = [[UINavigationController alloc] initWithRootViewController:webVC];
        [self presentViewController:temp animated:YES completion:nil];
//    }];
    
}

- (IBAction)btnPayDidPressed:(id)sender {
    
    if(isAgreed){
        if([txtName validate] && [txtEmail validate] && [txtContact validate]){
            
            if([[NSUserDefaults standardUserDefaults] boolForKey:IS_GUEST]){
                // E-Guest
                UIAlertController *alert = [UIAlertController
                                            alertControllerWithTitle: @""
                                            message:[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"lbl_Flash_Non_Refundable", nil), self.selectedBookingModel.total]
                                            preferredStyle:UIAlertControllerStyleAlert
                                            ];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    if([[SharedMethod new] checkNetworkConnectivity]){
                        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
                        isEGuest = YES;
                        [self confirmPayment];
                        
                    }else{
                        [self presentViewController:[[SharedMethod new]
                                                     setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil)
                                                     message:NSLocalizedString(@"msg_Internet", nil)
                                                     cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
                         ];
                        
                    }
                    
                }];
                [alert addAction:ok];
                
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
                [alert addAction:cancel];
                
                [self presentViewController:alert animated:NO completion:nil];
                
            }else{
               // E-Member
                    UIAlertController *alert = [UIAlertController
                                                alertControllerWithTitle: @""
                                                message:[NSString stringWithFormat:@"%@%@\n%@%@",
                                                         NSLocalizedString(@"lbl_Flash_MOL", nil),
                                                         //self.selectedBookingModel.total,
                                                         self.priceMOL,
                                                         NSLocalizedString(@"lbl_Flash_Wallet", nil),
                                                         self.priceEWallet
                                                         ]
                                                preferredStyle:UIAlertControllerStyleAlert
                                                ];
                    
                    UIAlertAction *mol = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Flash_MOL", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        isMOL = YES;
                        //self.selectedBookingModel.total = self.priceMOL;
                        [self confirmPayment];
                    }];
                    [alert addAction:mol];
                    
                    UIAlertAction *wallet = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Flash_Wallet", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        isEWallet = YES;
                        /*
                         JSONObject walletObject = new JSONObject(jObjBooking.toString());
                         walletObject.put("payWallet", "Pay using E-Wallet");
                         walletObject.put("amount", price);
                         walletObject.put("credit", "-" + price);
                         
                         VolleyReq(
                         walletObject,
                         URLConstant.APP_BOOKING_SPEND,
                         Request.Method.POST,
                         REQ_SPEND
                         );
                         */
                        [self confirmPayment];
                    }];
                    [alert addAction:wallet];
                    
                    [self presentViewController:alert animated:NO completion:nil];
                }
            }
    } else{
        [self.view makeToast:NSLocalizedString(@"err_Invalid_Terms_Condition", nil)];
    }
}

@end
