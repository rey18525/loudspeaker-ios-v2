//
//  BookingConfirmationVC.h
//  LoudSpeaker
//
//  Created by Appandus on 30/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BookingModel.h"
#import "Outlet.h"
#import "BookingSession.h"

@interface BookingConfirmationVC : UIViewController

@property (nonatomic) BookingModel *selectedBookingModel;
@property (nonatomic) Outlet *selectedOutlet;
@property (nonatomic) BookingSession *selectedBookingSession;
@property (nonatomic) NSString *selectedSession;
@property (nonatomic) NSString *priceEWallet;
@property (nonatomic) NSString *priceMOL;

@end
