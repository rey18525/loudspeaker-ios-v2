//
//  TransactionVC.m
//  Oasis
//
//  Created by Wong Ryan on 28/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "TransactionVC.h"
#import "TransactionCell.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "EmptyTableCell.h"
#import "Transaction.h"
#import "TransactionDetailVC.h"

@interface TransactionVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *lblBalance;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@import Firebase;

@implementation TransactionVC{
    UIStoryboard *storyboard;
    NSArray *transactionArray;
    BOOL isEmpty;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBar];
    [self setupInitialization];
    [self setupDelegates];
    [self setupData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:DISPLAY_TRANSACTION]){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:DISPLAY_TRANSACTION];
    }
}

#pragma mark - Setup Initialization
-(void)setupNavigationBar{
    [self.navigationItem setTitle:NSLocalizedString(@"nav_TransactionHistory_V2", nil)];
}

-(void)setupInitialization{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    [self.view setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.tableView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

-(void)setupDelegates{
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
}

-(void)setupData{
    if([self.transactions count]>0){
        isEmpty = NO;
    } else{
        isEmpty = YES;
    }
}

-(void)setupAttributedText:(NSString*)text andLabel:(UILabel*)label{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_REGULAR size:27] range:NSMakeRange(0, 2)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_REGULAR size:12] range:NSMakeRange(2, 10)];
    [label setAttributedText:attributedString];
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(isEmpty){
        return 1;
    } else{
        return [self.transactions count];
    }
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isEmpty){
        EmptyTableCell *cell = (EmptyTableCell*)[tableView dequeueReusableCellWithIdentifier:@"emptyCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EmptyTableCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell.imgIcon setImage:[UIImage imageNamed:@"ic_empty"]];
        [cell.lblDescription setText:NSLocalizedString(@"lbl_Empty", nil)];
        
        return cell;
    } else{
        TransactionCell *cell = (TransactionCell*)[tableView dequeueReusableCellWithIdentifier:@"transactionCell"];
        
        Transaction *transaction = [self.transactions objectAtIndex:indexPath.row];
        NSString *date = [[SharedMethod new] convertToNotificationDateFormat_V2:transaction.created_at];
        if(date.length==12){
            [self setupAttributedText:date andLabel:cell.lblDate];
        } else{
            [cell.lblDate setText:date];
        }
        
        [cell.lblDescription setText:[NSString stringWithFormat:@"%@",transaction.content]];
        [cell.imgArrow setImage:[UIImage imageNamed:@"ic_arrow_round"]];
        [cell.lblAmount setText:[[SharedMethod new] convertDateTimeFormat:transaction.created_at andInputFormat:@"yyyy-MM-dd HH:mm:ss" andOutputFormat:@"HH:mm:ss"]];
        
        /*
        if([transaction.type isEqualToString:@"credit"] || [transaction.type isEqualToString:@"topup"]){
            [cell.lblAmount setText:[NSString stringWithFormat:@"+%@",transaction.credit]];
            [cell.lblAmount setTextColor:[[SharedMethod new] colorWithHexString:BLUE_COLOR andAlpha:1.0f]];
        } else if([transaction.type isEqualToString:@"spend"] || [transaction.type isEqualToString:@"redeem"]){
            [cell.lblAmount setText:transaction.credit];
            [cell.lblAmount setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        }*/
        
        /*
        if([transaction.status isEqualToString:@"voided"]){
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:cell.lblAmount.text];
            [attributeString addAttribute:NSStrikethroughStyleAttributeName
                                    value:@2
                                    range:NSMakeRange(0, [attributeString length])];
            [attributeString addAttribute:NSFontAttributeName
                                    value:[UIFont fontWithName:@"SegoeUI-Semilight" size:13.0]
                                    range:NSMakeRange(0, [attributeString length])];
            [attributeString addAttribute:NSForegroundColorAttributeName
                                    value:cell.lblAmount.textColor
                                    range:NSMakeRange(0, [attributeString length])];
            [cell.lblAmount setAttributedText:attributeString];
        }
         */
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!isEmpty){
        Transaction *selectedTransaction = [_transactions objectAtIndex:indexPath.row];
        
        TransactionDetailVC *transactionDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"transactionDetailVC"];
        transactionDetailVC.selectedTransaction = selectedTransaction;
        [transactionDetailVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:transactionDetailVC animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isEmpty){
        return self.tableView.frame.size.height+64;
    } else{
        return 100;
    }
}

@end
