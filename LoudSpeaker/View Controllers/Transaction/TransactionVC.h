//
//  TransactionVC.h
//  Oasis
//
//  Created by Wong Ryan on 28/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionVC : UIViewController
@property (nonatomic) NSMutableArray *transactions;
@end
