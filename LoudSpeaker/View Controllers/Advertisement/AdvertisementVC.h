//
//  AdvertisementVC.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 02/06/2017.
//  Copyright © 2017 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvertisementVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgAdvertisement;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@end
