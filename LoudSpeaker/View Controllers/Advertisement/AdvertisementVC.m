//
//  AdvertisementVC.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 02/06/2017.
//  Copyright © 2017 Ryan. All rights reserved.
//

#import "AdvertisementVC.h"
#import "Constant.h"

@interface AdvertisementVC ()

@end

@implementation AdvertisementVC

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (IBAction)btnCloseDidPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self removeFromParentViewController];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:DISPLAY_ADVERTISEMENT];
    }];
}

@end
