//
//  ProfileVC.m
//  Oasis
//
//  Created by Wong Ryan on 21/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "ProfileVC.h"
#import "LoginVC.h"
#import "Constant.h"
#import "SharedMethod.h"
#import "ProfileCell.h"
#import "EditProfileVC.h"
#import "QRCodeVC.h"
#import "TopUpVC.h"
#import "NotificationVC.h"
#import "MyVoucherVC.h"
#import "MyRewardVC.h"
#import "TransactionVC.h"
#import "WebAPI.h"
#import "Campaign.h"
#import "Reward.h"
#import "SVProgressHUD.h"
#import "Base64.h"  
#import "SDWebImage/UIImageView+WebCache.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import <PDKeychainBindings.h>
#import "CardExpireVC.h"
#import "ProfileBalanceCell.h"
#import "PaymentGatewayVC.h"
#import "BookingHistoryVC.h"

@interface ProfileVC ()<UITableViewDelegate,UITableViewDataSource,CardExpire_Delegate, DismissPaymentGatewayDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgPortrait;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblBalance;
@property (weak, nonatomic) IBOutlet UILabel *lblScan;
@property (weak, nonatomic) IBOutlet UILabel *lblTopUp;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *failedView;
@property (weak, nonatomic) IBOutlet UIButton *btnRetry;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContent;
@property (weak, nonatomic) IBOutlet UIView *frontView; // E-Member
@property (weak, nonatomic) IBOutlet UIView *backView;  // E-Member
@property (weak, nonatomic) IBOutlet UIImageView *imgCardFront;
@property (weak, nonatomic) IBOutlet UILabel *lblExpire;
@property (weak, nonatomic) IBOutlet UIImageView *imgCardBack;
@property (weak, nonatomic) IBOutlet UIView *profileContent;
//@property (weak, nonatomic) IBOutlet UIImageView *imgQR;
@property (weak, nonatomic) IBOutlet UILabel *lblCardTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCard;
@property (weak, nonatomic) IBOutlet UIButton *btnRenew;
@property (weak, nonatomic) IBOutlet UIView *expiryView;
@property (weak, nonatomic) IBOutlet UILabel *lblRenewHere;

@property (weak, nonatomic) IBOutlet UIButton *btnPaymentQr;

// E-Guest
@property (weak, nonatomic) IBOutlet UIButton *btnGuestUpgrade;
@property (weak, nonatomic) IBOutlet UIView *guestFrontView;
@property (weak, nonatomic) IBOutlet UIView *guestBackView;
@property (weak, nonatomic) IBOutlet UIImageView *imgGuestPortrait;
@property (weak, nonatomic) IBOutlet UILabel *lblGuestName;
@property (weak, nonatomic) IBOutlet UIButton *btnGuestQR;
@property (weak, nonatomic) IBOutlet UILabel *lblGuestCard;

@end

@import Firebase;

@implementation ProfileVC{
    UIStoryboard *storyboard;
    NSArray *iconArray,*labelArray;
    NSDictionary *userDetail;
    UIAlertController *alertController;
    UITapGestureRecognizer *flipTapGesture;
    UITapGestureRecognizer *guestFlipTapGesture;
    UIImage *imageQR;
    NSData *convertedData;
    QRCodeVC *qrCodeVC;
    CardExpireVC *cardExpireVC;
    BOOL flipCount,isRenew,isExpired;
    float brightness;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(SYSTEM_VERSION_LESS_THAN(@"10")){
        [self updateTabBarController];
    }
    
    // For testing
    //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_GUEST];
    //[[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_MEMBER];
    //NSLog(@"ASDFX_register_rate - %@", [[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_REGISTER_RATE]);
    
    flipCount = 0;
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    //[self setupTapGesture];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationItem setTitle:NSLocalizedString(@"nav_Profile_V2", nil)];
    [self.tabBarController.tabBar setHidden:NO];
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self setupInitialization];
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
        LoginVC *loginVC = [storyboard instantiateViewControllerWithIdentifier:@"loginVC"];
        [loginVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController.view.layer addAnimation:[[SharedMethod new] setupTransitionAnimation:TRANSITION_TOP] forKey:kCATransition];
        [self.navigationController pushViewController:loginVC animated:NO];
    }
    
    if([UIScreen mainScreen].bounds.size.height <= 568){
        [self.tableView setScrollEnabled:YES];
    } else{
        [self.tableView setScrollEnabled:NO];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setupData];
    [self setupUI];
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN] && [[NSUserDefaults standardUserDefaults] boolForKey:IS_SOCIAL] && [[SharedMethod new] loadProfileImage:YES]){
        [self convertImage];
    } else if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
        if(!isRenew){
            [self retrieveAll];
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self closeView];
}

#pragma mark - Custom Functions
-(void)updateTabBarController{
    UITabBar *tabBar = self.tabBarController.tabBar;
    UITabBarItem *tab1 = [[tabBar items] objectAtIndex:2];
    [tab1 setImage:[[UIImage imageNamed:@"ic_bottombar_profile"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab1 setSelectedImage:[[UIImage imageNamed:@"ic_bottombar_profile_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
}

-(void)setupTapGesture{
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_GUEST]){
        [_frontView setHidden:YES];
        [_backView setHidden:YES];
        
        guestFlipTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(flipGuestCardAction:)];
        [self.profileContent addGestureRecognizer:guestFlipTapGesture];
    }
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_MEMBER]){
        [_guestFrontView setHidden:YES];
        [_guestBackView setHidden:YES];
        
        flipTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(flipCardAction:)];
        [self.profileContent addGestureRecognizer:flipTapGesture];
    }
}

-(void)setupUI{
    
    [self.view setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.scrollView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.scrollContent setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.tableView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.profileContent setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_GUEST]) {
        NSLog(@"ProfileVC_eguest");
        [_frontView setHidden:YES];
        [_backView setHidden:YES];
        
        [_btnGuestUpgrade setHidden:NO];
        [_guestFrontView setHidden:NO];
        [_guestBackView setHidden:NO];
        
        [self.imgGuestPortrait setBackgroundColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.imgGuestPortrait setContentMode:UIViewContentModeScaleAspectFit];
        });
        
        [self.lblGuestName setTextColor:[[SharedMethod new] colorWithHexString:BLACK_COLOR andAlpha:1.0f]];
        [self.lblGuestName setTextAlignment:NSTextAlignmentCenter];
        [self.lblGuestName setFont:[UIFont fontWithName:FONT_BOLD size:11.0f]];
        
        [self.lblGuestCard setTextColor:[[SharedMethod new] colorWithHexString:BLACK_COLOR andAlpha:1.0f]];
        [self.lblGuestCard setFont:[UIFont fontWithName:FONT_BOLD size:13.0f]];
        
        [self setupTapGesture];
        
    }else{
        NSLog(@"ProfileVC_emember");
        [_frontView setHidden:NO];
        [_backView setHidden:NO];
        
        [_btnGuestUpgrade setHidden:YES];
        [_guestFrontView setHidden:YES];
        [_guestBackView setHidden:YES];
        
        [self.imgPortrait setBackgroundColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.imgPortrait setContentMode:UIViewContentModeScaleAspectFit];
        });
        
        [self.lblScan setText:NSLocalizedString(@"lbl_Scan", nil)];
        [self.lblTopUp setText:NSLocalizedString(@"lbl_Top_Up", nil)];
        
        [self.lblCard setTextColor:[[SharedMethod new] colorWithHexString:BLACK_COLOR andAlpha:1.0f]];
        [self.lblName setTextColor:[[SharedMethod new] colorWithHexString:BLACK_COLOR andAlpha:1.0f]];
        [self.lblCardTitle setTextColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
        //[self.lblExpire setTextColor:[[SharedMethod new] colorWithHexString:RED_COLOR andAlpha:1.0f]];
        [self.lblRenewHere setTextColor:[[SharedMethod new] colorWithHexString:DEEP_SKY_BLUE_COLOR andAlpha:1.0f]];
        [self.btnRenew setTitleColor:[[SharedMethod new] colorWithHexString:DEEP_SKY_BLUE_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
        
        [self.lblName setTextAlignment:NSTextAlignmentCenter];
        [self.lblCard setFont:[UIFont fontWithName:FONT_BOLD size:13.0f]];
        [self.lblCardTitle setFont:[UIFont fontWithName:FONT_REGULAR size:12.0f]];
        [self.lblName setFont:[UIFont fontWithName:FONT_BOLD size:11.0f]];
        //[self.lblExpire setFont:[UIFont fontWithName:FONT_BOLD size:10.0f]];
        [self.lblRenewHere setFont:[UIFont fontWithName:FONT_BOLD size:13.0f]];
        [[self.btnRenew titleLabel] setFont:[UIFont fontWithName:FONT_BOLD size:12.0f]];
        
        [self setupTapGesture];
        
    }
}

-(void)setupInitialization{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

-(void)setupData{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    iconArray = @[@"ic_profile_voucher",@"ic_profile_notify",@"ic_profile_history",@"ic_profile_booking"];
    labelArray = @[NSLocalizedString(@"nav_MyVoucher_V2", nil),NSLocalizedString(@"nav_Notification_V2", nil),NSLocalizedString(@"nav_TransactionHistory_V2", nil),NSLocalizedString(@"nav_Booking_History", nil)];
    isExpired = NO;
//    [self.btnRenew setTitle:NSLocalizedString(@"btn_Renew", nil) forState:UIControlStateNormal];
    [self.lblRenewHere setText:NSLocalizedString(@"btn_Renew", nil)];
    [self.tableView reloadData];
    
    userDetail = [[NSUserDefaults standardUserDefaults] objectForKey:USER_DETAIL];
    
    #ifdef DEBUG
    NSLog(@"profile userDetails - %@",userDetail);
    #endif
    
    [self.failedView setBackgroundColor:[[SharedMethod new] colorWithHexString:RED_COLOR andAlpha:1.0f]];
    [self.btnRetry setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnRetry setTitle:NSLocalizedString(@"btn_Retry", nil) forState:UIControlStateNormal];
    
    // card_number
    //NSString *cardNumber = [[userDetail objectForKey:@"card"] objectForKey:@"number"];
    NSString *cardNumber = [userDetail objectForKey:@"card_number"];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_GUEST]) {
        // E-Guest
        [self.lblGuestName setText:[userDetail objectForKey:@"fullname"]];
        
        if(cardNumber.length > 0){
            [self.lblGuestCard setText:[NSString stringWithFormat:@"%@ %@ %@ %@",[cardNumber substringToIndex:4],[cardNumber substringWithRange:NSMakeRange(4, 4)],[cardNumber substringWithRange:NSMakeRange(8, 4)],[cardNumber substringFromIndex:12]]];
        }
        
        [self.imgGuestPortrait setClipsToBounds:YES];
        if([[userDetail objectForKey:@"image"] isKindOfClass:[NSDictionary class]]){
            NSString *image = [[userDetail objectForKey:@"image"] objectForKey:@"src"];
            if(image.length>0){
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.imgGuestPortrait setContentMode:UIViewContentModeScaleAspectFit];
                        if([[SharedMethod new] checkNetworkConnectivity]){
                            [[SharedMethod new] setLoadingIndicatorOnImage:self.imgGuestPortrait];
                            [self.imgGuestPortrait sd_setImageWithURL:[NSURL URLWithString:image] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                [[SharedMethod new] saveNewProfileImage:image andIsSocial:NO];
                                for(UIView *view in self.imgGuestPortrait.subviews){
                                    if(view.tag==100){
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [view removeFromSuperview];
                                        });
                                    }
                                }
                            }];
                        } else{
                            [self.imgGuestPortrait setImage:[[SharedMethod new] loadProfileImage:NO]];
                        }
                    });
                });
            } else{
                [self.imgGuestPortrait setImage:[UIImage imageNamed:@"ic_profile_placeholder"]];
            }
        } else{
            [self.imgGuestPortrait setImage:[UIImage imageNamed:@"ic_profile_placeholder"]];
        }
        
    }else{
        // E-Member
        [self.lblName setText:[userDetail objectForKey:@"fullname"]];
     
        if(cardNumber.length>0){
            [self.lblCard setText:[NSString stringWithFormat:@"%@ %@ %@ %@",[cardNumber substringToIndex:4],[cardNumber substringWithRange:NSMakeRange(4, 4)],[cardNumber substringWithRange:NSMakeRange(8, 4)],[cardNumber substringFromIndex:12]]];
            
            NSString *expireDate = [[userDetail objectForKey:@"card"] objectForKey:@"expiry"];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
            dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",expireDate]];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSString *myDate = [dateFormatter stringFromDate:date];
            //[self.lblExpire setText:[[NSString stringWithFormat:@"%@: %@",@"",[[SharedMethod new] convertDateTimeFormat:myDate andInputFormat:@"yyyy-MM-dd" andOutputFormat:@"dd/MM/yyyy"]] uppercaseString]];
            [self.lblExpire setText:myDate];
        } else{
            [self.lblExpire setText:[[NSString stringWithFormat:@"%@: -",@""] uppercaseString]];
        }
        
        [self.lblBalance setText:[NSString stringWithFormat:@"%@: $%@",NSLocalizedString(@"lbl_Balance", nil),[[NSUserDefaults standardUserDefaults] objectForKey:USER_POINT]]];
        
        [self.imgPortrait setClipsToBounds:YES];
        if([[userDetail objectForKey:@"image"] isKindOfClass:[NSDictionary class]]){
            NSString *image = [[userDetail objectForKey:@"image"] objectForKey:@"src"];
            if(image.length>0){
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.imgPortrait setContentMode:UIViewContentModeScaleAspectFit];
                        if([[SharedMethod new] checkNetworkConnectivity]){
                            [[SharedMethod new] setLoadingIndicatorOnImage:self.imgPortrait];
                            [self.imgPortrait sd_setImageWithURL:[NSURL URLWithString:image] placeholderImage:nil options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                [[SharedMethod new] saveNewProfileImage:image andIsSocial:NO];
                                for(UIView *view in self.imgPortrait.subviews){
                                    if(view.tag==100){
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [view removeFromSuperview];
                                        });
                                    }
                                }
                            }];
                        } else{
                            [self.imgPortrait setImage:[[SharedMethod new] loadProfileImage:NO]];
                        }
                    });
                });
            } else{
                [self.imgPortrait setImage:[UIImage imageNamed:@"ic_profile_placeholder"]];
            }
        } else{
            [self.imgPortrait setImage:[UIImage imageNamed:@"ic_profile_placeholder"]];
        }
    }
    
    //[self.imgQR setImage:[UIImage imageNamed:@"ic_qr"]];
 
    [self.tableView reloadData];
}

-(void)retrieveAll{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSString *deviceToken = [[SharedMethod new] checkDeviceToken];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        [params setObject:PLATFORM forKey:@"platform"];
        [params setObject:deviceToken forKey:@"push_token"];
        [params setObject:[SharedMethod getTokenWithRoute:@"user/all" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        
        #ifdef DEBUG
        NSLog(@"Request All - %@",params);
        #endif
        
        [WebAPI retrieveAll:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self.failedView setHidden:NO];
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.tabBarController setSelectedIndex:0];
                }
            } else{
                
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    [self.failedView setHidden:YES];
                    result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%.2f",[[result objectForKey:@"balance"] floatValue]] forKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] setObject:result forKey:USER_POINT_DETAIL];
                    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"user"] forKey:USER_DETAIL];

                    int badgeNo = [[result objectForKey:@"inbox_unread"] intValue];
                    [[NSUserDefaults standardUserDefaults] setInteger:badgeNo forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeNo];
                    
                    [self setupData];
                    [self checkCardExpire:[[[result objectForKey:@"user"] objectForKey:@"card"] objectForKey:@"expiry"]];
//
//                    if([[NSUserDefaults standardUserDefaults] boolForKey:@"fromVoucherDetail"] || [[NSUserDefaults standardUserDefaults] boolForKey:@"myVoucher"]){
//                        MyVoucherVC *myVoucherVC = [storyboard instantiateViewControllerWithIdentifier:@"myVoucherVC"];
//                        myVoucherVC.fromProfile = YES;
//                        myVoucherVC.myVouchers = [self filterVouchers];
//                        [myVoucherVC setHidesBottomBarWhenPushed:YES];
//                        [self.navigationController pushViewController:myVoucherVC animated:YES];
//                    }
//
                    if([[NSUserDefaults standardUserDefaults] boolForKey:DISPLAY_REWARD]){
                        //My Voucher//
                        MyVoucherVC *myVoucherVC = [storyboard instantiateViewControllerWithIdentifier:@"myVoucherVC"];
                        myVoucherVC.fromProfile = YES;
                        [myVoucherVC setHidesBottomBarWhenPushed:YES];
                        
                        NSMutableArray *voucherFilter = [[NSMutableArray alloc] init];
                        voucherFilter = [self filterVouchers];
                        
                        //For navigating into specific voucher//
                        /*for(Reward *temp in voucherFilter){
                            if(temp.ID == [[NSUserDefaults standardUserDefaults] objectForKey:@"rewardID"]){
                                myVoucherVC.myVouchers = (NSMutableArray*)@[temp];
                                break;
                            }
                        }*/

                        myVoucherVC.myVouchers = [self filterVouchers];
                        [self.navigationController pushViewController:myVoucherVC animated:YES];
                    }
                    
                    if([[NSUserDefaults standardUserDefaults] boolForKey:DISPLAY_TRANSACTION]){
                        //Go Transaction//
                        NSMutableArray *transactions = [[SharedMethod new] processTransactions:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_POINT_DETAIL] objectForKey:@"transactions"]];
                        TransactionVC *transactionVC = [storyboard instantiateViewControllerWithIdentifier:@"transactionVC"];
                        transactionVC.transactions = transactions;
                        [transactionVC setHidesBottomBarWhenPushed:YES];
                        [self.navigationController pushViewController:transactionVC animated:YES];
                    }
                    
                    if([[NSUserDefaults standardUserDefaults] boolForKey:COMPLETE_BOOKING]){
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:COMPLETE_BOOKING];
                        BookingHistoryVC *bookingHistory = [storyboard instantiateViewControllerWithIdentifier:@"bookingHistoryVC"];
                        [bookingHistory setHidesBottomBarWhenPushed:YES];
                        [self.navigationController pushViewController:bookingHistory animated:YES];
                    }
                } else{
                    [self.failedView setHidden:NO];
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"Message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
    } else{
        [self.failedView setHidden:NO];
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)logoutProcess{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN];
        
        if(deviceToken.length<=0){
            deviceToken=@"";
        }
        
        NSDictionary *params = @{@"userId"           : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"platform"         : PLATFORM,
                                 @"push_token"       : deviceToken,
                                 @"token"            : [SharedMethod getTokenWithRoute:@"user/logout" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
        #ifdef DEBUG
        NSLog(@"Request Logout - %@",params);
        #endif
        
        [WebAPI logoutUser:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"msg_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                NSString *code = [result objectForKey:@"code"];
                alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"App_Name", nil) message:[result objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    if([code isEqualToString:@"000"]){
                        [[SharedMethod new] removeProfileImage:YES];
                        [[SharedMethod new] removeProfileImage:NO];
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                        [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                        [self.tabBarController setSelectedIndex:0];
                    }
                }];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)generateQR{
    NSString *QRData = [[userDetail objectForKey:@"card"] objectForKey:@"number"];
    
    #ifdef DEBUG
    NSLog(@"QRData - %@",QRData);
    #endif
    
    // QRCode
    CIImage *imgQR = [[SharedMethod new] generateQRCode:QRData];
    CGFloat scaleX = self.imgPortrait.frame.size.width / imgQR.extent.size.width;
    CGFloat scaleY = self.imgPortrait.frame.size.height / imgQR.extent.size.height;
    
    CIImage *imgTransformed = [imgQR imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    imageQR = [UIImage imageWithCIImage:imgTransformed];
}

-(void)generatePaymentQr{
    NSTimeZone *MsiaTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setTimeZone:MsiaTimeZone];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *currentDate = [NSDate date];
    NSString *tempDate = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:currentDate]];
    
    NSDate *formattedDate = [dateFormatter dateFromString:tempDate];
    
    double timeStamp = [formattedDate timeIntervalSince1970];
    
#ifdef DEBUG
    NSLog(@"timeStamp - %.0f",timeStamp);
#endif
    
    NSString *encryptResult = [[SharedMethod new] sha256:[NSString stringWithFormat:@"%@%@",[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY],[NSString stringWithFormat:@"%.0f",timeStamp]]];
    
#ifdef DEBUG
    NSLog(@"EncryptResult - %@",encryptResult);
    NSLog(@"Secret Key - %@",[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]);
#endif
    
    NSString *QRData = [NSString stringWithFormat:@"%@%@/%@/%@%@",LOUDSPEAKER_URL,LOUDSPEAKER_USER,[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],[NSString stringWithFormat:@"%.0f",timeStamp],[encryptResult substringToIndex:6]];
    
#ifdef DEBUG
    NSLog(@"QRData - %@",QRData);
#endif
    
    // QRCode
    CIImage *imgQR = [[SharedMethod new] generateQRCode:QRData];
    CGFloat scaleX = self.imgPortrait.frame.size.width / imgQR.extent.size.width;
    CGFloat scaleY = self.imgPortrait.frame.size.height / imgQR.extent.size.height;
    
    CIImage *imgTransformed = [imgQR imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    imageQR = [UIImage imageWithCIImage:imgTransformed];
}

-(NSMutableArray*)filterVouchers{
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    NSMutableArray *tempReward = [[NSMutableArray alloc] init];
    NSMutableArray *tempRewards = [[NSMutableArray alloc] init];
    NSMutableArray *vouchers = [[NSMutableArray alloc] init];
    temp = [[[NSUserDefaults standardUserDefaults] objectForKey:USER_POINT_DETAIL] objectForKey:@"vouchers"];
    
    for(NSDictionary *dict in temp){
        NSMutableArray *_reward = [[NSMutableArray alloc] init];
        [_reward addObject:dict];
        [tempReward addObject:_reward];
    }

    tempRewards = [[SharedMethod new] processReward:tempReward andCurrentArray:tempRewards];
    for(Reward *r in tempRewards){
        if([r.type isEqualToString:@"voucher"]){
            [vouchers addObject:r];
        }
    }
    return vouchers;
}

- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)convertImage{
    UIImage *initiaImage = [[SharedMethod new] loadProfileImage:YES];
    UIImage *image = [self squareImageFromImage:initiaImage scaledToSize:320];
    
    convertedData = UIImageJPEGRepresentation(image, 0.4f);//Double check the compression values
    
    #ifdef DEBUG
    NSLog(@"Image size(bytes) - %d",(int)convertedData.length);
    #endif
    
    UIImage *temp = [UIImage imageWithData:convertedData];
    [self.imgPortrait setImage:temp];
    [self uploadPortrait];
}

-(void)uploadPortrait{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        [params setObject:PLATFORM forKey:@"platform"];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN] forKey:@"push_token"];
        [params setObject:[SharedMethod getTokenWithRoute:@"user/profile/image" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        [params setObject:[convertedData base64EncodedString] forKey:@"image:base64"];
        
        #ifdef DEBUG
        NSLog(@"Prepare Upload Profile Picture Params - %@",params);
        #endif
        
        [WebAPI uploadProfilePicture:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"balance"] forKey:USER_BALANCE];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.navigationController popViewControllerAnimated:YES];
                    [self.tabBarController setSelectedIndex:0];
                }
            } else{
                NSString *code = [result objectForKey:@"code"];
                if([code isEqualToString:@"000"]){
                    #ifdef DEBUG
                    NSLog(@"Done uploading portrait");
                    #endif
                    [[SharedMethod new] removeProfileImage:YES];
                    [self retrieveAll];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)checkCardExpire:(NSString*)expireDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",expireDate]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *myDate = [dateFormatter stringFromDate:date];
    NSDate *expiryDate = [dateFormatter dateFromString:myDate];
    NSDate *currentDate = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay
                                               fromDate:currentDate
                                                 toDate:expiryDate
                                                options:0];

    if(![expireDate isEqual: @""]){
        if([components day] <= 30){
            cardExpireVC = [storyboard instantiateViewControllerWithIdentifier:@"cardExpireVC"];
            cardExpireVC.cardExpireDelegate = self;
            
            if([components day] <= 0){
                isExpired = YES;
                cardExpireVC.promptMessage = NSLocalizedString(@"msg_Card_Expired", nil);
            } else{
                isExpired = NO;
                cardExpireVC.promptMessage = NSLocalizedString(@"msg_Card_Expiring", nil);
            }
            
            [cardExpireVC.view setTag:100];
            [self.view.window addSubview:cardExpireVC.view];
            [self addChildViewController:cardExpireVC];
        }
    }
}

-(void)checkOrderDetail{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSDictionary *params = @{@"email"      : [userDetail objectForKey:@"email"],
                                 @"userId"     : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"push_token" : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                 @"token"      : [SharedMethod getTokenWithRoute:@"user/order/latest" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
#ifdef DEBUG
        NSLog(@"Retrieve Latest Order Detail Param - %@",params);
#endif
        
        [WebAPI retrieveLatestOrderDetail:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                if([[result objectForKey:@"errCode"] isEqualToString:@"OD4"]){
                    [self retrievePrice];
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"approved"] || [[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"paid"]){
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    } else if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"completed"]){
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    } else if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"pending"]){
                        PaymentGatewayVC *paymentGatewayVC = [storyboard instantiateViewControllerWithIdentifier:@"paymentGatewayVC"];
                        paymentGatewayVC.dismissPaymentGatewayDelegate = self;
                        paymentGatewayVC.userId = [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID];
                        paymentGatewayVC.dict = [result objectForKey:@"order"];
                        paymentGatewayVC.type = @"renew";
                        [paymentGatewayVC setHidesBottomBarWhenPushed:YES];
                        [self.navigationController pushViewController:paymentGatewayVC animated:YES];
                    } else{
                        [self retrievePrice];
                    }
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)retrievePrice{
    BOOL guestType = [[NSUserDefaults standardUserDefaults] boolForKey:IS_GUEST];
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSDictionary *params = @{@"push_token" : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                 @"userId"     : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"token"      : [SharedMethod getTokenWithRoute:@"user/pricing/virtualcard" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
#ifdef DEBUG
        NSLog(@"Retrieve Price Param - %@",params);
#endif
        
        [WebAPI retrievePrice:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                NSString *amount = [NSString stringWithFormat:@"%@",[result objectForKey:@"renew"]];
                
                if (guestType) {
                    amount = [NSString stringWithFormat:@"%@",[result objectForKey:@"register"]];
                    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_GuestUpgrade", nil) message:[NSString stringWithFormat:@"%@%@%@%@",NSLocalizedString(@"msg_GuestUpgrade_1", nil),@"RM",amount,NSLocalizedString(@"msg_GuestUpgrade_2", nil)] preferredStyle:UIAlertControllerStyleAlert];
                }else {
                    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Renew", nil) message:[NSString stringWithFormat:@"%@%@%@%@",NSLocalizedString(@"msg_Renew_1", nil),@"RM",amount,NSLocalizedString(@"msg_Renew_2", nil)] preferredStyle:UIAlertControllerStyleAlert];
                }
                
                UIAlertAction *proceedAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Proceed", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    if (guestType) {
                        [self retrieveUpgradeOrderId:amount];
                    }else {
                        [self retrieveOrderId:amount];
                    }
                }];
                
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
                
                [alertController addAction:proceedAction];
                [alertController addAction:cancelAction];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

// For E-Guest upgrade to E-Member
-(void)retrieveUpgradeOrderId:(NSString*)price{
    
    //FOR UAT PURPOSE//
    //    price = @"1.00";
    
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSDictionary *params = @{@"userId"     : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"fullname"   : [userDetail objectForKey:@"fullname"],
                                 @"contact"    : [userDetail objectForKey:@"contact"],
                                 @"email"      : [userDetail objectForKey:@"email"],
                                 @"description": @"E-Guest Upgrade",
                                 @"amount"     : price,
                                 @"type"       : @"eguest_upgrade",
                                 @"push_token" : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                 @"token"      : [SharedMethod getTokenWithRoute:@"user/order/create" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
#ifdef DEBUG
        NSLog(@"Retrieve Order Id Param - %@",params);
#endif
        
        [WebAPI generateOrderId:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"pending"]){
                        
                        UIAlertController *upgradeAlert = [UIAlertController
                                                    alertControllerWithTitle: NSLocalizedString(@"App_Name", nil)
                                                    message:[NSString stringWithFormat:@"%@%@%@",
                                                             NSLocalizedString(@"ttl_Guest_Upgrade_1", nil),
                                                             [[result objectForKey:@"order"] objectForKey:@"amount"],
                                                             NSLocalizedString(@"ttl_Guest_Upgrade_2", nil)
                                                             ]
                                                    preferredStyle:UIAlertControllerStyleAlert
                                                    ];
                        
                        UIAlertAction *upgradeOk = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                            UIAlertController *alert = [UIAlertController
                                                        alertControllerWithTitle: NSLocalizedString(@"App_Name", nil)
                                                        message:[NSString stringWithFormat:@"%@%@",
                                                                 NSLocalizedString(@"lbl_Flash_Non_Refundable", nil),
                                                                 [[result objectForKey:@"order"] objectForKey:@"amount"]
                                                                 ]
                                                        preferredStyle:UIAlertControllerStyleAlert
                                                        ];
                            
                            UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                if([[SharedMethod new] checkNetworkConnectivity]){
                                    [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
                                     PaymentGatewayVC *paymentGatewayVC = [storyboard instantiateViewControllerWithIdentifier:@"paymentGatewayVC"];
                                                               paymentGatewayVC.dismissPaymentGatewayDelegate = self;
                                                               paymentGatewayVC.userId = [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID];
                                                               paymentGatewayVC.dict = [result objectForKey:@"order"];
                                                               paymentGatewayVC.type = @"eguest_upgrade";
                                                               [paymentGatewayVC setHidesBottomBarWhenPushed:YES];
                                                               [self.navigationController pushViewController:paymentGatewayVC animated:YES];
                                    
                                }else{
                                    [self presentViewController:[[SharedMethod new]
                                                                 setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil)
                                                                 message:NSLocalizedString(@"msg_Internet", nil)
                                                                 cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil
                                     ];
                                    
                                }
                                
                            }];
                            [alert addAction:ok];
                            
                            UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
                            [alert addAction:cancel];
                            
                            [self presentViewController:alert animated:NO completion:nil];
                            
                        }];
                        [upgradeAlert addAction:upgradeOk];
                        
                        UIAlertAction *upgradeCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
                        [upgradeAlert addAction:upgradeCancel];
                        
                        [self presentViewController:upgradeAlert animated:NO completion:nil];
                        
                    } else{
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    }
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)retrieveOrderId:(NSString*)price{
    //FOR UAT PURPOSE//
//    price = @"1.00";
    
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSDictionary *params = @{@"userId"     : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"fullname"   : [userDetail objectForKey:@"fullname"],
                                 @"contact"    : [userDetail objectForKey:@"contact"],
                                 @"email"      : [userDetail objectForKey:@"email"],
                                 @"description": NSLocalizedString(@"ipay_Renew_Description", nil),
                                 @"amount"     : price,
                                 @"type"       : @"renew",
                                 @"push_token" : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                 @"token"      : [SharedMethod getTokenWithRoute:@"user/order/create" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
#ifdef DEBUG
        NSLog(@"Retrieve Order Id Param - %@",params);
#endif
        
        [WebAPI generateOrderId:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"pending"]){
                        
                        UIAlertController *alert = [UIAlertController
                                                    alertControllerWithTitle: @""
                                                    message:[NSString stringWithFormat:@"%@%@", NSLocalizedString(@"lbl_Flash_Non_Refundable", nil), price]
                                                    preferredStyle:UIAlertControllerStyleAlert
                                                    ];
                        
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            PaymentGatewayVC *paymentGatewayVC = [storyboard instantiateViewControllerWithIdentifier:@"paymentGatewayVC"];
                            paymentGatewayVC.dismissPaymentGatewayDelegate = self;
                            paymentGatewayVC.userId = [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID];
                            paymentGatewayVC.dict = [result objectForKey:@"order"];
                            paymentGatewayVC.type = @"renew";
                            [paymentGatewayVC setHidesBottomBarWhenPushed:YES];
                            [self.navigationController pushViewController:paymentGatewayVC animated:YES];
                        }];
                        [alert addAction:ok];
                        
                        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
                        [alert addAction:cancel];
                        
                        [self presentViewController:alert animated:NO completion:nil];
                        
                        
                        
                        
                    } else{
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    }
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)displayPaymentGatewayMessage:(NSString*)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"App_Name", nil) message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)dismissPaymentGatewayVC:(NSString*)message{
    if(message.length>0){
        [self performSelector:@selector(displayPaymentGatewayMessage:) withObject:message afterDelay:0.2];
    }
}

-(void)processEguestUpgrade:(NSDictionary *) basicDetails {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_SOCIAL];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_GUEST];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_MEMBER];
    [self retrieveAll];
}

-(void)processRenew:(NSDictionary *)basicDetails{
    isRenew = !isRenew;
//    if([[basicDetails objectForKey:@"status"] isEqualToString:@"paid"] || [[basicDetails objectForKey:@"status"] isEqualToString:@"approved"]){
    [self performSelector:@selector(renewMembership:) withObject:basicDetails afterDelay:0.2];
//    }
}

-(void)renewMembership:(NSDictionary*)userDetails{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        [params setObject:[userDetails objectForKey:@"id"] forKey:@"order_id"];
        [params setObject:[userDetails objectForKey:@"secret"] forKey:@"order_secret"];
        [params setObject:[[userDetail objectForKey:@"card"] objectForKey:@"number"] forKey:@"card_number"];
        [params setObject:PLATFORM forKey:@"platform"];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN] forKey:@"push_token"];
        [params setObject:[SharedMethod getTokenWithRoute:@"user/card/renew" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        
#ifdef DEBUG
        NSLog(@"Renew Membership Params - %@",params);
#endif
        
        [WebAPI renewMembership:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.navigationController popViewControllerAnimated:YES];
                    [self.tabBarController setSelectedIndex:0];
                }
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                NSString *code = [result objectForKey:@"code"];
                if([code isEqualToString:@"000"]){
                    
                    NSString *beforeRenewExpireDate = [[userDetail objectForKey:@"card"] objectForKey:@"expiry"];
                    NSString *afterRenewExpireDate = [[result objectForKey:@"card"] objectForKey:@"expiry"];
                    
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
                    
                    NSDate *before = [dateFormatter dateFromString:beforeRenewExpireDate];
                    NSDate *after = [dateFormatter dateFromString:afterRenewExpireDate];
                    
                    if([self isRenewCompletedByComparingTwoDates:before andEndDate:after]){
                        //Renew Completed//
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"msg_Renew_Success", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    } else{
                        //Failed to Renew//
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"msg_Renew_Fail", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    }
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Default", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(BOOL)isRenewCompletedByComparingTwoDates:(NSDate*)startDate andEndDate:(NSDate*)endDate{
    NSComparisonResult result;

    result = [startDate compare:endDate]; // comparing two dates
    if(result == NSOrderedSame){
        //Both date are same//
        return NO;
    } else{
        return YES;
    }
}

#pragma mark - Actions
- (IBAction)btnGuestUpgradeDidPressed:(id)sender {
    //NSLog(@"Upgrade Pressed");
    //[self retrieveUpgradeOrderId];
    [self retrievePrice];
}

- (IBAction)bbtnEditDidPressed:(id)sender {
    if(!self.failedView.hidden){
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Internet_Edit", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    } else{
        EditProfileVC *editProfileVC = [storyboard instantiateViewControllerWithIdentifier:@"editProfileVC"];
        editProfileVC.currentImage = self.imgPortrait.image;
        [editProfileVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:editProfileVC animated:YES];
    }
}

- (IBAction)bbtnLogoutDidPressed:(id)sender {
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Confirmation", nil) message:NSLocalizedString(@"msg_Logout", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self logoutProcess];
    }];
    
    UIAlertAction *noAction= [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_No", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)btnRetryDidPressed:(id)sender {
    [self retrieveAll];
}

- (IBAction)flipCardAction:(id)sender{
    CATransition *transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.duration = .8f;
    transition.type = @"flip";
    transition.subtype = @"fromLeft";
    
    if(flipCount==0){
        [UIView animateWithDuration:0.8f animations:^{
            [self.frontView setUserInteractionEnabled:NO];
            [self.backView setUserInteractionEnabled:NO];
            [self.frontView setAlpha:0.0f];
            [self.backView setAlpha:1.0f];
        } completion:^(BOOL finished) {
            [self.frontView setUserInteractionEnabled:YES];
            [self.backView setUserInteractionEnabled:YES];
            [self.frontView setHidden:YES];
            [self.backView setHidden:NO];
        }];
        
        flipCount=1;
        
        [self.frontView.layer removeAllAnimations];
        [self.frontView.layer setCornerRadius:8.0f];
        [self.frontView.layer setMasksToBounds:NO];
        [self.frontView.layer addAnimation:transition forKey:kCATransition];
        
        [self.backView.layer removeAllAnimations];
        [self.backView.layer setCornerRadius:8.0f];
        [self.backView.layer setMasksToBounds:NO];
        [self.backView.layer addAnimation:transition forKey:kCATransition];
    } else if(flipCount==1){
        [UIView animateWithDuration:0.8f animations:^{
            [self.frontView setUserInteractionEnabled:NO];
            [self.backView setUserInteractionEnabled:NO];
            [self.frontView setAlpha:1.0f];
            [self.backView setAlpha:0.0f];
        } completion:^(BOOL finished) {
            [self.frontView setUserInteractionEnabled:YES];
            [self.backView setUserInteractionEnabled:YES];
            [self.frontView setHidden:NO];
            [self.backView setHidden:YES];
        }];
        
        flipCount=0;
        
        [self.frontView.layer removeAllAnimations];
        [self.frontView.layer setCornerRadius:8.0f];
        [self.frontView.layer setMasksToBounds:NO];
        [self.frontView.layer addAnimation:transition forKey:kCATransition];
        
        [self.backView.layer removeAllAnimations];
        [self.backView.layer setCornerRadius:8.0f];
        [self.backView.layer setMasksToBounds:NO];
        [self.backView.layer addAnimation:transition forKey:kCATransition];
    }
}

- (IBAction)flipGuestCardAction:(id)sender{
    CATransition *transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.duration = .8f;
    transition.type = @"flip";
    transition.subtype = @"fromLeft";
    
    if(flipCount==0){
        [UIView animateWithDuration:0.8f animations:^{
            [self.guestFrontView setUserInteractionEnabled:NO];
            [self.guestBackView setUserInteractionEnabled:NO];
            [self.guestFrontView setAlpha:0.0f];
            [self.guestBackView setAlpha:1.0f];
        } completion:^(BOOL finished) {
            [self.guestFrontView setUserInteractionEnabled:YES];
            [self.guestBackView setUserInteractionEnabled:YES];
            [self.guestFrontView setHidden:YES];
            [self.guestBackView setHidden:NO];
        }];
        
        flipCount=1;
        
        [self.guestFrontView.layer removeAllAnimations];
        [self.guestFrontView.layer setCornerRadius:8.0f];
        [self.guestFrontView.layer setMasksToBounds:NO];
        [self.guestFrontView.layer addAnimation:transition forKey:kCATransition];
        
        [self.guestBackView.layer removeAllAnimations];
        [self.guestBackView.layer setCornerRadius:8.0f];
        [self.guestBackView.layer setMasksToBounds:NO];
        [self.guestBackView.layer addAnimation:transition forKey:kCATransition];
    } else if(flipCount==1){
        [UIView animateWithDuration:0.8f animations:^{
            [self.guestFrontView setUserInteractionEnabled:NO];
            [self.guestBackView setUserInteractionEnabled:NO];
            [self.guestFrontView setAlpha:1.0f];
            [self.guestBackView setAlpha:0.0f];
        } completion:^(BOOL finished) {
            [self.guestFrontView setUserInteractionEnabled:YES];
            [self.guestBackView setUserInteractionEnabled:YES];
            [self.guestFrontView setHidden:NO];
            [self.guestBackView setHidden:YES];
        }];
        
        flipCount=0;
        
        [self.guestFrontView.layer removeAllAnimations];
        [self.guestFrontView.layer setCornerRadius:8.0f];
        [self.guestFrontView.layer setMasksToBounds:NO];
        [self.guestFrontView.layer addAnimation:transition forKey:kCATransition];
        
        [self.guestBackView.layer removeAllAnimations];
        [self.guestBackView.layer setCornerRadius:8.0f];
        [self.guestBackView.layer setMasksToBounds:NO];
        [self.guestBackView.layer addAnimation:transition forKey:kCATransition];
    }
}
- (IBAction)btnQrGuestDidPressed:(id)sender {
    [self generateQR];
    qrCodeVC = [storyboard instantiateViewControllerWithIdentifier:@"qrCodeVC"];
    qrCodeVC.imgQR = imageQR;
    
    brightness = [[UIScreen mainScreen] brightness];
    [[UIScreen mainScreen] setBrightness:1.0f];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnDismissDidPressed:)];
    [qrCodeVC.view addGestureRecognizer:tapGesture];
    [qrCodeVC.view setTag:100];
    [self.view.window addSubview:qrCodeVC.view];
    [self addChildViewController:qrCodeVC];
}

- (IBAction)btnQrDidPressed:(id)sender {
    [self generateQR];
    qrCodeVC = [storyboard instantiateViewControllerWithIdentifier:@"qrCodeVC"];
    qrCodeVC.imgQR = imageQR;
    
    brightness = [[UIScreen mainScreen] brightness];
    [[UIScreen mainScreen] setBrightness:1.0f];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnDismissDidPressed:)];
    [qrCodeVC.view addGestureRecognizer:tapGesture];
    [qrCodeVC.view setTag:100];
    [self.view.window addSubview:qrCodeVC.view];
    [self addChildViewController:qrCodeVC];
}

- (IBAction)btnPaymentQrDidPressed:(id)sender {
    if(isExpired){
        cardExpireVC.promptMessage = NSLocalizedString(@"msg_Card_Expired", nil);
        [cardExpireVC.view setTag:100];
        [self.view.window addSubview:cardExpireVC.view];
        [self addChildViewController:cardExpireVC];
    } else{
        [self generatePaymentQr];
        qrCodeVC = [storyboard instantiateViewControllerWithIdentifier:@"qrCodeVC"];
        qrCodeVC.imgQR = imageQR;
        
        brightness = [[UIScreen mainScreen] brightness];
        [[UIScreen mainScreen] setBrightness:1.0f];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnDismissDidPressed:)];
        [qrCodeVC.view addGestureRecognizer:tapGesture];
        [qrCodeVC.view setTag:100];
        [self.view.window addSubview:qrCodeVC.view];
        [self addChildViewController:qrCodeVC];
    }
}

- (IBAction)btnScanDidPressed:(id)sender {
    [self generateQR];
    qrCodeVC = [storyboard instantiateViewControllerWithIdentifier:@"qrCodeVC"];
    qrCodeVC.imgQR = imageQR;
    
    brightness = [[UIScreen mainScreen] brightness];
    [[UIScreen mainScreen] setBrightness:1.0f];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnDismissDidPressed:)];
    [qrCodeVC.view addGestureRecognizer:tapGesture];
    [qrCodeVC.view setTag:100];
    [self.view.window addSubview:qrCodeVC.view];
    [self addChildViewController:qrCodeVC];
    /*
    //For TouchID//
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    NSString *myLocalizedReasonString = @"By verifying the finger print to agree to the terms and conditions.";
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                  localizedReason:myLocalizedReasonString
                            reply:^(BOOL success, NSError *error) {
                                if (success) {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        [self generateQR];
                                        qrCodeVC = [storyboard instantiateViewControllerWithIdentifier:@"qrCodeVC"];
                                        qrCodeVC.imgQR = imageQR;
                                        
                                        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnDismissDidPressed:)];
                                        [qrCodeVC.view addGestureRecognizer:tapGesture];
                                        [qrCodeVC.view setTag:100];
                                        [self.view.window addSubview:qrCodeVC.view];
                                        [self addChildViewController:qrCodeVC];
                                    });
                                } else {
                                    if(kLAErrorAuthenticationFailed){
                                        [myContext evaluatePolicy:kLAPolicyDeviceOwnerAuthentication localizedReason:myLocalizedReasonString reply:^(BOOL success, NSError * _Nullable error) {
                                            if(success){
                                                [self generateQR];
                                                qrCodeVC = [storyboard instantiateViewControllerWithIdentifier:@"qrCodeVC"];
                                                qrCodeVC.imgQR = imageQR;
                                                
                                                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnDismissDidPressed:)];
                                                [qrCodeVC.view addGestureRecognizer:tapGesture];
                                                [qrCodeVC.view setTag:100];
                                                [self.view.window addSubview:qrCodeVC.view];
                                                [self addChildViewController:qrCodeVC];
                                            } else{
                                                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"msg_Authentication_Fail", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                                            }
                                        }];
                                    } else if(kLAErrorUserCancel){
                                        NSLog(@"Cancelled");
                                    } else if(kLAErrorUserFallback){
                                        [myContext evaluatePolicy:kLAPolicyDeviceOwnerAuthentication localizedReason:myLocalizedReasonString reply:^(BOOL success, NSError * _Nullable error) {
                                            if(success){
                                                [self generateQR];
                                                qrCodeVC = [storyboard instantiateViewControllerWithIdentifier:@"qrCodeVC"];
                                                qrCodeVC.imgQR = imageQR;
                                                
                                                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnDismissDidPressed:)];
                                                [qrCodeVC.view addGestureRecognizer:tapGesture];
                                                [qrCodeVC.view setTag:100];
                                                [self.view.window addSubview:qrCodeVC.view];
                                                [self addChildViewController:qrCodeVC];
                                            } else{
                                                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"msg_Authentication_Fail", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                                            }
                                        }];
                                    }
                                }
                            }];
    } else{
        if(authError.code == -8){
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:authError.localizedDescription cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        }
        NSLog(@"myContext code - %d && error - %@",(int)authError.code,authError.localizedDescription);
    }
     */
}

- (IBAction)btnTopUpDidPressed:(id)sender {
    if(isExpired){
        cardExpireVC.promptMessage = NSLocalizedString(@"msg_Card_Expired", nil);
        [cardExpireVC.view setTag:100];
        [self.view.window addSubview:cardExpireVC.view];
        [self addChildViewController:cardExpireVC];
    } else{
        TopUpVC *topUpVC = [storyboard instantiateViewControllerWithIdentifier:@"topUpVC"];
        [topUpVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:topUpVC animated:YES];
    }
}

- (IBAction)btnRenewDidPressed:(id)sender{
//    [self retrievePrice];
    [self checkOrderDetail];
}

- (IBAction)btnDismissDidPressed:(id)sender {
    [[UIScreen mainScreen] setBrightness:brightness];
    for(UIView *temp in self.view.window.subviews){
        if(temp.tag == 100){
            [temp removeFromSuperview];
        }
    }
    [qrCodeVC removeFromParentViewController];
    
    [self retrieveAll];
}

-(void)closeView{
    for(UIView *temp in self.view.window.subviews){
        if(temp.tag == 100){
            [temp removeFromSuperview];
        }
    }
    [cardExpireVC removeFromParentViewController];
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [labelArray count]+1;
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        ProfileBalanceCell *cell = (ProfileBalanceCell*)[tableView dequeueReusableCellWithIdentifier:@"profileBalanceCell"];
        if(cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProfileBalanceCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        [cell setupBalance:[[NSUserDefaults standardUserDefaults] objectForKey:USER_POINT]];
        [cell.btnTopUp addTarget:self action:@selector(btnTopUpDidPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnWalletQr addTarget:self action:@selector(btnPaymentQrDidPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_GUEST]) {
            [cell setHidden:YES];
        } else {
            [cell setHidden:NO];
        }
        
        return cell;
    } else{
        ProfileCell *cell = (ProfileCell*)[tableView dequeueReusableCellWithIdentifier:@"profileCell"];
        [cell.imgIcon setImage:[UIImage imageNamed:[iconArray objectAtIndex:indexPath.row-1]]];
        [cell.lblTitle setText:[labelArray objectAtIndex:indexPath.row-1]];
        
        int voucherCount = [[[[NSUserDefaults standardUserDefaults] objectForKey:USER_POINT_DETAIL] objectForKey:@"vouchers_available"] intValue];
        if(indexPath.row==1 && voucherCount>0){
            [cell.lblCount setHidden:NO];
            [cell.lblCount setText:[NSString stringWithFormat:@"%d",voucherCount]];
        } else if(indexPath.row==1 && voucherCount<=0){
            [cell.lblCount setHidden:YES];
        }
        
        NSInteger badgeNo = [[NSUserDefaults standardUserDefaults] integerForKey:INBOX_COUNT];
        if(indexPath.row==2 && badgeNo>0){
            [cell.lblCount setHidden:NO];
            [cell.lblCount setText: [NSString stringWithFormat:@"%d",(int)badgeNo]];
        } else if(indexPath.row==2 && badgeNo<=0){
            [cell.lblCount setHidden:YES];
        }
        
        if(indexPath.row>2){
            [cell.lblCount setHidden:YES];
        }
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==1){
        //My Voucher//
        MyVoucherVC *myVoucherVC = [storyboard instantiateViewControllerWithIdentifier:@"myVoucherVC"];
        myVoucherVC.fromProfile = YES;
        [myVoucherVC setHidesBottomBarWhenPushed:YES];
        myVoucherVC.myVouchers = [self filterVouchers];
        [self.navigationController pushViewController:myVoucherVC animated:YES];
    } else if(indexPath.row==2){
        //Notification//
        NotificationVC *notificationVC = [storyboard instantiateViewControllerWithIdentifier:@"notificationVC"];
        [notificationVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:notificationVC animated:YES];
    } else if(indexPath.row==3){
        //Transaction History//
        NSMutableArray *transactions = [[SharedMethod new] processTransactions:[[[NSUserDefaults standardUserDefaults] objectForKey:USER_POINT_DETAIL] objectForKey:@"transactions"]];
        TransactionVC *transactionVC = [storyboard instantiateViewControllerWithIdentifier:@"transactionVC"];
        transactionVC.transactions = transactions;
        [transactionVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:transactionVC animated:YES];  
    } else if(indexPath.row==4){
        //Booking History//
        BookingHistoryVC *bookingHistoryVC = [storyboard instantiateViewControllerWithIdentifier:@"bookingHistoryVC"];
        [bookingHistoryVC setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:bookingHistoryVC animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        return 110;
    } else{
        if([UIScreen mainScreen].bounds.size.height <= 568){
            return 60.0f;
        } else{
            return (self.tableView.bounds.size.height-112)/[labelArray count];
        }
    }
}

@end
