//
//  PromotionDetailVC.h
//  Oasis
//
//  Created by Wong Ryan on 22/08/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Promotion.h"

@interface PromotionDetailVC : UIViewController
@property (nonatomic) Promotion *selectedPromotion;
@property (nonatomic) UIImage *bannerImage;
@property (nonatomic) BOOL isPromo;
@end
