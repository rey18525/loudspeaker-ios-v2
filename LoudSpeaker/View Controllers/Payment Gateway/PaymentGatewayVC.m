//
//  PaymentGatewayVC.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 03/01/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "PaymentGatewayVC.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "Ipay.h"
#import "IpayPayment.h"
#import "WebAPI.h"
#import "SVProgressHUD.h"
#import "CardlessRegistrationVC.h"
#import <PDKeychainBindingsController/PDKeychainBindingsController.h>
#import "CardRegistrationVC.h"

#import <MOLPayXDK/MOLPayLib.h>


@interface PaymentGatewayVC ()<PaymentResultDelegate, DismissPaymentGatewayDelegate, DismissCardRegistrationDelegate, MOLPayLibDelegate> {
    MOLPayLib *mp;
    BOOL isCloseButtonClick;
    BOOL isPaymentInstructionPresent;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbtnBack;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@end

@implementation PaymentGatewayVC{
    UIStoryboard *storyboard;
    UIAlertController *alertController;
    UIAlertAction *okAction;
    NSArray *topUpArray;
    NSDictionary *result;
    Ipay *paymentsdk;
    BOOL isPaymentSuccess;
    
	//MOLPayLib *mp;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    isPaymentSuccess = NO;
    
    [self setupNavigationBar];
    [self setupData];
    
    [self performSelector:@selector(prepareMOLPay) withObject:self afterDelay:0.5];
}
/*
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}*/

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //NSLog(@"isPaymentSuccess - %id", isPaymentSuccess);
    /*
    if(isPaymentSuccess == YES) {
        [self performSelector:@selector(dismissPaymentGate) withObject:self afterDelay:1.0];
        
    }else if(isPaymentSuccess == NO){
        [self performSelector:@selector(prepareMOLPay) withObject:self afterDelay:1.5];

    }*/
}

#pragma mark - Setup Initialization
-(void)setupNavigationBar{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)setupData{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self bbtnBackDidPressed:self];
    }];
}

-(void)updateUI{
    
}

- (IBAction)closemolpay:(id)sender
{
    // Closes MOLPay
    [mp closemolpay];
    
    [self performSelector:@selector(dismissPaymentGate) withObject:self afterDelay:1.5];

}

-(void)dismissPaymentGate{
    //[self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}


-(void)prepareMOLPay{
    //isPaymentSuccess = NO;
    
    NSLog(@"prepareMOLPay");
    // Store order_id to be passed to /latest API again
    [[NSUserDefaults standardUserDefaults] setValue:[self.dict objectForKey:@"id"] forKey:MOL_CREATE_ID];
    
	/*NSDictionary * paymentRequestDict = @{
										  // Optional, REQUIRED when use online Sandbox environment and account credentials.
										  @"mp_dev_mode": [NSNumber numberWithBool:NO],
										  
										  // Mandatory String. Values obtained from MOLPay.
										  @"mp_username": @"username",
										  @"mp_password": @"password",
										  @"mp_merchant_ID": @"merchantid",
										  @"mp_app_name": @"appname",
										  @"mp_verification_key": @"vkey123",
										  
										  // Mandatory String. Payment values.
										  @"mp_amount": @"1.10", // Minimum 1.01
										  @"mp_order_ID": @"orderid123",
										  @"mp_currency": @"MYR",
										  @"mp_country": @"MY",
										  
										  // Optional, but required payment values. User input will be required when values not passed.
										  @"mp_channel": @"multi", // Use 'multi' for all available channels option. For individual channel seletion, please refer to https://github.com/MOLPay/molpay-mobile-xdk-examples/blob/master/channel_list.tsv.
										  @"mp_bill_description": @"billdesc",
										  @"mp_bill_name": @"billname",
										  @"mp_bill_email": @"email@domain.com",
										  @"mp_bill_mobile": @"+1234567",
										  
										  // Optional, allow channel selection.
										  @"mp_channel_editing": [NSNumber numberWithBool:NO],
										  
										  // Optional, allow billing information editing.
										  @"mp_editing_enabled": [NSNumber numberWithBool:NO],
										  
										  // Optional, for Escrow.
										  @"mp_is_escrow": @"0", // Put "1" to enable escrow
										  
										  // Optional, for credit card BIN restrictions and campaigns.
										  @"mp_bin_lock": [NSArray arrayWithObjects:@"414170", @"414171", nil],
										  
										  // Optional, for mp_bin_lock alert error.
										  @"mp_bin_lock_err_msg": @"Only UOB allowed",
										  
										  // WARNING! FOR TRANSACTION QUERY USE ONLY, DO NOT USE THIS ON PAYMENT PROCESS.
										  // Optional, provide a valid cash channel transaction id here will display a payment instruction screen. Required if mp_request_type is 'Receipt'.
										  @"mp_transaction_id": @"",
										  // Optional, use 'Receipt' for Cash channels, and 'Status' for transaction status query.
										  @"mp_request_type": @"",
										  
										  // Optional, use this to customize the UI theme for the payment info screen, the original XDK custom.css file can be obtained at https://github.com/MOLPay/molpay-mobile-xdk-examples/blob/master/custom.css.
										  @"mp_custom_css_url": [[NSBundle mainBundle] pathForResource:@"custom.css" ofType:nil],
										  
										  // Optional, set the token id to nominate a preferred token as the default selection, set "new" to allow new card only.
										  @"mp_preferred_token": @"",
										  
										  // Optional, credit card transaction type, set "AUTH" to authorize the transaction.
										  @"mp_tcctype": @"",
										  
										  // Optional, required valid credit card channel, set true to process this transaction through the recurring api, please refer the MOLPay Recurring API pdf.
										  @"mp_is_recurring": [NSNumber numberWithBool:NO],
										  
										  // Optional, show nominated channels.
										  @"mp_allowed_channels": [NSArray arrayWithObjects:@"credit", @"credit3", nil],
										  
										  // Optional, simulate offline payment, set boolean value to enable.
										  @"mp_sandbox_mode": [NSNumber numberWithBool:YES],
										  
										  // Optional, required a valid mp_channel value, this will skip the payment info page and go direct to the payment screen.
										  @"mp_express_mode": [NSNumber numberWithBool:YES],
										  
										  // Optional, extended email format validation based on W3C standards.
										  @"mp_advanced_email_validation_enabled": [NSNumber numberWithBool:YES],
										  
										  // Optional, extended phone format validation based on Google i18n standards.
										  @"mp_advanced_phone_validation_enabled": [NSNumber numberWithBool:YES],
										  
										  // Optional, explicitly force disable user input.
										  @"mp_bill_name_edit_disabled": [NSNumber numberWithBool:YES],
										  @"mp_bill_email_edit_disabled": [NSNumber numberWithBool:YES],
										  @"mp_bill_mobile_edit_disabled": [NSNumber numberWithBool:YES],
										  @"mp_bill_description_edit_disabled": [NSNumber numberWithBool:YES],
										  
										  // Optional, EN, MS, VI, TH, FIL, MY, KM, ID, ZH.
										  @"mp_language": @"EN",
										  
										  // Optional, Cash channel payment request expiration duration in hour.
										  @"mp_cash_waittime": @"48",
										  
										  // Optional, allow bypass of 3DS on some credit card channels.
										  @"mp_non_3DS": [NSNumber numberWithBool:YES],
										  
										  // Optional, disable card list option.
										  @"mp_card_list_disabled": [NSArray arrayWithObjects:@"credit", nil],
										  
										  // Optional for channels restriction, this option has less priority than mp_allowed_channels.
										  @"mp_disabled_channels": [NSArray arrayWithObjects:@"credit", nil]
										  };*/
	
    // This is a hack and also part of logic to only retrieve DEV or PROD endpoints for MOL
    NSString *mpUsername;
    NSString *mpPassword;
    NSString *mpMerchantId;
    NSString *mpAppName = @"Apppay";
    NSString *mpVerificationKey;
    NSNumber *mpDevMode;
    if([IS_DEBUG_FIREBASE_OVERRIDE isEqualToString:@"1"]) {
        // PROD
        mpDevMode = [NSNumber numberWithBool:NO];
        mpUsername = @"api_apppay";
        mpPassword = @"api_Yapp1501pA#";
        mpMerchantId = @"apppay";
        mpAppName = @"apppay";
        mpVerificationKey = @"a572cbb3f6d60a46e6c458286a6e90b9";
        
    } else {
        // DEV
        mpDevMode = [NSNumber numberWithBool:YES];
        mpUsername = @"api_SB_apppay";
        mpPassword = @"api_aPP34ay@";
        mpMerchantId = @"SB_apppay";
        mpAppName = @"Apppay";
        mpVerificationKey = @"448fdfeea0fe7cf825d63cda0a66df13";
    }
    
	NSDictionary * paymentRequestDict = @{
										  // Optional, REQUIRED when use online Sandbox environment and account credentials.
										  
										  @"mp_dev_mode": mpDevMode,
										  
                                          // Mandatory String. Values obtained from MOLPay.
                                          @"mp_username": mpUsername,
                                          @"mp_password": mpPassword,
                                          @"mp_merchant_ID": mpMerchantId,
                                          @"mp_app_name": mpAppName,
                                          @"mp_verification_key": mpVerificationKey,
                                          
										  // Mandatory String. Payment values.
										  @"mp_amount": [self.dict objectForKey:@"amount"], // Minimum 1.01
										  @"mp_order_ID": [self.dict objectForKey:@"id"],
										  @"mp_currency": @"MYR",
										  @"mp_country": @"MY",
										  
										  // Optional, but required payment values. User input will be required when values not passed.
										  @"mp_channel": @"multi", // Use 'multi' for all available channels option. For individual channel seletion, please refer to https://github.com/MOLPay/molpay-mobile-xdk-examples/blob/master/channel_list.tsv.
										  @"mp_bill_description": [self.dict objectForKey:@"description"],
										  @"mp_bill_name": [self.dict objectForKey:@"fullname"],
										  @"mp_bill_email": [self.dict objectForKey:@"email"],
										  @"mp_bill_mobile": [self.dict objectForKey:@"contact"],

										  // Optional, EN, MS, VI, TH, FIL, MY, KM, ID, ZH.
										  @"mp_language": @"EN",
										  
										  // Optional, Cash channel payment request expiration duration in hour.
										  @"mp_cash_waittime": @"48",
										  };
    NSLog(@"mol_request - %@", paymentRequestDict);
	/*
	mp = [[MOLPayLib alloc] initWithDelegate:self andPaymentDetails:paymentRequestDict];
	
	[self.navigationController pushViewController:mp animated:NO];*/
	//[self presentViewController:mp animated:NO completion:nil];
    
    mp = [[MOLPayLib alloc] initWithDelegate:self andPaymentDetails:paymentRequestDict];
    mp.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Close"
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(closemolpay:)];
    mp.navigationItem.hidesBackButton = YES;

    // Present method (Simple mode)
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:mp];
    [self presentViewController:nc animated:NO completion:nil];
}

-(void)transactionResult:(NSDictionary *)result{
	
	//[mp dismissViewControllerAnimated:NO completion:nil];
	[self dismissViewControllerAnimated:YES completion:nil];
    
#ifdef DEBUG
	NSLog(@"transactionResult - %@",result);
#endif
	
	NSString *sts = [NSString stringWithFormat:@"%@", [result valueForKey:@"status_code"]];
	NSString *transId = [NSString stringWithFormat:@"%@", [result valueForKey:@"txn_ID"]];
	
	if ([sts isEqualToString:@"00"]){
        isPaymentSuccess = YES;
		[self updateOrderStatus:transId andStatus:@"paid"];
        
	}else if ([sts isEqualToString:@"11"]){
		isPaymentSuccess = NO;
        [self updateOrderStatus:transId andStatus:@"rejected"];
        
	}else if ([sts isEqualToString:@"22"]){
        isPaymentSuccess = YES;
		[self updateOrderStatus:transId andStatus:@"pending"];
        
	}
	
}

-(void)preparePayment{
    NSString *callbackUrl = [[NSUserDefaults standardUserDefaults] objectForKey:PAYMENT_CALLBACK];
    dispatch_async(dispatch_get_main_queue(), ^{
        paymentsdk = [[Ipay alloc] init];
        paymentsdk.delegate = self;
        IpayPayment *payment = [[IpayPayment alloc] init];
        [payment setPaymentId:@""];
        [payment setMerchantKey:[[NSUserDefaults standardUserDefaults] objectForKey:MERCHANT_KEY]];
        [payment setMerchantCode:[[NSUserDefaults standardUserDefaults] objectForKey:MERCHANT_CODE]];
        [payment setRefNo:[self.dict objectForKey:@"id"]];
        [payment setAmount:[self.dict objectForKey:@"amount"]];
        [payment setCurrency:@"MYR"];
        [payment setProdDesc:[self.dict objectForKey:@"description"]];
        [payment setUserName:[self.dict objectForKey:@"fullname"]];
        [payment setUserEmail:[self.dict objectForKey:@"email"]];
        [payment setUserContact:[self.dict objectForKey:@"contact"]];
        [payment setRemark:@""];
//        [payment setLang:@"ISO-8859-1"];
        [payment setCountry:@"MY"];
//        [payment setBackendPostURL:@"http://apppay.my/TBD"];
        [payment setBackendPostURL:callbackUrl];
        UIView *paymentView = [paymentsdk checkout:payment];
        [paymentView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-64)];
        [self.contentView addSubview:paymentView];
    });
}

-(void)updateOrderStatus:(NSString*)refID andStatus:(NSString*)status{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSDictionary *params;
        
        /*if([self.type isEqualToString:@"register"]){
            params = @{@"id"                : [self.dict objectForKey:@"id"],
                       @"secret"            : [self.dict objectForKey:@"secret"],
                       @"status"            : status,
                       @"payment_type"      : @"ipay88",
                       @"payment_reference" : refID,
                       @"push_token"        : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                       @"token"             : [SharedMethod getTokenWithRoute:@"user/order/update" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                       };
        } else{
            params = @{@"userId"            : self.userId,
                       @"id"                : [self.dict objectForKey:@"id"],
                       @"secret"            : [self.dict objectForKey:@"secret"],
                       @"status"            : status,
                       @"payment_type"      : @"ipay88",
                       @"payment_reference" : refID,
                       @"push_token"        : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                       @"token"             : [SharedMethod getTokenWithRoute:@"user/order/update" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                       };
        }*/
		
		if([self.type isEqualToString:@"register"]){
			params = @{@"id"                : [self.dict objectForKey:@"id"],
					   @"secret"            : [self.dict objectForKey:@"secret"],
					   @"status"            : status,
					   @"payment_type"      : @"MOLPay",
					   @"payment_reference" : refID,
					   @"push_token"        : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
					   @"token"             : [SharedMethod getTokenWithRoute:@"user/order/update" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
					   };
		} else{
			params = @{@"userId"            : self.userId,
					   @"id"                : [self.dict objectForKey:@"id"],
					   @"secret"            : [self.dict objectForKey:@"secret"],
					   @"status"            : status,
					   @"payment_type"      : @"MOLPay",
					   @"payment_reference" : refID,
					   @"push_token"        : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
					   @"token"             : [SharedMethod getTokenWithRoute:@"user/order/update" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
					   };
		}
        
        
#ifdef DEBUG
        NSLog(@"Update Order Param - %@",params);
#endif
        
        [WebAPI updateOrderStatus:params andSuccess:^(id successBlock) {
			[mp dismissViewControllerAnimated:NO completion:nil];
            [SVProgressHUD dismiss];
            result = successBlock;
            
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    if([self.type isEqualToString:@"register"]){
                        if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"pending"]){
                            [self.dismissPaymentGatewayDelegate proceedRegister:[result objectForKey:@"order"]];
                            [self bbtnBackDidPressed:self];
                        } else if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"rejected"]){
                            [self.dismissPaymentGatewayDelegate dismissPaymentGatewayVC:@""];
                            [self bbtnBackDidPressed:self];
                        } else if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"cancelled"]){
                            [self.dismissPaymentGatewayDelegate dismissPaymentGatewayVC:@""];
                            [self bbtnBackDidPressed:self];
                        } else if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"approved"] || [[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"paid"]){
                            // Reset MOL order_id
                            [[NSUserDefaults standardUserDefaults] setValue:0 forKey:MOL_CREATE_ID];
                            
                            [self.dismissPaymentGatewayDelegate proceedRegister:[result objectForKey:@"order"]];
                            [self bbtnBackDidPressed:self];
                        }
                        
                    } else if([self.type isEqualToString:@"renew"]){
                        //Renew//
                        [self.dismissPaymentGatewayDelegate processRenew:[result objectForKey:@"order"]];
                        [self bbtnBackDidPressed:self];
                        
                    } else if([self.type isEqualToString:@"topup"]){
                        //Topup//
                        [self.dismissPaymentGatewayDelegate processTopup:[result objectForKey:@"order"]];
                        [self bbtnBackDidPressed:self];
                        
                    } else if([self.type isEqualToString:@"booking"]){
                        //Booking//
//                        [self.dismissPaymentGatewayDelegate processBooking:[result objectForKey:@"order"]];
                        [self bbtnBackDidPressed:self];
                        
                    } else if ([self.type isEqualToString:@"eguest_upgrade"]) {
                        [self.dismissPaymentGatewayDelegate processEguestUpgrade:[result objectForKey:@"order"]];
                        [self bbtnBackDidPressed:self];
                        
                    } else if([self.type isEqualToString:@"flashdeal"]){
                        //Flash deal
                        [self bbtnBackDidPressed:self];
                        if(isPaymentSuccess){
                            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"msg_Payment_Success", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                        }
                    }
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)paymentFailed:(NSString *)refNo withTransId:(NSString *)transId withAmount:(NSString *)amount withRemark:(NSString *)remark withErrDesc:(NSString *)errDesc{
    #ifdef DEBUG
    NSLog(@"Payment failed - %@",errDesc);
    #endif
    [self updateOrderStatus:transId andStatus:@"rejected"];
}

-(void)paymentSuccess:(NSString *)refNo withTransId:(NSString *)transId withAmount:(NSString *)amount withRemark:(NSString *)remark withAuthCode:(NSString *)authCode{
    #ifdef DEBUG
    NSLog(@"Payment success - %@",remark);
    #endif
    
    [self updateOrderStatus:transId andStatus:@"paid"] ;
}

-(void)paymentCancelled:(NSString *)refNo withTransId:(NSString *)transId withAmount:(NSString *)amount withRemark:(NSString *)remark withErrDesc:(NSString *)errDesc{
    #ifdef DEBUG
    NSLog(@"Payment cancelled - %@\nTransId - %@",errDesc,transId);
    #endif
    [self updateOrderStatus:transId andStatus:@"cancelled"];
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    if([self.type isEqualToString:@"register"]){
        [self removeFromParentViewController];
        [self dismissViewControllerAnimated:YES completion:nil];
    } else if([self.type isEqualToString:@"renew"]){
//        [self.dismissPaymentGatewayDelegate processRenew:@{@"id":@"test",@"secret":@"qweqwe"}];
        [self.navigationController popViewControllerAnimated:YES];
    } else if([self.type isEqualToString:@"booking"]){
//        [self.navigationController popViewControllerAnimated:YES];
        [self.dismissPaymentGatewayDelegate processBooking:[result objectForKey:@"order"]];
    } else if([self.type isEqualToString:@"topup"]){
        [self.navigationController popViewControllerAnimated:YES];
    } else if ([self.type isEqualToString:@"eguest_upgrade"]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else if([self.type isEqualToString:@"flashdeal"]){
        [self.navigationController popViewControllerAnimated:YES];
    }
}


@end
