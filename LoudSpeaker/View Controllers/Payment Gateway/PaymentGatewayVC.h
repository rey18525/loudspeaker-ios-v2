//
//  PaymentGatewayVC.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 03/01/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DismissPaymentGatewayDelegate
@optional
-(void)dismissPaymentGatewayVC:(NSString*)message;
-(void)proceedRegister:(NSDictionary*)basicDetails;
-(void)processRenew:(NSDictionary*)basicDetails;
-(void)processTopup:(NSDictionary*)basicDetails;
-(void)processBooking:(NSDictionary*)basicDetails;
-(void)processFlash:(NSDictionary*)basicDetails;
-(void)processEguestUpgrade:(NSDictionary*)basicDetails;
@end


@interface PaymentGatewayVC : UIViewController<DismissPaymentGatewayDelegate>
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *userId;
@property (nonatomic) NSDictionary *dict;

@property (nonatomic) id <DismissPaymentGatewayDelegate> dismissPaymentGatewayDelegate;

@end
