//
//  OutletVC.m
//  Oasis
//
//  Created by Wong Ryan on 21/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "OutletVC.h"
#import "CustomInfoVC.h"
#import "Outlet.h"
#import "OutletCell.h"
#import "Constant.h"
#import "SQLManager.h"
#import "SharedMethod.h"
#import "WebAPI.h"
#import "SVProgressHUD.h"
#import <UIView+Toast.h>
#import <QuartzCore/QuartzCore.h>
#import <MessageUI/MessageUI.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "EmptyTableCell.h"
#import "BusinessHourCell.h"
#import <CoreLocation/CoreLocation.h>
#import <PDKeychainBindings.h>

@import GoogleMaps;
@import MapKit;
@import Firebase;

@interface OutletVC ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,GMSMapViewDelegate,MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbtnAction;
@property (weak, nonatomic) IBOutlet UIImageView *imgOutlet;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblBusinessHour;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblFax;
@property (weak, nonatomic) IBOutlet UITableView *businessTableView;
@property (weak, nonatomic) IBOutlet UIImageView *imgFax;
@property (weak, nonatomic) IBOutlet UIButton *btnEmail;
@property (weak, nonatomic) IBOutlet UIImageView *imgEmail;

@property (weak, nonatomic) IBOutlet UIImageView *imgNavigate;
@property (weak, nonatomic) IBOutlet UIImageView *imgCall;
@property (weak, nonatomic) IBOutlet UIImageView *imgTimeIcon;

@property (weak, nonatomic) IBOutlet UIImageView *imgShare;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnNavigate;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UIView *shareView;
@property (weak, nonatomic) IBOutlet UIView *navigateView;
@property (weak, nonatomic) IBOutlet UIView *callView;
@property (weak, nonatomic) IBOutlet UIView *emailView;
@end

@implementation OutletVC{
    UIAlertController *alertController;
    UIImageView *imgView;
    NSMutableArray *outlets,*defaultOutlets,*myMarkers,*businessHours;
    Outlet *selectedOutlet;
    CGRect originalRect,moreViewRect;
    BOOL isSearch,isMoreOpen,isEmpty,isTimeOut;
    UIStoryboard *storyboard;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    if(SYSTEM_VERSION_LESS_THAN(@"10")){
        [self updateTabBarController];
    }
    
    [self setupInitialization];
    [self setupUI];
    [self setupDelegates];
    [self setupData];
    //    [self retrieveLocalOutlets];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self retrieveOutlets];
    });
    
    [self.contentView setHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.contentView setHidden:YES];
    [self.contentView setFrame:CGRectMake(originalRect.origin.x, originalRect.origin.y+originalRect.size.height, originalRect.size.width, originalRect.size.height)];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.mapView setSelectedMarker:nil];
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.contentView setAlpha:0.0f];
        [self.contentView setFrame:CGRectMake(originalRect.origin.x, originalRect.origin.y+originalRect.size.height, originalRect.size.width, originalRect.size.height)];
    } completion:^(BOOL finished) {
        [self.contentView setHidden:YES];
    }];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    originalRect = self.contentView.frame;
//    [self.contentView setFrame:CGRectMake(originalRect.origin.x, originalRect.origin.y+originalRect.size.height, originalRect.size.width, originalRect.size.height)];
}

#pragma mark - Custom Functions
-(void)setupNavigationBar{
    if(self.tableView.isHidden){
        [self.navigationItem setTitle:NSLocalizedString(@"nav_Outlet_V2", nil)];
        [self.bbtnAction setImage:[UIImage imageNamed:@"ic_action_list"]];
    } else{
        [self.navigationItem setTitle:NSLocalizedString(@"nav_OutletList_V2", nil)];
        [self.bbtnAction setImage:[UIImage imageNamed:@"ic_action_map"]];
    }
}

-(void)updateTabBarController{
    UITabBar *tabBar = self.tabBarController.tabBar;
    UITabBarItem *tab1 = [[tabBar items] objectAtIndex:3];
    [tab1 setImage:[[UIImage imageNamed:@"ic_bottombar_outlet"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab1 setSelectedImage:[[UIImage imageNamed:@"ic_bottombar_outlet_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
}

-(void)setupInitialization{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self.searchBar setPlaceholder:NSLocalizedString(@"phd_Search_Outlet", nil)];
    outlets = [[NSMutableArray alloc]init];
    defaultOutlets = [[NSMutableArray alloc] init];
    myMarkers = [[NSMutableArray alloc] init];
    businessHours = [[NSMutableArray alloc] init];
}

-(void)setupUI{
    [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    [[SharedMethod new] setCornerRadius:self.imgOutlet andCornerRadius:10.0f];
    [self.businessTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.businessTableView setEstimatedRowHeight:25];
    [self.businessTableView setRowHeight:UITableViewAutomaticDimension];
    [self.businessTableView setScrollEnabled:NO];
    
    [self.searchBar setHidden:NO];
    for (UIView *subview in self.searchBar.subviews) {
        [subview setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [subview removeFromSuperview];
            break;
        }
    }
    [self.view setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    UITextField *tempTextfield = [self.searchBar valueForKey:@"_searchField"];
    [tempTextfield setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [tempTextfield setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [tempTextfield setFont:[UIFont fontWithName:FONT_REGULAR size:12.0f]];
    
    [self.searchBar setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.searchBar setTintColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.searchBar setBarTintColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [[SharedMethod new] setBorder:tempTextfield borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    [tempTextfield setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
//    [[SharedMethod new] setBorder:self.shareView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
//    [[SharedMethod new] setBorder:self.navigateView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
//    [[SharedMethod new] setBorder:self.callView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
//    [[SharedMethod new] setBorder:self.emailView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    
    [self.lblName setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblName setFont:[UIFont fontWithName:FONT_REGULAR size:12.0f]];
    [self.lblAddress setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblAddress setFont:[UIFont fontWithName:FONT_REGULAR size:11.8f]];
    [self.lblAddress setAdjustsFontSizeToFitWidth:YES];
    
    [self.tableView setHidden:NO];
}

-(void)setupDelegates{
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.businessTableView setDelegate:self];
    [self.businessTableView setDataSource:self];
    [self.searchBar setDelegate:self];
    [self.mapView setDelegate:self];
}

-(void)setupData{
    isMoreOpen=NO;
    isEmpty=NO;
    isTimeOut=NO;
    [self.searchBar setText:@""];
    [self setupNavigationBar];
}

-(void)processOutletImage2:(Outlet*)outlet{
    NSURL *url = [NSURL URLWithString:outlet.imageString];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [[manager imageDownloader] downloadImageWithURL:url
                                            options:SDWebImageDownloaderLowPriority
                                           progress:nil
                                          completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                                              outlet.image = image;
                                          }];
}

-(void)retrieveLocalOutlets{
    NSMutableArray *temp = [[NSMutableArray alloc] init];
//    temp = [[SQLManager new] retrieveOutlets];
    
    NSDictionary *tempDict = [[SharedMethod new] readFromFile:OUTLET_FILE];
    temp = [[SharedMethod new] processOutlet:[tempDict objectForKey:@"outlets"]];
    
//    NSLog(@"tempDict - %@",temp);
    
    NSMutableArray *responseOutletID = [[NSMutableArray alloc] init];
    NSMutableArray *dbOutletID = [[SQLManager new] retrieveOutlets];
    NSMutableArray *outletsID = [[NSMutableArray alloc] init];
    
    for(Outlet *tempOutlet in temp){
        [responseOutletID addObject:tempOutlet.ID];
    }
    
    for(Outlet *tempOutlet in dbOutletID){
        [outletsID addObject:tempOutlet.ID];
    }
    
    [outletsID removeObjectsInArray:responseOutletID];
    [[SQLManager new] removeNotLatest:outletsID];
    
    for(Outlet *tempOutlet in temp){
        [self calculateDistance:tempOutlet];
    }
    
    [temp sortUsingComparator:^NSComparisonResult(Outlet *obj1, Outlet *obj2) {
        return [obj1.distance compare:obj2.distance];
    }];
    
    if([temp count]>0){
        for(Outlet *_temp in temp){
            [self processOutletImage2:_temp];
            [defaultOutlets addObject:_temp];
            [outlets addObject:_temp];
        }
        
        if([defaultOutlets count]<=0){
            isEmpty = YES;
            isTimeOut = NO;
        } else{
            isEmpty = NO;
            isTimeOut = NO;
        }
        
        isSearch=YES;
        [self setupMapViewData];
        [self.tableView reloadData];
    }
}

-(void)retrieveOutlets{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSDictionary *params = @{@"userId"           : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"token"            : [SharedMethod getTokenWithRoute:@"user/outlets" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
        #ifdef DEBUG
        NSLog(@"Request Outlets - %@",params);
        #endif
        
        [WebAPI retrieveOutlets:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.tabBarController setSelectedIndex:0];
                }
                [self retrieveLocalOutlets];
            } else{
                NSString *code = [result objectForKey:@"code"];
                if([code isEqualToString:@"000"]){
                    if([[result objectForKey:@"outlets"] count]>0){
                        result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                        [[SharedMethod new] writeToFile:OUTLET_FILE andDictioanry:result];
                        NSDictionary *tempDict = [[SharedMethod new] readFromFile:OUTLET_FILE];
                        NSMutableArray *temp = [[NSMutableArray alloc] init];
                        temp = [[SharedMethod new] processOutlet:[tempDict objectForKey:@"outlets"]];
                        [self retrieveLocalOutlets];
                    }
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    [self retrieveLocalOutlets];
                }
            }
        }];
    } else{
        [SVProgressHUD dismiss];
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        [self retrieveLocalOutlets];
    }
}

-(void)setupMapViewData{
    for(Outlet *temp in defaultOutlets){
        [self calculateDistance:temp];
    }
    
    [outlets sortUsingComparator:^NSComparisonResult(Outlet *obj1, Outlet *obj2) {
        return [obj1.distance compare:obj2.distance];
    }];
    
    [defaultOutlets sortUsingComparator:^NSComparisonResult(Outlet *obj1, Outlet *obj2) {
        return [obj1.distance compare:obj2.distance];
    }];
    
    if([myMarkers count]>0){
        [myMarkers removeAllObjects];
    }
    
    Outlet *temp = [defaultOutlets objectAtIndex:0];
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake([temp.latitude floatValue], [temp.longitude floatValue]);
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:position zoom:15];
    [self.mapView setCamera:camera];
    [self.mapView setMyLocationEnabled:YES];
    
    for(int count=0;count<[defaultOutlets count];count++){
        Outlet *loc = [defaultOutlets objectAtIndex:count];
        GMSMarker *marker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake([loc.latitude floatValue], [loc.longitude floatValue])];
        marker.title = loc.ID;//[NSString stringWithFormat:@"%d",count];
        [marker setIcon:[UIImage imageNamed:@"ic_outlet_marker"]];
        marker.map = self.mapView;
        [myMarkers addObject:marker];
    }
    
    [self.tableView reloadData];
}

-(void)calculateDistance:(Outlet*)loc{
    CLLocationManager *manager = [[CLLocationManager alloc]init];
    CLLocation *finalLoc = [[CLLocation alloc] initWithLatitude:[loc.latitude floatValue] longitude:[loc.longitude floatValue]];
    CLLocationDistance distance = [finalLoc distanceFromLocation:manager.location];
    
    loc.distance = [NSNumber numberWithDouble:(distance/1000)];
//    NSLog(@"Distance - %@",loc.distance);
}

-(NSString *)getCurrentDay{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [dateFormatter setDateFormat:@"EEEE"];
    NSString *currentDay = [dateFormatter stringFromDate:[NSDate date]];
    return currentDay;
}

-(NSString *)formatTime:(NSString*)time{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [formatter setDateFormat:@"HH:mm:ss"];
    NSDate *tempDate = [formatter dateFromString:time];
    
    [formatter setDateFormat:@"h:mm a"];
    NSString *converted = [formatter stringFromDate:tempDate];
    
    return converted;
}

-(NSString *)formatDate:(NSString*)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    
    //Previous Format//
//    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //New Format//
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *tempDate = [formatter dateFromString:date];
    
    [formatter setDateFormat:@"dd-MM-yyyy"];
    NSString *converted = [formatter stringFromDate:tempDate];
    
    return converted;
}

#pragma mark - UITableView Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView.tag==0){
        if(isSearch){
            if(isEmpty){
                return 1;
            } else{
                return [outlets count];
            }
        } else{
            if(isEmpty){
                return 1;
            } else{
                return [defaultOutlets count];
            }
        }
    } else{
        return [businessHours count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView.tag==0){
        if(isEmpty){
            EmptyTableCell *cell = (EmptyTableCell*)[tableView dequeueReusableCellWithIdentifier:@"emptyCell"];
            if(cell==nil){
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EmptyTableCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            if(isTimeOut){
                [cell.imgIcon setImage:[UIImage imageNamed:@"ic_no_internet"]];
                [cell.lblDescription setText:NSLocalizedString(@"lbl_No_Internet", nil)];
            } else{
                [cell.imgIcon setImage:[UIImage imageNamed:@"ic_empty"]];
                [cell.lblDescription setText:NSLocalizedString(@"lbl_Empty", nil)];
            }
            
            return cell;
        } else{
            OutletCell *cell = (OutletCell*)[tableView dequeueReusableCellWithIdentifier:@"outletCell" forIndexPath:indexPath];
            
            if([outlets count]>0){
                Outlet *location;
                if(isSearch){
                    location = [outlets objectAtIndex:indexPath.row];
                } else{
                    location = [defaultOutlets objectAtIndex:indexPath.row];
                }
                
                [[SharedMethod new] setCornerRadius:cell.imgOutlet andCornerRadius:5.0f];
                [cell.imgOutlet setContentMode:UIViewContentModeScaleAspectFit];
                
                [cell.imgOutlet sd_setImageWithURL:[NSURL URLWithString:location.imageString] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                }];
                
                if([[SDImageCache sharedImageCache] imageFromDiskCacheForKey:location.imageString]){
                    [cell.imgOutlet setImage:[[SDImageCache sharedImageCache] imageFromDiskCacheForKey:location.imageString]];
                    
                } else{
                    [cell.imgOutlet setImage:[UIImage imageNamed:@"bg_outlet"]];
                }
                
                //[cell.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:LABEL_COLOR andAlpha:1.0f]];
                //[cell.lblAddress setTextColor:[[SharedMethod new] colorWithHexString:LABEL_COLOR andAlpha:1.0f]];
                //[cell.lblDistance setTextColor:[[SharedMethod new] colorWithHexString:LABEL_COLOR andAlpha:1.0f]];
                
                [cell.lblTitle setText:location.name];
                [cell.lblAddress setText:location.address];
                
                float distance = [location.distance floatValue];
                if(distance>99){
                    [cell.lblDistance setText:[NSString stringWithFormat:@">99%@",NSLocalizedString(@"lbl_KM", nil)]];
                } else{
                    [cell.lblDistance setText:[NSString stringWithFormat:@"%0.2f%@",distance,NSLocalizedString(@"lbl_KM", nil)]];
                }
                
                
                if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
                    [cell.lblDistance setHidden:YES];
                }
            }
            return cell;
        }
    } else{
        BusinessHourCell *cell = (BusinessHourCell*)[tableView dequeueReusableCellWithIdentifier:@"businessHourCell"];
        
        BOOL closed = [[[businessHours objectAtIndex:indexPath.row] objectForKey:@"closed"] intValue];
        NSString *type = [[businessHours objectAtIndex:indexPath.row] objectForKey:@"type"];
        
        if([type isEqualToString:@"everyday"] || [type isEqualToString:@"weekday"] || [type isEqualToString:@"weekend"]){
            if(closed){
                [cell.lblDay setText:NSLocalizedString(@"lbl_Closed", nil)];
                [cell.lblBusinessHour setText:[NSString stringWithFormat:@": %@%@",[[type substringToIndex:1] uppercaseString],[type substringFromIndex:1]]];
            } else{
                [cell.lblDay setText:[[businessHours objectAtIndex:indexPath.row] objectForKey:@"type"]];
                [cell.lblDay setText:[NSString stringWithFormat:@"%@%@",[[cell.lblDay.text substringToIndex:1] uppercaseString],[cell.lblDay.text substringFromIndex:1]]];
                NSString *start = [self formatTime:[[businessHours objectAtIndex:indexPath.row] objectForKey:@"time_open"]];
//                NSString *end = [self formatTime:[[businessHours objectAtIndex:indexPath.row] objectForKey:@"time_close"]];
                [cell.lblBusinessHour setText:[NSString stringWithFormat:@": %@ - %@",start,NSLocalizedString(@"lbl_Till_Late", nil)]];
            }
        } else if([type isEqualToString:@"day"]){
            NSString *day = [[businessHours objectAtIndex:indexPath.row] objectForKey:@"day"];
            if([day isEqualToString:@"monday"]){
                day = NSLocalizedString(@"day_Monday", nil);
            } else if([day isEqualToString:@"tuesday"]){
                day = NSLocalizedString(@"day_Tuesday", nil);
            } else if([day isEqualToString:@"wednesday"]){
                day = NSLocalizedString(@"day_Wednesday", nil);
            } else if([day isEqualToString:@"thursday"]){
                day = NSLocalizedString(@"day_Thursday", nil);
            } else if([day isEqualToString:@"friday"]){
                day = NSLocalizedString(@"day_Friday", nil);
            } else if([day isEqualToString:@"saturday"]){
                day = NSLocalizedString(@"day_Saturday", nil);
            } else if([day isEqualToString:@"sunday"]){
                day = NSLocalizedString(@"day_Sunday", nil);
            }
            [cell.lblDay setText:day];
            
            if(closed){
                [cell.lblDay setText:NSLocalizedString(@"lbl_Closed", nil)];
                [cell.lblBusinessHour setText:[NSString stringWithFormat:@": %@",day]];
            } else{
                [cell.lblDay setText:[NSString stringWithFormat:@"%@%@",[[cell.lblDay.text substringToIndex:1] uppercaseString],[cell.lblDay.text substringFromIndex:1]]];
                
                NSString *start = [self formatTime:[[businessHours objectAtIndex:indexPath.row] objectForKey:@"time_open"]];
//                NSString *end = [self formatTime:[[businessHours objectAtIndex:indexPath.row] objectForKey:@"time_close"]];
                [cell.lblBusinessHour setText:[NSString stringWithFormat:@": %@ - %@",start,NSLocalizedString(@"lbl_Till_Late", nil)]];
            }
        } else if([type isEqualToString:@"date"] || [type isEqualToString:@"datetime"]){
            NSString *startDate, *endDate, *startTime, *endTime;
            startDate = [[businessHours objectAtIndex:indexPath.row] objectForKey:@"date"];
            endDate = [[businessHours objectAtIndex:indexPath.row] objectForKey:@"date_end"];
            startTime = [[businessHours objectAtIndex:indexPath.row] objectForKey:@"time_open"];
            endTime = [[businessHours objectAtIndex:indexPath.row] objectForKey:@"time_close"];
            
            if([startDate isEqual:[NSNull null]]){
                startDate = @"";
            } else{
                startDate = [self formatDate:startDate];
            }
            
            if([endDate isEqual:[NSNull null]]){
                endDate = @"";
            } else{
                endDate = [self formatDate:endDate];
            }
            
            if([startTime isEqual:[NSNull null]]){
                startTime = @"";
            } else{
                startTime = [self formatTime:startTime];
            }
            
            if([endTime isEqual:[NSNull null]]){
                endTime = @"";
            } else{
                endTime = [self formatTime:endTime];
            }
            
            if(closed){
                if(startDate.length>0 && endDate.length<=0){
                    if(startTime.length>0 && endTime.length>0){
                        //Close, Date_Start, Time_Open, Time_Close//
                        [cell.lblDay setText:[NSString stringWithFormat:@"%@\n%@",NSLocalizedString(@"lbl_Closed", nil),startDate]];
                        [cell.lblBusinessHour setText:[NSString stringWithFormat:@": %@ - %@",startTime, endTime]];
                    } else{
                        //Close, Date_Start//
                        [cell.lblDay setText:NSLocalizedString(@"lbl_Closed", nil)];
                        [cell.lblBusinessHour setText:[NSString stringWithFormat:@": %@",startDate]];
                    }
                } else if(startDate.length>0 && endDate.length>0){
                    if(startTime.length>0 && endTime.length>0){
                        //Close, Date_Start, Date_End, Time_Open, Time_Close//
                        [cell.lblDay setText:[NSString stringWithFormat:@"%@\n%@ - %@",NSLocalizedString(@"lbl_Closed", nil),startDate, endDate]];
                        [cell.lblBusinessHour setText:[NSString stringWithFormat:@": %@ - %@",startTime, endTime]];
                    } else{
                        //Close, Date_Start, Date_End//
                        [cell.lblDay setText:NSLocalizedString(@"lbl_Closed", nil)];
                        [cell.lblBusinessHour setText:[NSString stringWithFormat:@": %@ - %@",startDate, endDate]];
                    }
                }
            } else{
                if(startDate.length>0 && endDate.length<=0){
                    //Open, Date_Start, Time_Open, Time_End//
                    [cell.lblDay setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"lbl_Open", nil),startDate]];
                    [cell.lblBusinessHour setText:[NSString stringWithFormat:@": %@ - %@",startTime, endTime]];
                } else{
                    //Open, Date_Start, Date_End, Time_Open, Time_End//
                    [cell.lblDay setText:[NSString stringWithFormat:@"%@ %@ - %@",NSLocalizedString(@"lbl_Open", nil),startDate, endDate]];
                    [cell.lblBusinessHour setText:[NSString stringWithFormat:@": %@ - %@",startTime, endTime]];
                }
            }
        }
        [cell.lblDay sizeToFit];
        [cell.lblBusinessHour sizeToFit];
        return cell;
    }
}

#pragma mark - UITableView Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag==0){
        if(!isEmpty){
            [self.searchBar resignFirstResponder];
            
            NSLog(@"isSearch = %d",isSearch);
            
            isSearch=!isSearch;
            [self.searchBar setHidden:!isSearch];
            [self.tableView setHidden:!isSearch];
            [self setupNavigationBar];
            
            Outlet *loc;
            
            if(!isSearch){
                loc = [outlets objectAtIndex:indexPath.row];
            } else{
                loc = [defaultOutlets objectAtIndex:indexPath.row];
            }
            selectedOutlet = loc;
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:CLLocationCoordinate2DMake([loc.latitude floatValue], [loc.longitude floatValue]) zoom:15];
            [self.mapView setCamera:camera];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                for(GMSMarker *marker in myMarkers){
                    if([marker.title isEqualToString:loc.ID]){
                        [self.mapView setSelectedMarker:marker];
                    }
                }
            });
//            [self.contentView setHidden:NO];
            [self.contentView setHidden:YES];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag==0){
        if(isEmpty){
            return tableView.frame.size.height;
        } else{
            return 100;
        }
    }
    else{
        return UITableViewAutomaticDimension;
    }
}

#pragma mark - UISearchBar Delegate
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [self.tableView setHidden:NO];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    outlets = [[NSMutableArray alloc] init];
    
    if(searchText.length<=0){
        outlets = [defaultOutlets mutableCopy];
    } else{
        NSLog(@"default outlets %@",defaultOutlets);
        [outlets removeAllObjects];
        for(Outlet *temp in defaultOutlets){
            NSRange range = [temp.name rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if(range.location!=NSNotFound){
                NSLog(@"FOUND!");
                [outlets addObject:temp];
            } else{
                NSRange range2 = [temp.address rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if(range2.location!=NSNotFound){
                    NSLog(@"FOUND@");
                    [outlets addObject:temp];
                }
            }
        }
    }
    
    for(Outlet *x in outlets){
        NSLog(@"x - %@",x.name);
    }
    
    if([outlets count]<=0){
        isEmpty = YES;
        isTimeOut = NO;
    } else{
        isEmpty = NO;
        isTimeOut = NO;
    }
    
    [self.tableView reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

#pragma mark - Google Maps Delegate
-(UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker{
    //Open description//
    
    CustomInfoVC *infoWindow = [[CustomInfoVC alloc] initWithNibName:@"CustomInfoVC" bundle:nil];
    [infoWindow.view setFrame:CGRectMake(marker.map.bounds.origin.x-16, marker.map.bounds.origin.y-50, [UIScreen mainScreen].bounds.size.width-32, 60)];
    
//    Outlet *loc = [defaultOutlets objectAtIndex:[marker.title intValue]];
    Outlet *loc;
    
    for(Outlet *temp in defaultOutlets){
        if([temp.ID isEqualToString:marker.title]){
            selectedOutlet = temp;
            loc = temp;
            break;
        }
    }
    
//    selectedOutlet = loc;
    
    //    [[SharedMethod new] setCornerRadius:infoWindow.imgPicture andCornerRadius:10.0f];
    
    infoWindow.lblTitle.text = loc.name;
    infoWindow.lblSubtitle.text = loc.address;
    
    if([[SDImageCache sharedImageCache] imageFromDiskCacheForKey:loc.imageString]){
        [infoWindow.imgPicture setImage:[[SDImageCache sharedImageCache] imageFromDiskCacheForKey:loc.imageString]];
        [self.imgOutlet setImage:[[SDImageCache sharedImageCache] imageFromDiskCacheForKey:loc.imageString]];
    } else{
        [infoWindow.imgPicture setImage:[UIImage imageNamed:@"bg_outlet"]];
        [self.imgOutlet setImage:[UIImage imageNamed:@"bg_outlet"]];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SharedMethod new] setCornerRadius:infoWindow.imgPicture andCornerRadius:10.0f];
    });
    
    NSArray *tempBusiness = loc.hours;
    NSString *openHour, *closeHour, *day;
    BOOL isClose;
    
    if([businessHours count]>0){
        [businessHours removeAllObjects];
        [self.businessTableView reloadData];
    }
    
    [businessHours addObjectsFromArray:loc.hours];
    [self.businessTableView reloadData];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //This code will run in the main thread:
        CGRect frame = self.businessTableView.frame;
        frame.size.height = self.businessTableView.contentSize.height;
        self.businessTableView.frame = frame;
        [self.imgFax setFrame:CGRectMake(self.imgFax.frame.origin.x, self.businessTableView.frame.origin.y+self.businessTableView.frame.size.height+8, self.imgFax.frame.size.width, self.imgFax.frame.size.height)];
        [self.lblFax setFrame:CGRectMake(self.lblFax.frame.origin.x, self.imgFax.frame.origin.y, self.lblFax.frame.size.width, self.lblFax.frame.size.height)];
    });
    
    #ifdef DEBUG
    NSLog(@"tempBusiness - %@",loc.hours);
    #endif
    
    if([tempBusiness count]>0){
        for(NSDictionary *temp in tempBusiness){
            if([[temp objectForKey:@"type"] isEqualToString:@"everyday"]){
                openHour = [temp objectForKey:@"time_open"];
                closeHour = [temp objectForKey:@"time_close"];
                day = [temp objectForKey:@"type"];
                int close = [[temp objectForKey:@"closed"] boolValue];
                isClose = close;
                break;
            } else if([[temp objectForKey:@"type"] isEqualToString:@"weekday"]){
                if([[self getCurrentDay] isEqualToString:@"Monday"] || [[self getCurrentDay] isEqualToString:@"Tuesday"] || [[self getCurrentDay] isEqualToString:@"Wednesday"] || [[self getCurrentDay] isEqualToString:@"Thursday"] || [[self getCurrentDay] isEqualToString:@"Friday"]){
                    openHour = [temp objectForKey:@"time_open"];
                    closeHour = [temp objectForKey:@"time_close"];
                    day = [temp objectForKey:@"type"];
                    int close = [[temp objectForKey:@"closed"] boolValue];
                    isClose = close;
                    break;
                }
            } else if([[temp objectForKey:@"type"] isEqualToString:@"day"]){
                if([[temp objectForKey:@"day"] isEqualToString:[[self getCurrentDay] lowercaseString]]){
                    if([[temp objectForKey:@"time_open"] isEqual:[NSNull null]]){
                        openHour = @"";
                    } else{
                        openHour = [temp objectForKey:@"time_open"];
                    }
                    
                    if([[temp objectForKey:@"time_close"] isEqual:[NSNull null]]){
                        closeHour = @"";
                    } else{
                        closeHour = [temp objectForKey:@"time_close"];
                    }
                    day = [temp objectForKey:@"type"];
                    int close = [[temp objectForKey:@"closed"] boolValue];
                    isClose = close;
                    break;
                }
            } else if([[temp objectForKey:@"type"] isEqualToString:@"weekend"]){
                if([[self getCurrentDay] isEqualToString:@"Satuday"] || [[self getCurrentDay] isEqualToString:@"Sunday"]){
                    openHour = [temp objectForKey:@"time_open"];
                    closeHour = [temp objectForKey:@"time_close"];
                    day = [temp objectForKey:@"type"];
                    int close = [[temp objectForKey:@"closed"] boolValue];
                    isClose = close;
                    break;
                }
            }
        }
    }
    
    [self.lblName setText:loc.name];
    
    if(day.length<=0){
        [self.lblDate setText:@"-"];
    } else{
        [self.lblDate setText:day];
    }
    
    if(isClose){
        [self.lblBusinessHour setText:@"Closed"];
    } else{
        if(openHour.length<=0 || closeHour.length<=0){
            [self.lblBusinessHour setText:@"-"];
        } else{
            [self.lblBusinessHour setText:[NSString stringWithFormat:@"%@: %@ - %@",NSLocalizedString(@"lbl_Business_Hour", nil),[[self formatTime:openHour] lowercaseString],[[self formatTime:closeHour] lowercaseString]]];
        }
    }
    
    [self.lblPhone setText:loc.phone];
    [self.lblAddress setText:loc.address];
    [self.lblAddress sizeToFit];
    [self.lblEmail setText:loc.email];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.businessTableView setFrame:CGRectMake(self.businessTableView.frame.origin.x, self.lblAddress.frame.size.height+self.lblAddress.frame.origin.y+8, self.businessTableView.frame.size.width, self.lblAddress.frame.size.height)];
        [self.imgTimeIcon setFrame:CGRectMake(self.imgTimeIcon.frame.origin.x, self.businessTableView.frame.origin.y+2, self.imgTimeIcon.frame.size.width, self.imgTimeIcon.frame.size.height)];
    });
    
    if(loc.email.length<=0 || [loc.email containsString:@"null"]){
        [self.view makeToast:NSLocalizedString(@"msg_Email_Empty", nil)];
//        [self.btnEmail setHidden:YES];
//        [self.imgEmail setHidden:YES];
    } else{
        [self.btnEmail setHidden:NO];
        [self.imgEmail setHidden:NO];
    }
    
    [self.lblFax setText:loc.fax];
    
    if(self.lblFax.text.length<=0){
        [self.lblFax setHidden:YES];
        [self.imgFax setHidden:YES];
    } else{
        [self.lblFax setHidden:NO];
        [self.imgFax setHidden:NO];
    }
    //    [self.tableView reloadData];
    return infoWindow.view;
}

-(void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    //Open Content View//
    [[SharedMethod new] addStatistics:@"outlet_view" itemID:selectedOutlet.ID];
    [self.contentView setHidden:NO];
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.contentView setAlpha:1.0f];
        [self.contentView setFrame:originalRect];
    } completion:^(BOOL finished) {
        
    }];
    
    float negative;
    if([UIScreen mainScreen].bounds.size.height==480){
        negative=0.0003;
    } else{
        negative=0.0005;
    }
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:CLLocationCoordinate2DMake([selectedOutlet.latitude floatValue]-negative, [selectedOutlet.longitude floatValue]) zoom:18];
    [self.mapView setCamera:camera];
}

-(void)mapView:(GMSMapView *)mapView didCloseInfoWindowOfMarker:(GMSMarker *)marker{
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.contentView setAlpha:0.0f];
        [self.contentView setFrame:CGRectMake(originalRect.origin.x, originalRect.origin.y+originalRect.size.height, originalRect.size.width, originalRect.size.height)];
    } completion:^(BOOL finished) {
        [self.contentView setHidden:YES];
    }];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:CLLocationCoordinate2DMake([selectedOutlet.latitude floatValue], [selectedOutlet.longitude floatValue]) zoom:15];
    [self.mapView setCamera:camera];
}

- (void)displayRegionCenteredOnMapItem:(MKMapItem*)from {
    CLLocation* fromLocation = from.placemark.location;
    
    // Create a region centered on the starting point with a 10km span
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(fromLocation.coordinate, 10000, 10000);
    
    // Open the item in Maps, specifying the map region to display.
    [MKMapItem openMapsWithItems:[NSArray arrayWithObject:from]
                   launchOptions:[NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSValue valueWithMKCoordinate:region.center], MKLaunchOptionsMapCenterKey,
                                  [NSValue valueWithMKCoordinateSpan:region.span], MKLaunchOptionsMapSpanKey, nil]];
    
}

#pragma mark - Actions
- (IBAction)btnNavigateDidPressed:(id)sender {
    NSString *wazeAppURL = [NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes",[selectedOutlet.latitude floatValue],[selectedOutlet.longitude floatValue]];
    NSString *mapsAppURL = [NSString stringWithFormat:@"maps://?center=%f,%f",[selectedOutlet.latitude floatValue],[selectedOutlet.longitude floatValue]];
    NSString *googleAppURL = [NSString stringWithFormat:@"comgooglemapsurl://maps.google.com/?q=%f,%f",[selectedOutlet.latitude floatValue],[selectedOutlet.longitude floatValue]];
    
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Navigation", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:wazeAppURL]]){
        UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Waze", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:wazeAppURL]];
            [[SharedMethod new] addStatistics:@"outlet_navigate" itemID:selectedOutlet.ID];
        }];
        
        [alertController addAction:action];
    }
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:googleAppURL]]){
        UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Google_Map", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleAppURL]];
            [[SharedMethod new] addStatistics:@"outlet_navigate" itemID:selectedOutlet.ID];
        }];
        
        [alertController addAction:action];
    }
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:mapsAppURL]]){
        UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Maps", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[SharedMethod new] addStatistics:@"outlet_navigate" itemID:selectedOutlet.ID];
            MKPlacemark *placemark = [[MKPlacemark alloc]initWithCoordinate:CLLocationCoordinate2DMake([selectedOutlet.latitude floatValue], [selectedOutlet.longitude floatValue]) addressDictionary:nil];
            MKMapItem *mapItem = [[MKMapItem alloc]initWithPlacemark:placemark];
            [mapItem setName:selectedOutlet.name];
            [self displayRegionCenteredOnMapItem:mapItem];
        }];
        
        [alertController addAction:action];
    }
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:cancelAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)btnBusinessHourDidPressed:(id)sender {
//    businessHourVC = [storyboard instantiateViewControllerWithIdentifier:@"businessHourVC"];
//    businessHourVC.businessHours = selectedOutlet.hours;
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnDismissDidPressed:)];
//    [businessHourVC.view addGestureRecognizer:tapGesture];
//    [businessHourVC.view setTag:100];
//    [self.view.window addSubview:businessHourVC.view];
//    [self addChildViewController:businessHourVC];
}

- (IBAction)btnDismissDidPressed:(id)sender{
//    for(UIView *temp in self.view.window.subviews){
//        if(temp.tag == 100){
//            [temp removeFromSuperview];
//        }
//    }
//    [businessHourVC removeFromParentViewController];
}

- (IBAction)btnShareDidPressed:(id)sender {
    NSURL *myWebsite;
    NSArray *objectsToShare;
    
    myWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",[[NSUserDefaults standardUserDefaults] objectForKey:SHARE_BASE],OUTLET_SHARE,selectedOutlet.ID]];
    objectsToShare = @[myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:^{
        [[SharedMethod new] addStatistics:@"outlet_share" itemID:selectedOutlet.ID];
    }];
}

- (IBAction)btnEmailDidPressed:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
        mailComposer.mailComposeDelegate=self;
        [mailComposer setToRecipients:@[self.lblEmail.text]];
        [mailComposer setSubject:@""];
        
        [self presentViewController:mailComposer animated:YES completion:nil];
    } else{
        alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Send_Mail_Fail", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController removeFromParentViewController];
        }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)btnPhoneDidPressed:(id)sender {
    NSArray *separator = [selectedOutlet.phone componentsSeparatedByString:@"/"];
    
    if([separator count]>0){
        alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"ttl_Default", nil) preferredStyle:UIAlertControllerStyleActionSheet];
        
        for(NSString *temp in separator){
            UIAlertAction *action = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"msg_MakeCall", nil), temp] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                NSString *cleanedString = [[temp componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
                NSURL *contactURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",cleanedString]];
                if([[UIApplication sharedApplication] canOpenURL:contactURL]){
                    [[SharedMethod new] addStatistics:@"outlet_phone" itemID:temp];
                    [[UIApplication sharedApplication] openURL:contactURL];
                } else{
                    NSLog(@"Not able to make call on simulator.");
                }
            }];
            [alertController addAction:action];
        }
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    } else{
        NSString *tel = selectedOutlet.phone;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
            alertController = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"msg_MakeCall", nil), tel] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleDefault handler:nil];
            
            UIAlertAction *callAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Call", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                NSString *cleanedString = [[tel componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
                NSURL *contactURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",cleanedString]];
                if([[UIApplication sharedApplication] canOpenURL:contactURL]){
                    [[SharedMethod new] addStatistics:@"outlet_phone" itemID:selectedOutlet.phone];
                    [[UIApplication sharedApplication] openURL:contactURL];
                } else{
                    NSLog(@"Not able to make call on simulator.");
                }
            }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:callAction];
        } else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"err_Call_Error", nil) preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:nil];
            
            [alertController addAction:okAction];
        }
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)btnFaxDidPressed:(id)sender {
    
}

- (IBAction)searchAction:(id)sender {
    //    NSLog(@"search trigger");
    //    if(self.tableView.hidden){
    //        isSearch = NO;
    //        [self.tableView setHidden:NO];
    //    } else{
    //        isSearch = YES;
    //        [self.tableView setHidden:YES];
    //    }
    
    [self.searchBar resignFirstResponder];
    self.searchBar.text = @"";
    
    //    NSLog(@"default outlets - %@",defaultOutlets);
    
    
    //    [outlets removeAllObjects];
    //    [outlets addObjectsFromArray:defaultOutlets];
    
    //    //DOUBLE CHECK HERE!!!!//
    //    NSMutableArray *temp = [[NSMutableArray alloc] init];
    //    temp = [[SQLManager new] retrieveOutlets];
    //
    //    for(Outlet *tempOutlet in temp){
    //        [self calculateDistance:tempOutlet];
    //    }
    //
    //
    //    for(Outlet *o in temp){
    //        NSLog(@"o.name - %@",o.name);
    //        NSLog(@"o.imageString - %@",o.imageString);
    //    }
    
    if([outlets count]>0){
        [outlets removeAllObjects];
    }
    
    [outlets addObjectsFromArray:defaultOutlets];
    
    //    if([defaultOutlets count]>0){
    //        [defaultOutlets removeAllObjects];
    //    }
    
    //    [outlets addObjectsFromArray:temp];
    //    [defaultOutlets addObjectsFromArray:temp];

    isSearch=!isSearch;
    
    [self.searchBar setHidden:!isSearch];
    [self.tableView setHidden:!isSearch];
    
    isEmpty = NO;
    isTimeOut = NO;
    [self.tableView reloadData];
    [self setupNavigationBar];
    //     */
}

#pragma mark - MFMailComposeViewController Delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    switch (result) {
        case MFMailComposeResultCancelled:
            [self.view makeToast:NSLocalizedString(@"msg_Mail_Cancel", nil) duration:1.0f position:CSToastPositionCenter];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved.");
            break;
        case MFMailComposeResultSent:
            [[SharedMethod new] addStatistics:@"outlet_email" itemID:selectedOutlet.ID];
            [self.view makeToast:NSLocalizedString(@"msg_Mail_Sent", nil) duration:1.0f position:CSToastPositionCenter];
            break;
        case MFMailComposeResultFailed:
            [self.view makeToast:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"msg_Mail_Fail", nil),[error localizedDescription]] duration:1.0f position:CSToastPositionCenter];
            break;
        default:
            break;
    }
    [self.mapView setSelectedMarker:nil];
    [controller dismissViewControllerAnimated:YES completion:nil];
}


@end
