#import <UIKit/UIKit.h>
#import "Promotion.h"

@interface FlashDetailVC : UIViewController
@property (nonatomic) Promotion *selectedPromotion;
@property (nonatomic) UIImage *bannerImage;
@property (nonatomic) BOOL isPromo;
@end
