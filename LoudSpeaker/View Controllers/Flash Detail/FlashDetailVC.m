#import "FlashDetailVC.h"
#import "Constant.h"
#import "WebAPI.h"
#import "SharedMethod.h"
#import "SVProgressHUD.h"
#import "WebVC.h"
#import "FlashBannerCell.h"
#import "FlashDetailCell.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "GalleryVC.h"
#import <MessageUI/MessageUI.h>
#import <PDKeychainBindings.h>

@interface FlashDetailVC ()<UITableViewDelegate,UITableViewDataSource, UIWebViewDelegate, MFMessageComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation FlashDetailVC{
    UIStoryboard *storyboard;
    UIAlertController *alertController;
    NSMutableArray *promotions;
    Promotion *promotion;
    int cellSize;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupInitialization];
    [self setupDelegates];
    [self setupData];
    //Specific promotion//
    //    [self retrievePostingDetail];
}

#pragma mark Setup Initialization
-(void)setupInitialization{
    promotion = [[Promotion alloc] init];
    promotions = [[NSMutableArray alloc] init];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    [self.tableView setEstimatedRowHeight:100];
    [self.tableView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.view setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

-(void)setupDelegates{
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
}

-(void)setupData{
    if(self.isPromo){
        [self.navigationItem setTitle:NSLocalizedString(@"nav_Promotion_Detail_V2", nil)];
    } else{
        [self.navigationItem setTitle:NSLocalizedString(@"nav_Event_Detail_V2", nil)];
    }
    
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

-(void)retrievePostingDetail{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        
        NSString *route = [NSString stringWithFormat:@"user/post/%@",self.selectedPromotion.ID];
        
        NSDictionary *params = @{@"posting_id"       : self.selectedPromotion.ID,
                                 @"userId"          : [[NSUserDefaults standardUserDefaults] objectForKey:USER_ID],
                                 @"platform"         : PLATFORM,
                                 @"push_token"       : [[NSUserDefaults standardUserDefaults] objectForKey: DEVICE_TOKEN],
                                 @"token"            : [SharedMethod getTokenWithRoute:route apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
#ifdef DEBUG
        NSLog(@"Prepare Retrieve Posting Detail Params - %@",params);
#endif
        
        [WebAPI retrievePostingDetail:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    NSMutableArray *temp = [[NSMutableArray alloc] init];
                    [temp addObject:[result objectForKey:@"post"]];
                    promotions = [[SharedMethod new] processPosting:temp andCurrentArray:promotions];
                    
                    [self.tableView reloadData];
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"Message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
        
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)bbtnShareDidPressed:(id)sender {
    NSURL *myWebsite;
    NSArray *objectsToShare;
    
    if(self.selectedPromotion.url.length>0){
        myWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"%@",self.selectedPromotion.url]];
    } else{
        myWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"%@p/%@",[[NSUserDefaults standardUserDefaults] objectForKey:SHARE_BASE],self.selectedPromotion.ID]];
    }
    objectsToShare = @[myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:^{
        [[SharedMethod new] addStatistics:@"post_share" itemID:self.selectedPromotion.ID];
    }];
}

- (IBAction)btnGalleryDidPressed:(id)sender{
    GalleryVC *galleryVC = [storyboard instantiateViewControllerWithIdentifier:@"galleryVC"];
    galleryVC.image = self.selectedPromotion.cachedImage;
    [galleryVC.view setTag:100];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view.window addSubview:galleryVC.view];
        [self addChildViewController:galleryVC];
        [galleryVC didMoveToParentViewController:self];
    });
}

- (IBAction)btnYoutubeDidPressed:(id)sender{
    NSString *url = self.selectedPromotion.url;
    url = [url stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]]){
        url = [url stringByReplacingOccurrencesOfString:@"http://" withString:@"youtube://"];
        url = [url stringByReplacingOccurrencesOfString:@"https://" withString:@"youtube://"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    } else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}

- (IBAction)btnNavigationDidPressed:(id)sender {
    
}

#pragma mark - UIWebView Delegate
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss];
    
    if(webView.tag==100){
        CGRect frame = webView.frame;
        frame.size.height = 1;
        webView.frame = frame;
        CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
        frame.size = fittingSize;
        webView.frame = frame;
        
        cellSize = fittingSize.height+8;
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
    }
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        NSURL *url = request.URL;
        
        if([url.absoluteString containsString:@"www"] || [url.absoluteString containsString:@"http"]){
            if([[UIApplication sharedApplication] canOpenURL:url]){
                WebVC *webVC = [storyboard instantiateViewControllerWithIdentifier:@"webVC"];
                webVC.selectedURL = url;
                [self.navigationController pushViewController:webVC animated:YES];
            }
        } else if([url.absoluteString hasPrefix:@"tel:"]){
            if([[UIApplication sharedApplication] canOpenURL:url]){
                NSString *tel = [[url.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                alertController = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"%@ %@?",NSLocalizedString(@"msg_MakeCall", nil),[tel substringFromIndex:4]] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Call", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [[UIApplication sharedApplication] openURL:url];
                }];
                
                UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_No", nil) style:UIAlertActionStyleDefault handler:nil];
                
                [alertController addAction:noAction];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }else if([[url scheme] isEqual:@"mailto"]){
            if ([MFMailComposeViewController canSendMail]) {
                [[UIApplication sharedApplication] openURL:url];
            } else{
                alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Send_Mail_Fail", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [alertController removeFromParentViewController];
                }];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        return NO;
    }
    return YES;
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3; // Banner + Label + WebView
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        FlashBannerCell *cell = (FlashBannerCell*)[tableView dequeueReusableCellWithIdentifier:@"flashBannerCell"];
        
        if(self.selectedPromotion.image.length>0){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [cell.imgBanner setContentMode:UIViewContentModeScaleAspectFit];
                    [cell.imgBanner setClipsToBounds:YES];
                    [cell.imgBanner sd_setImageWithURL:[NSURL URLWithString:self.selectedPromotion.image] placeholderImage:[UIImage imageNamed:@"bg_promotion"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        self.selectedPromotion.cachedImage = image;
                        for(UIView *view in cell.imgBanner.subviews){
                            if(view.tag==100){
                                [view removeFromSuperview];
                            }
                        }
                    }];
                });
            });
        } else{
            [cell.imgBanner setImage:[UIImage imageNamed:@"bg_promotion"]];
        }
        
        UIButton *btn = [[UIButton alloc] initWithFrame:cell.imgBanner.frame];
        [btn setTitle:@"" forState:UIControlStateNormal];
        
        if(self.selectedPromotion.url.length<=0 && (![self.selectedPromotion.url containsString:@"youtube"] || ![self.selectedPromotion.url containsString:@"youtu.be"])){
            [cell.imgPlay setHidden:YES];
            [btn addTarget:self action:@selector(btnGalleryDidPressed:) forControlEvents:UIControlEventTouchUpInside];
        } else if(self.selectedPromotion.url>0 && ([self.selectedPromotion.url containsString:@"youtube"] || [self.selectedPromotion.url containsString:@"youtu.be"])){
            [cell.imgPlay setHidden:NO];
            [btn addTarget:self action:@selector(btnYoutubeDidPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
        [cell addSubview:btn];
        [cell bringSubviewToFront:btn];
        
        return cell;
    } else if(indexPath.row==1){
        FlashDetailCell *cell = (FlashDetailCell*)[tableView dequeueReusableCellWithIdentifier:@"flashDetailCell"];
        [cell.lblTitle setHidden:NO];
        [cell.lblSubtitle setHidden:NO];
        [cell.webView setHidden:YES];
        [cell.lblTitle setText:self.selectedPromotion.title];
        [cell.lblSubtitle setText:self.selectedPromotion.subtitle];
        [cell.separatorView setHidden:NO];
        [[cell.separatorView layer] setCornerRadius:2.0f];
        [cell.separatorView setBackgroundColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        return cell;
    } else if(indexPath.row==2){
        FlashDetailCell *cell = (FlashDetailCell*)[tableView dequeueReusableCellWithIdentifier:@"flashDetailCell"];
        [cell.lblTitle setHidden:YES];
        [cell.lblSubtitle setHidden:YES];
        [cell.webView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
        [cell.webView setHidden:NO];
        [cell.webView setTag:100];
        [cell.webView setDelegate:self];
        [cell.webView.scrollView setScrollEnabled:NO];
        
        //        NSString *finalString = [NSString stringWithFormat:@"<html><head><meta name='viewport' http-equiv='Content-Type' content='text/html;charset=UTF-8;width=device-width,initial-scale=0.85'></head><body><font face='%@' color=#474747>%@</body></html>", FONT_REGULAR, self.selectedPromotion.content];
        //
        //        [cell.webView loadHTMLString:finalString baseURL:nil];
        
        NSString *myBase = [NSString stringWithFormat:@"<html><head><script>function nav(url){document.location.href = url;}</script><meta content=\"html; charset=utf-8\" http-equiv=\"Content-Type\"/><meta name=\"viewport\" content=\"width=device-width\"/><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/></head><body><font face='%@' color=#474747>%@</body></html>", FONT_REGULAR, self.selectedPromotion.content];
        
        //                NSLog(@"mybase - %@",myBase);
        [cell.webView loadHTMLString:myBase baseURL:[[NSBundle mainBundle] URLForResource:@"style" withExtension:@"css"]];
        
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    NSLog(@"Selected row %d",(int)indexPath.row);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        return [UIScreen mainScreen].bounds.size.width/2;
    } else if(indexPath.row==1){
        return 40;
        //        return UITableViewAutomaticDimension;
    } else if(indexPath.row==2){
        if(cellSize>44){
            return cellSize;
        } else{
            return 44;
        }
    } else{
        return UITableViewAutomaticDimension;
    }
}


@end
