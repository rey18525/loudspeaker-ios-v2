//
//  SearchTableVC.h
//  Oasis
//
//  Created by Wong Ryan on 05/12/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableVC : UITableViewController

@property (nonatomic) NSString *searchText;
@property (nonatomic) NSMutableArray *promotionsArray;
@property (nonatomic) NSMutableArray *campaignsArray;

@property (nonatomic) BOOL isPromotion;
@property (nonatomic) BOOL isCampaign;

-(void)searchPromotion;
-(void)searchCampaign;  

@end
