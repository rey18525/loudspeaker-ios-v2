//
//  SearchTableVC.m
//  Oasis
//
//  Created by Wong Ryan on 05/12/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "SearchTableVC.h"
#import "SharedMethod.h"
#import "SVProgressHUD.h"
#import "Constant.h"
#import "WebAPI.h"
#import "SQLManager.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "PromotionCell.h"
#import "PromotionDetailVC.h"
#import "VoucherTableCell.h"
#import "VoucherDetailVC.h"
#import <PDKeychainBindings.h>

@interface SearchTableVC ()

@end

@implementation SearchTableVC{
    UIStoryboard *storyboard;
    int paging, totalCount;
    BOOL isEmpty;
}
@synthesize promotionsArray,campaignsArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupInitialization];
    [self setupData];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if([promotionsArray count]>0){
        [promotionsArray removeAllObjects];
    }
    
    [self.tableView reloadData];
}

#pragma mark - Custom Functions
-(void)setupInitialization{
    promotionsArray = [[NSMutableArray alloc] init];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

-(void)setupData{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    paging = 1;
    totalCount = 0;
    isEmpty = NO;
}

-(void)searchPromotion{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:PLATFORM forKey:@"platform"];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN] forKey:@"push_token"];
        [params setObject:[SharedMethod getTokenWithRoute:@"user/posts/promo" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        [params setObject:POST_TYPE_1 forKey:@"type"];
        [params setObject:PROMOTION_LOAD forKey:@"limit"];
        [params setObject:[NSString stringWithFormat:@"%d",paging] forKey:@"page"];
        [params setObject:@"order_by[0][0]=priority&order_by[0][1]=asc&order_by[1][0]=publish_at&order_by[1][1]=desc" forKey:@"order_by"];
        [params setObject:[self.searchText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:@"q"];
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        }
        
#ifdef DEBUG
        NSLog(@"Search Posting Params - %@",params);
#endif
        
        [WebAPI retrievePosting:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                promotionsArray = [[SQLManager new] retrievePromotions:YES];
                [self.tableView reloadData];
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.tabBarController setSelectedIndex:0];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            } else{
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    totalCount = [[result objectForKey:@"total_items"] intValue];
                    promotionsArray = [[SharedMethod new] processPosting:[result objectForKey:@"posts"] andCurrentArray:promotionsArray];
                    
                    if([promotionsArray count]<=0){
                        isEmpty = YES;
                    } else{
                        isEmpty = NO;
                    }
                    
                    [self.tableView reloadData];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        promotionsArray = [[SQLManager new] retrievePromotions:YES];
        [self.tableView reloadData];
    }
}

-(void)searchCampaign{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        
        [params setObject:PLATFORM forKey:@"platform"];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN] forKey:@"push_token"];
        [params setObject:[SharedMethod getTokenWithRoute:@"user/campaigns" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        [params setObject:PROMOTION_LOAD forKey:@"limit"];
        [params setObject:[NSString stringWithFormat:@"%d",paging] forKey:@"page"];
        [params setObject:@[@"start",@"desc"] forKey:@"order_by"];
        [params setObject:self.searchText forKey:@"q"];
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        }
        
        #ifdef DEBUG
        NSLog(@"Search Campaigns - %@",params);
        #endif
        
        [WebAPI retrieveCampaign:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.tabBarController setSelectedIndex:0];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            } else{
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    totalCount = [[result objectForKey:@"total_items"] intValue];
                    NSMutableArray *tempCampaign = [[NSMutableArray alloc] init];
                    for(int count=0;count<[[result objectForKey:@"campaigns"] count];count++){
                        [tempCampaign addObject:[[result objectForKey:@"campaigns"] objectAtIndex:count]];
                    }
                    
                    self.campaignsArray = [[SharedMethod new] processCampaign:tempCampaign andCurrentArray:self.campaignsArray];
                    
                    if([campaignsArray count]<=0){
                        isEmpty = YES;
                    } else{
                        isEmpty = NO;
                    }
                    
                    [self.tableView reloadData];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}


#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.isPromotion){
        if(isEmpty){
            return 1;
        } else{
            return [promotionsArray count];
        }
    } else if(self.isCampaign){
        if(isEmpty){
            return 1;
        } else{
            return [campaignsArray count];
        }
    } else{
        return 0;
    }
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isEmpty){
        if(self.isPromotion){
            PromotionCell *cell = (PromotionCell*)[tableView dequeueReusableCellWithIdentifier:@"promotionCell"];
            if(cell==nil){
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PromotionCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }

            UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, self.tableView.frame.size.width-16, self.tableView.frame.size.height)];
            [lblTitle setText:NSLocalizedString(@"lbl_Empty", nil)];
            
            [lblTitle setTextColor:[[SharedMethod new] colorWithHexString:BLACK_COLOR andAlpha:1.0f]];
            [lblTitle setTextAlignment:NSTextAlignmentCenter];
            
            [cell addSubview:lblTitle];
            
            [cell.contentsView setHidden:YES];
            [cell.lblMore setHidden:YES];
            [cell.imgMore setHidden:YES];
            [cell.lblTitle setHidden:YES];
            [cell.lblDescription setHidden:YES];
            [cell.imgView setHidden:YES];
            
            return cell;
        } else if(self.isCampaign){
            VoucherTableCell *cell = (VoucherTableCell*)[tableView dequeueReusableCellWithIdentifier:@"voucherCell"];
            if(cell==nil){
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VoucherTableCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, self.tableView.frame.size.width-16, self.tableView.frame.size.height)];
            [lblTitle setText:NSLocalizedString(@"lbl_Empty", nil)];
            
            [lblTitle setTextColor:[[SharedMethod new] colorWithHexString:BLACK_COLOR andAlpha:1.0f]];
            [lblTitle setTextAlignment:NSTextAlignmentCenter];
            
            [cell addSubview:lblTitle];
            
            [cell.lblExpire setHidden:YES];
            [cell.lblExpireDate setHidden:YES];
            [cell.lblDescription setHidden:YES];
            [cell.imgUsed setHidden:YES];
            [cell.imgVoucher setHidden:YES];
            
            return cell;
        }else{
            return nil;
        }
    } else{
        if(self.isPromotion){
            PromotionCell *cell = (PromotionCell*)[tableView dequeueReusableCellWithIdentifier:@"promotionCell"];
            if(cell==nil){
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PromotionCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            Promotion *promotion = [promotionsArray objectAtIndex:indexPath.row];
            
            [cell.contentsView setHidden:YES];
            [cell.lblMore setHidden:YES];
            [cell.imgMore setHidden:YES];
            
            if(promotion.image.length>0){
                //        [[SharedMethod new] setLoadingIndicatorOnImage:cell.imgView];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [cell.imgView setContentMode:UIViewContentModeScaleAspectFit];
                        [cell.imgView setClipsToBounds:YES];
                        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:promotion.image] placeholderImage:[UIImage imageNamed:@"bg_promotion"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            promotion.cachedImage = image;
                            for(UIView *view in cell.imgView.subviews){
                                if(view.tag==100){
                                    [view removeFromSuperview];
                                }
                            }
                        }];
                    });
                });
            } else{
                [cell.imgView setImage:[UIImage imageNamed:@"bg_promotion"]];
            }
            
            [cell.lblTitle setText:promotion.title];
            [cell.lblDescription setText:promotion.subtitle];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedMethod new] setGradient:cell.imgView];
            });
            
            if(indexPath.row == [promotionsArray count] - 1 && [promotionsArray count] < totalCount){
                if([[SharedMethod new] checkNetworkConnectivity]){
                    paging+=1;
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self searchPromotion];
                        });
                    });
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"err_Error", nil) message:NSLocalizedString(@"msg_No_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
            return cell;
        } else if(self.isCampaign){
            VoucherTableCell *cell = (VoucherTableCell*)[tableView dequeueReusableCellWithIdentifier:@"voucherCell"];
            if(cell==nil){
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VoucherTableCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            Campaign *campaign = [campaignsArray objectAtIndex:indexPath.row];
            Voucher *voucher = campaign.voucher;
            
            [cell.imgUsed setHidden:YES];
            [cell.expireView setHidden:YES];
            
            if(voucher.image.length>0){
                //        [[SharedMethod new] setLoadingIndicatorOnImage:cell.imgVoucher];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [cell.imgVoucher setContentMode:UIViewContentModeScaleAspectFit];
                        [cell.imgVoucher setClipsToBounds:YES];
                        [cell.imgVoucher sd_setImageWithURL:[NSURL URLWithString:voucher.image] placeholderImage:[UIImage imageNamed:@"bg_voucher"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            voucher.cachedImage = image;
                            //                    for(UIView *view in cell.imgVoucher.subviews){
                            //                        if(view.tag==100){
                            //                            [view removeFromSuperview];
                            //                        }
                            //                    }
                        }];
                    });
                });
            } else{
                [cell.imgVoucher setImage:[UIImage imageNamed:@"bg_voucher"]];
            }
            
            [cell.lblDescription setText:voucher.title];
            
            if(indexPath.row == [campaignsArray count] - 1 && [campaignsArray count] < totalCount){
                if([[SharedMethod new] checkNetworkConnectivity]){
                    paging+=1;
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self searchCampaign];
                        });
                    });
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"err_Error", nil) message:NSLocalizedString(@"msg_No_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
            return cell;
        } else{
            return nil;
        }
    }
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!isEmpty){
        if(self.isPromotion){
            PromotionCell *cell = (PromotionCell*)[tableView dequeueReusableCellWithIdentifier:@"promotionCell"];
            cell.imgView.layer.sublayers = nil;
            cell.contentsView.layer.sublayers = nil;
        } else if(self.isCampaign){
            VoucherTableCell *myCell = (VoucherTableCell*)[tableView dequeueReusableCellWithIdentifier:@"voucherCell"];
            myCell.imgVoucher.layer.sublayers = nil;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!isEmpty){
        if(self.isPromotion){
            Promotion *selectedPromotion = [promotionsArray objectAtIndex:indexPath.row];
            [[SharedMethod new] addStatistics:@"post_view" itemID:selectedPromotion.ID];
            
            PromotionDetailVC *promotionDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"promotionDetailVC"];
            promotionDetailVC.selectedPromotion = selectedPromotion;
            [self.navigationController pushViewController:promotionDetailVC animated:YES];
        } else if(self.isCampaign){
            Campaign *campaign = [campaignsArray objectAtIndex:indexPath.row];
            Voucher *selectedVoucher = campaign.voucher;
            [[SharedMethod new] addStatistics:@"post_view" itemID:selectedVoucher.ID];
            
            VoucherDetailVC *voucherDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"voucherDetailVC"];
            voucherDetailVC.isMyVoucher = NO;
            voucherDetailVC.selectedCampaign = campaign;
            voucherDetailVC.selectedVoucher = selectedVoucher;
            [self.navigationController pushViewController:voucherDetailVC animated:YES];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isEmpty){
        return self.tableView.frame.size.height;
    } else{
        if(self.isPromotion){
            return ([UIScreen mainScreen].bounds.size.width/3)+80;
        } else if(self.isCampaign){
            return ([UIScreen mainScreen].bounds.size.width/2)+44;//+60;
        } else{
            return UITableViewAutomaticDimension;
        }
    }
}

@end
