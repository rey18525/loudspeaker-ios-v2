//
//  WebVC.h
//  Oasis
//
//  Created by Wong Ryan on 29/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebVC : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic) NSString *imgUrlString;
@property (nonatomic) NSString *urlString;
@property (nonatomic) NSURL *selectedURL;
@property (nonatomic) NSString *normalContent;
@property (nonatomic) BOOL fromMenu;
@property (nonatomic) BOOL fromSignUp;
@property (nonatomic) BOOL isScaleToFit;
@property (nonatomic) NSString *navigationTitle;

@end
