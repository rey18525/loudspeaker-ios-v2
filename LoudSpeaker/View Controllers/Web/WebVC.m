//
//  WebVC.m
//  Oasis
//
//  Created by Wong Ryan on 29/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "WebVC.h"
#import "SVProgressHUD.h"
#import "SharedMethod.h"
#import "Constant.h"
#import <MessageUI/MessageUI.h>
#import <UIView+Toast.h>

@interface WebVC ()<UIWebViewDelegate,MFMessageComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbtnBrowser;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bbtnBack;
@end

@implementation WebVC{
    UIAlertController *alertController;
    UIStoryboard *storyboard;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setupNavigationBar];
    [self setupData];
    [self setupWebView];
}

#pragma mark - Custom Functions
-(void)setupNavigationBar{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationItem setTitle:self.navigationTitle];
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [self setHidesBottomBarWhenPushed:YES];
}

-(void)setupUI{
    [self.view setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

-(void)setupData{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

-(void)setupWebView{
    [self.webView setBackgroundColor:[UIColor whiteColor]];
    [self.webView setOpaque:NO];
    [self.webView setDelegate:self];
    if(self.imgUrlString.length<=0){
//        NSLog(@"WebView URL - %@",self.urlString);
//        NSURL *URL = [NSURL URLWithString:self.urlString];
        NSURLRequest *requestObj;
        
        if(self.selectedURL.absoluteString.length>0){
//            NSLog(@"Using selectedURL - %@",self.selectedURL.absoluteString);
            requestObj = [NSURLRequest requestWithURL:self.selectedURL];
            [self.webView loadRequest:requestObj];
        } else if(self.imgUrlString.length>0){
//            NSLog(@"Using imgUrlString - %@",self.imgUrlString);
            requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:self.imgUrlString]];
            [self.webView loadRequest:requestObj];
        } else if(self.urlString.length>0){
            requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]];
            [self.webView loadRequest:requestObj];
        } else if(self.normalContent.length>0){
            NSString *finalString = [NSString stringWithFormat:@"<html><head><meta name='viewport' http-equiv='Content-Type' content='text/html;charset=UTF-8;width=device-width,initial-scale=0.85'></head><body style=' color:#000000'>%@</body></html>", self.normalContent];
            
            [self.webView loadHTMLString:finalString baseURL:nil];
        }
    } else{
        [self.webView loadHTMLString:self.imgUrlString baseURL:nil];
    }
    [self.webView setScalesPageToFit:self.isScaleToFit];
}

#pragma mark - Actions
-(IBAction)bbtnBackDidPressed:(id)sender{
    if(self.fromSignUp){
        [self dismissViewControllerAnimated:YES completion:nil];
    } else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)bbtnBrowserDidPressed:(id)sender {
    NSString *URL;
    
    if(self.selectedURL.absoluteString.length>0){
        URL = self.selectedURL.absoluteString;
    } else if(self.urlString.length>0){
        URL = self.urlString;
    }
    
    if(self.selectedURL.absoluteString.length>0 || self.urlString.length>0){
        alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Confirmation", nil) message:[NSString stringWithFormat:@"%@ %@ %@",NSLocalizedString(@"msg_Open", nil), URL, NSLocalizedString(@"msg_Open_Web", nil)] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:self.selectedURL];
        }];
        
        UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_No", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [alertController addAction:noAction];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else{
        alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"App_Name", nil) message:NSLocalizedString(@"err_Open_Web", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //Do nothing//
        }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

#pragma mark - WebView Delegate
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss];
    
    self.navigationTitle = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    
    if(self.navigationTitle.length<=0){
        self.navigationTitle = NSLocalizedString(@"App_Name", nil);
    }
    [self setupNavigationBar];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    //    NSLog(@"url is %@",request.URL.absoluteString);
    if (navigationType == UIWebViewNavigationTypeLinkClicked){
        NSURL *url = request.URL;
        
        if([url.absoluteString containsString:@"www"] || [url.absoluteString containsString:@"http"]){
            if([[UIApplication sharedApplication] canOpenURL:url]){
                WebVC *webVC = [storyboard instantiateViewControllerWithIdentifier:@"webVC"];
                webVC.selectedURL = url;
                [self.navigationController pushViewController:webVC animated:YES];
            }
        } else if([url.absoluteString hasPrefix:@"tel:"]){
            if([[UIApplication sharedApplication] canOpenURL:url]){
                NSString *tel = [[url.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                alertController = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"%@ %@?",NSLocalizedString(@"msg_MakeCall", nil),[tel substringFromIndex:4]] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Call", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [[UIApplication sharedApplication] openURL:url];
                }];
                
                UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_No", nil) style:UIAlertActionStyleDefault handler:nil];
                
                [alertController addAction:noAction];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        } else if([[url scheme] isEqual:@"mailto"]){
            if ([MFMailComposeViewController canSendMail]) {
                [[UIApplication sharedApplication] openURL:url];
            } else{
                alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Send_Mail_Fail", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [alertController removeFromParentViewController];
                }];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
        
        return NO;
    }
    return YES;
}

#pragma mark - MFMailComposeViewController Delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    switch (result) {
        case MFMailComposeResultCancelled:
            [self.view makeToast:NSLocalizedString(@"msg_Mail_Cancel", nil) duration:1.0f position:CSToastPositionCenter];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved.");
            break;
        case MFMailComposeResultSent:
//            [[SharedMethod new] addStatistics:@"outlet_email" itemID:selectedOutlet.ID];
            [self.view makeToast:NSLocalizedString(@"msg_Mail_Sent", nil) duration:1.0f position:CSToastPositionCenter];
            break;
        case MFMailComposeResultFailed:
            [self.view makeToast:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"msg_Mail_Fail", nil),[error localizedDescription]] duration:1.0f position:CSToastPositionCenter];
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
