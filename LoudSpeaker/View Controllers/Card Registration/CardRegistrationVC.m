//
//  CardRegistrationVC.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 07/12/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "CardRegistrationVC.h"
#import "Constant.h"
#import "SharedMethod.h"
#import "WebAPI.h"
#import "TextFieldValidator.h"
#import "SVProgressHUD.h"
#import "SignUpVC.h"
#import "PaymentGatewayVC.h"
#import "CardlessRegistrationVC.h"
#import <PDKeychainBindings.h>

@interface CardRegistrationVC ()<UITextFieldDelegate,DismissLoginDelegate,DismissPaymentGatewayDelegate,DismissCardlessLoginDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblCard;
@property (weak, nonatomic) IBOutlet UILabel *lblCardSub;
@property (weak, nonatomic) IBOutlet UILabel *lblCardGuide;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtCard;
@property (weak, nonatomic) IBOutlet UILabel *lblCvv;
@property (weak, nonatomic) IBOutlet UILabel *lblCvvSub;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtCvv;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnCard;
@property (weak, nonatomic) IBOutlet UIButton *btnCardless;
@property (weak, nonatomic) IBOutlet UIButton *btnGuest;
@property (weak, nonatomic) IBOutlet UIView *indicatorView;
@property (weak, nonatomic) IBOutlet UIView *cardView;
@property (weak, nonatomic) IBOutlet UIView *cardlessView;
@property (weak, nonatomic) IBOutlet UIView *guestView;
@property (weak, nonatomic) IBOutlet UIView *cardContent;
@property (weak, nonatomic) IBOutlet UIView *cardlessContent;
@property (weak, nonatomic) IBOutlet UIScrollView *guestContent;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblFullname;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtFullname;
@property (weak, nonatomic) IBOutlet UILabel *lblContact;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtContact;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UILabel *lblCardlessGuide;
@property (weak, nonatomic) IBOutlet UILabel *lblPoweredBy;

@property (weak, nonatomic) IBOutlet UIView *scrollContent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *indicatorView_leading_cardView;

@property (weak, nonatomic) IBOutlet TextFieldValidator *txtGuestName;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtGuestIC;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtGuestDOB;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtGuestPhone;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtGuestEmail;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtGuestPassword;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtGuestConfirmPassword;
@property (weak, nonatomic) IBOutlet UISwitch *switchPDPA;
@property (weak, nonatomic) IBOutlet UISwitch *switchConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnGuestSubmit;


@end

@implementation CardRegistrationVC{
    UIStoryboard *storyboard;
    UISwipeGestureRecognizer *leftSwipe, *rightSwipe;
    UIDatePicker *datePicker;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInitialization];
    [self setupSwipeGesture];
    [self setupUI];
    [self setupData];
    [self setupTextFieldValidation];
    [self setupDelegates];
    [self setupDismissKeyboard];
    [self btnCardDidPressed:self];
    [self setupDoBKeyboard];
    /*
    _txtGuestName.text = @"Darryll";
    _txtGuestIC.text = @"123456789012";
    _txtGuestDOB.text = @"01-01-2000";
    _txtGuestPhone.text = @"0123456789";
    _txtGuestEmail.text = @"darryll.appstream@gmail.com";
    _txtGuestPassword.text = @"123456";
    _txtGuestConfirmPassword.text = @"123456";
    */
    // Reset MOL order_id
    [[NSUserDefaults standardUserDefaults] setValue:0 forKey:MOL_CREATE_ID];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)setupDoBKeyboard{
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setBarStyle:UIBarStyleBlackOpaque];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"btn_Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(bbtnDoneDidPressed:)];
    [btnDone setTintColor:[UIColor whiteColor]];
    [btnDone setTag:1];
    UIBarButtonItem *flexibleSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolbar.items = @[flexibleSeparator, btnDone];
    self.txtGuestDOB.inputAccessoryView = toolbar;
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setBackgroundColor:[UIColor whiteColor]];
    [datePicker setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [datePicker addTarget:self action:@selector(datePickerDidSelect:) forControlEvents:UIControlEventValueChanged];
    
    self.txtGuestDOB.inputView = datePicker;
}

#pragma mark - Setup Initialization
-(void)setupInitialization{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

-(void)setupDelegates{
    [self.txtCard setDelegate:self];
    [self.txtCvv setDelegate:self];
    /*
    [self.txtGuestName setDelegate:self];
    [self.txtGuestIC setDelegate:self];
    [self.txtGuestDOB setDelegate:self];
    [self.txtGuestPhone setDelegate:self];
    [self.txtGuestEmail setDelegate:self];
    [self.txtGuestPassword setDelegate:self];
    [self.txtGuestConfirmPassword setDelegate:self];*/
}

-(void)setupSwipeGesture{
    leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAction:)];
    [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.view addGestureRecognizer:leftSwipe];
    
    rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAction:)];
    [rightSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.view addGestureRecognizer:rightSwipe];
}

-(void)setupUI{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self.btnCard titleLabel] setFont:[UIFont fontWithName:FONT_REGULAR size:16.0]];
        [[self.btnCardless titleLabel] setFont:[UIFont fontWithName:FONT_REGULAR size:16.0]];
        [[self.btnGuest titleLabel] setFont:[UIFont fontWithName:FONT_REGULAR size:16.0]];
        [self.txtCard setFont:[UIFont fontWithName:FONT_REGULAR size:14.0]];
        [self.txtCvv setFont:[UIFont fontWithName:FONT_REGULAR size:14.0]];
        [self.txtEmail setFont:[UIFont fontWithName:FONT_REGULAR size:14.0]];
        [self.txtFullname setFont:[UIFont fontWithName:FONT_REGULAR size:14.0]];
        [self.txtContact setFont:[UIFont fontWithName:FONT_REGULAR size:14.0]];
        [self.btnNext.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:15.0]];
        [self.btnCancel.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:15.0]];
        [self.btnSubmit.titleLabel setFont:[UIFont fontWithName:FONT_REGULAR size:15.0]];
    
        [self.btnCard setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
        [self.btnCardless setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
        [self.btnGuest setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
        [self.lblCard setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.lblCardSub setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.lblCvv setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.lblCvvSub setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.lblEmail setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.lblFullname setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.lblContact setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.btnSubmit setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
        [self.lblPoweredBy setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
        [self.indicatorView setBackgroundColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.txtCard setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.txtCvv setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.txtEmail setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.txtContact setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.txtFullname setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        
        [[SharedMethod new] setPaddingToTextField:self.txtCard];
        [[SharedMethod new] setPaddingToTextField:self.txtCvv];
        [[SharedMethod new] setPaddingToTextField:self.txtEmail];
        [[SharedMethod new] setPaddingToTextField:self.txtContact];
        [[SharedMethod new] setPaddingToTextField:self.txtFullname];
        
        [[SharedMethod new] setPaddingToTextField:self.txtGuestName];
        [[SharedMethod new] setPaddingToTextField:self.txtGuestIC];
        [[SharedMethod new] setPaddingToTextField:self.txtGuestDOB];
        [[SharedMethod new] setPaddingToTextField:self.txtGuestPhone];
        [[SharedMethod new] setPaddingToTextField:self.txtGuestEmail];
        [[SharedMethod new] setPaddingToTextField:self.txtGuestPassword];
        [[SharedMethod new] setPaddingToTextField:self.txtGuestConfirmPassword];


        [[SharedMethod new] setBorder:self.txtCard borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [[SharedMethod new] setBorder:self.txtCvv borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [[SharedMethod new] setBorder:self.txtEmail borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [[SharedMethod new] setBorder:self.txtContact borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [[SharedMethod new] setBorder:self.txtFullname borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        
        [[SharedMethod new] setBorder:self.txtGuestName borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [[SharedMethod new] setBorder:self.txtGuestIC borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [[SharedMethod new] setBorder:self.txtGuestDOB borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [[SharedMethod new] setBorder:self.txtGuestPhone borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [[SharedMethod new] setBorder:self.txtGuestEmail borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [[SharedMethod new] setBorder:self.txtGuestPassword borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [[SharedMethod new] setBorder:self.txtGuestConfirmPassword borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        
        [[SharedMethod new] setCustomButtonStyle:self.btnNext image:@"" borders:YES borderWidth:1.0f cornerRadius:0.0f borderColor:THEME_COLOR textColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
        [[SharedMethod new] setCustomButtonStyle:self.btnCancel image:@"" borders:YES borderWidth:1.0f cornerRadius:0.0f borderColor:GREY_COLOR textColor:GREY_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
        [[SharedMethod new] setCustomButtonStyle:self.btnSubmit image:@"" borders:YES borderWidth:1.0f cornerRadius:0.0f borderColor:THEME_COLOR textColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
        [[SharedMethod new] setCustomButtonStyle:self.btnGuestSubmit image:@"" borders:YES borderWidth:1.0f cornerRadius:0.0f borderColor:THEME_COLOR textColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
        
        NSString *text = NSLocalizedString(@"lbl_Emember_Note", nil);
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
        [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_REGULAR size:11.6] range:NSMakeRange(0, text.length)];
        [attributedString addAttribute:NSForegroundColorAttributeName value:[[SharedMethod new] colorWithHexString:RED_COLOR andAlpha:1.0f] range:NSMakeRange(0, 6)];
        [attributedString addAttribute:NSForegroundColorAttributeName value:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] range:NSMakeRange(7, text.length-7)];
        [self.lblCardlessGuide setAttributedText:attributedString];
        
        NSString *cardText = NSLocalizedString(@"lbl_Card_Note", nil);
        NSMutableAttributedString *cardAttributedString = [[NSMutableAttributedString alloc] initWithString:cardText];
        [cardAttributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:FONT_REGULAR size:11.6] range:NSMakeRange(0, cardText.length)];
        [cardAttributedString addAttribute:NSForegroundColorAttributeName value:[[SharedMethod new] colorWithHexString:RED_COLOR andAlpha:1.0f] range:NSMakeRange(0, 6)];
        [cardAttributedString addAttribute:NSForegroundColorAttributeName value:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] range:NSMakeRange(7, cardText.length-7)];
        [self.lblCardGuide setAttributedText:cardAttributedString];
    });
    
    [self.btnCard setTitle:NSLocalizedString(@"btn_With_Card", nil) forState:UIControlStateNormal];
    [self.btnCardless setTitle:NSLocalizedString(@"btn_Without_Card", nil) forState:UIControlStateNormal];
    [self.btnGuest setTitle:NSLocalizedString(@"btn_Eguest", nil) forState:UIControlStateNormal];
    [self.lblCard setText:NSLocalizedString(@"lbl_Card", nil)];
    [self.lblCardSub setText:NSLocalizedString(@"lbl_Card_Sub", nil)];
    [self.lblCvv setText:NSLocalizedString(@"lbl_Cvv", nil)];
    [self.lblCvvSub setText:NSLocalizedString(@"lbl_Cvv_Sub", nil)];
    [self.lblEmail setText:NSLocalizedString(@"lbl_Email", nil)];
    [self.lblFullname setText:NSLocalizedString(@"lbl_Name", nil)];
    [self.lblContact setText:NSLocalizedString(@"lbl_Mobile", nil)];
    [self.txtCard setPlaceholder:NSLocalizedString(@"phd_Card", nil)];
    [self.txtCvv setPlaceholder:NSLocalizedString(@"phd_Cvv", nil)];
    [self.lblCvv setText:NSLocalizedString(@"lbl_Cvv", nil)];
    [self.txtEmail setPlaceholder:NSLocalizedString(@"phd_Email", nil)];
    [self.txtFullname setPlaceholder:NSLocalizedString(@"phd_Name_V2", nil)];
    [self.txtContact setPlaceholder:NSLocalizedString(@"phd_Phone_V2", nil)];
    [self.btnNext setTitle:NSLocalizedString(@"btn_Next", nil) forState:UIControlStateNormal];
    [self.btnCancel setTitle:NSLocalizedString(@"btn_Cancel", nil) forState:UIControlStateNormal];
    [self.btnSubmit setTitle:NSLocalizedString(@"btn_Submit", nil) forState:UIControlStateNormal];
    [self.btnGuestSubmit setTitle:NSLocalizedString(@"btn_Submit", nil) forState:UIControlStateNormal];
    
    [self.lblPoweredBy setText:NSLocalizedString(@"lbl_Powered_By", nil)];
    [self.lblPoweredBy setHidden:YES];
    
    [self setupDoneToolbar:self.txtCard];
    [self setupDoneToolbar:self.txtCvv];
    [self setupDoneToolbar:self.txtContact];
}

-(void)setupData{
    if(self.isFB){
        [self.txtEmail setText:[self.socialDetails objectForKey:@"email"]];
        
        if([self.socialDetails objectForKey:@"name"]){
            [self.txtFullname setText:[self.socialDetails objectForKey:@"name"]];
        }
        
        if([self.socialDetails objectForKey:@"contact"]){
            [self.txtContact setText:[self.socialDetails objectForKey:@"contact"]];
        }
    }
}

-(void)setupTextFieldValidation{
    [self.txtCard setValidateOnCharacterChanged:NO];
    [self.txtCard addRegx:@"[0-9]{16}" withMsg:NSLocalizedString(@"err_Invalid_Card", nil)];
    [self.txtCard updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    
    [self.txtCvv setValidateOnCharacterChanged:NO];
    [self.txtCvv addRegx:@"[0-9]{3}" withMsg:NSLocalizedString(@"err_Invalid_Cvv", nil)];
    [self.txtCvv updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];

    [self.txtEmail setValidateOnCharacterChanged:NO];
    [self.txtEmail addRegx:@"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" withMsg:NSLocalizedString(@"err_Invalid_Email", nil)];
    [self.txtEmail updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtEmail setIsMandatory:YES];
    
    [self.txtFullname setValidateOnCharacterChanged:NO];
    [self.txtFullname updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtFullname setIsMandatory:YES];
    [self.txtFullname setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    
    [self.txtContact setValidateOnCharacterChanged:NO];
    [self.txtContact addRegx:@"[0-9]{10,13}" withMsg:NSLocalizedString(@"err_Invalid_Phone", nil)];
    [self.txtContact updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtContact setIsMandatory:YES];
    
    [self.txtGuestName setValidateOnCharacterChanged:NO];
    [self.txtGuestName updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtGuestName setIsMandatory:YES];
    //[self.txtGuestName setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    
    [self.txtGuestIC setValidateOnCharacterChanged:NO];
    //[self.txtGuestIC addRegx:@"[0-9]{12}" withMsg:NSLocalizedString(@"err_Invalid_IC", nil)];
    [self.txtGuestIC updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    
    [self.txtGuestPhone setValidateOnCharacterChanged:NO];
    //[self.txtGuestPhone addRegx:@"[0-9]{10,13}" withMsg:NSLocalizedString(@"err_Invalid_Phone", nil)];
    [self.txtGuestPhone updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtGuestPhone setIsMandatory:YES];
    
    [self.txtGuestEmail setValidateOnCharacterChanged:NO];
    //[self.txtGuestEmail addRegx:@"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" withMsg:NSLocalizedString(@"err_Invalid_Email", nil)];
    [self.txtGuestEmail updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtGuestEmail setIsMandatory:YES];
    
    [self.txtGuestPassword setValidateOnCharacterChanged:NO];
    [self.txtGuestPassword updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtGuestPassword setIsMandatory:YES];
    
    [self.txtGuestConfirmPassword setValidateOnCharacterChanged:NO];
    [self.txtGuestConfirmPassword updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtGuestConfirmPassword setIsMandatory:YES];
}

-(void)setupDismissKeyboard{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)setupDoneToolbar:(UITextField*)selectedTextfield{
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setBarStyle:UIBarStyleBlackOpaque];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"btn_Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(bbtnDoneDidPressed:)];
    [doneButton setTag:2];
    [doneButton setTintColor:[UIColor whiteColor]];
    UIBarButtonItem *flexibleSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolbar.items = @[flexibleSeparator, doneButton];
    selectedTextfield.inputAccessoryView = toolbar;
}

-(void)dismissLoginVC{
    [self dismissViewControllerAnimated:YES completion:^{
        [self removeFromParentViewController];
        [self.dismissCardRegistrationDelegate dismissCardRegistrationVC];
    }];
}

-(void)dismissCardlessLoginVC{
    [self dismissViewControllerAnimated:YES completion:^{
        [self removeFromParentViewController];
        [self.dismissCardRegistrationDelegate dismissCardRegistrationVC];
    }];
}

-(void)displayPaymentGatewayMessage:(NSString*)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"App_Name", nil) message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)dismissPaymentGatewayVC:(NSString*)message{
    if(message.length>0){
        [self performSelector:@selector(displayPaymentGatewayMessage:) withObject:message afterDelay:0.2];
    }
}

-(void)displayCardlessRegistration:(NSDictionary*)basicDetails{
    [self.navigationController popViewControllerAnimated:NO];
    
    CardlessRegistrationVC *cardlessRegistrationVC = [storyboard instantiateViewControllerWithIdentifier:@"cardlessRegistrationVC"];
    cardlessRegistrationVC.dismissCardlessLoginDelegate = self;
    if(self.isFB){
        cardlessRegistrationVC.isFB = true;
        cardlessRegistrationVC.socialDetails = self.socialDetails;
        cardlessRegistrationVC.socialToken = self.socialToken;
    }
    cardlessRegistrationVC.basicDetail = basicDetails;
    
    [self presentViewController:cardlessRegistrationVC animated:YES completion:nil];
}

-(void)proceedRegister:(NSDictionary *)basicDetails{
    [self performSelector:@selector(displayCardlessRegistration:) withObject:basicDetails afterDelay:2.0];
}

-(void)animateIndicatorView:(int)selected{
    [self dismissKeyboard];
    [self.view layoutIfNeeded];
    if(selected==1){
        [UIView animateWithDuration:0.1f animations:^{
            [self.indicatorView_leading_cardView setConstant:self.cardView.frame.origin.x];
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [self.scrollContentHeight setConstant:self.scrollContent.frame.size.height];
            [self.cardContent setHidden:NO];
            [self.cardlessContent setHidden:YES];
            [self.guestContent setHidden:YES];
        }];
    } else if(selected==2){
        [UIView animateWithDuration:0.1f animations:^{
            [self.indicatorView_leading_cardView setConstant:self.cardlessView.frame.origin.x];
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [self.scrollContentHeight setConstant:self.scrollContent.frame.size.height];    
            [self.cardContent setHidden:YES];
            [self.cardlessContent setHidden:NO];
            [self.guestContent setHidden:YES];
        }];
    } else if(selected==3){
        [UIView animateWithDuration:0.1f animations:^{
            [self.indicatorView_leading_cardView setConstant:self.guestView.frame.origin.x];
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [self.scrollContentHeight setConstant:self.scrollContent.frame.size.height];
            [self.cardContent setHidden:YES];
            [self.cardlessContent setHidden:YES];
            [self.guestContent setHidden:NO];
        }];
    }
}


-(void)checkOrderDetail{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        
        NSString *MOLOrderId = [[NSUserDefaults standardUserDefaults] objectForKey:MOL_CREATE_ID];
        
        if(MOLOrderId == nil) {
            MOLOrderId = @"";
        }
        NSLog(@"latest_mol_id - %@", MOLOrderId);
        
        NSDictionary *params = @{@"email"      : self.txtEmail.text,
                                 @"type"       : @"register",
                                 @"order_id"   : MOLOrderId,
                                 @"push_token" : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                 @"token"      : [SharedMethod getTokenWithRoute:@"user/order/latest" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
#ifdef DEBUG
        NSLog(@"Retrieve Latest Order Detail Param - %@",params);
#endif
        
        [WebAPI retrieveLatestOrderDetail:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                if([[result objectForKey:@"errCode"] isEqualToString:@"OD4"]){
                    [self retrievePrice];
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"approved"] || [[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"paid"]){
                        CardlessRegistrationVC *cardlessRegistrationVC = [storyboard instantiateViewControllerWithIdentifier:@"cardlessRegistrationVC"];
                        
                        // Reset MOL order_id
                        [[NSUserDefaults standardUserDefaults] setValue:0 forKey:MOL_CREATE_ID];
                        
                        if(self.isFB){
                            cardlessRegistrationVC.isFB = true;
                            cardlessRegistrationVC.socialDetails = self.socialDetails;
                            cardlessRegistrationVC.socialToken = self.socialToken;
                        }
                        cardlessRegistrationVC.dismissCardlessLoginDelegate = self;
                        cardlessRegistrationVC.basicDetail = [result objectForKey:@"order"];
                        [self presentViewController:cardlessRegistrationVC animated:YES completion:nil];
                    } else if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"completed"]){
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"App_Name", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    } else if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"pending"]){
                        PaymentGatewayVC *paymentGatewayVC = [storyboard instantiateViewControllerWithIdentifier:@"paymentGatewayVC"];
                        paymentGatewayVC.dismissPaymentGatewayDelegate = self;
                        paymentGatewayVC.dict = [result objectForKey:@"order"];
                        paymentGatewayVC.type = @"register";
                        [self presentViewController:paymentGatewayVC animated:YES completion:nil];
                    } else{
                        [self retrievePrice];
                    }
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)retrievePrice{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSDictionary *params = @{@"push_token" : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                 @"token"      : [SharedMethod getTokenWithRoute:@"user/pricing/virtualcard" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
#ifdef DEBUG
        NSLog(@"Retrieve Price Param - %@",params);
#endif
        
        [WebAPI retrievePrice:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                
                NSString *amount = [NSString stringWithFormat:@"%@",[result objectForKey:@"register"]];
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"App_Name", nil) message:[NSString stringWithFormat:@"%@%@%@%@", NSLocalizedString(@"msg_Purchase_Membership_1", nil),@"RM",amount,NSLocalizedString(@"msg_Purchase_Membership_2", nil)] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Proceed", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self retrieveOrderId:amount];
                }];
                
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleDefault handler:nil];
                
                [alertController addAction:cancelAction];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)retrieveOrderId:(NSString*)price{
    //FOR UAT PURPOSE//
//    price = @"1.00";
    
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSDictionary *params = @{@"fullname"   : self.txtFullname.text,
                                 @"contact"    : self.txtContact.text,
                                 @"email"      : self.txtEmail.text,
                                 @"description": NSLocalizedString(@"ipay_Register_Description", nil),
                                 @"amount"     : price,
                                 @"type"       : @"register",
                                 @"push_token" : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                 @"token"      : [SharedMethod getTokenWithRoute:@"user/order/create" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                 };
        
#ifdef DEBUG
        NSLog(@"Retrieve Order Id Param - %@",params);
#endif
        
        [WebAPI generateOrderId:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
//            NSLog(@"result - %@",result);
            if([result objectForKey:@"errMsg"]){
                if([[result objectForKey:@"errCode"] isEqualToString:@"ODA"]){
                    //Go to sign up page//
                    NSDictionary *temp = @{@"fullname" : self.txtFullname.text,
                                           @"email"    : self.txtEmail.text,
                                           @"contact"  : self.txtContact.text
                                           };
                    
                    CardlessRegistrationVC *cardlessRegistrationVC = [storyboard instantiateViewControllerWithIdentifier:@"cardlessRegistrationVC"];
                    if(self.isFB){
                        cardlessRegistrationVC.isFB = true;
                        cardlessRegistrationVC.socialDetails = self.socialDetails;
                        cardlessRegistrationVC.socialToken = self.socialToken;
                    }
                    cardlessRegistrationVC.basicDetail = temp;
                    [self presentViewController:cardlessRegistrationVC animated:YES completion:nil];
                } else{
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                }
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    if([[[result objectForKey:@"order"] objectForKey:@"status"] isEqualToString:@"pending"]){
                        PaymentGatewayVC *paymentGatewayVC = [storyboard instantiateViewControllerWithIdentifier:@"paymentGatewayVC"];
                        paymentGatewayVC.dismissPaymentGatewayDelegate = self;
                        paymentGatewayVC.type = @"register";
                        paymentGatewayVC.dict = [result objectForKey:@"order"];
                        [self presentViewController:paymentGatewayVC animated:YES completion:nil];
                    } 
                } else{
                    if([[result objectForKey:@"code"] isEqualToString:@"ODA"]){
                        //Go to sign up page//
                        CardlessRegistrationVC *cardlessRegistrationVC = [storyboard instantiateViewControllerWithIdentifier:@"cardlessRegistrationVC"];
                        if(self.isFB){
                            cardlessRegistrationVC.isFB = true;
                            cardlessRegistrationVC.socialDetails = self.socialDetails;
                            cardlessRegistrationVC.socialToken = self.socialToken;
                        }
                        cardlessRegistrationVC.basicDetail = [result objectForKey:@"order"];
                        [self presentViewController:cardlessRegistrationVC animated:YES completion:nil];
                    } else{
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    }
                }
            }
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

-(void)datePickerDidSelect:(UIDatePicker*)sender{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
    NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",datePicker.date]];
    
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSString *myDate = [dateFormatter stringFromDate:date];
    [self.txtGuestDOB setText:[[SharedMethod new] convertToDefaultDateFormat:myDate]];
}

#pragma mark - UITextField Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField.tag==0){
        if(textField.text.length>=16 && range.length==0){
            return NO;
        }
        
        if([string rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet].invertedSet].location != NSNotFound){
            return NO;
        }
    } else if(textField.tag==1){
        if(textField.text.length>=3 && range.length==0){
            return NO;
        }
        
        if([string rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet].invertedSet].location != NSNotFound){
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - Actions
- (IBAction)btnGuestSubmitDidPressed:(id)sender {
    //NSLog(@"Pressed - %s", "YES");
    //[self dismissKeyboard];
    NSString *deviceToken = [[SharedMethod new] checkDeviceToken];
    
    if([self.txtGuestName validate]
       && [self.txtGuestIC validate]
       && [self.txtGuestDOB validate]
       && [self.txtGuestPhone validate]
       && [self.txtGuestEmail validate]
       && [self.txtGuestPassword validate]
       && [self.txtGuestConfirmPassword validate]){
        
        if(![_switchPDPA isOn]){
            [self presentViewController:[[SharedMethod new]
                                         setAndShowAlertController:NSLocalizedString(@"btn_SignUp", nil)
                                         message:NSLocalizedString(@"err_check_PDPA", nil)
                                         cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];

        } else if(![_switchConfirm isOn]) {
            [self presentViewController:[[SharedMethod new]
                                         setAndShowAlertController:NSLocalizedString(@"btn_SignUp", nil)
                                         message:NSLocalizedString(@"err_check_Confirm", nil)
                                         cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            
        } else if([[SharedMethod new] checkNetworkConnectivity]){
            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];

            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            [params setObject:[self.txtGuestName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"fullname"];
            [params setObject:self.txtGuestIC.text forKey:@"nric"];
            [params setObject:[[SharedMethod new] convertDateTimeFormat:self.txtGuestDOB.text andInputFormat:@"dd-MM-yyyy" andOutputFormat:@"yyyy-MM-dd"] forKey:@"birthday"];
            [params setObject:self.txtGuestPhone.text forKey:@"contact"];
            [params setObject:[self.txtGuestEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"email"];
            [params setObject:self.txtGuestPassword.text forKey:@"password"];
            [params setObject:self.txtGuestConfirmPassword.text forKey:@"password_confirm"];
            [params setObject:@"e-guest" forKey:@"type"];
            [params setObject:PLATFORM forKey:@"platform"];
            [params setObject:deviceToken forKey:@"push_token"];
            [params setObject:[SharedMethod getTokenWithRoute:@"user/register" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
            
            [WebAPI signUpNewUser:params andSuccess:^(id successBlock) {
                [SVProgressHUD dismiss];
                NSDictionary *result = successBlock;
                if([result objectForKey:@"errMsg"]){
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                } else{
                    if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                        result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_SOCIAL];
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_GUEST];
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_MEMBER];
                        [[PDKeychainBindings sharedKeychainBindings] setObject:[result objectForKey:@"secret"] forKey:SECRET_KEY];
                        [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"user"] forKey:USER_DETAIL];
                        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",[[[result objectForKey:@"user"] objectForKey:@"id"] intValue]] forKey:USER_ID];
                        [self dismissViewControllerAnimated:YES completion:^{
                            [self removeFromParentViewController];
                            [self.dismissCardRegistrationDelegate dismissCardRegistrationVC];
                            //[self.dismissLoginDelegate dismissLoginVC];
                            [self dismissLoginVC];
                        }];
                    } else{
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    }
                }
            }];
            
        }else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            
        }
        
    } else if(![self.txtGuestName validate]){
        [self.txtGuestName becomeFirstResponder];
    } else if(![self.txtGuestIC validate]){
        [self.txtGuestIC becomeFirstResponder];
    } else if(![self.txtGuestDOB validate]){
        [self.txtGuestDOB becomeFirstResponder];
    } else if(![self.txtGuestPhone validate]){
        [self.txtGuestPhone becomeFirstResponder];
    } else if(![self.txtGuestEmail validate]){
        [self.txtGuestEmail becomeFirstResponder];
    } else if(![self.txtGuestPassword validate]){
        [self.txtGuestPassword becomeFirstResponder];
    } else if(![self.txtGuestConfirmPassword validate]){
        [self.txtGuestConfirmPassword becomeFirstResponder];
    }
    
}

- (IBAction)btnBackDidPressed:(id)sender {
    [self removeFromParentViewController];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnCardDidPressed:(id)sender {
    [self animateIndicatorView:1];
}

- (IBAction)btnCardlessDidPressed:(id)sender {
    [self animateIndicatorView:2];
}

- (IBAction)btnGuestDidPress:(id)sender {
    [self animateIndicatorView:3];
}

- (IBAction)btnNextDidPressed:(id)sender {
    [self dismissKeyboard];
    
    if([self.txtCard validate] && [self.txtCvv validate]){
        if([[SharedMethod new] checkNetworkConnectivity]){
            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
            NSDictionary *params = @{@"card_number"      : self.txtCard.text,
                                     @"card_code"        : self.txtCvv.text,
                                     @"platform"         : PLATFORM,
                                     @"push_token"       : [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN],
                                     @"token"            : [SharedMethod getTokenWithRoute:@"user/card/verify" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                                     };
                
            #ifdef DEBUG
            NSLog(@"Card Sign Up Param - %@",params);
            #endif
            
            [WebAPI cardSignUp:params andSuccess:^(id successBlock) {
                [SVProgressHUD dismiss];
                NSDictionary *result = successBlock;
                if([result objectForKey:@"errMsg"]){
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                } else{
                    result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                    SignUpVC *signUpVC = [storyboard instantiateViewControllerWithIdentifier:@"signUpVC"];
                    signUpVC.cardNumber = self.txtCard.text;
                    signUpVC.cvvCode = self.txtCvv.text;
                    
                    if(self.socialToken.length>0){
                        signUpVC.socialToken = self.socialToken;
                        signUpVC.socialDetails = self.socialDetails;
                        signUpVC.isFB = YES;
                    }
                    
                    signUpVC.dismissLoginDelegate = self;
                    [self presentViewController:signUpVC animated:YES completion:nil];
                }
            }];
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        }
    }
    
}

- (IBAction)btnCancelDidPressed:(id)sender {
    [self removeFromParentViewController];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)bbtnDoneDidPressed:(id)sender{
    [self dismissKeyboard];
}

- (IBAction)btnSubmitDidPressed:(id)sender {
    [self dismissKeyboard];
    if([self.txtEmail validate] && [self.txtContact validate] && [self.txtFullname validate]){
        [self checkOrderDetail];
    }
}

- (IBAction)swipeAction:(UISwipeGestureRecognizer*)sender{
    [self dismissKeyboard];
    if(sender.direction == UISwipeGestureRecognizerDirectionRight){
        [self animateIndicatorView:1];
    } else if(sender.direction == UISwipeGestureRecognizerDirectionLeft){
        [self animateIndicatorView:2];
    } else if(sender.direction == UISwipeGestureRecognizerDirectionLeft){
        [self animateIndicatorView:3];
    }
}

@end
