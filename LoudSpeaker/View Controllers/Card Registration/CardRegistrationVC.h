//
//  CardRegistrationVC.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 07/12/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DismissCardRegistrationDelegate
@optional
-(void)dismissCardRegistrationVC;
@end

@interface CardRegistrationVC : UIViewController<DismissCardRegistrationDelegate>

@property (nonatomic) NSString *socialToken;
@property (nonatomic) NSDictionary *socialDetails;
@property (nonatomic) BOOL isFB;

@property (nonatomic) id <DismissCardRegistrationDelegate> dismissCardRegistrationDelegate;

@end
