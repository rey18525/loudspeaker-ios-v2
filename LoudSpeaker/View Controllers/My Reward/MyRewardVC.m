//
//  MyRewardVC.m
//  Oasis
//
//  Created by Wong Ryan on 28/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "MyRewardVC.h"
#import "RewardBannerCell.h"
#import "RewardDetailCell.h"
#import "SVProgressHUD.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "RewardPopVC.h"
#import "Reward.h"
#import "Campaign.h"
#import "Voucher.h"
#import "SDWebImage/UIImageView+WebCache.h"

@interface MyRewardVC ()<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MyRewardVC{
    UIStoryboard *storyboard;
    RewardPopVC *rewardPopVC;
    NSMutableArray *myReward;
    Campaign *selectedReward;
    int cellSize,progressBarWidth,currentCount,totalCount;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInitialization];
    [self setupNavigationBar];
    [self setupDelegates];
    [self setupData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"displayReward"]){
        [self displayReward];
    }
}

#pragma mark - Setup Initialization
-(void)setupNavigationBar{
    [self.navigationItem setTitle:NSLocalizedString(@"nav_MyReward", nil)];
    [self.navigationItem setHidesBackButton:YES animated:YES];
}

-(void)setupInitialization{
    myReward = [[NSMutableArray alloc] init];
    [self.view setBackgroundColor:[[SharedMethod new] colorWithHexString:GREY_COLOR andAlpha:1.0f]];
}

-(void)setupData{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    rewardPopVC = [storyboard instantiateViewControllerWithIdentifier:@"rewardPopVC"];
    [rewardPopVC.view setTag:901];
    
    currentCount = [[[[NSUserDefaults standardUserDefaults] objectForKey:USER_POINT_DETAIL] objectForKey:@"stamps_count"] intValue];
    totalCount = [[[[[NSUserDefaults standardUserDefaults] objectForKey:USER_POINT_DETAIL] objectForKey:@"active_set"] objectForKey:@"trigger_value"] intValue];
    
    myReward = [self filterStamps];
    [self.tableView reloadData];
}

-(void)setupDelegates{
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
}

-(float)calculateProgressBar:(int)current andMaximum:(int)max{
    float final = 0;
    float percentage = (current * 100) / max;
    final = (progressBarWidth * percentage) / 100;
    return final;
}

-(NSMutableArray*)filterStamps{
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    NSMutableArray *tempRewards = [[NSMutableArray alloc] init];

    [temp addObject: [[[NSUserDefaults standardUserDefaults] objectForKey:USER_POINT_DETAIL] objectForKey:@"active_set"]];
    
    tempRewards = [[SharedMethod new] processCampaign:temp andCurrentArray:tempRewards];
    selectedReward = [tempRewards objectAtIndex:0];
    
    return tempRewards;
}

-(void)displayReward{
    Campaign *campaign = selectedReward;
    Voucher *voucher = campaign.voucher;
    if(voucher.image>0){
        [[SharedMethod new] setLoadingIndicatorOnImage:rewardPopVC.imgReward];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [rewardPopVC.imgReward sd_setImageWithURL:[NSURL URLWithString:voucher.image] placeholderImage:[UIImage imageNamed:@"bg_voucher"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    voucher.cachedImage = image;
                    for(UIView *view in rewardPopVC.imgReward.subviews){
                        if(view.tag==100){
                            [view removeFromSuperview];
                        }
                    }
                }];
            });
        });
    } else{
        [rewardPopVC.imgReward setImage:[UIImage imageNamed:@"bg_voucher"]];
    }
    
    [rewardPopVC.lblDescription setText:NSLocalizedString(@"lbl_Reward_Description", nil)];
    [rewardPopVC.lblTitle setText:NSLocalizedString(@"lbl_Reward_Title", nil)];
    
    [self.view.window addSubview:rewardPopVC.view];
    [self addChildViewController:rewardPopVC];
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnStarDidPressed:(id)sender{
    /*
    Campaign *campaign = selectedReward;
    Voucher *voucher = campaign.voucher;
    if(voucher.image>0){
        [[SharedMethod new] setLoadingIndicatorOnImage:rewardPopVC.imgReward];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [rewardPopVC.imgReward sd_setImageWithURL:[NSURL URLWithString:voucher.image] placeholderImage:[UIImage imageNamed:@"bg_voucher"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    voucher.cachedImage = image;
                    for(UIView *view in rewardPopVC.imgReward.subviews){
                        if(view.tag==100){
                            [view removeFromSuperview];
                        }
                    }
                }];
            });
        });
    } else{
        [rewardPopVC.imgReward setImage:[UIImage imageNamed:@"bg_voucher"]];
    }
    
    [rewardPopVC.lblDescription setText:NSLocalizedString(@"lbl_Reward_Description", nil)];
    [rewardPopVC.lblTitle setText:NSLocalizedString(@"lbl_Reward_Title", nil)];
    
    [self.view.window addSubview:rewardPopVC.view];
    [self addChildViewController:rewardPopVC];
     */
}

#pragma mark - WebView Delegate
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [SVProgressHUD dismiss];
    
    if(webView.tag==100){
        CGRect frame = webView.frame;
        frame.size.height = 1;
        webView.frame = frame;
        CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
        frame.size = fittingSize;
        webView.frame = frame;
        
        cellSize = fittingSize.height+8;
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
    }
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Campaign *campaign = selectedReward;
    Voucher *voucher = campaign.voucher;
    
    if(indexPath.row==0){
        RewardBannerCell *cell = (RewardBannerCell*)[tableView dequeueReusableCellWithIdentifier:@"rewardBannerCell"];
        
        if(voucher.image>0){
            [[SharedMethod new] setLoadingIndicatorOnImage:cell.imgBanner];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{        
                dispatch_async(dispatch_get_main_queue(), ^{
                    [cell.imgBanner setContentMode:UIViewContentModeScaleAspectFit];
                    [cell.imgBanner setClipsToBounds:YES];
                    [cell.imgBanner sd_setImageWithURL:[NSURL URLWithString:voucher.image] placeholderImage:[UIImage imageNamed:@"bg_voucher"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        voucher.cachedImage = image;
                        for(UIView *view in cell.imgBanner.subviews){
                            if(view.tag==100){
                                [view removeFromSuperview];
                            }
                        }
                    }];
                });
            });
        } else{
            [cell.imgBanner setImage:[UIImage imageNamed:@"bg_voucher"]];
        }
        return cell;
    } else if(indexPath.row==1){
        RewardDetailCell *cell = (RewardDetailCell*)[tableView dequeueReusableCellWithIdentifier:@"rewardDetailCell"];
        
        [cell.detailView setHidden:NO];
        [cell.webView setHidden:YES];
        
        [[cell.progressView layer] setCornerRadius:10.0f];
        [[cell.progressBar layer] setCornerRadius:10.0f];
        
        [UIView animateWithDuration:0.6 animations:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                progressBarWidth = (self.tableView.frame.size.width-cell.btnStar.frame.size.width-20);
                float width = [self calculateProgressBar:currentCount andMaximum:totalCount];
                [cell.progressBar setFrame:CGRectMake(0, 0, width, cell.progressView.frame.size.height)];
            });
        }];
        
        NSString *count = [NSString stringWithFormat:@"%d / %d",currentCount,totalCount];
        NSArray *tempArray = [count componentsSeparatedByString:@"/"];
        NSString *current = [tempArray objectAtIndex:1];
        
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:count];
        [string addAttribute:NSForegroundColorAttributeName value:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] range:NSMakeRange(0,current.length)];
        
        [cell.lblCount setAttributedText:string];
        
        [cell.btnStar setTitle:@"" forState:UIControlStateNormal];
        
        if(currentCount < totalCount){
            [cell.btnStar setImage:[UIImage imageNamed:@"ic_reward_star"] forState:UIControlStateNormal];
//            [cell.btnStar addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
            [cell.btnStar removeTarget:self action:@selector(btnStarDidPressed:) forControlEvents:UIControlEventTouchUpInside];
        } else{
            [cell.btnStar setImage:[UIImage imageNamed:@"ic_reward_star_active"] forState:UIControlStateNormal];
            [cell.btnStar addTarget:self action:@selector(btnStarDidPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        return cell;
    } else if(indexPath.row==2){
        RewardDetailCell *cell = (RewardDetailCell*)[tableView dequeueReusableCellWithIdentifier:@"rewardDetailCell"];
        
        [cell.detailView setHidden:YES];
        [cell.webView setHidden:NO];
        
        [cell.webView setBackgroundColor:[UIColor whiteColor]];
        [cell.webView setOpaque:NO];
        [cell.webView setTag:100];
        [cell.webView.scrollView setScrollEnabled:NO];
        
        NSString *finalString = [NSString stringWithFormat:@"<html><head><meta name='viewport' http-equiv='Content-Type' content='text/html;charset=UTF-8;width=device-width,initial-scale=0.85'></head><body>%@</body></html>", voucher.content];
        
        [cell.webView loadHTMLString:finalString baseURL:nil];
        [cell.webView setDelegate:self];

        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"Selected row %d",(int)indexPath.row);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        return tableView.frame.size.width/2;
    } else if(indexPath.row==1){
        return 76;
    } else if(indexPath.row==2){
        if(cellSize>(tableView.frame.size.height - (tableView.frame.size.width/2) - 48)){
            return cellSize;
        } else{
            return tableView.frame.size.height - (tableView.frame.size.width/2) - 16;
        }
    } else{
        return UITableViewAutomaticDimension;
    }
}

@end
