//
//  SignUpVC.m
//  Oasis
//
//  Created by Wong Ryan on 25/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "SignUpVC.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "WebAPI.h"
#import "UIView+Toast.h"
#import "LoginVC.h"
#import "ProfileVC.h"
#import "SVProgressHUD.h"
#import "LoginVC.h"
#import "WebVC.h"
#import "TextFieldValidator.h"
#import <TPKeyboardAvoidingScrollView.h>
#import <PDKeychainBindings.h>

@interface SignUpVC ()<UITextFieldDelegate>
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (strong, nonatomic) IBOutletCollection(TextFieldValidator) NSArray *textfields;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContent;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtName;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtIC;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtPassword;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtEmail;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtGender;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtRace;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtDob;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtCity;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtState;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtCountry;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtPhone;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtAddress;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtAddress2;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtAddress3;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtPostcode;
@property (weak, nonatomic) IBOutlet UIImageView *imgTick;
@property (weak, nonatomic) IBOutlet UILabel *lblTerms;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIView *termsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtEmail_Top_PasswordView;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordView_Height;
@property (weak, nonatomic) IBOutlet UIView *tickView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblMobile_Top_PasswordView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentHeight;
@property (weak, nonatomic) IBOutlet UIView *scrollDetailContent;
@property (weak, nonatomic) IBOutlet UILabel *lblPolicy;
@property (weak, nonatomic) IBOutlet UIButton *btnTerms;

@end

@import Firebase;

@implementation SignUpVC{
    BOOL isAgreed;
    UIStoryboard *storyboard;
    UIAlertController *alertController;
    UIAlertAction *cancelAction;
    UIBarButtonItem *bbtnBack;
    UIDatePicker *datePicker;
    NSArray *labelsArray;
    CGRect nameRect;
    BOOL isVerified;
    NSString *selectedGender, *selectedRace, *selectedState, *selectedCountry;
    NSMutableArray *statesArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupBarButton];
    [self setupTextFieldValidation];
    [self setupDismissKeyboard];
    [self setupDoBKeyboard];
    [self setupInitialization];
    [self setupData];
    [self setupUI];
    [self setupText];
    [self setupDelegates];
    [self checkFromSocial];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationItem setTitle:NSLocalizedString(@"nav_SignUp_V2", nil)];
    isVerified=NO;
    
#ifdef DEBUG
    NSLog(@"fb token - %@",self.socialToken);
    NSLog(@"fb data - %@",self.socialDetails);
#endif
    
    //    [self checkFromSocial];
}

#pragma mark - Setup
-(void)setupBarButton{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    bbtnBack = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back"] style:UIBarButtonItemStyleDone target:self action:@selector(bbtnBackDidPressed:)];
    self.navigationItem.leftBarButtonItems = @[bbtnBack];
}

-(void)setupTextFieldValidation{
    [self.txtName setValidateOnCharacterChanged:NO];
    [self.txtName updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtName setIsMandatory:YES];
    [self.txtName setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    
    [self.txtIC setValidateOnCharacterChanged:NO];
    //    [self.txtIC addRegx:@"[0-9]{12}" withMsg:NSLocalizedString(@"err_Invalid_IC", nil)];
    [self.txtIC updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtIC setIsMandatory:YES];
    
    [self.txtPassword setValidateOnCharacterChanged:NO];
    [self.txtPassword updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtPassword setIsMandatory:YES];
    
    [self.txtConfirmPassword setValidateOnCharacterChanged:NO];
    [self.txtConfirmPassword updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtConfirmPassword setIsMandatory:YES];
    
    [self.txtConfirmPassword addConfirmValidationTo:self.txtPassword withMsg:NSLocalizedString(@"err_Invalid_Password", nil)];
    
    [self.txtEmail setValidateOnCharacterChanged:NO];
    [self.txtEmail addRegx:@"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" withMsg:NSLocalizedString(@"err_Invalid_Email", nil)];
    [self.txtEmail updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtEmail setIsMandatory:YES];
    
    [self.txtCity setValidateOnCharacterChanged:NO];
    [self.txtCity updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtCity setIsMandatory:YES];
    
    [self.txtPhone setValidateOnCharacterChanged:NO];
    [self.txtPhone addRegx:@"[0-9]{10,13}" withMsg:NSLocalizedString(@"err_Invalid_Phone", nil)];
    [self.txtPhone updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtPhone setIsMandatory:YES];
    
    [self.txtAddress setIsMandatory:NO];
    [self.txtAddress2 setIsMandatory:NO];
    [self.txtAddress3 setIsMandatory:NO];
    
    [self.txtPostcode setValidateOnCharacterChanged:NO];
    [self.txtPostcode updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtPostcode setIsMandatory:YES];
}

-(void)setupInitialization{
    statesArray = [[NSMutableArray alloc] init];
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [self.txtGender setEnabled:NO];
    [self.txtRace setEnabled:NO];
}

-(void)setupUI{
    for(UILabel *lbl in self.labels){
        [lbl setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [lbl setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
        [lbl setText:[labelsArray objectAtIndex:lbl.tag-1]];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        for(UITextField *txt in self.textfields){
            [[SharedMethod new] setupTextfield:txt fontType:FONT_REGULAR fontSize:14.0f textColor:THEME_COLOR textAlpha:1.0f addPadding:YES andAddBorder:YES];
        }
        
        [[SharedMethod new] setBorder:self.tickView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
        [[SharedMethod new] setCustomButtonStyle:self.btnSubmit image:@"" borders:YES borderWidth:1.0f cornerRadius:0.0f borderColor:THEME_COLOR textColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
        [self.imgTick setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    });
    
    [self.btnTerms setTitle:NSLocalizedString(@"lbl_SignUp_Terms", nil) forState:UIControlStateNormal];
    [self.btnTerms setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"lbl_SignUp_Terms", nil)];
    
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor blueColor]
                 range:NSMakeRange(13, 18)];
    
    [text addAttribute:NSForegroundColorAttributeName
                 value:[UIColor blueColor]
                 range:NSMakeRange(36, 14)];
    
    [text addAttribute:NSFontAttributeName
                 value:[UIFont fontWithName:FONT_REGULAR size:10.0]
                 range:NSMakeRange(0, text.length)];
    
    [self.btnTerms setAttributedTitle:text forState:UIControlStateNormal];
    [[self.btnTerms titleLabel] setNumberOfLines:3];
    //    [[self.btnTerms titleLabel] setLineBreakMode:NSLineBreakByClipping];
    [[self.btnTerms titleLabel] sizeToFit];
}

-(void)setupText{
    [self.navigationItem setTitle:NSLocalizedString(@"nav_SignUp", nil)];
    
    [self.txtName setPlaceholder:NSLocalizedString(@"phd_Name_V2", nil)];
    [self.txtIC setPlaceholder:NSLocalizedString(@"phd_IC_V2", nil)];
    [self.txtPassword setPlaceholder:NSLocalizedString(@"phd_Password_V2", nil)];
    [self.txtConfirmPassword setPlaceholder:NSLocalizedString(@"phd_ConfirmPassword_V2", nil)];
    [self.txtEmail setPlaceholder:NSLocalizedString(@"phd_Email_V2", nil)];
    [self.txtGender setPlaceholder:NSLocalizedString(@"phd_Gender_V2", nil)];
    [self.txtRace setPlaceholder:NSLocalizedString(@"phd_Race_V2", nil)];
    [self.txtDob setPlaceholder:NSLocalizedString(@"phd_Dob_V2", nil)];
    [self.txtCity setPlaceholder:NSLocalizedString(@"phd_City_V2", nil)];
    [self.txtCountry setPlaceholder:NSLocalizedString(@"phd_Country_V2", nil)];
    [self.txtState setPlaceholder:NSLocalizedString(@"phd_State_V2", nil)];
    [self.txtPhone setPlaceholder:NSLocalizedString(@"phd_Phone_V2", nil)];
    [self.txtAddress setPlaceholder:NSLocalizedString(@"phd_Address_V2", nil)];
    [self.txtAddress2 setPlaceholder:NSLocalizedString(@"phd_Address2_V2", nil)];
    [self.txtAddress3 setPlaceholder:NSLocalizedString(@"phd_Address3_V2", nil)];
    [self.txtPostcode setPlaceholder:NSLocalizedString(@"phd_Postcode_V2", nil)];
    
    [self.btnSubmit setTitle:NSLocalizedString(@"btn_Create_Account", nil) forState:UIControlStateNormal];
    [self.lblTerms setText:@""];
    //    [self setupTermsConditionLink:NSLocalizedString(@"lbl_SignUp_Terms", nil)];
    //    [self setupDoneToolbar:self.txtIC];
    [self setupDoneToolbar:self.txtPhone];
    [self setupDoneToolbar:self.txtPostcode];
}

-(void)setupData{
    isAgreed = NO;
    selectedGender = @"";
    selectedRace = @"";
    selectedState = @"";
    
    labelsArray = @[NSLocalizedString(@"lbl_Name", nil),
                    NSLocalizedString(@"lbl_IC", nil),
                    NSLocalizedString(@"lbl_Password", nil),
                    NSLocalizedString(@"lbl_Confirm_Password", nil),
                    NSLocalizedString(@"lbl_Mobile", nil),
                    NSLocalizedString(@"lbl_Email", nil),
                    NSLocalizedString(@"lbl_Gender", nil),
                    NSLocalizedString(@"lbl_Race", nil),
                    NSLocalizedString(@"lbl_Dob", nil),
                    NSLocalizedString(@"lbl_Address1", nil),
                    NSLocalizedString(@"lbl_Address2", nil),
                    NSLocalizedString(@"lbl_Address3", nil),
                    NSLocalizedString(@"lbl_City", nil),
                    NSLocalizedString(@"lbl_Country", nil),
                    NSLocalizedString(@"lbl_State", nil),
                    NSLocalizedString(@"lbl_Postcode", nil)];
    
    NSMutableArray *genderArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:GENDER]];
    if([genderArray count]>0){
        for(int count=0;count<[genderArray count];count++){
            [self.txtGender setText:[[genderArray objectAtIndex:0] objectForKey:@"label"]];
            selectedGender = [[genderArray objectAtIndex:0] objectForKey:@"code"];
        }
    }
    
    NSMutableArray *raceArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:RACE]];
    if([raceArray count]>0){
        for(int count=0;count<[raceArray count];count++){
            [self.txtRace setText:[[raceArray objectAtIndex:0] objectForKey:@"label"]];
            selectedRace = [[raceArray objectAtIndex:0] objectForKey:@"code"];
        }
    }
    
    NSMutableArray *countryArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:COUNTRIES]];
    if([countryArray count]>0){
        [self.txtCountry setText:[[countryArray objectAtIndex:0] objectForKey:@"name"]];
        selectedCountry = [[countryArray objectAtIndex:0] objectForKey:@"iso"];
        
        statesArray = [[NSMutableArray alloc] init];
        statesArray = [[countryArray objectAtIndex:0] objectForKey:@"states"];
        [self.txtState setText:[[statesArray objectAtIndex:0] objectForKey:@"name"]];
        selectedState = [[statesArray objectAtIndex:0] objectForKey:@"code"];
    }
}

-(void)setupDelegates{
    [self.txtName setDelegate:self];
    [self.txtIC setDelegate:self];
    [self.txtPassword setDelegate:self];
    [self.txtConfirmPassword setDelegate:self];
    [self.txtEmail setDelegate:self];
    [self.txtGender setDelegate:self];
    [self.txtRace setDelegate:self];
    [self.txtCity setDelegate:self];
    [self.txtState setDelegate:self];
    [self.txtPhone setDelegate:self];
    [self.txtAddress setDelegate:self];
    [self.txtAddress2 setDelegate:self];
    [self.txtAddress3 setDelegate:self];
    [self.txtPostcode setDelegate:self];
}

-(void)checkFromSocial{
    if(self.socialToken.length>0){
        nameRect = self.txtName.frame;
        [self.passwordView setHidden:YES];
        //        [self.txtPassword setHidden:YES];
        //        [self.txtConfirmPassword setHidden:YES];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.passwordView_Height setConstant:0];
            [self.lblMobile_Top_PasswordView setConstant:0];
            [self.scrollContentHeight setConstant:self.btnTerms.frame.size.height+self.btnTerms.frame.origin.y];
        });
        
        if([self.socialDetails objectForKey:@"email"]){
            [self.txtEmail setText:[self.socialDetails objectForKey:@"email"]];
        }
        
        [self.txtName setText:[self.socialDetails objectForKey:@"name"]];
        
        if([self.socialDetails objectForKey:@"contact"]){
            [self.txtPhone setText:[self.socialDetails objectForKey:@"contact"]];
        }
        
        if([self.socialDetails objectForKey:@"birthday"]){
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
            dateFormatter.dateFormat = @"MM/dd/yyyy";
            NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[self.socialDetails objectForKey:@"birthday"]]];
            
            dateFormatter.dateFormat = @"dd/MM/yyyy";
            NSString *myDate = [dateFormatter stringFromDate:date];
//            [self.txtDob setText:myDate];
            [self.txtDob setText:[[SharedMethod new] convertDateTimeFormat:myDate andInputFormat:@"dd/MM/yyyy" andOutputFormat:@"dd MMM yyyy"]];
        }
    }
}

-(void)setupDismissKeyboard{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)setupDoneToolbar:(UITextField*)selectedTextfield{
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setBarStyle:UIBarStyleBlackOpaque];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"btn_Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(bbtnDoneDidPressed:)];
    [doneButton setTag:2];
    [doneButton setTintColor:[UIColor whiteColor]];
    UIBarButtonItem *flexibleSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolbar.items = @[flexibleSeparator, doneButton];
    selectedTextfield.inputAccessoryView = toolbar;
}

-(void)setupDoBKeyboard{
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setBarStyle:UIBarStyleBlackOpaque];
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"btn_Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(bbtnDoneDidPressed:)];
    [btnDone setTintColor:[UIColor whiteColor]];
    [btnDone setTag:1];
    UIBarButtonItem *flexibleSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolbar.items = @[flexibleSeparator, btnDone];
    self.txtDob.inputAccessoryView = toolbar;
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    [datePicker setBackgroundColor:[UIColor whiteColor]];
    //    datePicker.maximumDate = [NSDate date];
    //
    //    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    //    NSDate *currentDate = [NSDate date];
    //    NSDateComponents *comps = [[NSDateComponents alloc] init];
    //    [comps setYear:-100];
    //    NSDate *minDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    //    [comps setYear:-18];
    //    NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    //
    //    datePicker.minimumDate = minDate;
    //    datePicker.maximumDate = maxDate;
    
    [datePicker addTarget:self action:@selector(datePickerDidSelect:) forControlEvents:UIControlEventValueChanged];
    
    self.txtDob.inputView = datePicker;
}

#pragma mark - UITextField Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField.tag==2){
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:IC_CHARACTERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    } else if(textField.tag==10 || textField.tag==11 || textField.tag==12 || textField.tag==13){
        if(textField.text.length>=40 && range.length==0){
            return NO;
        }
    } else if(textField.tag==16){
        if(textField.text.length>=6 && range.length==0){
            return NO;
        }
    }
    
    //    if(textField.tag==2){
    //        if(textField.text.length>12 && range.length==0){
    //            return NO;
    //        }
    //
    //        if([string rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet].invertedSet].location != NSNotFound){
    //            return NO;
    //        }
    //    }
    
    /*else if(textField.tag==11){
     if([string rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet].invertedSet].location != NSNotFound){
     return NO;
     }
     }*/
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self dismissKeyboard];
    return YES;
}

#pragma mark - Validations
-(BOOL)validateAccount{
    if(self.txtGender.text.length<=0){
        [self.scrollView setContentOffset:CGPointMake(0, (self.txtGender.frame.origin.y+self.txtGender.frame.size.height) - (self.txtGender.frame.origin.y/2)) animated:YES];
        [self.view makeToast:NSLocalizedString(@"err_Empty_Gender", nil)];
        return NO;
    } else if(self.txtRace.text.length<=0){
        [self.scrollView setContentOffset:CGPointMake(0, (self.txtRace.frame.origin.y+self.txtRace.frame.size.height) - (self.txtRace.frame.origin.y/2)) animated:YES];
        [self.view makeToast:NSLocalizedString(@"err_Empty_Race", nil)];
        return NO;
    } else if(self.txtState.text.length<=0){
        [self.scrollView setContentOffset:CGPointMake(0, (self.txtState.frame.origin.y+self.txtState.frame.size.height) - (self.txtState.frame.origin.y/2)) animated:YES];
        [self.view makeToast:NSLocalizedString(@"err_Empty_State", nil)];
        return NO;
    } else if(!isAgreed){
        [self.view makeToast:NSLocalizedString(@"err_Invalid_Terms", nil)];
        return NO;
    }
    return YES;
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self dismissKeyboard];
    [self removeFromParentViewController];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)bbtnDoneDidPressed:(UIButton*)sender {
    if(sender.tag == 1){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
        NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",datePicker.date]];
        
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSString *myDate = [dateFormatter stringFromDate:date];
//        [self.txtDob setText:[[SharedMethod new] convertToDefaultDateFormat:myDate]];
        [self.txtDob setText:[[SharedMethod new] convertDateTimeFormat:myDate andInputFormat:@"yyyy-MM-dd" andOutputFormat:@"dd MMM yyyy"]];
    }
    [self dismissKeyboard];
}

-(void)datePickerDidSelect:(UIDatePicker*)sender{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_MY"]];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss Z";
    NSDate *date = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",datePicker.date]];
    
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSString *myDate = [dateFormatter stringFromDate:date];
//    [self.txtDob setText:[[SharedMethod new] convertToDefaultDateFormat:myDate]];
    [self.txtDob setText:[[SharedMethod new] convertDateTimeFormat:myDate andInputFormat:@"yyyy-MM-dd" andOutputFormat:@"dd MMM yyyy"]];
}

- (IBAction)btnGenderDidPressed:(id)sender {
    [self dismissKeyboard];
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:NSLocalizedString(@"msg_Gender", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSMutableArray *genderArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:GENDER]];
    
    if([genderArray count]>0){
        for(int count=0;count<[genderArray count];count++){
            UIAlertAction *action = [UIAlertAction actionWithTitle:[[genderArray objectAtIndex:count] objectForKey:@"label"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.txtGender setText:[[genderArray objectAtIndex:count] objectForKey:@"label"]];
                selectedGender = [[genderArray objectAtIndex:count] objectForKey:@"code"];
            }];
            
            [alertController addAction:action];
        }
    }
    
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:^{
        [self dismissKeyboard];
    }];
}

- (IBAction)btnRaceDidPressed:(id)sender {
    [self dismissKeyboard];
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:NSLocalizedString(@"msg_Race", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSMutableArray *raceArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:RACE]];
    
    if([raceArray count]>0){
        for(int count=0;count<[raceArray count];count++){
            UIAlertAction *action = [UIAlertAction actionWithTitle:[[raceArray objectAtIndex:count] objectForKey:@"label"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.txtRace setText:[[raceArray objectAtIndex:count] objectForKey:@"label"]];
                selectedRace = [[raceArray objectAtIndex:count] objectForKey:@"code"];
            }];
            
            [alertController addAction:action];
        }
    }
    
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:^{
        [self dismissKeyboard];
    }];
}

- (IBAction)btnCountryDidPressed:(id)sender {
    [self dismissKeyboard];
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:NSLocalizedString(@"msg_Country", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSMutableArray *countryArray = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:COUNTRIES]];
    
    if([countryArray count]>0){
        for(int count=0;count<[countryArray count];count++){
            UIAlertAction *action = [UIAlertAction actionWithTitle:[[countryArray objectAtIndex:count] objectForKey:@"name"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.txtCountry setText:[[countryArray objectAtIndex:count] objectForKey:@"name"]];
                selectedCountry = [[countryArray objectAtIndex:count] objectForKey:@"iso"];
                
                statesArray = [[countryArray objectAtIndex:count] objectForKey:@"states"];
                [self.txtState setText:[[[[countryArray objectAtIndex:count] objectForKey:@"states"] objectAtIndex:0] objectForKey:@"name"]];
                selectedState = [[[[countryArray objectAtIndex:count] objectForKey:@"states"] objectAtIndex:0] objectForKey:@"code"];
            }];
            
            [alertController addAction:action];
        }
    }
    
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:^{
        [self dismissKeyboard];
    }];
}

- (IBAction) btnStateDidPressed:(id)sender {
    [self dismissKeyboard];
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:NSLocalizedString(@"msg_State", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSMutableArray *stateArray = [[NSMutableArray alloc] initWithArray:statesArray];
    
    if([stateArray count]>0){
        for(int count=0;count<[stateArray count];count++){
            UIAlertAction *action = [UIAlertAction actionWithTitle:[[stateArray objectAtIndex:count] objectForKey:@"name"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.txtState setText:[[stateArray objectAtIndex:count] objectForKey:@"name"]];
                selectedState = [[stateArray objectAtIndex:count] objectForKey:@"code"];
            }];
            
            [alertController addAction:action];
        }
    }
    
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:^{
        [self dismissKeyboard];
    }];
}

- (IBAction)btnDobDidPressed:(id)sender {
    
}

- (IBAction)btnTickDidPressed:(id)sender {
    if(isAgreed){
        [self.imgTick setImage:nil];
        [self.imgTick setBackgroundColor:[UIColor clearColor]];
    } else{
        [self.imgTick setImage:[UIImage imageNamed:@"ic_check"]];
        [self.imgTick setBackgroundColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    }
    isAgreed = !isAgreed;
}

- (IBAction)btnTermsDidPressed:(id)sender {
    alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *termsAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"lbl_Terms_Condition", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        WebVC *webVC = [storyboard instantiateViewControllerWithIdentifier:@"webVC"];
        NSString *urlString = [[NSUserDefaults standardUserDefaults] objectForKey:TERMS_CONDITION];
        if(urlString.length<=0){
            urlString = [[NSUserDefaults standardUserDefaults] objectForKey:COMPANY_WEBSITE];
        }
        webVC.urlString = urlString;
        webVC.fromMenu=NO;
        webVC.fromSignUp=YES;
        [webVC setHidesBottomBarWhenPushed:YES];
        UINavigationController *temp = [[UINavigationController alloc] initWithRootViewController:webVC];
        [self presentViewController:temp animated:YES completion:nil];
    }];
    
    UIAlertAction *privacyAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"lbl_Privacy", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        WebVC *webVC = [storyboard instantiateViewControllerWithIdentifier:@"webVC"];
        NSString *urlString = [[NSUserDefaults standardUserDefaults] objectForKey:PRIVACY];
        if(urlString.length<=0){
            urlString = [[NSUserDefaults standardUserDefaults] objectForKey:COMPANY_WEBSITE];
        }
        webVC.urlString = urlString;
        webVC.fromMenu=NO;
        webVC.fromSignUp=YES;
        [webVC setHidesBottomBarWhenPushed:YES];
        UINavigationController *temp = [[UINavigationController alloc] initWithRootViewController:webVC];
        [self presentViewController:temp animated:YES completion:nil];
    }];
    
    //    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:termsAction];
    [alertController addAction:privacyAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)btnSubmitDidPressed:(id)sender {
    [self dismissKeyboard];
    NSString *deviceToken = [[SharedMethod new] checkDeviceToken];
    if([self.txtName validate] && [self.txtIC validate] && [self.txtEmail validate] && [self.txtCity validate] && [self.txtDob validate] && [self.txtPostcode validate] && [self validateAccount]){
        //        NSLog(@"socialToken - %@",self.socialToken);
        if([[SharedMethod new] checkNetworkConnectivity]){
            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
            if(self.socialToken.length<=0 && [self.txtPassword validate] && [self.txtConfirmPassword validate]){
                //                NSDictionary *params = @{@"fullname"         : [self.txtName.text stringByTrimmingCharactersInSet:
                //                                                                [NSCharacterSet whitespaceCharacterSet]],
                //                                         @"nric"             : self.txtIC.text,
                //                                         @"card_number"      : self.cardNumber,
                //                                         @"card_code"        : self.cvvCode,
                //                                         @"email"            : [self.txtEmail.text stringByTrimmingCharactersInSet:
                //                                                                [NSCharacterSet whitespaceCharacterSet]],
                //                                         @"password"         : self.txtPassword.text,
                //                                         @"password_confirm" : self.txtConfirmPassword.text,
                //                                         @"gender"           : selectedGender,
                //                                         @"race"             : selectedRace,
                //                                         @"birthday"         : self.txtDob.text,
                //                                         @"city"             : self.txtCity.text,
                //                                         @"country_code"     : selectedCountry,
                //                                         @"state_code"       : selectedState,
                //                                         @"contact"          : self.txtPhone.text,
                //                                         @"address1"         : self.txtAddress.text,
                //                                         @"address2"         : self.txtAddress2.text,
                //                                         @"address3"         : self.txtAddress3.text,
                //                                         @"postcode"         : self.txtPostcode.text,
                //                                         @"platform"         : PLATFORM,
                //                                         @"push_token"       : deviceToken,
                //                                         @"token"            : [SharedMethod getTokenWithRoute:@"user/register" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                //                                         };
                
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                [params setObject:[self.txtName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"fullname"];
                [params setObject:self.txtIC.text forKey:@"nric"];
                [params setObject:self.cardNumber forKey:@"card_number"];
                [params setObject:self.cvvCode forKey:@"card_code"];
                [params setObject:[self.txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"email"];
                [params setObject:self.txtPassword.text forKey:@"password"];
                [params setObject:self.txtConfirmPassword.text forKey:@"password_confirm"];
                [params setObject:selectedGender forKey:@"gender"];
                [params setObject:selectedRace forKey:@"race"];
                [params setObject:[[SharedMethod new] convertDateTimeFormat:self.txtDob.text andInputFormat:@"dd MMM yyyy" andOutputFormat:@"yyyy-MM-dd"] forKey:@"birthday"];
                [params setObject:self.txtCity.text forKey:@"city"];
                [params setObject:selectedCountry forKey:@"country_code"];
                [params setObject:selectedState forKey:@"state_code"];
                [params setObject:self.txtPhone.text forKey:@"contact"];
                [params setObject:self.txtAddress.text forKey:@"address1"];
                [params setObject:self.txtAddress2.text forKey:@"address2"];
                [params setObject:self.txtAddress3.text forKey:@"address3"];
                [params setObject:self.txtPostcode.text forKey:@"postcode"];
                [params setObject:PLATFORM forKey:@"platform"];
                [params setObject:deviceToken forKey:@"push_token"];
                [params setObject:[SharedMethod getTokenWithRoute:@"user/register" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
                
#ifdef DEBUG
                NSLog(@"Sign Up Param - %@",params);
#endif
                
                [WebAPI signUpNewUser:params andSuccess:^(id successBlock) {
                    [SVProgressHUD dismiss];
                    NSDictionary *result = successBlock;
                    if([result objectForKey:@"errMsg"]){
                        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    } else{
                        if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                            result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
                            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_SOCIAL];
                            [[PDKeychainBindings sharedKeychainBindings] setObject:[result objectForKey:@"secret"] forKey:SECRET_KEY];
                            [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"user"] forKey:USER_DETAIL];
                            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",[[[result objectForKey:@"user"] objectForKey:@"id"] intValue]] forKey:USER_ID];
                            [self dismissViewControllerAnimated:YES completion:^{
                                [self removeFromParentViewController];
                                [self.dismissLoginDelegate dismissLoginVC];
                            }];
                        } else{
                            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                        }
                    }
                }];
            } else{
                if([self.txtEmail.text isEqualToString:[self.socialDetails objectForKey:@"email"]]){
                    isVerified=YES;
                } else{
                    isVerified=NO;
                }
                
                NSString *route;
                
                if(self.isFB){
                    route = @"user/register/facebook";
                } else{
                    route = @"user/register/google";
                }
                
                /*NSDictionary *params = @{@"card_number"      : self.cardNumber,
                 @"card_code"        : self.cvvCode,
                 @"fullname"         : [[self.socialDetails objectForKey:@"name"]
                 stringByTrimmingCharactersInSet:
                 [NSCharacterSet whitespaceCharacterSet]],
                 @"email"            : [[self.socialDetails objectForKey:@"email"]
                 stringByTrimmingCharactersInSet:
                 [NSCharacterSet whitespaceCharacterSet]],
                 @"nric"             : self.txtIC.text,
                 @"provider_id"      : [self.socialDetails objectForKey:@"id"],
                 @"socialToken"      : self.socialToken,
                 @"gender"           : selectedGender,
                 @"race"             : selectedRace,
                 @"birthday"         : self.txtDob.text,
                 @"city"             : self.txtCity.text,
                 @"country_code"     : selectedCountry,
                 @"state_code"       : selectedState,
                 @"contact"          : self.txtPhone.text,
                 @"address1"         : self.txtAddress.text,
                 @"address2"         : self.txtAddress2.text,
                 @"address3"         : self.txtAddress3.text,
                 @"postcode"         : self.txtPostcode.text,
                 @"email_verified"   : [NSString stringWithFormat:@"%d",isVerified],
                 @"platform"         : PLATFORM,
                 @"push_token"       : deviceToken,
                 @"token"            : [SharedMethod getTokenWithRoute:route apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]]
                 };
                 */
                
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                [params setObject:[self.txtName.text
                                   stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]] forKey:@"fullname"];
                [params setObject:self.txtIC.text forKey:@"nric"];
                [params setObject:self.cardNumber forKey:@"card_number"];
                [params setObject:self.cvvCode forKey:@"card_code"];
                [params setObject:[self.txtEmail.text
                                   stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]] forKey:@"email"];
                [params setObject:[self.socialDetails objectForKey:@"id"] forKey:@"provider_id"];
                [params setObject:self.socialToken forKey:@"socialToken"];
                [params setObject:selectedGender forKey:@"gender"];
                [params setObject:selectedRace forKey:@"race"];
                [params setObject:[[SharedMethod new] convertDateTimeFormat:self.txtDob.text andInputFormat:@"dd MMM yyyy" andOutputFormat:@"yyyy-MM-dd"] forKey:@"birthday"];
                [params setObject:self.txtCity.text forKey:@"city"];
                [params setObject:selectedCountry forKey:@"country_code"];
                [params setObject:selectedState forKey:@"state_code"];
                [params setObject:self.txtPhone.text forKey:@"contact"];
                [params setObject:self.txtAddress.text forKey:@"address1"];
                [params setObject:self.txtAddress2.text forKey:@"address2"];
                [params setObject:self.txtAddress3.text forKey:@"address3"];
                [params setObject:self.txtPostcode.text forKey:@"postcode"];
                [params setObject:[NSString stringWithFormat:@"%d",isVerified] forKey:@"email_verified"];
                [params setObject:PLATFORM forKey:@"platform"];
                [params setObject:deviceToken forKey:@"push_token"];
                [params setObject:[SharedMethod getTokenWithRoute:route apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
                
#ifdef DEBUG
                NSLog(@"Prepare Sign Up With Social Params - %@",params);
#endif
                
                if(self.isFB){
                    [WebAPI signUpFacebookNewUser:params andSuccess:^(id successBlock) {
                        [SVProgressHUD dismiss];
                        NSDictionary *result = successBlock;
                        if([result objectForKey:@"errMsg"]){
                            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                        } else{
                            if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_SOCIAL];
                                [[PDKeychainBindings sharedKeychainBindings] setObject:[result objectForKey:@"secret"] forKey:SECRET_KEY];
                                [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"user"] forKey:USER_DETAIL];
                                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",[[[result objectForKey:@"user"] objectForKey:@"id"] intValue]] forKey:USER_ID];
                                [self dismissViewControllerAnimated:YES completion:^{
                                    [self removeFromParentViewController];
                                    [self.dismissLoginDelegate dismissLoginVC];
                                }];
                            } else{
                                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                            }                         }
                    }];
                } else{
                    [WebAPI signUpGoogleNewUser:params andSuccess:^(id successBlock) {
                        [SVProgressHUD dismiss];
                        NSDictionary *result = successBlock;
                        if([result objectForKey:@"errMsg"]){
                            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                        } else{
                            if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
                                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_SOCIAL];
                                [[PDKeychainBindings sharedKeychainBindings] setObject:[result objectForKey:@"secret"] forKey:SECRET_KEY];
                                [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"user"] forKey:USER_DETAIL];
                                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",[[[result objectForKey:@"user"] objectForKey:@"id"] intValue]] forKey:USER_ID];
                                [self dismissViewControllerAnimated:YES completion:^{
                                    [self removeFromParentViewController];
                                    [self.dismissLoginDelegate dismissLoginVC];
                                }];
                            } else{
                                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"message"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                            }
                        }
                    }];
                }
            }
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        }
    } else if(![self.txtName validate]){
        [self.txtName becomeFirstResponder];
    } else if(![self.txtIC validate]){
        [self.txtIC becomeFirstResponder];
    } else if(![self.txtPhone validate]){
        [self.txtPhone becomeFirstResponder];
    } else if(![self.txtEmail validate]){
        [self.txtEmail becomeFirstResponder];
    } else if(![self.txtDob validate]){
        [self.txtDob becomeFirstResponder];
    } else if(![self.txtCity validate]){
        [self.txtCity becomeFirstResponder];
    } else if(![self.txtPostcode validate]){
        [self.txtPostcode becomeFirstResponder];
    }
}

@end
