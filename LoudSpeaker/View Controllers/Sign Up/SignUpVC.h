//
//  SignUpVC.h
//  Oasis
//
//  Created by Wong Ryan on 25/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DismissLoginDelegate
@optional
-(void)dismissLoginVC;
@end

@interface SignUpVC : UIViewController<DismissLoginDelegate>
@property (nonatomic) NSDictionary *socialDetails;
@property (nonatomic) NSString *socialToken;
@property (nonatomic) BOOL isFB;
@property (nonatomic) NSString *cardNumber;
@property (nonatomic) NSString *cvvCode;
@property (nonatomic) id <DismissLoginDelegate> dismissLoginDelegate;
@end
