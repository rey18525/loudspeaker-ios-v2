//
//  QRCodeVC.m
//  Oasis
//
//  Created by Wong Ryan on 26/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "QRCodeVC.h"
#import "SharedMethod.h"

@interface QRCodeVC ()
@property (weak, nonatomic) IBOutlet UIView *qrView;
@property (weak, nonatomic) IBOutlet UIView *qrBG;
@property (weak, nonatomic) IBOutlet UIImageView *imgQRCode;
@end

@implementation QRCodeVC
//@synthesize imgQRCode;

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.qrBG setBackgroundColor:[UIColor whiteColor]];
        [[SharedMethod new] setCornerRadius:self.qrView andCornerRadius:self.qrView.frame.size.width/2];
        [self.view setBackgroundColor:[[SharedMethod new] colorWithHexString:@"000000" andAlpha:0.7]];
        [self.imgQRCode setImage:self.imgQR];
    });
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(CGSize)preferredContentSize{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
}

- (IBAction)btnDismissDidPressed:(id)sender {
//    for(UIView *temp in self.parentViewController.view.window.subviews){
//        if(temp.tag == 100){
//            [temp removeFromSuperview];
//        }
//    }
//    [self removeFromParentViewController];
}

@end
