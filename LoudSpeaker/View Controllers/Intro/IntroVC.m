//
//  IntroVC.m
//  Oasis
//
//  Created by Wong Ryan on 02/11/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "IntroVC.h"
#import "Constant.h"
#import "SharedMethod.h"

@interface IntroVC () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContent;
@property (strong, nonatomic) NSArray *pageImages;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentWidth;
@end

@implementation IntroVC{
    int currentIndex;
    NSMutableArray *screenWidth;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupDelegate];
    [self setupData];
    [self setupUI];
    [self setupScrollDetail];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark - Setup Initialization
-(void)setupDelegate{
    [self.scrollView setDelegate:self];
}

-(void)setupData{
    self.pageImages = @[@"img_intro_1.png", @"img_intro_2.png", @"img_intro_3.png", @"img_intro_4.png", @"img_intro_5.png"];
    currentIndex = 0;
    
    screenWidth = [[NSMutableArray alloc] init];
    CGFloat width = 0;
    for(int count=0;count<[self.pageImages count];count++){
        width += [UIScreen mainScreen].bounds.size.width;
        [screenWidth addObject:[NSString stringWithFormat:@"%f",width]];
    }
}

-(void)setupUI{
    [[self.btnSkip titleLabel] setFont:[UIFont fontWithName:FONT_BOLD size:15.0f]];
    [self.btnSkip setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
    [[SharedMethod new] setBorder:self.btnSkip borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    [self.pageControl setCurrentPageIndicatorTintColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
}

-(void)setupScrollDetail{
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.scrollContent setFrame:CGRectMake(0, 0, ([UIScreen mainScreen].bounds.size.width*[self.pageImages count]), self.scrollContent.frame.size.height)];
        [self.scrollContentWidth setConstant:([UIScreen mainScreen].bounds.size.width*[self.pageImages count])];
        [self.scrollView setContentSize:CGSizeMake(self.scrollContent.frame.size.width, self.scrollContent.frame.size.height)];
        
        CGFloat x=0;
        for(int count=0;count<[self.pageImages count];count++){
            UIImageView *imgIntro = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[self.pageImages objectAtIndex:count]]];
            [imgIntro setFrame:CGRectMake(x, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
            [imgIntro setContentMode:UIViewContentModeScaleAspectFit];
            x+=[UIScreen mainScreen].bounds.size.width;
            [self.scrollContent addSubview:imgIntro];
        }
    });
}

#pragma mark - Actions
- (IBAction)btnSkipDidPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_SECOND_RUN];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIScrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat viewWidth = _scrollView.frame.size.width;
    int pageNumber = floor((_scrollView.contentOffset.x - viewWidth/2) / viewWidth) +1;
    [self.pageControl setCurrentPage:pageNumber];
}


@end
