//
//  PrivilegeVC.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 09/12/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "PrivilegeVC.h"
#import "Constant.h"

@interface PrivilegeVC ()
@property (weak, nonatomic) IBOutlet UIView *scrollContent;
@property (weak, nonatomic) IBOutlet UIImageView *imgPrivilege;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentHeight;

@end

@implementation PrivilegeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self setupNavigationBar];
    [self setupUI];
}

#pragma mark - Setup Initialization
-(void)setupNavigationBar{
    [self.navigationItem setTitle:NSLocalizedString(@"nav_Privilege", nil)];
}

-(void)setupUI{
    NSLog(@"screen height - %f",[UIScreen mainScreen].bounds.size.height);
    if([UIScreen mainScreen].bounds.size.height == 568){
        [self.scrollContentHeight setConstant:2138];
        [self.imgPrivilege reloadInputViews];
        [self.imgPrivilege setNeedsLayout];
        [self.imgPrivilege setNeedsUpdateConstraints];
    } else if([UIScreen mainScreen].bounds.size.height==736){
        [self.scrollContentHeight setConstant:2766];
        [self.imgPrivilege reloadInputViews];
        [self.imgPrivilege setNeedsLayout];
        [self.imgPrivilege setNeedsUpdateConstraints];
    } else if([UIScreen mainScreen].bounds.size.height == 480){
        [self.scrollContentHeight setConstant:2138];
        [self.imgPrivilege reloadInputViews];
        [self.imgPrivilege setNeedsLayout];
        [self.imgPrivilege setNeedsUpdateConstraints];
    } else if([UIScreen mainScreen].bounds.size.height == 667){
        [self.scrollContentHeight setConstant:2506];
        [self.imgPrivilege reloadInputViews];
        [self.imgPrivilege setNeedsLayout];
        [self.imgPrivilege setNeedsUpdateConstraints];
    } else{
        [self.scrollContentHeight setConstant:2506];
        [self.imgPrivilege reloadInputViews];
        [self.imgPrivilege setNeedsLayout];
        [self.imgPrivilege setNeedsUpdateConstraints];
    }
}

#pragma mark - Action
- (IBAction)bbtnBackDidPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
