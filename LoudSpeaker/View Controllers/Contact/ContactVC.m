//
//  ContactVC.m
//  Oasis
//
//  Created by Wong Ryan on 29/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "ContactVC.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "WebAPI.h"
#import "SVProgressHUD.h"
#import "TextFieldValidator.h"
#import <UIView+Toast.h>
#import <PDKeychainBindings.h>

@interface ContactVC ()<UITextViewDelegate,UITextFieldDelegate>
@property (strong, nonatomic) IBOutletCollection(TextFieldValidator) NSArray *textfields;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtName;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtEmail;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtContact;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtMessage;
@property (weak, nonatomic) IBOutlet UITextView *tvDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;

@end

@implementation ContactVC{
    UIStoryboard *storyboard;
    NSArray *labelArray;
    NSString *selectedTopic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:NSLocalizedString(@"nav_Contact_V2", nil)];
    [self setupInitialization];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
        [self setupData];
    }
    
    [self setupUI];
    [self setupTextFieldValidation];
    [self setupDelegates];
}

#pragma mark - Setup Initialization
-(void)setupInitialization{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self setupDismissKeyboard];
    
    labelArray = @[NSLocalizedString(@"phd_Name", nil),
                   NSLocalizedString(@"phd_Email", nil),
                   NSLocalizedString(@"phd_Contact", nil),
                   NSLocalizedString(@"phd_Topic", nil),
                   NSLocalizedString(@"phd_Message", nil)
                   ];
    
    [self setupDoneToolbar:self.txtContact];
}

-(void)setupDelegates{
    [self.txtName setDelegate:self];
    [self.txtEmail setDelegate:self];
    [self.txtTitle setDelegate:self];
    [self.txtContact setDelegate:self];
    [self.tvDescription setDelegate:self];
}

-(void)setupTextFieldValidation{
    [self.txtEmail setValidateOnCharacterChanged:NO];
    [self.txtEmail updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    
    [self.txtName setValidateOnCharacterChanged:NO];
    [self.txtName updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    
    [self.txtTitle setValidateOnCharacterChanged:NO];
    [self.txtTitle updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    
    [self.txtContact setValidateOnCharacterChanged:NO];
    [self.txtContact updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
}

-(void)setupData{
    NSDictionary *userDetail = [[NSUserDefaults standardUserDefaults] objectForKey:USER_DETAIL];
    [self.txtName setText:[userDetail objectForKey:@"fullname"]];
    [self.txtEmail setText:[userDetail objectForKey:@"email"]];
    [self.txtContact setText:[userDetail objectForKey:@"contact"]];
}

-(void)setupDismissKeyboard{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(void)setupDoneToolbar:(UITextField*)selectedTextfield{
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolbar setBarStyle:UIBarStyleBlackOpaque];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"btn_Done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(bbtnDoneDidPressed:)];
    [doneButton setTag:2];
    [doneButton setTintColor:[UIColor whiteColor]];
    UIBarButtonItem *flexibleSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    toolbar.items = @[flexibleSeparator, doneButton];
    selectedTextfield.inputAccessoryView = toolbar;
}

-(void)setupUI{
    for(UILabel *lbl in self.labels){
        [lbl setText:[labelArray objectAtIndex:lbl.tag-1]];
        [lbl setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [lbl setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
    }
    
    for(UITextField *txt in self.textfields){
        [txt setPlaceholder:[labelArray objectAtIndex:txt.tag-1]];
        [txt setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [txt setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
        [[SharedMethod new] setBorder:txt borderColor:THEME_COLOR andBackgroundColor:BOTTOM_BORDER_COLOR];
        [[SharedMethod new] setPaddingToTextField:txt];
    }
    
    [self.tvDescription setText:NSLocalizedString(@"phd_Message", nli)];
    [[self.btnSend titleLabel] setText:NSLocalizedString(@"btn_Send", nil)];
    [self.btnSend setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
    [[self.btnSend titleLabel] setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self.tvDescription layer] setBorderWidth:1.0f];
        [[self.tvDescription layer] setBorderColor:[[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] CGColor]];
//        [self.tvDescription setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        [self.tvDescription setTextColor:[UIColor lightGrayColor]];
    });
    
    [[SharedMethod new] setBorder:self.btnSend borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)bbtnDoneDidPressed:(id)sender{
    [self dismissKeyboard];
}

- (IBAction)topicAction:(id)sender{
    NSMutableArray *topics = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:TOPICS]];
    
//    NSLog(@"Topicsss - %@",topics);
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ttl_Default", nil) message:NSLocalizedString(@"msg_Topic", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    for(int count=0;count<[topics count];count++){
        UIAlertAction *action = [UIAlertAction actionWithTitle:[[topics objectAtIndex:count] objectForKey:@"name"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.txtTitle setText:[[topics objectAtIndex:count] objectForKey:@"name"]];
            selectedTopic = [[topics objectAtIndex:count] objectForKey:@"id"];
        }];
        
        [alertController addAction:action];
    }
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)btnSendDidPressed:(id)sender {
    [self dismissKeyboard];
    if([self.txtName validate] && [self.txtEmail validate] && [self.txtTitle validate] && (self.tvDescription.text.length >0 && ![self.tvDescription.text isEqualToString:NSLocalizedString(@"phd_Message", nil)])){
        if([[SharedMethod new] checkNetworkConnectivity]){
            [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            
            if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
                [params setValue:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
            }
            
            [params setValue:PLATFORM forKey:@"platform"];
            [params setValue:[self.txtTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"subject"];
            [params setValue:selectedTopic forKey:@"category_id"];
            [params setValue:[self.txtName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"sender_name"];
            [params setValue:[self.txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"sender_email"];
            [params setValue:[self.txtContact.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"sender_contact"];
            [params setValue:[self.tvDescription.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"message"];
            [params setValue:[[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_TOKEN] forKey:@"push_token"];
            [params setValue:[SharedMethod getTokenWithRoute:@"user/enquiry" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
            
            #ifdef DEBUG
            NSLog(@"Send Enquiry Params - %@",params);
            #endif
            
            [WebAPI sendEnquiry:params andSuccess:^(id successBlock) {
                [SVProgressHUD dismiss];
                NSDictionary *result = successBlock;
                if([result objectForKey:@"errMsg"]){
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                } else{
                    result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"App_Name", nil) message:[result objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"btn_Ok", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    
                    [alertController addAction:okAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            }];
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        }
    } else if((self.txtName.text.length>0 && self.txtEmail.text.length>0 && self.txtContact.text.length>0) && (self.tvDescription.text.length<=0 || [self.tvDescription.text isEqualToString:NSLocalizedString(@"phd_Message", nil)])){
        [self.view makeToast:NSLocalizedString(@"msg_Message_Empty", nil)];
    }
}

#pragma mark - UITextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self dismissKeyboard];
    return YES;
}

#pragma mark - UITextView Delegate
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [self.tvDescription setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    if([textView.text isEqualToString:NSLocalizedString(@"phd_Message", nil)]){
        [self.tvDescription setText:@""];
    }
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    if(self.tvDescription.text.length<=0){
        [self.tvDescription setText:NSLocalizedString(@"phd_Message", nil)];
        [self.tvDescription setTextColor:[UIColor lightGrayColor]];
        [self dismissKeyboard];
    }
}

@end
