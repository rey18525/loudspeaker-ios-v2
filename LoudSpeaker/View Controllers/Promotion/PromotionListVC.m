#import "PromotionListVC.h"
#import "PromotionCell.h"
#import "SharedMethod.h"
#import "SVProgressHUD.h"
#import "Constant.h"
#import "WebAPI.h"
#import "SQLManager.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "PromotionDetailVC.h"
#import "SearchTableVC.h"
#import <PDKeychainBindings.h>

@interface PromotionListVC ()<UITableViewDelegate,UITableViewDataSource,UISearchResultsUpdating,UISearchBarDelegate,UISearchControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UISearchController *searchController;
@end

@import Firebase;

@implementation PromotionListVC{
    UIStoryboard *storyboard;
    NSMutableArray *promotionsArray;
    NSString *searchText;
    SearchTableVC *searchTableVC;
    int paging, totalCount;
    BOOL isSearch;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self retrievePosting];
    [self setupInitialization];
    [self setupDelegates];
    [self setupData];
    [self setupRefreshControl];
    [self setupSearchController];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - Setup Initialization
-(void)setupInitialization{
//    [self.navigationItem setTitle:NSLocalizedString(@"nav_Promotion_V2", nil)];
    [self.tabBarController setHidesBottomBarWhenPushed:YES];
    promotionsArray = [[NSMutableArray alloc] init];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.view setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

-(void)setupSearchController{
    UINavigationController *searchResultController = [storyboard instantiateViewControllerWithIdentifier:@"searchTableVC"];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultController];
    [self.searchController setSearchResultsUpdater:self];
    [self.searchController setDimsBackgroundDuringPresentation:YES];
    [[self.searchController searchBar] setDelegate:self];
    [self setDefinesPresentationContext:YES];
    [self.searchController.searchBar sizeToFit];
    
    [self.searchController.searchBar setTintColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
    [self.searchController.searchBar setBarTintColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
    self.searchController.searchBar.frame = CGRectMake(self.searchController.searchBar.frame.origin.x, self.searchController.searchBar.frame.origin.y, self.searchController.searchBar.frame.size.width, 44.0);
}

-(void)setupDelegates{
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
}

-(void)setupData{
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [self.navigationItem setTitle:[self.myTitle uppercaseString]];
    paging = 1;
    totalCount = 0;
    isSearch = NO;
}

-(void)setupRefreshControl{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl setTintColor:[UIColor blackColor]];
    [refreshControl addTarget:self action:@selector(refreshAction:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

-(void)retrievePosting{
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSString *deviceToken = [[SharedMethod new] checkDeviceToken];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:PLATFORM forKey:@"platform"];
        [params setObject:deviceToken forKey:@"push_token"];
        if(self.isPromo){
            [params setObject:[SharedMethod getTokenWithRoute:@"user/posts/promo" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        } else{
            [params setObject:[SharedMethod getTokenWithRoute:@"user/posts/event" apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
        }
        [params setObject:self.categoryID forKey:@"category_id"];
        [params setObject:PROMOTION_LOAD forKey:@"limit"];
        [params setObject:[NSString stringWithFormat:@"%d",paging] forKey:@"page"];
        [params setObject:@"order_by[0][0]=priority&order_by[0][1]=asc&order_by[1][0]=publish_at&order_by[1][1]=desc" forKey:@"order_by"];
        if([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGGED_IN]){
            [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        }
        if(self.isPromo){
            [params setObject:POST_TYPE_1 forKey:@"type"];
        }
        
        #ifdef DEBUG
        if(self.isPromo){
            NSLog(@"Retrieve Posting Params - %@",params);
        } else{
            NSLog(@"Retrieve Event Params - %@",params);
        }
        #endif
        
        if(self.isPromo){
            [WebAPI retrievePosting:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                promotionsArray = [[SQLManager new] retrievePromotions:self.isPromo];
                [self.tableView reloadData];
                if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                    [[SharedMethod new] removeProfileImage:YES];
                    [[SharedMethod new] removeProfileImage:NO];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                    [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                    [self.tabBarController setSelectedIndex:0];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            } else{
                if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                    totalCount = [[result objectForKey:@"total_items"] intValue];
                    promotionsArray = [[SharedMethod new] processPosting:[result objectForKey:@"posts"] andCurrentArray:promotionsArray];
                    [[SQLManager new] insertAndUpdatePromotions:promotionsArray];
                    
                    [self.tableView reloadData];
                }
            }
        }];
        } else{
            [WebAPI retrieveEvent:params andSuccess:^(id successBlock) {
                [SVProgressHUD dismiss];
                NSDictionary *result = successBlock;
                if([result objectForKey:@"errMsg"]){
                    [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
                    promotionsArray = [[SQLManager new] retrievePromotions:self.isPromo];
                    [self.tableView reloadData];
                    if([[result objectForKey:@"errCode"] isEqualToString:@"912"] || [[result objectForKey:@"errCode"] isEqualToString:@"-1011"]){
                        [[SharedMethod new] removeProfileImage:YES];
                        [[SharedMethod new] removeProfileImage:NO];
                        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:IS_LOGGED_IN];
                        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:USER_ID];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_DETAIL];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_POINT_DETAIL];
                        [[PDKeychainBindings sharedKeychainBindings] setObject:[[PDKeychainBindings sharedKeychainBindings] objectForKey:INITIAL_KEY] forKey:SECRET_KEY];
                        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:INBOX_COUNT];
                        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                        [self.tabBarController setSelectedIndex:0];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                } else{
                    if([[result objectForKey:@"code"] isEqualToString:@"000"]){
                        totalCount = [[result objectForKey:@"total_items"] intValue];
                        promotionsArray = [[SharedMethod new] processPosting:[result objectForKey:@"posts"] andCurrentArray:promotionsArray];
                        [[SQLManager new] insertAndUpdatePromotions:promotionsArray];
                        
                        [self.tableView reloadData];
                    }
                }
            }];
        }
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        promotionsArray = [[SQLManager new] retrievePromotions:self.isPromo];
        [self.tableView reloadData];
    }
}

#pragma mark - Actions
- (IBAction)bbtnCloseDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)bbtnSearchDidPressed:(id)sender {
    if(!isSearch){
        [self.tableView setTableHeaderView:self.searchController.searchBar];
        isSearch = YES;
    } else{
        [self.tableView setTableHeaderView:nil];
        isSearch = NO;
    }
}

-(void)refreshAction:(UIRefreshControl *)refreshControl {
    [refreshControl endRefreshing];
    paging=1;
    if([promotionsArray count]>0){
        [promotionsArray removeAllObjects];
        [self.tableView reloadData];
    }
    [self retrievePosting];
}

#pragma mark - UISearchController Delegate
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    //Do nothing//
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    searchText = searchBar.text;
    searchTableVC = (SearchTableVC *)self.searchController.searchResultsController;
    [self addChildViewController:searchTableVC];
    
    searchTableVC.view.frame = CGRectMake(searchTableVC.view.frame.origin.x, searchTableVC.view.frame.origin.y, searchTableVC.view.frame.size.width, searchTableVC.view.frame.size.height);
    searchTableVC.isPromotion = YES;
    searchTableVC.searchText = searchText;
    searchTableVC.promotionsArray = [[NSMutableArray alloc] init];
    [searchTableVC.tableView reloadData];
    [searchTableVC searchPromotion];
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchText = @"";
    [self.searchController.searchBar setText:@""];
    
    searchTableVC = (SearchTableVC *)self.searchController.searchResultsController;
    searchTableVC.promotionsArray = [[NSMutableArray alloc] init];
    [searchTableVC.tableView reloadData];
    
    isSearch = NO;
    
    [self.tableView setTableHeaderView:nil];
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [promotionsArray count];
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PromotionCell *cell = (PromotionCell*)[tableView dequeueReusableCellWithIdentifier:@"promotionCell"];
    if(cell==nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PromotionCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Promotion *promotion = [promotionsArray objectAtIndex:indexPath.row];

    [cell.contentsView setHidden:YES];
    [cell.lblMore setHidden:YES];
    [cell.imgMore setHidden:YES];
    
    if(promotion.image.length>0){
//        [[SharedMethod new] setLoadingIndicatorOnImage:cell.imgView];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [cell.imgView setContentMode:UIViewContentModeScaleAspectFit];
                [cell.imgView setClipsToBounds:YES];
                [cell.imgView sd_setImageWithURL:[NSURL URLWithString:promotion.image] placeholderImage:[UIImage imageNamed:@"bg_promotion"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    promotion.cachedImage = image;
                    for(UIView *view in cell.imgView.subviews){
                        if(view.tag==100){
                            [view removeFromSuperview];
                        }
                    }
                }];
            });
        });
    } else{
        [cell.imgView setImage:[UIImage imageNamed:@"bg_promotion"]];
    }
    
    [cell.lblTitle setText:promotion.title];
    [cell.lblDescription setText:promotion.subtitle];
    
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[SharedMethod new] setGradient:cell.imgView];
//    });
    
    if(indexPath.row == [promotionsArray count] - 1 && [promotionsArray count] < totalCount){
        if([[SharedMethod new] checkNetworkConnectivity]){
            paging+=1;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self retrievePosting];
                });
            });
        } else{
            [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"err_Error", nil) message:NSLocalizedString(@"msg_No_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didEndDisplayingCell:(PromotionCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.imgView.layer.sublayers = nil;
    cell.contentsView.layer.sublayers = nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Promotion *selectedPromotion = [promotionsArray objectAtIndex:indexPath.row];
    
    [[SharedMethod new] addStatistics:@"post_view" itemID:selectedPromotion.ID];
    PromotionDetailVC *promotionDetailVC = [storyboard instantiateViewControllerWithIdentifier:@"promotionDetailVC"];
    promotionDetailVC.isPromo = self.isPromo;
    promotionDetailVC.selectedPromotion = selectedPromotion;
    [self.navigationController pushViewController:promotionDetailVC animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ([UIScreen mainScreen].bounds.size.width/3)+110;
}

@end
