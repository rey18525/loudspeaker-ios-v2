//
//  PromotionListVC.h
//  Oasis
//
//  Created by Wong Ryan on 04/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Promotion.h"

@interface PromotionListVC : UIViewController
@property (assign) NSString *categoryID;
@property (assign) NSString *myTitle;
@property (nonatomic, assign) BOOL isPromo;
@end
