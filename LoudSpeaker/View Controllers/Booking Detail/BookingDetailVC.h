//
//  BookingDetailVC.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 20/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingDetailVC : UIViewController

@property (nonatomic) NSString *bookingId;

@end
