//
//  BookingDetailVC.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 20/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingDetailVC.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "BookingCell.h"
#import "WebAPI.h"
#import "SVProgressHUD.h"
#import "BookingModel.h"
#import "Outlet.h"
#import "SVProgressHUD.h"
#import "BookingParticipantsModel.h"
#import <PDKeychainBindingsController/PDKeychainBindings.h>

@interface BookingDetailVC ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation BookingDetailVC{
    NSArray *iconArray;
    NSMutableArray *titleArray, *countArray;
    NSString *bigMouthQty, *regularQty, *guestQty;
    BookingModel *bookingModel;
    Outlet *selectedOutlet;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupData];
    [self setupDelegate];
    [self retrieveBookingDetail];
}

-(void)setupData{
    [self.navigationItem setTitle:NSLocalizedString(@"nav_Booking_Detail", nil)];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    iconArray = @[[UIImage imageNamed:@"ic_booking_outlet"],[UIImage imageNamed:@"ic_booking_big_member"],[UIImage imageNamed:@"ic_booking_regular_member"],[UIImage imageNamed:@"ic_booking_non_member"],[UIImage imageNamed:@"ic_booking_date"],[UIImage imageNamed:@"ic_booking_time"]];
    
    NSArray *tempTitleArray = @[NSLocalizedString(@"phd_Booking_Outlet", nil),NSLocalizedString(@"phd_Booking_Big_Member", nil),NSLocalizedString(@"phd_Booking_Regular_Member", nil),NSLocalizedString(@"phd_Booking_Non_Member", nil),NSLocalizedString(@"phd_Booking_Date", nil),NSLocalizedString(@"phd_Booking_Time", nil)];
    titleArray = [[NSMutableArray alloc] initWithArray:tempTitleArray];
    
    countArray = [[NSMutableArray alloc] init];
    [countArray addObject:@""];
    [countArray addObject:@""];
    [countArray addObject:@""];
    [countArray addObject:@""];
    [countArray addObject:@""];
    [countArray addObject:@""];
    
    
    bigMouthQty = @"0";
    regularQty = @"0";
    guestQty = @"0";
}

-(void)setupDelegate{
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
}

-(Outlet*)retrieveOutletBasedOnOutletId:(NSString*)outletId{
    NSDictionary *tempDict = [[SharedMethod new] readFromFile:OUTLET_FILE];
    NSArray *outletArray = [[SharedMethod new] processOutlet:[tempDict objectForKey:@"outlets"]];
    
    for(Outlet *outlet in outletArray){
        NSString *tempID = [NSString stringWithFormat:@"%d",[outlet.ID intValue]];
        if([tempID isEqualToString:outletId]){
            return outlet;
        }
    }
    
    return nil;
}

-(NSString*)calculateQuantityForMemberType:(NSString*)member_type{
    NSArray *participants = bookingModel.participants;
    int count=0;
    for(BookingParticipantsModel *participant in participants){
        if([participant.member_type isEqualToString:member_type]){
            count+=1;
        }
    }
    return [NSString stringWithFormat:@"%d",count];
}

-(void)reconfigureData{
    if(bookingModel){
        selectedOutlet = [self retrieveOutletBasedOnOutletId:[NSString stringWithFormat:@"%d",[bookingModel.outlet_id intValue]]];
        [titleArray replaceObjectAtIndex:0 withObject:selectedOutlet.name];
        [countArray replaceObjectAtIndex:1 withObject:[self calculateQuantityForMemberType:@"big_mouth"]];
        [countArray replaceObjectAtIndex:2 withObject:[self calculateQuantityForMemberType:@"regular"]];
        [countArray replaceObjectAtIndex:3 withObject:[self calculateQuantityForMemberType:@"guest"]];
        [titleArray replaceObjectAtIndex:4 withObject:[[SharedMethod new] convertDateTimeFormat:bookingModel.booking_date andInputFormat:@"yyyy-MM-dd hh:mm:ss" andOutputFormat:@"yyyy-MM-dd"]];
        [titleArray replaceObjectAtIndex:5 withObject:[[SharedMethod new] convertDateTimeFormat:bookingModel.time_start andInputFormat:@"HH:mm:ss" andOutputFormat:@"hh:mm a"]];
//        [titleArray replaceObjectAtIndex:6 withObject:bookingModel.booking_session_name];
    }
}

-(void)retrieveBookingDetail {
    if([[SharedMethod new] checkNetworkConnectivity]){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"msg_Loading", nil)];
        NSString *route = [NSString stringWithFormat:@"user/booking/view/%@",self.bookingId];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:PLATFORM forKey:@"platform"];
        [params setObject:self.bookingId forKey:@"booking_id"];
        [params setObject:[[SharedMethod new] checkDeviceToken] forKey:@"push_token"];
        [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] forKey:@"userId"];
        [params setObject:[SharedMethod getTokenWithRoute:route apiKey:[[PDKeychainBindings sharedKeychainBindings] objectForKey:SECRET_KEY]] forKey:@"token"];
//        NSLog(@"Retrieve Booking Detail Param - %@",params);
        [WebAPI retrieveBookingDetail:params andSuccess:^(id successBlock) {
            [SVProgressHUD dismiss];
            NSDictionary *result = successBlock;
            if([result objectForKey:@"errMsg"]){
                [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Error", nil) message:[result objectForKey:@"errMsg"] cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
            } else{
                result = [[SharedMethod new] nestedDictionaryByReplacingNullsWithNil:result];
                bookingModel = [[BookingModel alloc] initWithAttributes:[result objectForKey:@"booking"]];
                [self reconfigureData];
            }
            [self.tableView reloadData];
        }];
    } else{
        [self presentViewController:[[SharedMethod new] setAndShowAlertController:NSLocalizedString(@"ttl_Internet", nil) message:NSLocalizedString(@"msg_Internet", nil) cancelButton:NSLocalizedString(@"btn_Ok", nil)] animated:YES completion:nil];
    }
}

#pragma mark - UITableView Data Source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [titleArray count]+3;
}

#pragma mark - UITableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BookingCell *cell = (BookingCell*)[tableView dequeueReusableCellWithIdentifier:@"bookingCell"];
    if(cell==nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BookingCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if(indexPath.row==0){
        [cell setupBookingIdView];
        [cell setupBookingIdDetails:bookingModel.booking_id andStatus:bookingModel.status];
    } else if(indexPath.row==1){
        [cell setupBookingIdView];
        [cell setupPricing:bookingModel.total];
    } else if(indexPath.row==2){
        [cell setupBookingIdView];
        [cell setupCreatedDate:@"Created at" andDateTime:[[SharedMethod new] convertDateTimeFormat:bookingModel.created_at andInputFormat:@"yyyy-MM-dd HH:mm:ss" andOutputFormat:@"yyyy-MM-dd hh:mm a"]];
    } else{//} if(indexPath.row>1 && indexPath.row < ([titleArray count])){
        [cell setupTitleViewAndHideTxt:YES];
        [cell setupTitleDetails:[iconArray objectAtIndex:indexPath.row-3] title:[titleArray objectAtIndex:indexPath.row-3] andCount:[countArray objectAtIndex:indexPath.row-3]];
    }
//    else{
//        [cell setupRemarkView];
//        [cell setupRemarkDetails:[iconArray objectAtIndex:indexPath.row-2] title:[titleArray objectAtIndex:indexPath.row-2] andRemark:bookingModel.remark];
//        [cell.tvRemark setEditable:NO];
//    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    NSLog(@"Selected row %d",(int)indexPath.row);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if(indexPath.row<8){
        return 40.0f;
//    } else {
//        return 120.0f;
//    }
}

#pragma mark - Actions
- (IBAction)bbtnBackDidPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
