//
//  EmptyCollectionCell.h
//  Oasis
//
//  Created by Wong Ryan on 19/09/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@end
