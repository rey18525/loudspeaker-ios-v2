//
//  EmptyCollectionCell.m
//  Oasis
//
//  Created by Wong Ryan on 19/09/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "EmptyCollectionCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation EmptyCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.lblDescription setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
    [self.lblDescription setTextColor:[[SharedMethod new] colorWithHexString:GREY_COLOR andAlpha:1.0f]];
}
@end
