//
//  BookingConfirmCell.h
//  LoudSpeaker
//
//  Created by Appandus on 01/08/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingConfirmCell : UITableViewCell

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (weak, nonatomic) IBOutlet UILabel *lblBookingTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBookingValue;

-(void)setupBookingView:(NSString*)title andValue:(NSString*)value;

@end
