//
//  BookingConfirmCell.m
//  LoudSpeaker
//
//  Created by Appandus on 01/08/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingConfirmCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation BookingConfirmCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    for(UILabel *lbl in self.labels){
        [lbl setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        if([UIScreen mainScreen].bounds.size.height <= 568){
            [lbl setFont:[UIFont fontWithName:FONT_REGULAR size:12.5f]];
        } else{
            [lbl setFont:[UIFont fontWithName:FONT_REGULAR size:14.5f]];
        }
    }
}

-(void)setupBookingView:(NSString*)title andValue:(NSString*)value{
    [self.lblBookingTitle setText:title];
    if(value.length<=0){
        [self.lblBookingValue setText:@"-"];
    } else{
        [self.lblBookingValue setText:value];
    }
}


    
@end
