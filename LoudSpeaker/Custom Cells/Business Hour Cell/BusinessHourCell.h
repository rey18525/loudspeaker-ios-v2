//
//  BusinessHourCell.h
//  Oasis
//
//  Created by Wong Ryan on 12/10/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusinessHourCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDay;
@property (weak, nonatomic) IBOutlet UILabel *lblBusinessHour;

@end
