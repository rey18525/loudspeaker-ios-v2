//
//  BusinessHourCell.m
//  Oasis
//
//  Created by Wong Ryan on 12/10/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "BusinessHourCell.h"
#import "Constant.h"
#import "SharedMethod.h"

@implementation BusinessHourCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self.lblDay setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblDay setFont:[UIFont fontWithName:FONT_REGULAR size:11.5f]];
    
    [self.lblBusinessHour setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblBusinessHour setFont:[UIFont fontWithName:FONT_REGULAR size:11.5f]];
    
}

@end
