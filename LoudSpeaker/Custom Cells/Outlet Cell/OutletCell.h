//
//  OutletCell.h
//  Oasis
//
//  Created by Wong Ryan on 29/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OutletCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgOutlet;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UIView *borderView;

@end
