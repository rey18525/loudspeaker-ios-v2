//
//  OutletCell.m
//  Oasis
//
//  Created by Wong Ryan on 29/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "OutletCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation OutletCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [[SharedMethod new] setBorder:self.borderView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    [self.borderView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
    
    [self.lblAddress setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblAddress setFont:[UIFont fontWithName:FONT_REGULAR size:12.0f]];
    
    [self.lblDistance setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblDistance setFont:[UIFont fontWithName:FONT_REGULAR size:11.0f]];
}

@end
