//
//  PromotionDetailCell.m
//  Oasis
//
//  Created by Wong Ryan on 23/08/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "PromotionDetailCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation PromotionDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setFont:[UIFont fontWithName:FONT_BOLD size:17.2f]];
    
    [self.lblSubtitle setHidden:YES];
    [self.separatorView setHidden:YES];
    
    [self.webView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    
//    [self.lblSubtitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
//    [self.lblSubtitle setFont:[UIFont fontWithName:FONT_BOLD size:12.0f]];
    
//    [[SharedMethod new] setCornerRadius:self.separatorView andCornerRadius:10.0f];
   
    
    
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

@end
