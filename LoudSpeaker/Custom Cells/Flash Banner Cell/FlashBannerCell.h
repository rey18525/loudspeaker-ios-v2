#import <UIKit/UIKit.h>

@interface FlashBannerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlay;

@end
