#import "FlashBannerCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation FlashBannerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.imgBanner setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

@end
