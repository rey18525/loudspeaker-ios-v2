//
//  ProfileBalanceCell.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 20/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileBalanceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblBalance;
@property (weak, nonatomic) IBOutlet UILabel *lblBalanceTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnTopUp;
@property (weak, nonatomic) IBOutlet UIButton *btnWalletQr;
@property (weak, nonatomic) IBOutlet UIView *myBackground;

-(void)setupBalance:(NSString*)balance;

@end
