//
//  ProfileBalanceCell.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 19/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "ProfileBalanceCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation ProfileBalanceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self setupUI];
}

-(void)setupUI{
    [self.lblBalance setTextColor:[[SharedMethod new] colorWithHexString:LABEL_COLOR andAlpha:1.0f]];
    [self.lblBalance setFont:[UIFont fontWithName:FONT_BOLD size:25.0f]];
    
    [self.lblBalanceTitle setTextColor:[[SharedMethod new] colorWithHexString:LABEL_COLOR andAlpha:1.0f]];
    [self.lblBalanceTitle setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
    
    [self.btnTopUp setBackgroundColor:[[SharedMethod new] colorWithHexString:MEDIUM_GREEN_COLOR andAlpha:1.0f]];
    [self.btnTopUp setTitleColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
    [[self.btnTopUp titleLabel] setFont:[UIFont fontWithName:FONT_REGULAR size:16.5f]];
    [[self.btnTopUp layer] setCornerRadius:8.0f];
    [self.btnTopUp setClipsToBounds:YES];
    [self.btnTopUp setTitle:NSLocalizedString(@"btn_Top_Up", nil) forState:UIControlStateNormal];
    
    [self.btnWalletQr setBackgroundColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
    [self.btnWalletQr setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
    [[self.btnWalletQr titleLabel] setFont:[UIFont fontWithName:FONT_REGULAR size:15.5f]];
    [[self.btnWalletQr layer] setBorderColor:[[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] CGColor]];
    [[self.btnWalletQr layer] setBorderWidth:1.0f];
    [[self.btnWalletQr layer] setCornerRadius:8.0f];
    [self.btnWalletQr setClipsToBounds:YES];
    [self.btnWalletQr setTitle:NSLocalizedString(@"btn_Payment_Qr", nil) forState:UIControlStateNormal];
}

-(void)setupBalance:(NSString *)balance{
    [self.lblBalance setText:[NSString stringWithFormat:@"RM%@", balance]];
}

@end
