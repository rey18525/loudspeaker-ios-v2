//
//  MenuDetailCell.m
//  Oasis
//
//  Created by Wong Ryan on 04/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "MenuDetailCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation MenuDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
        [self.webView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
//        [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        
    });
//    [self.imgView setClipsToBounds:YES];
//    [[self.imgView layer] setCornerRadius:5.0f];
//    [[self.imgView layer] setMasksToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
