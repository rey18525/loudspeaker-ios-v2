//
//  BookingCell.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 20/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (weak, nonatomic) IBOutlet UIView *titleView;

@property (weak, nonatomic) IBOutlet UIImageView *imgRemarkIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblRemarkTitle;
@property (weak, nonatomic) IBOutlet UITextView *tvRemark;
@property (weak, nonatomic) IBOutlet UIView *remarkView;

@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIButton *btnQuestion;

@property (weak, nonatomic) IBOutlet UIView *buttonView;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property (weak, nonatomic) IBOutlet UIView *bookingView;
@property (weak, nonatomic) IBOutlet UILabel *lblBookingId;
@property (weak, nonatomic) IBOutlet UILabel *lblBookingStatus;

-(void)setupTitleViewAndHideTxt:(BOOL)enableTextField;
-(void)setupRemarkView;
-(void)setupButtonView;
-(void)setupBookingIdView;

-(void)setupTitleDetails:(UIImage*)icon title:(NSString*)title andCount:(NSString*)count;
-(void)setupRemarkDetails:(UIImage*)icon title:(NSString*)title andRemark:(NSString*)remark;

-(void)updateSelectedValue:(NSString*)value;
-(void)setupBookingIdDetails:(NSString*)bookingId andStatus:(NSString*)bookingStatus;

-(void)setupPricing:(NSString*)price;
-(void)setupCreatedDate:(NSString*)createdDate andDateTime:(NSString*)dateTime;
@end
