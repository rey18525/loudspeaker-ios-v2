//
//  BookingCell.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 20/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation BookingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self.titleView setHidden:YES];
    [self.remarkView setHidden:YES];
    [self.buttonView setHidden:YES];
    [self.bookingView setHidden:YES];
}

-(void)setupTitleViewAndHideTxt:(BOOL)enableTextField{
    [self.titleView setHidden:NO];
    [self.remarkView setHidden:YES];
    [self.buttonView setHidden:YES];
    [self.bookingView setHidden:YES];
    [self.btnQuestion setHidden:YES];
    
    [self.txtTitle setHidden:enableTextField];
    [self.separatorView setHidden:NO];
    [self.imgIcon setContentMode:UIViewContentModeScaleAspectFit];
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:15.0f]];
    [self.txtTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.txtTitle setFont:[UIFont fontWithName:FONT_REGULAR size:15.0f]];
    [self.lblCount setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblCount setFont:[UIFont fontWithName:FONT_REGULAR size:15.0f]];
}

-(void)setupRemarkView{
    [self.titleView setHidden:YES];
    [self.remarkView setHidden:NO];
    [self.buttonView setHidden:YES];
    [self.bookingView setHidden:YES];
    [self.separatorView setHidden:NO];
    
    [self.imgRemarkIcon setContentMode:UIViewContentModeScaleAspectFit];
    [self.lblRemarkTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblRemarkTitle setFont:[UIFont fontWithName:FONT_REGULAR size:15.0f]];
    [self.tvRemark setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.tvRemark setFont:[UIFont fontWithName:FONT_REGULAR size:13.0f]];
    [[self.tvRemark layer] setBorderColor:[[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] CGColor]];
    [[self.tvRemark layer] setBorderWidth:1.0f];
}

-(void)setupButtonView{
    [self.titleView setHidden:YES];
    [self.remarkView setHidden:YES];
    [self.buttonView setHidden:NO];
    [self.separatorView setHidden:YES];
    [self.bookingView setHidden:YES];
    
    [self.btnNext setTitle:NSLocalizedString(@"btn_Next", nil) forState:UIControlStateNormal];
    [self.btnNext setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
    [[self.btnNext titleLabel] setFont:[UIFont fontWithName:FONT_REGULAR size:15.0f]];
    [[self.btnNext layer] setBorderColor:[[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] CGColor]];
    [[self.btnNext layer] setBorderWidth:1.0f];
}

-(void)setupBookingIdView{
    [self.titleView setHidden:YES];
    [self.remarkView setHidden:YES];
    [self.buttonView setHidden:YES];
    [self.separatorView setHidden:NO];
    [self.bookingView setHidden:NO];
    
    [self.lblBookingId setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblBookingId setFont:[UIFont fontWithName:FONT_REGULAR size:15.0f]];
    [self.lblBookingId setText:@""];
    
    [self.lblBookingStatus setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblBookingStatus setFont:[UIFont fontWithName:FONT_REGULAR size:15.0f]];
    [self.lblBookingStatus setText:@""];
}

-(void)setupTitleDetails:(UIImage*)icon title:(NSString*)title andCount:(NSString*)count{
    [self.imgIcon setImage:icon];
    [self.lblTitle setText:title];
    [self.lblCount setText:count];
}

-(void)setupRemarkDetails:(UIImage*)icon title:(NSString*)title andRemark:(NSString*)remark{
    [self.imgRemarkIcon setImage:icon];
    [self.lblRemarkTitle setText:title];
    [self.tvRemark setText:remark];
}

-(void)updateSelectedValue:(NSString*)value{
    [self.lblTitle setText:value];
}

-(void)setupBookingIdDetails:(NSString*)bookingId andStatus:(NSString*)bookingStatus{
    [self.lblBookingId setText:[NSString stringWithFormat:@"%@ #%@",NSLocalizedString(@"lbl_Booking_Id", nil), bookingId]];
    [self.lblBookingStatus setText:bookingStatus];
    
    if(bookingStatus.length>0){
        [self.lblBookingStatus setText:[NSString stringWithFormat:@"%@%@",[[bookingStatus substringToIndex:1] uppercaseString],[bookingStatus substringFromIndex:1]]];
    }
    if([[bookingStatus lowercaseString] isEqualToString:@"paid"] || [[bookingStatus lowercaseString] containsString:@"complete"]){
        [self.lblBookingStatus setTextColor:[[SharedMethod new] colorWithHexString:MEDIUM_GREEN_COLOR andAlpha:1.0f]];
    } else if([[bookingStatus lowercaseString] containsString:@"cancel"]){
        [self.lblBookingStatus setTextColor:[[SharedMethod new] colorWithHexString:RED_COLOR andAlpha:1.0f]];
    } else{
        [self.lblBookingStatus setTextColor:[[SharedMethod new] colorWithHexString:LABEL_COLOR andAlpha:1.0f]];
    }
}

-(void)setupCreatedDate:(NSString*)createdDate andDateTime:(NSString*)dateTime{
    [self.lblBookingId setText:createdDate];
    [self.lblBookingStatus setText:dateTime];
}

-(void)setupPricing:(NSString*)price{
    [self.lblBookingId setText:[NSString stringWithFormat:@"%@",NSLocalizedString(@"lbl_Total", nil)]];
    [self.lblBookingStatus setText:[NSString stringWithFormat:@"RM%@",price]];
}

@end
