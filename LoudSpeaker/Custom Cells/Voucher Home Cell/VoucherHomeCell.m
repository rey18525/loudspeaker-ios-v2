//
//  VoucherHomeCell.m
//  Oasis
//
//  Created by Wong Ryan on 30/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "VoucherHomeCell.h"
#import "VoucherCollectionCell.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "VoucherListVC.h"
#import "Voucher.h"
#import "Campaign.h"
#import "SDWebImage/UIImageView+WebCache.h"

@interface VoucherHomeCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,Voucher_Delegate>

@end

@implementation VoucherHomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self.paddingBG setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

-(void)setupCollectionView{
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
    [self.collectionView setShowsHorizontalScrollIndicator:NO];
    [self.collectionView setShowsVerticalScrollIndicator:NO];
    [self.collectionView registerNib:[UINib nibWithNibName:@"VoucherCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"voucherCell"];
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.campaignsArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    VoucherCollectionCell *cell = (VoucherCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"voucherCell" forIndexPath:indexPath];    
    [cell setBackgroundColor:[UIColor clearColor]];
    
    Campaign *campaign = [self.campaignsArray objectAtIndex:indexPath.row];
    Voucher *voucher = campaign.voucher;
    
    int maximum = [VOUCHER_LIMIT intValue];
    
    if(indexPath.row == maximum-1){
        [cell.contentsView setHidden:NO];
        [cell.contentsView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:0.05f]];
        [cell.lblMore setHidden:NO];
        [cell.lblMore setText:NSLocalizedString(@"lbl_More", nil)];
        [cell.lblDescription setText:@""];
        [cell.imgMore setHidden:NO];
        [cell.imgView setHidden:NO];
        [cell.imgView setImage:[UIImage imageNamed:@"bg_voucher.png"]];
    } else{
        [cell.contentsView setHidden:YES];
        [cell.lblMore setHidden:YES];
        [cell.imgMore setHidden:YES];
        [cell.lblDescription setText:voucher.title];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [cell.imgView setContentMode:UIViewContentModeScaleAspectFit];
                [cell.imgView setClipsToBounds:YES];
                [cell.imgView sd_setImageWithURL:[NSURL URLWithString:voucher.image] placeholderImage:[UIImage imageNamed:@"bg_voucher.png"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    voucher.cachedImage = image;
                }];
            });
        });

    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(VoucherCollectionCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    cell.imgView.layer.sublayers = nil;
    for(UIView *view in cell.imgView.subviews){
        if(view.tag==100){
            [view removeFromSuperview];
        }
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    int maximum = [VOUCHER_LIMIT intValue];
    if(indexPath.row == maximum-1){
        [self.voucherDelegate viewMoreVouchers];
    } else{
        [self.voucherDelegate selectVoucher:(int)indexPath.row];
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(200, 130);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 8, 0, 8);
}


@end
