//
//  VoucherHomeCell.h
//  Oasis
//
//  Created by Wong Ryan on 30/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol Voucher_Delegate <NSObject>
@optional
-(void)selectVoucher:(int)index;
-(void)viewMoreVouchers;
@end

@interface VoucherHomeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *paddingBG;
@property (nonatomic) NSMutableArray *vouchersArray;
@property (nonatomic) NSMutableArray *campaignsArray;
-(void)setupCollectionView;
@property (nonatomic) id <Voucher_Delegate> voucherDelegate;
@end
