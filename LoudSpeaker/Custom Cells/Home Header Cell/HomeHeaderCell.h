//
//  HomeHeaderCell.h
//  Oasis
//
//  Created by Wong Ryan on 30/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIView *seperatorView;

@end
