//
//  HomeHeaderCell.m
//  Oasis
//
//  Created by Wong Ryan on 30/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "HomeHeaderCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation HomeHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self setupUI];
}

-(void)setupUI{
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.lblTitle setText:NSLocalizedString(@"lbl_Promotion", nil)];
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setFont:[UIFont fontWithName:FONT_BOLD size:18.0f]];
}

@end
