//
//  TransactionDetailCell.m
//  Oasis
//
//  Created by Wong Ryan on 28/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "TransactionDetailCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation TransactionDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblSemicolon setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblDescription setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
    [self.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
    [self.lblSemicolon setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
    [self.lblDescription setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
}

@end
