//
//  MenuCell.m
//  Oasis
//
//  Created by Wong Ryan on 30/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "MenuCell.h"
#import "Constant.h"
#import "SharedMethod.h"

@implementation MenuCell

- (void)awakeFromNib {
    [super awakeFromNib];

    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:11.2f]];
    [self.lblTitle setNumberOfLines:2];
    [[SharedMethod new] setBorder:self borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.animationView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.lblTitle setTextAlignment:NSTextAlignmentCenter];
}

@end
