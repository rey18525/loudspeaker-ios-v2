//
//  MenuCell.h
//  Oasis
//
//  Created by Wong Ryan on 30/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CSAnimationView.h>

@interface MenuCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet CSAnimationView *animationView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon1;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon2;
@property (weak, nonatomic) IBOutlet UIView *borderBottom;
@property (weak, nonatomic) IBOutlet UIView *borderRight;
@property (weak, nonatomic) IBOutlet UIView *borderLeft;

@end
