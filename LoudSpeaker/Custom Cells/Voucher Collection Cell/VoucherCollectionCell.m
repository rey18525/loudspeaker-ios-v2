//
//  VoucherCell.m
//  Oasis
//
//  Created by Wong Ryan on 29/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "VoucherCollectionCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation VoucherCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
//    [[SharedMethod new] setCornerRadius:self.animationView andCornerRadius:10.0f];
}

-(void)setupUI{
    [[SharedMethod new] setBorder:self borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.lblDescription setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblDescription setFont:[UIFont fontWithName:FONT_REGULAR size:13.0f]];
}

@end
