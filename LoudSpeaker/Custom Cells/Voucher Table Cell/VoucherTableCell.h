//
//  VoucherTableCell.h
//  Oasis
//
//  Created by Wong Ryan on 04/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSAnimationView.h"

@interface VoucherTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgVoucher;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet CSAnimationView *contentsView;
@property (weak, nonatomic) IBOutlet UIView *expireView;
@property (weak, nonatomic) IBOutlet UILabel *lblExpire;
@property (weak, nonatomic) IBOutlet UILabel *lblExpireDate;
@property (weak, nonatomic) IBOutlet UIImageView *imgUsed;

@end
