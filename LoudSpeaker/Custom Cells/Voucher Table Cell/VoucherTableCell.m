//
//  VoucherTableCell.m
//  Oasis
//
//  Created by Wong Ryan on 04/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "VoucherTableCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation VoucherTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self setupUI];
}

-(void)setupUI{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SharedMethod new] setBorder:self.contentsView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
        [[self.expireView layer] setCornerRadius:self.expireView.frame.size.width/2];
    });
    
    [self.expireView setClipsToBounds:YES];
    
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.contentsView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    
//    [[SharedMethod new] setBorder:self.expireView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    [self.expireView setBackgroundColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:0.8f]];
    
    [self.lblExpireDate setTextColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
    [self.lblExpireDate setFont:[UIFont fontWithName:FONT_REGULAR size:12.0f]];
    
    [self.lblExpire setTextColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
    [self.lblExpire setFont:[UIFont fontWithName:FONT_REGULAR size:12.0f]];
    
    [self.lblDescription setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblDescription setFont:[UIFont fontWithName:FONT_REGULAR size:16.0f]];

}

@end
