#import <UIKit/UIKit.h>
@protocol Flash_Delegate <NSObject>
@optional
-(void)selectFlash:(int)index;
-(void)viewMoreFlash;
@end


@interface FlashHomeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic) NSMutableArray *flashArray;
//@property (nonatomic) BOOL isFlash;
-(void)setupCollectionView;
@property (nonatomic) id <Flash_Delegate> flashDelegate;
@end
