#import "FlashHomeCell.h"
#import "FlashCollectionCell.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "Flash.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface FlashHomeCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,Flash_Delegate>
@end

@implementation FlashHomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

-(void)setupCollectionView{
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
    [self.collectionView setShowsHorizontalScrollIndicator:NO];
    [self.collectionView setShowsVerticalScrollIndicator:NO];
    [self.collectionView registerNib:[UINib nibWithNibName:@"FlashCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"flashCell"];
    [self.collectionView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.flashArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FlashCollectionCell *cell = (FlashCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"flashCell" forIndexPath:indexPath];
    
    Flash *flash = [self.flashArray objectAtIndex:indexPath.row];
    Post *post = flash.post;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell.imgFlash setContentMode:UIViewContentModeScaleToFill];
            [cell.imgFlash setClipsToBounds:YES];
            [cell.imgFlash sd_setImageWithURL:[NSURL URLWithString:post.image] placeholderImage:[UIImage imageNamed:@"bg_menu"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            }];
        });
    });
    
    [cell.lblTitle setText:flash.name];
    
    //NSTimeInterval currentTime = ([[NSDate date] timeIntervalSince1970]);
    //double expiryDateTime = [flash.expiry_date_millisecond doubleValue];
    double remainingTime = [flash.expiry_date_millisecond doubleValue] - ([[NSDate date] timeIntervalSince1970]);
    
    if(remainingTime > 0) {
        remainingTime =  remainingTime/1000;
        NSInteger mSeconds = fmod(remainingTime, 60);
        NSInteger mMinutes = fmod((remainingTime / 60), 60);
        NSInteger mHours = fmod((remainingTime / (60*60)), 60);
        NSInteger mDays = fmod(remainingTime / ((60*60)*24), 24);

        if(mDays < 10) {
            [cell.lblDaysNum setText:[NSString stringWithFormat:@"0%ld", mDays]];
        } else {
            [cell.lblDaysNum setText:[NSString stringWithFormat:@"%ld", mDays]];
        }
        
        if(mHours < 10) {
            [cell.lblHoursNum setText:[NSString stringWithFormat:@"0%ld", mHours]];
        } else {
            [cell.lblHoursNum setText:[NSString stringWithFormat:@"%ld", mHours]];
        }
        
        if(mMinutes < 10) {
            [cell.lblMinutesNum setText:[NSString stringWithFormat:@"0%ld", mMinutes]];
        } else {
            [cell.lblMinutesNum setText:[NSString stringWithFormat:@"%ld", mMinutes]];
        }
        
        if(mSeconds < 10) {
            [cell.lblSecondsNum setText:[NSString stringWithFormat:@"0%ld", mSeconds]];
        } else {
            [cell.lblSecondsNum setText:[NSString stringWithFormat:@"%ld", mSeconds]];
        }
    }
    
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.flashDelegate selectFlash:(int)indexPath.row];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(150, 180);
}

-(CGPoint)collectionView:(UICollectionView *)collectionView targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset{
    return CGPointMake(0, 0);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 8, 0, 8);
}

@end
