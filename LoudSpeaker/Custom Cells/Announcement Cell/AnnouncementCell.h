//
//  ExtraCell.h
//  Oasis
//
//  Created by Wong Ryan on 28/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnnouncementCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *contentsView;

@end
