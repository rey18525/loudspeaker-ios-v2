//
//  ExtraCell.m
//  Oasis
//
//  Created by Wong Ryan on 28/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "AnnouncementCell.h"

@implementation AnnouncementCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
