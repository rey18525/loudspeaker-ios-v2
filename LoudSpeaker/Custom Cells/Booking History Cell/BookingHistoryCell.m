//
//  BookingHistoryCell.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 20/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingHistoryCell.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "Outlet.h"

@implementation BookingHistoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [self.lblReference setTextColor: [[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblReference setFont:[UIFont fontWithName:FONT_REGULAR size:14.5f]];
    
    [self.lblStatus setTextColor: [[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblStatus setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
    
    [self.lblOutlet setTextColor: [[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblOutlet setFont:[UIFont fontWithName:FONT_REGULAR size:14.5f]];
    
    [self.lblDate setTextColor: [[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblDate setFont:[UIFont fontWithName:FONT_REGULAR size:14.5f]];
    
    [self.lblTime setTextColor: [[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTime setFont:[UIFont fontWithName:FONT_REGULAR size:14.5f]];
}

-(void)setupBookingHistoryWithData:(BookingModel *)bookingModel{
    Outlet *outlet = [self retrieveOutletById:[NSString stringWithFormat:@"%d",[bookingModel.outlet_id intValue]]];
    [self.lblReference setText:[NSString stringWithFormat:@"%@ #%@",NSLocalizedString(@"lbl_Booking_Id", nil), bookingModel.booking_id]];
    
    if(bookingModel.status.length>0){
        [self.lblStatus setText:[NSString stringWithFormat:@"%@%@",[[bookingModel.status substringToIndex:1] uppercaseString],[bookingModel.status substringFromIndex:1]]];
    }
    if([[bookingModel.status lowercaseString] isEqualToString:@"paid"] || [[bookingModel.status lowercaseString] containsString:@"complete"]){
        [self.lblStatus setTextColor:[[SharedMethod new] colorWithHexString:MEDIUM_GREEN_COLOR andAlpha:1.0f]];
    } else if([[bookingModel.status lowercaseString] containsString:@"cancel"]){
        [self.lblStatus setTextColor:[[SharedMethod new] colorWithHexString:RED_COLOR andAlpha:1.0f]];
    } else{
        [self.lblStatus setTextColor:[[SharedMethod new] colorWithHexString:LABEL_COLOR andAlpha:1.0f]];
    }
    
    [self.lblOutlet setText:outlet.name];
    [self.lblDate setText:[[SharedMethod new] convertDateTimeFormat:bookingModel.booking_date andInputFormat:@"yyyy-MM-dd hh:mm:ss" andOutputFormat:@"yyyy-MM-dd"]];
    [self.lblTime setText:[[SharedMethod new] convertDateTimeFormat:bookingModel.time_start andInputFormat:@"HH:mm:ss" andOutputFormat:@"hh:mm a"]];
}

-(Outlet*)retrieveOutletById:(NSString*)outletId{
    NSDictionary *tempDict = [[SharedMethod new] readFromFile:OUTLET_FILE];
    NSMutableArray *outletArray = [[NSMutableArray alloc] init];
    
    for(NSDictionary *temp in [tempDict objectForKey:@"outlets"]){
        Outlet *outlet = [[Outlet alloc] initWithAttributes:temp];
        [outletArray addObject:outlet];
    }
    
    for(Outlet *temp in outletArray){
        NSString *tempID = [NSString stringWithFormat:@"%d",[temp.ID intValue]];
        if([tempID isEqualToString:outletId]){
            return temp;
        }
    }
    return nil;
}

@end
