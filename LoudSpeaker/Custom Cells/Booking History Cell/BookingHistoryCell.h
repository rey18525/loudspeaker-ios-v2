//
//  BookingHistoryCell.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 20/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BookingModel.h"

@interface BookingHistoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblReference;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

-(void)setupBookingHistoryWithData:(BookingModel*)bookingModel;

@end
