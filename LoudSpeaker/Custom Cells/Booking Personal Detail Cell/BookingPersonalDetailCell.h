//
//  BookingPersonalDetailCell.h
//  LoudSpeaker
//
//  Created by Appandus on 01/08/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldValidator.h"

@interface BookingPersonalDetailCell : UITableViewCell

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (strong, nonatomic) IBOutletCollection(TextFieldValidator) NSArray *textfields;

@property (weak, nonatomic) IBOutlet UIStackView *personalDetailStackView;
@property (weak, nonatomic) IBOutlet UIView *personalTitleView;
@property (weak, nonatomic) IBOutlet UILabel *lblPersonalDetail;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtValue;
@property (weak, nonatomic) IBOutlet UIView *buttonView;
@property (weak, nonatomic) IBOutlet UIImageView *imgAgreement;
@property (weak, nonatomic) IBOutlet UIButton *btnAgreement;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsCondition;
@property (weak, nonatomic) IBOutlet UIView *termsBottomLineView;
@property (weak, nonatomic) IBOutlet UIButton *btnPay;

-(void)setupTitleView;
-(void)setupPersonalName:(NSString*)title andValue:(NSString*)value;
-(void)setupPersonalEmail:(NSString*)title andValue:(NSString*)value;
-(void)setupPersonalContact:(NSString*)title andValue:(NSString*)value;
-(void)setupButtons;

@end
