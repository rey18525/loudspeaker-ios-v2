//
//  BookingPersonalDetailCell.m
//  LoudSpeaker
//
//  Created by Appandus on 01/08/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingPersonalDetailCell.h"
#import "Constant.h"
#import "SharedMethod.h"

@implementation BookingPersonalDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self.personalDetailStackView setHidden:YES];
    [self.personalTitleView setHidden:YES];
    [self.buttonView setHidden:YES];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    for(UILabel *lbl in self.labels){
        [lbl setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        if([UIScreen mainScreen].bounds.size.height <= 568){
            [lbl setFont:[UIFont fontWithName:FONT_REGULAR size:12.5f]];
        } else{
            [lbl setFont:[UIFont fontWithName:FONT_REGULAR size:14.5f]];
        }
    }
    
    for(UITextField *txt in self.textfields){
        [[SharedMethod new] setupTextfield:txt fontType:FONT_REGULAR fontSize:14.0f textColor:THEME_COLOR textAlpha:1.0f addPadding:YES andAddBorder:YES];
    }
    
    [[self.imgAgreement layer] setBorderColor:[[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] CGColor]];
    [[self.imgAgreement layer] setBorderWidth:1.0f];
    [self.imgAgreement setBackgroundColor:[UIColor clearColor]];
    
    [self.btnTermsCondition setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
    [[self.btnTermsCondition titleLabel] setFont:[UIFont fontWithName:FONT_REGULAR size:15.0f]];
    
    [self.termsBottomLineView setBackgroundColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
    [[self.btnPay layer] setBorderColor:[[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] CGColor]];
    [[self.btnPay layer] setBorderWidth:1.0f];
    [self.btnPay setTitleColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] forState:UIControlStateNormal];
    [[self.btnPay titleLabel] setFont:[UIFont fontWithName:FONT_REGULAR size:14.5f]];
}

-(void)setupPersonalName:(NSString*)title andValue:(NSString*)value{
    [self.personalDetailStackView setHidden:NO];
    [self.personalTitleView setHidden:YES];
    [self.buttonView setHidden:YES];
    
    [self.txtValue setValidateOnCharacterChanged:NO];
    [self.txtValue updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtValue setIsMandatory:YES];
    [self.txtValue setAutocapitalizationType:UITextAutocapitalizationTypeWords];
    
    [self.lblTitle setText:title];
    [self.txtValue setText:value];
}

-(void)setupPersonalEmail:(NSString*)title andValue:(NSString*)value{
    [self.personalDetailStackView setHidden:NO];
    [self.personalTitleView setHidden:YES];
    [self.buttonView setHidden:YES];
    
    [self.txtValue setValidateOnCharacterChanged:NO];
    [self.txtValue addRegx:@"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" withMsg:NSLocalizedString(@"err_Invalid_Email", nil)];
    [self.txtValue updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtValue setIsMandatory:YES];
    
    [self.lblTitle setText:title];
    [self.txtValue setText:value];
}

-(void)setupPersonalContact:(NSString*)title andValue:(NSString*)value{
    [self.personalDetailStackView setHidden:NO];
    [self.personalTitleView setHidden:YES];
    [self.buttonView setHidden:YES];
    
    [self.txtValue setValidateOnCharacterChanged:NO];
    [self.txtValue updateLengthValidationMsg:NSLocalizedString(@"err_Input_Blank", nil)];
    [self.txtValue setIsMandatory:YES];
    
    [self.lblTitle setText:title];
    [self.txtValue setText:value];
}

-(void)setupTitleView{
    [self.personalDetailStackView setHidden:YES];
    [self.personalTitleView setHidden:NO];
    [self.buttonView setHidden:YES];
    
    [self.lblPersonalDetail setText:NSLocalizedString(@"lbl_Personal_Detail", nil)];
}

-(void)setupButtons{
    [self.personalDetailStackView setHidden:YES];
    [self.personalTitleView setHidden:YES];
    [self.buttonView setHidden:NO];
    
    [self.btnTermsCondition setTitle:NSLocalizedString(@"btn_Agree_Terms", nil) forState:UIControlStateNormal];
    [self.btnPay setTitle:NSLocalizedString(@"btn_Pay", nil) forState:UIControlStateNormal];
}

@end
