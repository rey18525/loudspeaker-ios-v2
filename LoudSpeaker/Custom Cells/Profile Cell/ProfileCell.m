//
//  ProfileCell.m
//  Oasis
//
//  Created by Wong Ryan on 25/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "ProfileCell.h"
#import "Constant.h"
#import "SharedMethod.h"

@implementation ProfileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.profileContent setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.profileContent setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.imgIcon setBackgroundColor:[UIColor clearColor]];
    
    [[SharedMethod new] setBorder:self.imageContentView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    [[SharedMethod new] setBorder:self.titleContentView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:17]];
    [self.lblCount setTextColor:[[SharedMethod new] colorWithHexString:DARK_GREY_1_COLOR andAlpha:1.0f]];
    [self.lblCount setFont:[UIFont fontWithName:FONT_REGULAR size:10]];
}

@end
