//
//  BannerCell.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 08/12/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KIImagePager.h>

@interface BannerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet KIImagePager *imagePager;

@end
