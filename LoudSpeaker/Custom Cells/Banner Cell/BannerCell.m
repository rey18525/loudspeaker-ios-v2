//
//  BannerCell.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 08/12/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "BannerCell.h"

@implementation BannerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
