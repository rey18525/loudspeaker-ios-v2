//
//  PromotionBannerCell.h
//  Oasis
//
//  Created by Wong Ryan on 23/08/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromotionBannerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlay;

@end
