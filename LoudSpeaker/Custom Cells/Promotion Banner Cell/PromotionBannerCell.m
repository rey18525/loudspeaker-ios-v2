//
//  PromotionBannerCell.m
//  Oasis
//
//  Created by Wong Ryan on 23/08/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "PromotionBannerCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation PromotionBannerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.imgBanner setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

@end
