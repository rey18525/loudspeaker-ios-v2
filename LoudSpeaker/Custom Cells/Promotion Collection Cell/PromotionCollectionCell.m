//
//  PromotionCollectionCell.m
//  LoudSpeaker
//
//  Created by Wong Ryan on 08/12/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "PromotionCollectionCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation PromotionCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [[SharedMethod new] setBorder:self borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    [self.imgPromotion setContentMode:UIViewContentModeScaleAspectFit];
    
    [self.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:13.0f]];
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
}

@end
