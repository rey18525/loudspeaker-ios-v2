//
//  PromotionCollectionCell.h
//  LoudSpeaker
//
//  Created by Wong Ryan on 08/12/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CSAnimationView.h>

@interface PromotionCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet CSAnimationView *animationView;
@property (weak, nonatomic) IBOutlet UIImageView *imgPromotion;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
