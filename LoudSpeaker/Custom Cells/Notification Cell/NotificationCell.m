//
//  NotificationCell.m
//  Oasis
//
//  Created by Wong Ryan on 26/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "NotificationCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation NotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SharedMethod new] setBorder:self.notificationView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
        [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
        [self.notificationView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    });
    
    [self.imgIndicator setBackgroundColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblDate setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblDescription setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:15.0f]];
    [self.lblDescription setFont:[UIFont fontWithName:FONT_REGULAR size:12.0f]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
