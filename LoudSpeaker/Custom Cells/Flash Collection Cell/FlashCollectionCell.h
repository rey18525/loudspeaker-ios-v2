#import <UIKit/UIKit.h>
#import <CSAnimationView.h>

@interface FlashCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet CSAnimationView *animationView;
@property (weak, nonatomic) IBOutlet UIImageView *imgFlash;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDaysNum;
@property (weak, nonatomic) IBOutlet UILabel *lblHoursNum;
@property (weak, nonatomic) IBOutlet UILabel *lblMinutesNum;
@property (weak, nonatomic) IBOutlet UILabel *lblSecondsNum;


@end
