#import "FlashCollectionCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation FlashCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [[SharedMethod new] setBorder:self borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    //[self.imgFlash setContentMode:UIViewContentModeScaleAspectFit];
    [self.imgFlash setContentMode:UIViewContentModeScaleToFill];
    
    [self.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:13.0f]];
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
    [self.lblDaysNum setFont:[UIFont fontWithName:FONT_REGULAR size:13.0f]];
    [self.lblDaysNum setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
    [self.lblHoursNum setFont:[UIFont fontWithName:FONT_REGULAR size:13.0f]];
    [self.lblHoursNum setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
    [self.lblMinutesNum setFont:[UIFont fontWithName:FONT_REGULAR size:13.0f]];
    [self.lblMinutesNum setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
    [self.lblSecondsNum setFont:[UIFont fontWithName:FONT_REGULAR size:13.0f]];
    [self.lblSecondsNum setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
}

@end
