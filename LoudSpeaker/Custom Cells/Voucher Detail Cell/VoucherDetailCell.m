//
//  VoucherDetailCell.m
//  Oasis
//
//  Created by Wong Ryan on 17/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "VoucherDetailCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation VoucherDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self setupUI];
}

-(void)setupUI{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SharedMethod new] setBorder:self.expireView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    });
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self.webView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.webView setOpaque:NO];
    
    [self.lblExpireDayTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblExpireDayTitle setFont:[UIFont fontWithName:FONT_REGULAR size:12.0f]];
    [self.lblExpireDayTitle setBackgroundColor:[UIColor clearColor]];
    
    [self.lblExpireDayValue setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblExpireDayValue setFont:[UIFont fontWithName:FONT_BOLD size:24.0f]];
    [self.lblExpireDayValue setBackgroundColor:[UIColor clearColor]];
    
    [self.lblExpireHourTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblExpireHourTitle setFont:[UIFont fontWithName:FONT_REGULAR size:12.0f]];
    [self.lblExpireHourTitle setBackgroundColor:[UIColor clearColor]];
    
    [self.lblExpireHourValue setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblExpireHourValue setFont:[UIFont fontWithName:FONT_BOLD size:24.0f]];
    [self.lblExpireHourValue setBackgroundColor:[UIColor clearColor]];
    
    [self.lblLeft setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblLeft setFont:[UIFont fontWithName:FONT_BOLD size:18.0f]];
    [self.lblLeft setBackgroundColor:[UIColor clearColor]];
    
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setFont:[UIFont fontWithName:FONT_BOLD size:15.0f]];
    [self.lblTitle setBackgroundColor:[UIColor clearColor]];
    
    [self.separatorView2 setBackgroundColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [[self.separatorView2 layer] setCornerRadius:2.0f];
}

@end
