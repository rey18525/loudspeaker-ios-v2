//
//  VoucherDetailCell.h
//  Oasis
//
//  Created by Wong Ryan on 17/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoucherDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *expireView;
@property (weak, nonatomic) IBOutlet UILabel *lblLeft;
@property (weak, nonatomic) IBOutlet UILabel *lblExpireDayTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblExpireDayValue;
@property (weak, nonatomic) IBOutlet UILabel *lblExpireHourTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblExpireHourValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIView *separatorView2;

@end
