//
//  TopUpCell.h
//  TKBakery
//
//  Created by Wong Ryan on 14/02/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopUpCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgBackground;

@end
