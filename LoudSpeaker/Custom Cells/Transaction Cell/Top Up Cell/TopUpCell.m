//
//  TopUpCell.m
//  TKBakery
//
//  Created by Wong Ryan on 14/02/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "TopUpCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation TopUpCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:20.0f]];
    
//    self.contentView.layer.cornerRadius = 10.0f;
//    self.contentView.layer.masksToBounds = YES;
    
    [[self.contentView layer] setBorderColor:[[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f] CGColor]];
//    [[self.contentView layer] setBorderWidth:1.0f];
    
//    self.layer.shadowColor = [UIColor lightGrayColor].CGColor;
//    self.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
//    self.layer.shadowRadius = 0.1f;
//    self.layer.shadowOpacity = 0.2f;
//    self.layer.masksToBounds = NO;
//    self.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, self.bounds.size.width-10, self.bounds.size.height-12) cornerRadius:self.contentView.layer.cornerRadius].CGPath;
    
}

@end
