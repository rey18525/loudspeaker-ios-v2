//
//  TransactionCell.m
//  Oasis
//
//  Created by Wong Ryan on 28/07/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "TransactionCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation TransactionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SharedMethod new] setBorder:self.transactionView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    });
    
    [self.transactionView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [self.lblDate setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblDescription setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblAmount setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    
    [self.lblDescription setFont:[UIFont fontWithName:FONT_REGULAR size:14.0f]];
    [self.lblAmount setFont:[UIFont fontWithName:FONT_REGULAR size:11.0f]];
    
    [self.lblAmount setTextAlignment:NSTextAlignmentRight];
}

@end
