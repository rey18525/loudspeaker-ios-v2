//
//  BookingPriceCell.m
//  LoudSpeaker
//
//  Created by Appandus on 01/08/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingPriceCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation BookingPriceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupUI];
}

-(void)setupUI{
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    for(UILabel *lbl in self.labels){
        [lbl setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
        if([UIScreen mainScreen].bounds.size.height <= 568){
            [lbl setFont:[UIFont fontWithName:FONT_REGULAR size:12.5f]];
        } else{
            [lbl setFont:[UIFont fontWithName:FONT_REGULAR size:14.5f]];
        }
    }
    
    for(UIView *view in self.priceSeparatorView){
        [view setBackgroundColor:[[SharedMethod new] colorWithHexString:BOTTOM_BORDER_COLOR andAlpha:1.0f]];
    }
}

-(void)setupPriceView:(NSString*)title quantity:(NSString*)quantity value:(NSString*)value andIsTotal:(BOOL)isTotal{
    if(!isTotal){
        [self.lblPriceQuantity setText:quantity];
        [self.lblColon setText:@":"];
        for(UIView *temp in self.priceSeparatorView){
            [temp setHidden:YES];
        }
    } else{
        [self.lblColon setText:@""];
        [self.lblPriceQuantity setText:@""];
        for(UIView *temp in self.priceSeparatorView){
            [temp setHidden:NO];
        }
    }
    
    [self.lblPriceTitle setText:title];
    [self.lblPriceValue setText:[NSString stringWithFormat:@"RM%@",value]];
}

@end
