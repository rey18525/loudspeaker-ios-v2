//
//  BookingPriceCell.h
//  LoudSpeaker
//
//  Created by Appandus on 01/08/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingPriceCell : UITableViewCell

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblColon;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *priceSeparatorView;
@property (weak, nonatomic) IBOutlet UIView *topSeparatorView;

-(void)setupPriceView:(NSString*)title quantity:(NSString*)quantity value:(NSString*)value andIsTotal:(BOOL)isTotal;

@end
