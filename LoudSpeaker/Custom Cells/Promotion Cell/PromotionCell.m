//
//  PromotionCell.m
//  Oasis
//
//  Created by Wong Ryan on 29/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "PromotionCell.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation PromotionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self setupUI];
}

-(void)setupUI{
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
    [[SharedMethod new] setBorder:self.animationView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
    [self.lblTitle setTextColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:1.0f]];
    [self.lblTitle setFont:[UIFont fontWithName:FONT_REGULAR size:15.0f]];
}

@end
