//
//  PromotionCell.h
//  Oasis
//
//  Created by Wong Ryan on 29/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CSAnimationView.h>

@interface PromotionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *contentsView;
@property (weak, nonatomic) IBOutlet UILabel *lblMore;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imgMore;
@property (weak, nonatomic) IBOutlet CSAnimationView *animationView;

@end
