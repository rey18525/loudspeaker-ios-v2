#import "PromotionHomeCell.h"
#import "PromotionCollectionCell.h"
#import "SharedMethod.h"
#import "Constant.h"
#import "Category.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PromotionHomeCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,Promotion_Delegate>
@end

@implementation PromotionHomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

-(void)setupCollectionView{
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
    [self.collectionView setShowsHorizontalScrollIndicator:NO];
    [self.collectionView setShowsVerticalScrollIndicator:NO];
    [self.collectionView registerNib:[UINib nibWithNibName:@"PromotionCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"promotionCell"];
    [self.collectionView setBackgroundColor:[[SharedMethod new] colorWithHexString:LIGHT_GREY_COLOR andAlpha:1.0f]];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.promotionsArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PromotionCollectionCell *cell = (PromotionCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"promotionCell" forIndexPath:indexPath];
    
    Category *category = [self.promotionsArray objectAtIndex:indexPath.row];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell.imgPromotion setContentMode:UIViewContentModeScaleAspectFit];
            [cell.imgPromotion setClipsToBounds:YES];
            [cell.imgPromotion sd_setImageWithURL:[NSURL URLWithString:category.image] placeholderImage:[UIImage imageNamed:@"bg_menu"] options:0 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                category.cachedImage = image;
            }];
        });
    });
    
    [cell.lblTitle setText:category.name];
    
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.promotionDelegate selectPromotion:(int)indexPath.row isPromo:self.isPromo];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(150, 180);
}

-(CGPoint)collectionView:(UICollectionView *)collectionView targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset{
    return CGPointMake(0, 0);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 8, 0, 8);
}

@end
