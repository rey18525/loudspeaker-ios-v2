#import <UIKit/UIKit.h>
@protocol Promotion_Delegate <NSObject>
@optional
-(void)selectPromotion:(int)index isPromo:(BOOL)result;
-(void)viewMorePromotion;
@end


@interface PromotionHomeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic) NSMutableArray *promotionsArray;
@property (nonatomic) BOOL isPromo;
-(void)setupCollectionView;
@property (nonatomic) id <Promotion_Delegate> promotionDelegate;
@end
