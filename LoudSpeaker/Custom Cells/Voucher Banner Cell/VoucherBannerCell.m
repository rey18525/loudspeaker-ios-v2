//
//  VoucherBannerCell.m
//  Oasis
//
//  Created by Wong Ryan on 17/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "VoucherBannerCell.h"
#import "Constant.h"
#import "SharedMethod.h"

@implementation VoucherBannerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self.lblExpire setText:NSLocalizedString(@"lbl_Expire_On", nil)];
    
    [self setupUI];
}

-(void)setupUI{
    dispatch_async(dispatch_get_main_queue(), ^{
//        [[SharedMethod new] setBorder:self.expireView borderColor:THEME_COLOR andBackgroundColor:LIGHT_GREY_COLOR];
        [self.expireView setBackgroundColor:[[SharedMethod new] colorWithHexString:THEME_COLOR andAlpha:0.8f]];
        [[self.expireView layer] setCornerRadius:self.expireView.frame.size.width/2];
        [self.expireView setClipsToBounds:YES];
    });
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self.lblExpire setTextColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
    [self.lblExpire setFont:[UIFont fontWithName:FONT_REGULAR size:12.0f]];
    [self.lblExpireDate setTextColor:[[SharedMethod new] colorWithHexString:WHITE_COLOR andAlpha:1.0f]];
    [self.lblExpireDate setFont:[UIFont fontWithName:FONT_REGULAR size:12.0f]];
}

@end
