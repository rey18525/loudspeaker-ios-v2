//
//  VoucherBannerCell.h
//  Oasis
//
//  Created by Wong Ryan on 17/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoucherBannerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner;
@property (weak, nonatomic) IBOutlet UIView *expireView;
@property (weak, nonatomic) IBOutlet UILabel *lblExpire;
@property (weak, nonatomic) IBOutlet UILabel *lblExpireDate;
@property (weak, nonatomic) IBOutlet UIImageView *imgUsed;

@end
