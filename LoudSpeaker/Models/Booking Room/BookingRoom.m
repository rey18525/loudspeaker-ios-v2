//
//  BookingRoom.m
//  LoudSpeaker
//
//  Created by Appandus on 06/08/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingRoom.h"

@implementation BookingRoom

- (instancetype)initWithAttributes:(NSDictionary *)attributes{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.available = [attributes valueForKey:@"available"];
    self.created_at = [attributes valueForKey:@"created_at"];
    self.booking_description = [attributes valueForKey:@"description"];
    self.booking_id = [attributes valueForKey:@"id"];
    self.max_pax = [attributes valueForKey:@"max_pax"];
    self.min_pax = [attributes valueForKey:@"min_pax"];
    self.name = [attributes valueForKey:@"name"];
    self.outlet_id = [attributes valueForKey:@"outlet_id"];
    self.room_type = [attributes valueForKey:@"type"];
    self.updated_at = [attributes valueForKey:@"updated_at"];
    
    return self;
}

@end

/*
 rooms =                 (
 {
 available = 2;
 "created_at" = "2018-07-26 18:48:14";
 description = "<null>";
 id = 1;
 "max_pax" = 4;
 "min_pax" = 2;
 name = "Small Room";
 "outlet_id" = 1;
 pivot =                         {
 available = 2;
 "booking_session_id" = 1;
 "room_id" = 1;
 };
 type = small;
 "updated_at" = "2018-07-26 18:48:14";
 }
 );
 
 */
