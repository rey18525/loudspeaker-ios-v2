//
//  BookingRoom.h
//  LoudSpeaker
//
//  Created by Appandus on 06/08/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookingRoom : NSObject

@property (nonatomic) NSString *available;
@property (nonatomic) NSString *created_at;
@property (nonatomic) NSString *booking_description;
@property (nonatomic) NSString *booking_id;
@property (nonatomic) NSString *max_pax;
@property (nonatomic) NSString *min_pax;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *outlet_id;
@property (nonatomic) NSString *room_type;
@property (nonatomic) NSString *updated_at;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end

