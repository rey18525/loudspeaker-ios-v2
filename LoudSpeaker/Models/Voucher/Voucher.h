//
//  Voucher.h
//  Oasis
//
//  Created by Wong Ryan on 27/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Voucher : NSObject
@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *category_id;
@property (strong, nonatomic) NSString *campaign_id;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subtitle;
@property (strong, nonatomic) NSString *image;
@property (strong, nonatomic) NSString *images;
@property (nonatomic) UIImage *cachedImage;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) NSString *terms;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSString *featured;
@property (strong, nonatomic) NSString *featured_note;
@property (strong, nonatomic) NSString *created_at;
@property (strong, nonatomic) NSString *updated_at;
@property (strong, nonatomic) NSString *publish_at;
@property (strong, nonatomic) NSString *priority;
@end
