#import <Foundation/Foundation.h>
@import UIKit;

@interface Category : NSObject

@property (nonatomic) NSString *ID;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *content;
@property (nonatomic) NSString *image;
@property (nonatomic) UIImage *cachedImage;
@property (nonatomic) NSString *created_at;
@property (nonatomic) NSString *updated_at;
@property (nonatomic) NSString *deleted_at;

@end
