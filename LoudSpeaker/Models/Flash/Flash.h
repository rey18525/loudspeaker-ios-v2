#import <Foundation/Foundation.h>
#import "Price.h"
#import "Post.h"
@import UIKit;

@interface Flash : NSObject

@property (nonatomic) NSString *ID;
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *start_date; // dateTime
@property (nonatomic) NSString *expire_date; // dateTime
@property (nonatomic) NSString *publish_date; // dateTime
@property (nonatomic) NSString *status;
@property (nonatomic) NSString *trigger_frequency;
@property (nonatomic) NSString *payment_method;
@property (nonatomic) NSString *applicable_to;

@property (nonatomic) NSString *uom;
@property (nonatomic) NSString *voucher_type;
@property (nonatomic) NSString *voucher_limit;
@property (nonatomic) NSString *total_grabbed;
@property (nonatomic) NSString *post_id;
@property (nonatomic) NSString *outlet_id;
@property (nonatomic) NSString *is_public;
@property (nonatomic) NSString *voucher_expiry_type;
@property (nonatomic) NSString *voucher_expiry_value;
@property (nonatomic) NSString *created_at; // dateTime
@property (nonatomic) NSString *updated_at; // dateTime

@property (nonatomic) NSString *deleted_at; // dateTime
@property (nonatomic) NSString *expiry_millisecond;
@property (nonatomic) NSString *expiry_date_millisecond;
@property (nonatomic) NSString *terms;
@property (nonatomic) Price *price;
@property (nonatomic) NSString *outlet;
@property (nonatomic) NSString *url;
@property (nonatomic) Post *post;
@property (nonatomic) NSString *hasOwn;

@end
