#import <Foundation/Foundation.h>
@import UIKit;

@interface Price : NSObject

@property (nonatomic) NSString *e_guest_value;
@property (nonatomic) NSString *e_member_online;
@property (nonatomic) NSString *e_member_wallet;
@property (nonatomic) NSString *topup_value;
@property (nonatomic) NSString *ewallet_value;

@end
