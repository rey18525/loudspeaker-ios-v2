#import <Foundation/Foundation.h>
@import UIKit;

@interface Post : NSObject

@property (nonatomic) NSString *ID;
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *category_id;
@property (nonatomic) NSString *campaign_id;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *subtitle;
@property (nonatomic) NSString *image_id;
@property (nonatomic) NSString *content;
@property (nonatomic) NSString *url;
@property (nonatomic) NSString *featured;

@property (nonatomic) NSString *featured_note;
@property (nonatomic) NSString *created_at; // dateTime
@property (nonatomic) NSString *updated_at; // dateTime
@property (nonatomic) NSString *publish_at; // dateTime
@property (nonatomic) NSString *priority;
@property (nonatomic) NSString *deleted_at; // dateTime
@property (nonatomic) NSString *status;
@property (nonatomic) NSString *terms;
@property (nonatomic) NSString *image;

@end
