//
//  Outlet.m
//  Oasis
//
//  Created by Wong Ryan on 21/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import "Outlet.h"
#import "SharedMethod.h"
#import "Constant.h"

@implementation Outlet

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    NSString *temp = [attributes valueForKey:@"address"];
    self.address = [temp stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    self.address1 = [attributes valueForKey:@"address1"];
    self.address2 = [attributes valueForKey:@"address2"];
    self.address3 = [attributes valueForKey:@"address3"];
    self.city = [attributes valueForKey:@"city"];
    self.country = [attributes valueForKey:@"country"];
    self.email = [attributes valueForKey:@"email"];
    self.fax = [attributes valueForKey:@"fax"];
    self.hours = [attributes valueForKey:@"hours"];
    self.ID = [NSString stringWithFormat:@"%d",[[attributes objectForKey:@"id"] intValue]];
    self.latitude = [attributes valueForKey:@"latitude"];
    self.longitude = [attributes valueForKey:@"longitude"];
    self.name = [attributes valueForKey:@"name"];
    self.phone = [attributes valueForKey:@"phone"];
    self.postcode = [attributes valueForKey:@"postcode"];
    self.priority = [attributes valueForKey:@"priority"];
    self.status = [attributes valueForKey:@"status"];
    self.state = [attributes valueForKey:@"state"];
    self.type = [attributes valueForKey:@"type"];
    
    if([[attributes valueForKey:@"image"] isKindOfClass:[NSDictionary class]]){
        self.imageString = [[attributes valueForKey:@"image"] valueForKey:@"src"];
    } else{
        self.imageString = @"";
    }
    
    return self;
}

@end
