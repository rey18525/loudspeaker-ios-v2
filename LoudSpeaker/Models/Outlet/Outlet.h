//
//  Outlet.h
//  Oasis
//
//  Created by Wong Ryan on 21/06/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>

@import UIKit;
@interface Outlet : NSObject
@property (nonatomic) NSString *address;
@property (nonatomic) NSString *address1;
@property (nonatomic) NSString *address2;
@property (nonatomic) NSString *address3;
@property (nonatomic) NSString *city;
@property (nonatomic) NSString *country;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *fax;
@property (nonatomic) NSString *hours;
@property (nonatomic) NSString *ID;
@property (nonatomic) NSString *latitude;
@property (nonatomic) NSString *longitude;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *phone;
@property (nonatomic) NSString *postcode;
@property (nonatomic) NSString *priority;
@property (nonatomic) NSString *state;
@property (nonatomic) NSString *type;
@property (nonatomic) UIImage *image;
@property (nonatomic) NSString *status;
@property (nonatomic) NSString *subtitle; //description
@property (nonatomic) NSString *imageString;
@property (nonatomic) NSNumber *distance;
@property (nonatomic) NSString *distanceString;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end
