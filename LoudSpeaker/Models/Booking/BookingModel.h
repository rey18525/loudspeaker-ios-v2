//
//  BookingModel.h
//  LoudSpeaker
//
//  Created by Appandus on 30/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookingModel : NSObject

@property (nonatomic) NSString *booking_date;
@property (nonatomic) NSString *booking_session_id;
@property (nonatomic) NSString *booking_session_name;
@property (nonatomic) NSString *created_at;
@property (nonatomic) NSString *booking_id;
@property (nonatomic) NSString *order_id;
@property (nonatomic) NSString *outlet_id;
@property (nonatomic) NSString *paid_by;
@property (nonatomic) NSMutableArray *participants;
@property (nonatomic) NSString *reference;
@property (nonatomic) NSString *remark;
@property (nonatomic) NSString *room_description;
@property (nonatomic) NSString *room_id;
@property (nonatomic) NSString *status;
@property (nonatomic) NSString *time_end;
@property (nonatomic) NSString *time_start;
@property (nonatomic) NSString *total;
@property (nonatomic) NSString *void_reason;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end
