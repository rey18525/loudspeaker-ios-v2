//
//  BookingModel.m
//  LoudSpeaker
//
//  Created by Appandus on 30/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingModel.h"
#import "BookingParticipantsModel.h"

@implementation BookingModel

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.booking_date = [attributes valueForKey:@"booking_date"];
    self.booking_session_id = [attributes valueForKey:@"booking_session_id"];
    self.booking_session_name = [[attributes valueForKey:@"session"] valueForKey:@"name"];
    self.created_at = [attributes valueForKey:@"created_at"];
    self.booking_id = [attributes valueForKey:@"id"];
    self.order_id = [attributes valueForKey:@"order_id"];
    self.outlet_id = [attributes valueForKey:@"outlet_id"];
    self.paid_by = [attributes valueForKey:@"paid_by"];
    
    self.participants = [[NSMutableArray alloc] init];
    for(NSDictionary *temp in [attributes objectForKey:@"participants"]){
        BookingParticipantsModel *participantsModel = [[BookingParticipantsModel alloc] initWithAttributes:temp];
        [self.participants addObject:participantsModel];
    }
    
    self.reference = [attributes valueForKey:@"reference"];
    self.room_description = [attributes valueForKey:@"room_description"];
    self.room_id = [attributes valueForKey:@"room_id"];
    self.status = [attributes valueForKey:@"status"];
    self.time_start = [attributes valueForKey:@"time_start"];
    self.time_end = [attributes valueForKey:@"time_end"];
    self.total = [attributes valueForKey:@"total"];
    self.remark = [attributes valueForKey:@"remark"];
    self.void_reason = [attributes valueForKey:@"void_reason"];
    
    return self;
}

@end
