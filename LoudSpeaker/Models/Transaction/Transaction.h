//
//  Transaction.h
//  Oasis
//
//  Created by Wong Ryan on 23/08/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Transaction : NSObject
@property (strong, nonatomic) NSString *additional_text;
@property (strong, nonatomic) NSString *amount;
@property (strong, nonatomic) NSString *campaign_id;
@property (strong, nonatomic) NSString *card_number;
@property (strong, nonatomic) NSString *app_datetime;
@property (strong, nonatomic) NSString *created_at;
@property (strong, nonatomic) NSString *credit;
@property (strong, nonatomic) NSString *deleted_at;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) NSString *device_id;
@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *item_id;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *nfc_tag_id;
@property (strong, nonatomic) NSString *outlet_id;
@property (strong, nonatomic) NSString *reason;
@property (strong, nonatomic) NSString *reward_id;
@property (strong, nonatomic) NSString *staff_id;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *terminal_id;
@property (strong, nonatomic) NSString *terminal_sequence;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *type_id;
@property (strong, nonatomic) NSString *updated_at;
@property (strong, nonatomic) NSString *user_id;

@end
