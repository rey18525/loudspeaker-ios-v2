//
//  BookingParticipantsModel.m
//  LoudSpeaker
//
//  Created by Appandus on 30/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingParticipantsModel.h"

@implementation BookingParticipantsModel

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.user_id = [attributes valueForKey:@"user_id"];
    self.member_type = [attributes valueForKey:@"member_type"];
    self.fullname = [attributes valueForKey:@"fullname"];
    self.contact = [attributes valueForKey:@"contact"];
    self.price = [attributes valueForKey:@"price"];
    self.email = [attributes valueForKey:@"email"];
    
    return self;
}

@end
