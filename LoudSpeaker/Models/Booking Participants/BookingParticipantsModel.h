//
//  BookingParticipantsModel.h
//  LoudSpeaker
//
//  Created by Appandus on 30/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookingParticipantsModel : NSObject

@property (nonatomic) NSString *user_id;
@property (nonatomic) NSString *member_type;
@property (nonatomic) NSString *fullname;
@property (nonatomic) NSString *contact;
@property (nonatomic) NSString *price;
@property (nonatomic) NSString *email;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end
