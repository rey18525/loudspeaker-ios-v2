//
//  BookingHistoryModel.m
//  LoudSpeaker
//
//  Created by Appandus on 02/08/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingHistoryModel.h"

@implementation BookingHistoryModel

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.booking_date = [attributes valueForKey:@"booking_date"];
    self.created_at = [attributes valueForKey:@"created_at"];
    self.booking_id = [attributes valueForKey:@"id"];
    self.order_id = [attributes valueForKey:@"order_id"];
    self.outlet_id = [attributes valueForKey:@"outlet_id"];
    self.room_id = [attributes valueForKey:@"room_id"];
    self.status = [attributes valueForKey:@"status"];
    self.time_end = [attributes valueForKey:@"time_end"];
    self.time_start = [attributes valueForKey:@"time_start"];
    self.updated_at = [attributes valueForKey:@"updated_at"];
    
    return self;
}


@end
