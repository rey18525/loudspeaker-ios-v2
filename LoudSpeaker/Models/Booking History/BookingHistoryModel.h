//
//  BookingHistoryModel.h
//  LoudSpeaker
//
//  Created by Appandus on 02/08/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookingHistoryModel : NSObject

@property (nonatomic) NSString *booking_date;
@property (nonatomic) NSString *created_at;
@property (nonatomic) NSString *booking_id;
@property (nonatomic) NSString *order_id;
@property (nonatomic) NSString *outlet_id;
@property (nonatomic) NSString *room_id;
@property (nonatomic) NSString *status;
@property (nonatomic) NSString *time_end;
@property (nonatomic) NSString *time_start;
@property (nonatomic) NSString *updated_at;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end
