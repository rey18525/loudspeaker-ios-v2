//
//  Campaign.h
//  Oasis
//
//  Created by Wong Ryan on 26/09/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Voucher.h"

@interface Campaign : NSObject

@property (nonatomic) NSString *ID;
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *start;
@property (nonatomic) NSString *expire;
@property (nonatomic) NSString *trigger_frequence;
@property (nonatomic) NSString *trigger_by;
@property (nonatomic) NSString *trigger_value;
@property (nonatomic) NSString *reward_type;
@property (nonatomic) NSString *trigger_limit;
@property (nonatomic) NSString *reward_value;
@property (nonatomic) NSString *reward_count;
@property (nonatomic) NSString *reward_limit;
@property (nonatomic) NSString *sequence;
@property (nonatomic) NSString *post_id;
@property (nonatomic) NSString *outlet_id;
@property (nonatomic) NSString *status;
@property (nonatomic) Voucher *voucher;

@end
