//
//  Reward.h
//  Oasis
//
//  Created by Wong Ryan on 26/09/2016.
//  Copyright © 2016 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Campaign.h"

@interface Reward : NSObject

@property (nonatomic) NSString *ID;
@property (nonatomic) NSString *hashKey;
@property (nonatomic) NSString *userID;
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *serial;
@property (nonatomic) NSString *value;
@property (nonatomic) NSString *campaign_id;
@property (nonatomic) NSString *from_transaction;
@property (nonatomic) NSString *created_at;
@property (nonatomic) NSString *updated_at;
@property (nonatomic) NSString *expires_at;
@property (nonatomic) NSString *deleted_at;
@property (nonatomic) NSString *redeemed_transaction;
@property (nonatomic) NSString *redeem_at;
@property (nonatomic) NSString *redeemed_outlet;
@property (nonatomic) Campaign *campaign;
@end
