//
//  BookingSession.m
//  LoudSpeaker
//
//  Created by Appandus on 06/08/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingSession.h"
#import "BookingRoom.h"

@implementation BookingSession

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
 
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.session_id = [attributes valueForKey:@"id"];
    self.slots = [attributes valueForKey:@"slots"];
    
    return self;
}

@end
