//
//  BookingSession.h
//  LoudSpeaker
//
//  Created by Appandus on 06/08/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookingSession : NSObject

@property (nonatomic) NSString *session_id;
@property (nonatomic) NSArray *slots;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end

