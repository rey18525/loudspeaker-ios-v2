//
//  BookingTimeModel.m
//  LoudSpeaker
//
//  Created by Appandus on 30/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import "BookingTimeModel.h"

@implementation BookingTimeModel

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.time_start = [attributes valueForKey:@"time_start"];
    self.time_end = [attributes valueForKey:@"time_end"];
    
    return self;
}

@end
