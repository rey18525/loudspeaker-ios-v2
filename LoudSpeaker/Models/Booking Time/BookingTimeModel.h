//
//  BookingTimeModel.h
//  LoudSpeaker
//
//  Created by Appandus on 30/07/2018.
//  Copyright © 2018 Ryan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookingTimeModel : NSObject

@property (nonatomic) NSString *time_start;
@property (nonatomic) NSString *time_end;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end
